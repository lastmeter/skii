
//
//  LoadingFootView.m
//  EZLearning
//
//  Created by xihan on 16/1/13.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "LoadingFootView.h"

@implementation LoadingFootView

- (instancetype)init{
    self = [super initWithFrame:CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 30)];
    if (self) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 21)];
        label.center = CGPointMake(self.center.x + 5, label.center.y);
        label.text = @"加载中...";
        label.textColor = [UIColor lightGrayColor];
        [self addSubview:label];
        
        UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        indicatorView.frame = CGRectMake(label.frame.origin.x - 30, 0, 21, 21);
        [indicatorView startAnimating];
        [self addSubview:indicatorView];
        
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    return self;
}

@end
