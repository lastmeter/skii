//
//  FabUserModel.h
//  sdjksd
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FabUserModel : NSObject

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *userId;
@property(nonatomic, assign) CGSize size;
+ (UIFont *)fabFont;
@end
