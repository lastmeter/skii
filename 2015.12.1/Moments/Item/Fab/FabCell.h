//
//  FabCell.h
//  Moments
//
//  Created by xihan on 15/12/16.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FabCell : UICollectionViewCell

@property (nonatomic, copy) NSString *text;
@property (nonatomic, weak) UIImage *image;


@end
