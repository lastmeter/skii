//
//  FabsView.m
//  sdjksd
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "FabsView.h"
#import "FabUserModel.h"
#import "FabCell.h"
#import "MomentShare.h"

@interface FabsView ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;

@end

@implementation FabsView

- (instancetype)init{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];

        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[FabCell class] forCellWithReuseIdentifier:@"fab"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];
        [self addSubview:_collectionView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    if (frame.size.height > 0) {
        _collectionView.frame = (CGRect){ 10, 3, frame.size.width - 10, frame.size.height - 3 };
    }
    else{
        _collectionView.frame = CGRectZero;
    }
    
}


- (void)setUsers:(NSArray *)users{
    _users = users;
    [_collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (self.users.count > 0) {
        return self.users.count + 1;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FabCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"fab" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.image = [UIImage imageNamed:@"fab"];
    }
    else{
        FabUserModel *fab = self.users[indexPath.row - 1];
        cell.text = fab.name;
    }
    [cell needsUpdateConstraints];
    [cell updateConstraintsIfNeeded];

    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return CGSizeMake(20, 20);
    }
    FabUserModel *fab = self.users[indexPath.row - 1];
    return fab.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return;
    }
    FabUserModel *fab = self.users[indexPath.row - 1];
    NSDictionary *dic = @{ @"uid":fab.userId, @"name" : fab.name };
    [[NSNotificationCenter defaultCenter] postNotificationName:EzlReadMomentsOfUser object:dic];
}

@end
