//
//  FabCell.m
//  Moments
//
//  Created by xihan on 15/12/16.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "FabCell.h"
#import "FabUserModel.h"

@interface FabCell()

@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation FabCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];
    }
    return self;
}

- (UILabel *)textLabel{
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = [FabUserModel fabFont];
        _textLabel.textColor = [UIColor colorWithRed:60.0/255.0 green:104.0/255.0 blue:161.0/255.0 alpha:1];
        [self.contentView addSubview:_textLabel];
        
        _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[textLabel]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"textLabel":_textLabel}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[textLabel]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"textLabel":_textLabel}]];
        _textLabel.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];
    }
    return _textLabel;
}

- (UIImageView *)imageView{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imageView];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        _imageView.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];

        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imageView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"imageView":_imageView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"imageView":_imageView}]];
    }
    return _imageView;
}

- (void)setImage:(UIImage *)image{
    self.imageView.image = image;
}

- (void)setText:(NSString *)text{
    self.textLabel.text = text;
    if (_imageView) {
        [_imageView removeFromSuperview];
        _imageView = nil;
    }
}


@end
