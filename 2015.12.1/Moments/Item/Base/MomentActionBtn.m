//
//  MomentActionBtn.m
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentActionBtn.h"
@interface MomentActionBtn()



@end

@implementation MomentActionBtn

- (instancetype)init{
    self = [super init];
    if (self) {
        _mImageView = [[UIImageView alloc] init];
        _mImageView.userInteractionEnabled = NO;
        _mImageView.backgroundColor = [UIColor redColor];
        [self addSubview:_mImageView];
        
        _mTitleLabel = [[UILabel alloc] init];
        _mTitleLabel.userInteractionEnabled = NO;
        _mTitleLabel.textColor = [UIColor blackColor];
        [self addSubview:_mTitleLabel];

    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    CGFloat HEIGHT = CGRectGetHeight(frame);
    _mImageView.frame = CGRectMake(0, (HEIGHT - 20) / 2, 20, 20);
    _mTitleLabel.frame = CGRectMake(25, 0, CGRectGetWidth(frame) - 25, HEIGHT);
}



@end
