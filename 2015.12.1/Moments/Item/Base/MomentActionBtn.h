//
//  MomentActionBtn.h
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MomentActionBtn : UIButton

@property(nonatomic, strong) UILabel *mTitleLabel;
@property(nonatomic, strong) UIImageView *mImageView;
@end
