//
//  MomentPortrait.m
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentPortrait.h"
#import "EzlWebImageManager.h"
#import "MomentShare.h"

@implementation MomentPortrait

- (instancetype)init{
    self = [super init];
    if (self) {
        [self p_InitSetting];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_InitSetting];
    }
    return self;
}

- (void)p_InitSetting{
    [self addTarget:self action:@selector(didClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClick{
    NSDictionary *dic = @{ @"uid" : _uid, @"name" : _name };
    [[NSNotificationCenter defaultCenter] postNotificationName:EzlReadMomentsOfUser object:dic];
}



- (void)fetchHeadPortrait{
    [self setImage:nil forState:UIControlStateNormal];
    __weak MomentPortrait *weakSelf = self;
    [EzlWebImageManager fetchHeadPortraitByEid:_eid uid:_uid handler:^(UIImage *image, NSString *eid) {
        if ([weakSelf.eid isEqualToString:eid]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf setImage:image forState:UIControlStateNormal];
            });
        }
    }];
}

@end
