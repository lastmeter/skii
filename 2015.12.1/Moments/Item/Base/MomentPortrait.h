//
//  MomentPortrait.h
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MomentPortrait : UIButton

@property (nonatomic, copy) NSString * uid;
@property (nonatomic, copy) NSString * eid;
@property (nonatomic, copy) NSString * name;
- (void)fetchHeadPortrait;
@end
