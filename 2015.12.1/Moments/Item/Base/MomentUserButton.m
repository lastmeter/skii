//
//  MomentUserButton.m
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentUserButton.h"
#import "MomentShare.h"

@implementation MomentUserButton

- (instancetype)init{
    self = [super init];
    if (self) {
          [self p_InitSetting];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self p_InitSetting];
    }
    return self;
}

- (void)p_InitSetting{
    [self addTarget:self action:@selector(didHighlighted:) forControlEvents:UIControlEventTouchDown];
    [self addTarget:self action:@selector(didSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self setTitleColor:[UIColor colorWithRed:60.0/255.0 green:104.0/255.0 blue:161.0/255.0 alpha:1] forState:UIControlStateNormal];
}

- (void)didSelected:(UIButton *)btn{
    [btn setBackgroundColor:[UIColor clearColor]];
    NSDictionary *dic = @{ @"uid" : _uid, @"name" : _name };
    [[NSNotificationCenter defaultCenter] postNotificationName:EzlReadMomentsOfUser object:dic];
}

- (void)didHighlighted:(UIButton *)btn{
    [btn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
}

- (void)setName:(NSString *)name{
    _name = name;
    [self setTitle:name forState:UIControlStateNormal];
}

- (void)setFont:(UIFont *)font{
    self.titleLabel.font = font;
}

@end
