//
//  MomentUserButton.h
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MomentUserButton : UIButton

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) UIFont *font;

@end
