//
//  RemarkInputBar.m
//  Moments
//
//  Created by xihan on 15/12/6.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "RemarkInputBar.h"
#import "EzlTextView.h"

const NSInteger kMaxTextLength = 140;

@interface RemarkInputBar()<EzlTextViewDelegate>

@end

@implementation RemarkInputBar{
    EzlTextView *_contentView;
    UIButton *_sendBtn;
    BOOL _contraintsUpdated, _textEditing;
    NSLayoutConstraint *_rightConstraint;
    UILabel *_textLenLabel;
    void(^_sendHanler)(NSString *text);
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.backgroundColor = [UIColor colorWithRed:235/255.0 green:236/255.0 blue:238/255.0 alpha:1];
     
        
        _sendBtn = [[UIButton alloc] init];
        [_sendBtn setTitleColor:[UIColor colorWithRed:31/255.0 green:186/255.0 blue:243/255.0 alpha:1] forState:UIControlStateNormal];
        _sendBtn.translatesAutoresizingMaskIntoConstraints = NO;
        [_sendBtn setTitle:@"发送" forState:UIControlStateNormal];
        [_sendBtn addTarget:self action:@selector(sendText:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_sendBtn];
        
        
        _contentView = [[EzlTextView alloc] init];
        _contentView.translatesAutoresizingMaskIntoConstraints = NO;
        _contentView.ezlDelegate = self;
        _contentView.font = [UIFont systemFontOfSize:17];
        [self addSubview:_contentView];
        
        _textLenLabel = [[UILabel alloc] init];
        _textLenLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _textLenLabel.font = [UIFont systemFontOfSize:15];
        _textLenLabel.textColor = [UIColor lightGrayColor];
        _textLenLabel.textAlignment = NSTextAlignmentRight;
        _textLenLabel.text = [NSString stringWithFormat:@"0/%@",@(kMaxTextLength)];
        [self addSubview:_textLenLabel];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    if (_contraintsUpdated) { return; }
    [self p_InitConstraints];
}

- (void)p_InitConstraints{

    NSDictionary *views = @{ @"contentView" : _contentView, @"sendBtn" : _sendBtn , @"textLen" : _textLenLabel };
    
    NSString *format1 = @"H:[sendBtn(36)]-5-|";
    NSString *format2 = @"V:|-8-[contentView]-2-[textLen]-8-|";
    NSString *format3 = @"V:|-8-[sendBtn(40)]";
    NSString *format4 = @"H:|-5-[contentView]";
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:format1 options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:format2 options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:format3 options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:format4 options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    
    _rightConstraint = [NSLayoutConstraint constraintWithItem:_contentView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:-5];
    [self addConstraint:_rightConstraint];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_textLenLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:_contentView attribute:NSLayoutAttributeRight multiplier:1 constant:0]];
    
    _contraintsUpdated  = YES;
}

-(void)textViewContentSizeDidChange:(CGSize)contentSize{
       NSInteger numberOfLine = [_contentView numberOfLine];
    
    if (numberOfLine > 5) {
        return;
    }
    
    CGFloat barHeight = 71;
    if (numberOfLine > 1 ) {
        barHeight = contentSize.height + 16 + 21;
    }
    [_delegate InputBar_BarHeightChange:CGRectGetHeight(self.frame) newHeight:barHeight];
}

- (void)textViewTextDidChange:(NSString *)text{
    BOOL needUpdate = NO;
    if (text.length > 0 && !_textEditing) {
        _textEditing = YES;
        needUpdate = YES;
        _rightConstraint.constant = -46;
    }
    else if (text.length == 0 && _textEditing){
        _textEditing = NO;
        needUpdate = YES;
        _rightConstraint.constant = -5;
    }
    
    if (needUpdate) {
        [self setNeedsUpdateConstraints];
        [UIView animateWithDuration:0.25 animations:^{
            [self layoutIfNeeded];
        }];
    }
    
    
    UITextRange *selectedRange = [_contentView markedTextRange];
    UITextPosition *position = [_contentView positionFromPosition:selectedRange.start offset:0];
    if (!position) {
        if (text.length > kMaxTextLength) {
            _contentView.text = [_contentView.text substringToIndex:kMaxTextLength];
            _textLenLabel.text = [NSString stringWithFormat:@"%@/%@",@(kMaxTextLength),@(kMaxTextLength)];
        }
        else{
            _textLenLabel.text = [NSString stringWithFormat:@"%@/%@",@(text.length),@(kMaxTextLength)];
        }
        
    }

    
}

- (void)sendText:(UIButton *)btn{
    if (_sendHanler) {
        _sendHanler(_contentView.text);
    }
    _contentView.text = @"";
    _textEditing = NO;
    _rightConstraint.constant = -5;
    [self setNeedsUpdateConstraints];
    _textLenLabel.text = [NSString stringWithFormat:@"0/%@",@(kMaxTextLength)];
    [UIView animateWithDuration:0.25 animations:^{
        [self layoutIfNeeded];
    }];
}


- (void)beginEdit{
    [_contentView becomeFirstResponder];
    _editing = YES;
}

- (void)endEdit{
    [_contentView resignFirstResponder];
    _editing = NO;
    _sendHanler = nil;
}

- (NSString *)placeholder{
    return _contentView.placeholder;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _contentView.placeholder = placeholder;
}


- (CGFloat)barHeight{
    CGFloat barHeight = 100;
    if ([_contentView numberOfLine] > 1) {
        barHeight = _contentView.contentSize.height + 16 + 21;
    }
    return barHeight;
}

- (void)beginEditWithSendHandler:(void(^)(NSString *text))handler{
    _sendHanler = handler;
    [self beginEdit];
}

@end
