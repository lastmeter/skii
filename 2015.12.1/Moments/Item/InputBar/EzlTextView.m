//
//  EzlTextView.m
//  Moments
//
//  Created by xihan on 15/12/6.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlTextView.h"

@implementation EzlTextView{
    UILabel *_placeholderLabel;
    BOOL _contraintsUpdated;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self addObserver:self forKeyPath:NSStringFromSelector(@selector(contentSize)) options:NSKeyValueObservingOptionNew context:NULL];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange:) name:UITextViewTextDidChangeNotification object:self];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        self.layoutManager.allowsNonContiguousLayout = NO;
        
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.textColor = [UIColor grayColor];
        _placeholderLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_placeholderLabel];
    }
    return self;
}

- (void)setPlaceholder:(NSString *)placeholder{
    _placeholderLabel.text = placeholder;
}

- (NSString *)placeholder{
    return _placeholderLabel.text;
}

- (void)setFont:(UIFont *)font{
    [super setFont:font];
    _placeholderLabel.font = font;
}

- (void)updateConstraints{
    [super updateConstraints];
    
    if (_contraintsUpdated) { return; }
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-8-[placeholder]-5-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"placeholder":_placeholderLabel }]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[placeholder]-8-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"placeholder":_placeholderLabel }]];
    
    _contraintsUpdated = YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:self] && [keyPath isEqualToString:NSStringFromSelector(@selector(contentSize))]) {
        [_ezlDelegate textViewContentSizeDidChange:self.contentSize];
    }
}

- (void)textDidChange:(NSNotification *)noti{

    
    if (self.placeholder.length > 0) {
        _placeholderLabel.hidden = (self.text.length > 0) ? YES : NO;
        [self setNeedsDisplay];
    }
    
    CGRect line = [self caretRectForPosition:
                   self.selectedTextRange.start];
    CGFloat overflow = line.origin.y + line.size.height
    - ( self.contentOffset.y + self.bounds.size.height
       - self.contentInset.bottom - self.contentInset.top );
    if ( overflow > 0 ) {
        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
        // Scroll caret to visible area
        CGPoint offset = self.contentOffset;
        offset.y += overflow ; // leave 7 pixels margin
        // Cannot animate with setContentOffset:animated: or caret will not appear
        [UIView animateWithDuration:0.25 animations:^{
            [self setContentOffset:offset];
        }];
    }
    

    [_ezlDelegate textViewTextDidChange:self.text];
}

- (void)setText:(NSString *)text{
    [super setText:text];
    if (self.text == nil || self.text.length == 0) {
        _placeholderLabel.hidden = NO;
    }
    else{
        _placeholderLabel.hidden = YES;
    }
}

- (NSInteger)numberOfLine{
    return fabs(self.contentSize.height/self.font.lineHeight );
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(contentSize))];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
