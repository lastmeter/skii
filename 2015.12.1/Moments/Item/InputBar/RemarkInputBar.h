//
//  RemarkInputBar.h
//  Moments
//
//  Created by xihan on 15/12/6.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RemarkInputBarDelegate <NSObject>

- (void)InputBar_BarHeightChange:(CGFloat)originHeight newHeight:(CGFloat)newHeight;
@end

@interface RemarkInputBar : UIView

@property (nonatomic, copy) NSString *placeholder;

@property(nonatomic, weak) id<RemarkInputBarDelegate>delegate;
@property(nonatomic, assign, readonly) BOOL editing;

- (void)beginEdit;
- (void)endEdit;
- (void)beginEditWithSendHandler:(void(^)(NSString *text))handler;

- (CGFloat)barHeight;

@end
