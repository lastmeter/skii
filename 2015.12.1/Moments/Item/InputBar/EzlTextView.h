//
//  EzlTextView.h
//  Moments
//
//  Created by xihan on 15/12/6.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EzlTextViewDelegate <NSObject>

- (void)textViewContentSizeDidChange:(CGSize)contentSize;
- (void)textViewTextDidChange:(NSString *)text;

@end

@interface EzlTextView : UITextView

@property(nonatomic, weak) id<EzlTextViewDelegate>ezlDelegate;
@property (nonatomic, copy) NSString *placeholder;
- (NSInteger)numberOfLine;


@end
