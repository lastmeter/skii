//
//  RemarkModel.h
//  sdjksd
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemarkModel : NSObject

@property (nonatomic, weak) NSDictionary *remarkDic;
@property (nonatomic, assign) CGFloat maxWidth;

@property (nonatomic, readonly) NSAttributedString *text;

@property (nonatomic, readonly) NSString *userId;
@property (nonatomic, readonly) NSString *userName;

@property (nonatomic, readonly) NSString *replyId;
@property (nonatomic, readonly) NSString *replyName;

@property (nonatomic, readonly) CGRect userFrame;
@property (nonatomic, readonly) CGRect replyFrame;
@property (nonatomic, readonly) CGRect contentFrame;
@property (nonatomic, readonly) CGFloat viewHeight;

+ (UIFont *)remarkFont;

@end
