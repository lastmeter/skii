//
//  RemarkCell.h
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemarkModel.h"

@interface RemarkCell : UITableViewCell

@property(nonatomic, weak)RemarkModel *remark;

@end
