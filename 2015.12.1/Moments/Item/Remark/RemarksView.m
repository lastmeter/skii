//
//  RemarksView.m
//  sdjksd
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "RemarksView.h"
#import "RemarkCell.h"
#import "RemarkModel.h"
#import "MomentShare.h"

@interface RemarksView()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)UIColor *bgColor;
@end

@implementation RemarksView

- (UIColor *)bgColor{
    return [UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:245.0/255.0 alpha:1];
}

- (instancetype)init{
    self = [super init];
    if (self) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.scrollEnabled = NO;
        _tableView.backgroundColor = self.bgColor;
        [self addSubview:_tableView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    _tableView.frame = self.bounds;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _remarksArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    RemarkModel *remark = _remarksArray[indexPath.row];
    return remark.viewHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RemarkCell * cell = [tableView dequeueReusableCellWithIdentifier:@"moment"];
    if ( cell == nil ) {
        cell = [[RemarkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moment"];
        cell.backgroundColor = self.bgColor;
    }
    cell.remark = _remarksArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    RemarkModel *remark = _remarksArray[indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //CGRect convertRect = [self.superview convertRect:cell.frame fromView:self];
    [_delegate addReplyToRemark:remark convertRect:cell.frame];
}

- (void)setRemarksArray:(NSArray *)remarksArray{
    _remarksArray = remarksArray;
    [_tableView reloadData];
}


@end
