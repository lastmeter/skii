//
//  RemarkCell.m
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "RemarkCell.h"
#import "MomentUserButton.h"


@implementation RemarkCell
{
    MomentUserButton *_userBtn;
    MomentUserButton *_replyBtn;
    UILabel *_mLabel;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        _mLabel = [[UILabel alloc] init];
        _mLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _mLabel.numberOfLines = 0;
        _mLabel.userInteractionEnabled = NO;
        [self.contentView addSubview:_mLabel];
        
        _userBtn = [[MomentUserButton alloc] init];
        _userBtn.font = [RemarkModel remarkFont];
        [self.contentView addSubview:_userBtn];
        
        _replyBtn = [[MomentUserButton alloc] init];
        _replyBtn.font = [RemarkModel remarkFont];
        [self.contentView addSubview:_replyBtn];
    }
    return self;
}


- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    _mLabel.frame = (CGRect){ 10, 0, frame.size.width - 10, frame.size.height };
}

- (void)setRemark:(RemarkModel *)remark{
    
    _remark = remark;
    _mLabel.attributedText = remark.text;

    _userBtn.frame   = remark.userFrame;
    _userBtn.uid     = remark.userId;
    _userBtn.name    = remark.userName;

    _replyBtn.frame  = remark.replyFrame;
    _replyBtn.uid    = remark.replyId;
    _replyBtn.name   = remark.replyName;
}

@end
