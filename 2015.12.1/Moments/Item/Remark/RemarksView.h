//
//  RemarksView.h
//  sdjksd
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemarkModel.h"

@protocol RemarksViewDelegate <NSObject>
- (void)addReplyToRemark:(RemarkModel *)remark convertRect:(CGRect)rect;
@end

@interface RemarksView : UIView

@property (nonatomic, weak) NSArray *remarksArray;
@property (nonatomic, weak) id<RemarksViewDelegate>delegate;

@end
