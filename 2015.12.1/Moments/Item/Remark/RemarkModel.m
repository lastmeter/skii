//
//  RemarkModel.m
//  sdjksd
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "RemarkModel.h"
#import "NSString+Size.h"

@implementation RemarkModel

- (void)setRemarkDic:(NSDictionary *)remarkDic{
    
    CGFloat maxWidth = _maxWidth;
    UIFont *font = [RemarkModel remarkFont];
    
    NSString * user     =   remarkDic[@"name"];
    NSString * reply    =   remarkDic[@"replyName"];

    NSString * content  =   remarkDic[@"content"];
    NSString * str      =   @" 回复 ";
    
    _userId     = remarkDic[@"userId"];
    _replyId    = remarkDic[@"replyUserId"];
    _userName   = user;
    _replyName  = reply;
    
    NSAttributedString *userAttribute = [[NSAttributedString alloc] initWithString:user attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor clearColor]}];
    NSAttributedString *contentAttribute = [[NSAttributedString alloc] initWithString:content attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor blackColor]}];
    
     NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] init];
    [attributeStr appendAttributedString:userAttribute];
    
    if (reply) {
         NSAttributedString *strAttribute = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor blackColor]}];
        NSAttributedString *replyAttribute = [[NSAttributedString alloc] initWithString:reply attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor clearColor]}];
        [attributeStr appendAttributedString:strAttribute];
        [attributeStr appendAttributedString:replyAttribute];
    }
    NSAttributedString *strAttribute2 = [[NSAttributedString alloc] initWithString:@"：" attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor blackColor]}];
    [attributeStr appendAttributedString:strAttribute2];
    [attributeStr appendAttributedString:contentAttribute];
    
    NSString *text;
    if (reply) {
        text = [NSString stringWithFormat:@"%@%@%@：%@ ", user, str, reply, content ];
    }
    else{
        text = [NSString stringWithFormat:@"%@：%@ ", user, content ];
    }
    

    CGSize textSize = [text textSizeWithWidth:maxWidth Font:font];
    CGSize userSize = [user textSizeWithWidth:maxWidth Font:font];
    
    _contentFrame   =   CGRectMake(0, 5, maxWidth, textSize.height);
    _userFrame      =   CGRectMake(10, 5, userSize.width, userSize.height);
    
    if (reply) {
        CGSize replySize    =   [reply textSizeWithWidth:maxWidth Font:font];
        CGSize strSize      =   [str textSizeWithWidth:maxWidth Font:font];
        _replyFrame         =   CGRectMake(userSize.width + strSize.width + 10, 5, replySize.width, replySize.height);
    }
    
    _viewHeight = textSize.height + 10;
    _text = attributeStr;
}

+ (UIFont *)remarkFont{
    return [UIFont systemFontOfSize:14];
}

@end
