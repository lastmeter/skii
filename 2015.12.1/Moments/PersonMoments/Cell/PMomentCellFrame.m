//
//  PMomentCellFrame.m
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "PMomentCellFrame.h"
#import "NSString+Size.h"
#import "RemarkModel.h"
#import "FabUserModel.h"

@implementation PMomentCellFrame

- (void)setPMoment:(PMomentModel *)pMoment{
    _pMoment = pMoment;

    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   80;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    
    _dateFrame      =   CGRectMake(10, 20, leftPading, 40);
    
    UIFont *contentFont =   [PMomentCellFrame contentFont];
    CGSize contentSize  =   [pMoment.content textSizeWithWidth:contentWidth - 20 Font:[PMomentCellFrame contentFont]];
    CGFloat contentH    =   contentSize.height;
    CGFloat showAllBtnH =   0;
    
    NSInteger numberOfLine = fabs(contentH / contentFont.lineHeight);
    if (numberOfLine > 6) {
        _isLongContent = YES;
        if ( !_showAll ) {
            contentH = contentFont.lineHeight * 6;
        }
        showAllBtnH = 30;
    }
    else{
        _isLongContent = NO;
        _showAll = YES;
    }
    _contentFrame       =   CGRectMake(leftPading, 0, contentWidth - 20, contentH + 10);
    _showAllBtnFrame    =   CGRectMake(leftPading, CGRectGetMaxY(_contentFrame), 50, showAllBtnH);

    CGFloat collectionX     =   leftPading;
    CGFloat collectionY     =   CGRectGetMaxY(_showAllBtnFrame) ;
    CGFloat collectionW     =   contentWidth;
    CGFloat collectionH     =   0;
    _colletionItemPad       =   5;
    _colletionItemW         =   0;
    _colletionItemH         =   0;
    
    NSInteger attchCount = pMoment.attch.count;
    if (attchCount > 0) {
        NSInteger currentLine =  1;
        if (attchCount == 1) {
            _colletionItemW  = contentWidth;
            _colletionItemH  = MAIN_SCREEN_HEIGHT * 0.4;
            collectionH = _colletionItemH;
        }
        else if (attchCount == 2 || attchCount == 4) {
            _colletionItemW  = (contentWidth - _colletionItemPad) / 2;
            if (attchCount == 4 ) { currentLine = 2; }
            _colletionItemH  = _colletionItemW;
            collectionH = (_colletionItemW + _colletionItemPad ) * currentLine;
        }
        else{
            _colletionItemW  = (contentWidth - 2 * _colletionItemPad) / 3;
            currentLine = pMoment.attch.count / 3;
            if (pMoment.attch.count % 3 != 0) { currentLine++; }
            _colletionItemH  = _colletionItemW;
            collectionH = (_colletionItemW + _colletionItemPad ) * currentLine;
        }
        collectionY += 5;
    }
    _collectionFrame = CGRectMake(collectionX, collectionY, collectionW, collectionH);
    

    
    CGFloat btnW    =   60;
    CGFloat btnH    =   pMoment.editEnable ? 30 : 0;
    CGFloat btnY    =   CGRectGetMaxY(_collectionFrame) + 5;
    
    _remarkBtnFrame =   CGRectMake(MainScreenWidth - rightPading - btnW, btnY, btnW, btnH);
    _fabBtnFrame    =   CGRectMake(CGRectGetMinX(_remarkBtnFrame) - btnW, btnY, btnW, btnH);
 
    if (pMoment.deleteEnable) {
        _deleteBtnFrame =  CGRectMake(leftPading, btnY, 50, btnH);
    }
    
    CGFloat fabH = 0;
    if (pMoment.fabs.count > 0) {
        
        UIFont *font = [FabUserModel fabFont];
        NSMutableString *mutableStr = [NSMutableString stringWithString:@"      "];
        NSMutableArray *array = [NSMutableArray array];
        [pMoment.fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            FabUserModel *user = [[FabUserModel alloc] init];
            NSString *name = [NSString stringWithFormat:@" %@ ",obj[@"name"]];
            user.name   = name;
            user.userId = obj[@"userId"];
            CGSize size = [user.name textSizeWithWidth:contentWidth Font:font];
            user.size = size;
            [array addObject:user];
            [mutableStr appendString:name];
        }];
        fabH = [mutableStr textSizeWithWidth:contentWidth - 10 Font:font].height + 10;
        _fabUsers = array;
    }
    
    _fabsFrame = CGRectMake(leftPading, CGRectGetMaxY(_remarkBtnFrame),contentWidth, fabH ? fabH + 3 : 0);
    
    __block CGFloat viewH = 0;
    if (pMoment.remark.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        [pMoment.remark enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RemarkModel *remarkModel = [[RemarkModel alloc] init];
            remarkModel.maxWidth = contentWidth;
            remarkModel.remarkDic = obj;
            viewH += remarkModel.viewHeight;
            [array addObject:remarkModel];
        }];
        _remarkModels = array;
    }
    _remarksFrame = CGRectMake(leftPading, CGRectGetMaxY(_fabsFrame), contentWidth, viewH);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;

}


- (void)fitToCollectionSize:(CGSize)collectionSize{
    _collectionFrame.size           =   collectionSize;
    _colletionItemH                 =   collectionSize.height;
    _colletionItemW                 =   collectionSize.width;
    CGFloat btnY                    =   CGRectGetMaxY(_collectionFrame);
    _deleteBtnFrame.origin.y        =   btnY;
    _fabBtnFrame.origin.y           =   btnY;
    _remarkBtnFrame.origin.y        =   btnY;
    _fabsFrame.origin.y             =   CGRectGetMaxY(_remarkBtnFrame);
    _remarksFrame.origin.y          =   CGRectGetMaxY(_fabsFrame);
    _cellHeight                     =   CGRectGetMaxY(_remarksFrame) + 10;
}


- (void)resetFabs:(NSArray *)fabs{
    
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   80;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    _pMoment.fabs = fabs;
    _pMoment.isFabed = YES;
    CGFloat fabH = 0;
    if (_pMoment.fabs.count > 0) {
        
        UIFont *font = [FabUserModel fabFont];
        NSMutableString *mutableStr = [NSMutableString stringWithString:@"      "];
        NSMutableArray *array = [NSMutableArray array];
        [_pMoment.fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            FabUserModel *user = [[FabUserModel alloc] init];
            NSString *name = [NSString stringWithFormat:@" %@ ",obj[@"name"]];
            user.name   = name;
            user.userId = obj[@"userId"];
            CGSize size = [name textSizeWithWidth:contentWidth Font:font];
            user.size = size;
            [array addObject:user];
            [mutableStr appendString:name];
        }];
        fabH = [mutableStr textSizeWithWidth:contentWidth - 10 Font:font].height + 10;
        _fabUsers = array;
    }
    _fabsFrame = CGRectMake(leftPading, CGRectGetMaxY(_remarkBtnFrame),contentWidth, fabH ? fabH + 3 : 0);
    _remarksFrame.origin.y = CGRectGetMaxY(_fabsFrame);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;
}

- (void)resetRemarks:(NSArray *)remarks{
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   80;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    _pMoment.remark = remarks;
    
    __block CGFloat viewH = 0;
    if (_pMoment.remark.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        [_pMoment.remark enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RemarkModel *remarkModel = [[RemarkModel alloc] init];
            remarkModel.maxWidth = contentWidth - 10;
            remarkModel.remarkDic = obj;
            viewH += remarkModel.viewHeight;
            [array addObject:remarkModel];
        }];
        _remarkModels = array;
    }
    _remarksFrame = CGRectMake(leftPading, CGRectGetMaxY(_fabsFrame), contentWidth, viewH);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;
    
}



+ (UIFont *)contentFont{
    return [UIFont systemFontOfSize:17];
}


@end
