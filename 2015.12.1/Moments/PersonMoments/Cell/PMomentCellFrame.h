//
//  PMomentCellFrame.h
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMomentModel.h"

@interface PMomentCellFrame : NSObject

@property(nonatomic, strong) PMomentModel *pMoment;

@property(nonatomic, readonly) CGRect contentFrame;

@property(nonatomic, readonly) CGRect dateFrame;
@property(nonatomic, readonly) CGRect remarkBtnFrame;
@property(nonatomic, readonly) CGRect fabBtnFrame;
@property(nonatomic, readonly) CGRect fabsFrame;
@property(nonatomic, readonly) CGRect remarksFrame;
@property(nonatomic, readonly) CGFloat cellHeight;

@property(nonatomic, readonly) CGRect collectionFrame;
@property(nonatomic, readonly) CGFloat colletionItemPad;
@property(nonatomic, readonly) CGFloat colletionItemW;
@property(nonatomic, readonly) CGFloat colletionItemH;

@property(nonatomic, readonly) NSArray *remarkModels;
@property(nonatomic, readonly) NSArray *fabUsers;

@property(nonatomic, readonly) CGRect deleteBtnFrame;

@property(nonatomic, assign) BOOL showAll;
@property(nonatomic, readonly) BOOL isLongContent;
@property(nonatomic, readonly) CGRect showAllBtnFrame;
@property(nonatomic, assign) BOOL loadImageFinish;

- (void)fitToCollectionSize:(CGSize)collectionSize;
- (void)resetRemarks:(NSArray *)remarks;
- (void)resetFabs:(NSArray *)fabs;

+ (UIFont *)contentFont;
@end
