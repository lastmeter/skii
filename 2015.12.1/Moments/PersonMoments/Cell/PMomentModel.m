//
//  PMomentModel.m
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "PMomentModel.h"
#import "NSString+Json.h"
#import "NSDate+String.h"
#import "EzlUser.h"
#import "WebImage.h"
#import "MomentShare.h"

@implementation PMomentModel

- (void)setDataDic:(NSDictionary *)dataDic{
    
    _content    =   dataDic[@"content"];
    _momentId   =   dataDic[@"id"];
    _market     =   dataDic[@"market"];
    _attch      =   [dataDic[@"attach"] toJsonObject];
    _remark     =   [dataDic[@"remark"] toJsonObject];
    _fabs       =   [dataDic[@"fab"] toJsonObject];
    _uid        =   dataDic[@"userId"];
    
    NSString *mUserId = [EzlUser currentUser].userId;
    _deleteEnable = [_uid isEqualToString:mUserId];
    
    NSArray *attch =   [dataDic[@"attach"] toJsonObject];
    NSMutableArray *array = [NSMutableArray array];
    [attch enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WebImage *webImage = [[WebImage alloc] init];
        webImage.urlString = obj[@"url"];
        [array addObject:webImage];
    }];
    _attch = array;
    
    if (_editEnable) {
        [_fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([_uid isEqualToString:mUserId]) {
                _isFabed = YES;
                *stop = YES;
            }
        }];
    }
    
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES];
    if (_fabs.count > 0) {
        _fabs = [_fabs sortedArrayUsingDescriptors:@[descriptor]];
    }
    if (_remark.count > 0) {
        _remark = [ _remark sortedArrayUsingDescriptors:@[descriptor] ];
    }
    
    NSTimeInterval timeSp = [dataDic[@"time"] doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeSp];
    _date = [date dateString];
    
}

- (void)addSendingResultObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(momentDidSendSucess:) name:EzlMomentDidSendSucess object:nil];
}

- (void)momentDidSendSucess:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
    if (!dic) {
        return;
    }
    if ([dic[@"timeSp"] isEqualToString:_momentId]) {
        _momentId = dic[@"momentId"];
        _uploaded = YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:EzlMomentDidSendSucess object:nil];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
