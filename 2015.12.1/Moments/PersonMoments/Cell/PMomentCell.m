//
//  PMomentsCell.m
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "PMomentCell.h"
#import "MomentPortrait.h"
#import "RemarksView.h"
#import "FabsView.h"
#import "ImageShowCell.h"
#import "WebImageBrowser.h"
#import "WebImage.h"
#import "LrhAlertView.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

@interface PMomentCell ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, RemarksViewDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;

@end

@implementation PMomentCell
{
    RemarksView *_remarks;
    FabsView *_fabs;
    UIButton *_remarkBtn, *_fabBtn;
    UILabel  *_date;
    UILabel *_content;
    
    UIButton *_deleteBtn, *_showAllBtn;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _content = [[UILabel alloc] init];
        _content.userInteractionEnabled = NO;
        _content.numberOfLines = 0;
        _content.font = [PMomentCellFrame contentFont];
        [self.contentView addSubview:_content];
        
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[ImageShowCell class] forCellWithReuseIdentifier:@"photoCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];

        
        _fabs = [[FabsView alloc] init];
        [self.contentView addSubview:_fabs];
        
        _remarks = [[RemarksView alloc] init];
        _remarks.delegate = self;
        [self.contentView addSubview:_remarks];
        
        _date = [[UILabel alloc] init];
        _date.font = [UIFont boldSystemFontOfSize:22];
        _date.textColor = [UIColor grayColor];
        [self.contentView addSubview:_date];
    }
    return self;
}

- (void)setEditEnable:(BOOL)editEnable{
    _editEnable = editEnable;
    if (editEnable) {
        if (!_fabBtn) {
            _fabBtn = [[UIButton alloc] init];
            [_fabBtn addTarget:self action:@selector(addFab:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:_fabBtn];
        }
        if (!_remarkBtn) {
            _remarkBtn = [[UIButton alloc] init];
            [_remarkBtn setImage:[UIImage imageNamed:@"remark"] forState:UIControlStateNormal];
            [_remarkBtn addTarget:self action:@selector(addRemark:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:_remarkBtn];
        }
    }
}

- (void)setCellFrame:(PMomentCellFrame *)cellFrame{

    _cellFrame = cellFrame;
    
    [_collectionView reloadData];
    _collectionView.frame = cellFrame.collectionFrame;
    
    _content.frame      =   cellFrame.contentFrame;
    _fabs.frame         =   cellFrame.fabsFrame;
    _remarks.frame      =   cellFrame.remarksFrame;
    _date.frame         =   cellFrame.dateFrame;
    
    PMomentModel * moment = cellFrame.pMoment;
    _content.text   =   moment.content;
    _date.text      =   moment.date;
    
    _remarks.remarksArray = cellFrame.remarkModels;
    _fabs.users = cellFrame.fabUsers;
    
    if (cellFrame.isLongContent) {
        if (!_showAllBtn) {
            _showAllBtn = [[UIButton alloc] init];
            _showAllBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [self.contentView addSubview:_showAllBtn];
            _showAllBtn.titleLabel.font = [UIFont systemFontOfSize:16];
            [_showAllBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_showAllBtn addTarget:self action:@selector(showAllContent:) forControlEvents:UIControlEventTouchUpInside];
        }
        _showAllBtn.frame   =   cellFrame.showAllBtnFrame;
        _showAllBtn.hidden  =   NO;
        
        [_showAllBtn setTitle:cellFrame.showAll ? @"收起" : @"全文" forState:UIControlStateNormal];
    }
    else if (_showAllBtn){
        _showAllBtn.hidden = YES;
    }
    
    if (_editEnable) {
        _fabBtn.frame       =   cellFrame.fabBtnFrame;
        _remarkBtn.frame    =   cellFrame.remarkBtnFrame;
        [_fabBtn setImage:[UIImage imageNamed:moment.isFabed ? @"fabed" : @"nofab"] forState:UIControlStateNormal];
        _fabBtn.userInteractionEnabled = !moment.isFabed ;
    }
    if (moment.deleteEnable) {
        if (!_deleteBtn) {
            _deleteBtn = [[UIButton alloc] init];
            [self.contentView addSubview:_deleteBtn];
            
            _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:16];
            [_deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
            _deleteBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [_deleteBtn addTarget:self action:@selector(deleteMoment:) forControlEvents:UIControlEventTouchUpInside];
        }
        _deleteBtn.frame = cellFrame.deleteBtnFrame;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.cellFrame.pMoment.attch.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    cell.needScale = !(self.cellFrame.pMoment.attch.count == 1);
    
    
    WebImage *webImage = self.cellFrame.pMoment.attch[indexPath.row];
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[webImage.urlString lastPathComponent]];
    if (image) {
        cell.imageView.image = image;
        if (self.cellFrame.pMoment.attch.count == 1) {
            if ( !self.cellFrame.loadImageFinish ) {
                [self fitToImageSize:image.size];
            }
        }
        
    }
    else{
        WEAKSELF
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:webImage.urlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (weakSelf.cellFrame.pMoment.attch.count == 1) {
                if ( !weakSelf.cellFrame.loadImageFinish ) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf fitToImageSize:image.size];
                    });
                }
            }
        }];
    }
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(_cellFrame.colletionItemW, _cellFrame.colletionItemH);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return _cellFrame.colletionItemPad;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return _cellFrame.colletionItemPad;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *attach = self.cellFrame.pMoment.attch;

    UITableView *tableView = (UITableView *)self.superview.superview ;
    CGPoint offset = tableView.contentOffset;
    
    [attach enumerateObjectsUsingBlock:^(WebImage *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ImageShowCell *cell = (ImageShowCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
        CGRect frame = [self convertRect:cell.realImageFrame fromView:collectionView];
        frame.origin.y = frame.origin.y + CGRectGetMinY(self.frame) - offset.y + 64;
        obj.originFrame = frame;
    }];
    
    WebImageBrowser *imageBrowser = [[WebImageBrowser alloc] init];
    [imageBrowser showImageWithWebImages:attach startIndex:indexPath.row];
}

#pragma mark-

- (void)addRemark:(UIButton *)btn{
    if (!_cellFrame.pMoment.uploaded) { return; }
    CGFloat y = CGRectGetMaxY(btn.frame) + CGRectGetMinY(self.frame);
    [_delegate addRemarkToMomentId:_cellFrame.pMoment.momentId maxY:y cellFrame:self.cellFrame];
}

- (void)addFab:(UIButton *)btn{
    if (!_cellFrame.pMoment.uploaded) { return; }
    [_delegate addFabToMomentId:_cellFrame.pMoment.momentId cellFrame:self.cellFrame];
}

- (void)addReplyToRemark:(RemarkModel *)remark convertRect:(CGRect)rect{
    if (!_cellFrame.pMoment.uploaded) { return; }
    if (!_editEnable) { return; }
    CGRect converRect = [self convertRect:rect fromView:_remarks];
    CGFloat y = CGRectGetMaxY(converRect) + CGRectGetMinY(self.frame);
    
    NSString *replyId = remark.userId;
    NSString *replyName = remark.userName;
    NSString *momentId = self.cellFrame.pMoment.momentId;
    [_delegate addReplyToMomentId:momentId replyId:replyId replyName:replyName maxY:y cellFrame:_cellFrame];
}

- (void)deleteMoment:(UIButton *)btn{
    if (!_cellFrame.pMoment.uploaded) { return; }
    LrhAlertAction *action1 = [LrhAlertAction actionWithTitle:@"确定" handler:^{
        [_delegate deleteMomentId:_cellFrame.pMoment.momentId cellFrame:self.cellFrame];
    }];
    LrhAlertAction *action2 = [LrhAlertAction actionWithTitle:@"取消" handler:nil];
    [LrhAlertView showMessage:@"确定删除吗？" actions:@[action1, action2]];
}

- (void)showAllContent:(UIButton *)btn{
    if (_cellFrame.isLongContent) {
        _cellFrame.showAll = !_cellFrame.showAll;
        _cellFrame.pMoment = _cellFrame.pMoment;
        [_showAllBtn setTitle:_cellFrame.showAll ? @"收起" : @"全文" forState:UIControlStateNormal];
        UITableView *tableView = (UITableView *)self.superview.superview;
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        //[tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.tag] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
}

#pragma mark-
- (void)fitToImageSize:(CGSize)imageSize{
    CGSize newSize = [self scaleSizeFormImageSize:imageSize];
    self.cellFrame.loadImageFinish = YES;
    [self.cellFrame fitToCollectionSize:newSize];
    [self.collectionView reloadData];
    UITableView *tableView = (UITableView *)self.superview.superview;
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tag] withRowAnimation:UITableViewRowAnimationNone];
}

- (CGSize)scaleSizeFormImageSize:(CGSize)imageSize{
    CGFloat xScale = self.cellFrame.colletionItemW / imageSize.width;
    CGFloat yScale = self.cellFrame.colletionItemH / imageSize.height;
    CGFloat minScale = MIN(xScale, yScale);
    
    CGFloat maxScale = 2.0 / [[UIScreen mainScreen] scale];
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    return CGSizeMake(imageSize.width * minScale, imageSize.height * minScale);}
@end
