//
//  PMomentsCell.h
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMomentCellFrame.h"

@protocol PMomentCellDelegate <NSObject>

- (void)addRemarkToMomentId:(NSString *)momentId maxY:(CGFloat)maxY cellFrame:(PMomentCellFrame *)cellFrame;
- (void)addFabToMomentId:(NSString *)momentId cellFrame:(PMomentCellFrame *)cellFrame;
- (void)addReplyToMomentId:(NSString *)momentId replyId:(NSString *)replyId replyName:(NSString *)replyName maxY:(CGFloat)maxY cellFrame:(PMomentCellFrame *)cellFrame;
- (void)deleteMomentId:(NSString *)momentId cellFrame:(PMomentCellFrame *)cellFrame;
@end

@interface PMomentCell : UITableViewCell

@property (nonatomic, weak) PMomentCellFrame *cellFrame;
@property (nonatomic, assign) BOOL editEnable;
@property (nonatomic, weak) id<PMomentCellDelegate>delegate;

@end
