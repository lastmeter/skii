//
//  PMomentModel.h
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMomentModel : NSObject

@property (nonatomic, weak) NSDictionary * dataDic;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *momentId;
@property (nonatomic, copy) NSString *market;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *uid;

@property (nonatomic, strong) NSArray  *remark;
@property (nonatomic, strong) NSArray *fabs;
@property (nonatomic, strong) NSArray * attch;

@property (nonatomic, assign) BOOL isFabed;
@property (nonatomic, assign) BOOL editEnable;
@property (nonatomic, assign) BOOL deleteEnable;
@property (nonatomic, assign) BOOL uploaded;


- (void)addSendingResultObserver;
@end
