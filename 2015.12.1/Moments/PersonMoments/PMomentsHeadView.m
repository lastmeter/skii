//
//  PMomentsHeadView.m
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "PMomentsHeadView.h"
#import "MomentsService.h"

@implementation PMomentsHeadView{
    UIImageView *_head;
    UILabel *_nameLabel, *_leaderLabel;
}

- (instancetype)init{
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;

    self = [super initWithFrame:CGRectMake(0, 0, MainScreenWidth, 100)];
    if (self) {
        _head = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 80, 80)];
        [self addSubview:_head];
        
        _leaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_head.frame) + 10, CGRectGetHeight(self.frame) - 31, CGRectGetWidth(self.bounds) - CGRectGetMaxX(_head.frame) - 20, 21)];
        _leaderLabel.font = [UIFont systemFontOfSize:15];
        _leaderLabel.textColor = [UIColor grayColor];
        [self addSubview:_leaderLabel];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_head.frame) + 10, CGRectGetMinY(_leaderLabel.frame) - 26, 200, 21)];
        _nameLabel.font = [UIFont systemFontOfSize:17];
        [self addSubview:_nameLabel];
    }
    return self;
}



- (void)setName:(NSString *)name{
    _nameLabel.text = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)setLeader:(NSString *)leader{
    _leaderLabel.text = leader;
}

- (void)setUid:(NSString *)uid{
    __weak UIImageView *weakImageView = _head;
    [MomentsService getHeadPortraitByUserId:uid handler:^(HandleResult result, NSData *data) {
        if (result == HandleResultSuccess) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakImageView.image = image;
                });
            }
        }
    }];
}


@end
