//
//  PMomentsViewController.h
//  Moments
//
//  Created by xihan on 15/12/4.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "NEzlViewController.h"

@interface PMomentsViewController : NEzlViewController

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, assign) BOOL editEnable;

@end
