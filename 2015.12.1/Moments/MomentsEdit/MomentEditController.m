//
//  MomentEditControllerViewController.m
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentEditController.h"
#import "EzlPhotoPicker.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MomentsService.h"
#import "ImageBrowser.h"
#import "EzlPhotoManager.h"
#import "ImageShowCell.h"
#import "UIView+Frame.h"
#import "WebImage.h"
#import "EzlWebImageManager.h"
#import "SDImageCache.h"
#import "MomentSendHelper.h"

const CGFloat kCollectionItemHW = 60;
const NSInteger kMaxMomentTextLen = 2048;

@interface MomentEditController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UITextView *contentView;
@property (strong, nonatomic) UIButton *sendBtn;
@property (strong, nonatomic) UILabel *placeholder;
@property (strong, nonatomic) UILabel *textLenLabel;
@property (weak, nonatomic) EzlPhotoManager *photoManager;

@end

@implementation MomentEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.photoManager = [EzlPhotoManager shareManager];
    
    [self p_InitBaseView];
    [self p_AddBackgroundTap];
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"pickImageFinish" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChange:) name:UITextViewTextDidChangeNotification object:_contentView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)p_InitBaseView{
    
    CGFloat MainScreenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    _contentView = [[UITextView alloc] initWithFrame:CGRectMake(20, 40, MainScreenWidth - 40, 200)];
    _contentView.font = [UIFont systemFontOfSize:17];
    _contentView.layer.masksToBounds = YES;
    _contentView.layer.cornerRadius = 7;
    _contentView.layer.borderWidth = 2;
    _contentView.layer.borderColor = [[UIColor orangeColor] CGColor];
    _contentView.textAlignment = NSTextAlignmentLeft;
    _contentView.delegate = self;
    [self.view addSubview:_contentView];
    
    _placeholder = [[UILabel alloc] initWithFrame:CGRectMake(5, 8, 100, 21)];
    [_contentView addSubview:_placeholder];
    _placeholder.font = _contentView.font;
    _placeholder.textColor = [UIColor grayColor];
    _placeholder.text = @"说点什么吧...";
    
    _textLenLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_contentView.frame), CGRectGetWidth(_contentView.frame), 21)];
    _textLenLabel.font = [UIFont systemFontOfSize:17];
    _textLenLabel.textColor = [UIColor lightGrayColor];
    _textLenLabel.text = [NSString stringWithFormat:@"0/%@",@(kMaxMomentTextLen)];
    [self.view addSubview:_textLenLabel];

    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(_textLenLabel.frame) + 10, CGRectGetWidth(self.view.frame) - 40, 60) collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerClass:[ImageShowCell class] forCellWithReuseIdentifier:@"photoCell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [self.view addSubview:_collectionView];
    
    _sendBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_collectionView.frame) + 20, MainScreenWidth - 40, 40)];
    [_sendBtn setTitle:@"发表" forState:UIControlStateNormal];
    [_sendBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    _sendBtn.layer.masksToBounds = YES;
    _sendBtn.layer.cornerRadius = 8;
    _sendBtn.layer.borderColor = [[UIColor blackColor] CGColor];
    _sendBtn.layer.borderWidth = 1;
    _sendBtn.enabled = NO;
    [_sendBtn addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_sendBtn addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
    [_sendBtn addTarget:self action:@selector(sendMoment:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendBtn];
}

- (void)btnTouchDown:(UIButton *)btn{
    btn.backgroundColor = [UIColor lightGrayColor];
}

- (void)btnTouchCancel:(UIButton *)btn{
    btn.backgroundColor = [UIColor whiteColor];
}

- (void)sendMoment:(UIButton *)btn{
    btn.backgroundColor = [UIColor whiteColor];
    [self waitting];
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *images = [NSMutableArray array];
    
    dispatch_group_t group = dispatch_group_create();
    [self.photoManager.seletedPhotos enumerateObjectsUsingBlock:^(EzlPhoto *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *data = obj.imageData;
            NSString *imageName = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970] * 1000];
            [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:data] forKey:imageName];
            [array addObject:data];
            
            WebImage *image = [[WebImage alloc] init];
            image.urlString = imageName;
            image.thumbUrlString = imageName;
            [images addObject: image];
        });
    }];
    NSString * timeSp = [@( [[NSDate date] timeIntervalSince1970] ) stringValue];
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self stopWaitting];
        [_delegate didSendMoment:timeSp content:_contentView.text attch:images];
        [self.navigationController popViewControllerAnimated:YES];
        [MomentSendHelper sendMomentWithContent:_contentView.text attach:array timeSp:timeSp];
       
    });
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    NSInteger seletedPhotos = self.photoManager.seletedPhotos.count ;
    if (seletedPhotos < 3) {
        return seletedPhotos + 1;
    }
    return seletedPhotos;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageShowCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    NSInteger seletedPhotos = self.photoManager.seletedPhotos.count ;

    if (seletedPhotos < 3 && indexPath.row == seletedPhotos) {
        cell.imageView.image = [UIImage imageNamed:@"article_photo_pressed"];
    }
    else{
        EzlPhoto *photo = self.photoManager.seletedPhotos[indexPath.row];
        cell.imageView.image = photo.thumbnail;
        CGRect frame = [cv convertRect:cell.frame toView:self.view];
        frame.origin.y += 64;
        photo.originFrame = frame;
    }
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat wh = kCollectionItemHW;
    return CGSizeMake(wh, wh);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     __unsafe_unretained MomentEditController* unSelf = self;
    if (indexPath.row == self.photoManager.seletedPhotos.count) {
        [EzlPhotoPicker showInSuperVC:self pickImage:^(NSArray *images) {
            [unSelf reloadData];
        }];
    }
    else{
        ImageBrowser *browser = [[ImageBrowser alloc] init];
        browser.editEnable = YES;
        [browser imageDidChange:^{
            [unSelf reloadData];
        }];
        [browser showWithImages:self.photoManager.seletedPhotos startIndex:indexPath.row];
    }
}

- (void)reloadData{
    CGFloat btnWH = kCollectionItemHW;
    CGFloat width = CGRectGetWidth(self.collectionView.bounds);
    NSInteger number = width / btnWH;
    NSInteger count = self.photoManager.seletedPhotos.count + 1;
    NSInteger currentLine = count / number;
    if (count % number != 0) {
        currentLine++;
    }
    [self.collectionView changeHeight:(btnWH + 5) * currentLine];
    [_sendBtn changeY:CGRectGetMaxY(self.collectionView.frame) + 10];
    [self.collectionView reloadData];
}

#pragma mark- Text View
- (void)textViewDidEndEdit:(NSNotification *)noti{
    _placeholder.hidden = !_contentView.text.length > 0;
}

- (void)textViewTextDidChange:(NSNotification *)noti{
    NSInteger length = _contentView.text.length;
    BOOL textNotEmpty = length > 0;
    if (_sendBtn.enabled != textNotEmpty) {
        _sendBtn.enabled = textNotEmpty;
        [_sendBtn setTitleColor:textNotEmpty ? [UIColor orangeColor] : [UIColor grayColor] forState:UIControlStateNormal];
    }
    
    
    UITextRange *selectedRange = [_contentView markedTextRange];
    UITextPosition *position = [_contentView positionFromPosition:selectedRange.start offset:0];
    if (!position) {
        if (length > kMaxMomentTextLen) {
            _contentView.text = [_contentView.text substringToIndex:kMaxMomentTextLen];
            _textLenLabel.text = [NSString stringWithFormat:@"%@/%@",@(kMaxMomentTextLen),@(kMaxMomentTextLen)];
        }
        else{
            _textLenLabel.text = [NSString stringWithFormat:@"%@/%@",@(length),@(kMaxMomentTextLen)];
        }
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _placeholder.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    _placeholder.hidden = textView.text.length > 0;
}

#pragma mark- Background Tap
- (void)p_AddBackgroundTap{

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.delegate = self;
    [tap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tap];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UITextView class]]) {
        return NO;
    }
    [self hideKeyboard];
    return NO;
}

- (void)hideKeyboard{
    [_contentView resignFirstResponder];
}


- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[EzlPhotoManager shareManager] free];
}

@end
