//
//  MomentEditControllerViewController.h
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "NEzlViewController.h"

@protocol MomentEditDelegate <NSObject>

- (void)didSendMoment:(NSString *)momentId content:(NSString *)content attch:(NSArray *)attch;

@end

@interface MomentEditController : NEzlViewController

@property (nonatomic, weak)id<MomentEditDelegate>delegate;

@end
