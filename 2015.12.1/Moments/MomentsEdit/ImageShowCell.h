//
//  ImageShowCell.h
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageShowCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
- (CGRect)realImageFrame;

@property (nonatomic, assign) BOOL needScale;
@end
