//
//  ImageShowCell.m
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ImageShowCell.h"

@implementation ImageShowCell


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        _imageView = [[UIImageView alloc] init];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imageView];
        
        _needScale = YES;
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"image":_imageView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"image":_imageView}]];
        
    }
    return self;
}

- (CGRect)realImageFrame{

    if (!_needScale) {
        return self.frame;
    }
    
    CGSize imageSize = _imageView.image.size;
    CGSize boundSize = self.bounds.size;
    
    CGFloat xScale = boundSize.width / imageSize.width;
    CGFloat yScale = boundSize.width / imageSize.height;
    
    BOOL imagePortrait = imageSize.height > imageSize.width;
    BOOL phonePortrait = NO;
    CGFloat minScale = imagePortrait == phonePortrait ? xScale : MIN(xScale, yScale);
    
    CGFloat maxScale = 1.0;
    
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    
    CGSize newSize = CGSizeMake(imageSize.width * minScale, imageSize.height * minScale);
    UIView *view = [[UIView alloc] initWithFrame:(CGRect){ 0,0,newSize }];
    view.center = self.center;

    return view.frame;
}
@end
