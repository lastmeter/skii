//
//  MomentsMarkController.m
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentsMarkController.h"
#import "MomentsService.h"
#import "MomentsViewController.h"
#import "MomentEditController.h"
#import "EzlUser.h"
#import "UDManager.h"
#import "NSDate+String.h"

@interface MomentsMarkController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    NSArray *_marks;
    NSMutableArray *_unread;
    NSDictionary *_imageDic;
}
@end

@implementation MomentsMarkController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableView];
    [self p_LoadData];

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setTitle:@"交流区" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0,0)];
    [btn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = backItem;
    
    _imageDic = @{ @"CCO"   : @"topic_center",
                   @"East"  : @"topic_east",
                   @"South" : @"topic_south",
                   @"West"  : @"topic_west",
                   @"GBJ"   : @"topic_gbj",
                   @"GSH"   : @"topic_gsh",
                   @"North" : @"topic_north"
                   };

    [EzlUser currentUser].numberOfUnreadMoment = 0;
}

- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)p_LoadData{
    
    [self waitting];
    [MomentsService getMarketWithHandler:^(HandleResult result, NSArray *dataArray) {
        if (result == HandleResultSuccess) {
            _marks = [dataArray copy];
            [_tableView reloadData];
            [self stopWaitting];
        }
    }];
}

- (void)initTableView {
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  _marks.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary *dic = _marks[indexPath.row];
    cell.textLabel.text = dic[@"name"];
    NSString *imgStr = _imageDic[dic[@"name"]];
    if (!imgStr) {
        imgStr = @"topic_default";
    }
    cell.imageView.image = [UIImage imageNamed:imgStr];
    
    CGFloat padding = 40;
    if (isPad) {
        padding = 100;
    }
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:99];
    if (!imageView) {
 
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - padding, 6, 30, 30)];
        imageView.image = [UIImage imageNamed:@"chat_unread"];
        imageView.tag = 99;
        [cell.contentView addSubview:imageView];
    }
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:98];
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - padding, 6, 30, 30)];
        label.textColor = [UIColor whiteColor];
        label.tag = 98;
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
    }
    
    imageView.hidden = YES;
    label.hidden = YES;
    
    __weak UIImageView * weakImage = imageView;
    __weak UILabel *weakLabel = label;
    __weak EzlUser *user = [EzlUser currentUser];

    if ([dic[@"isSelf"] boolValue]) {
        [MomentsService getNewOfMomentsBySectorId:dic[@"id"] afterDate:[user getLastReadDateForMarket:dic[@"id"]] handler:^(HandleResult result, NSString *dataStr) {
            if (result == HandleResultSuccess && dataStr) {
                if ([dataStr integerValue] > 0) {
                    weakImage.hidden = NO;
                    weakLabel.hidden = NO;
                    weakLabel.text = dataStr;
                    user.numberOfUnreadMoment += [dataStr integerValue];
                }
               
            }
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = _marks[indexPath.row];
 
    MomentsViewController *momentsVC = [ MomentsViewController new ];
    momentsVC.sectorId = dic[@"id"];
    momentsVC.editEnable = [dic[@"isSelf"] boolValue];

    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = dic[@"name"];
    self.navigationItem.backBarButtonItem = backItem;
    [self.navigationController pushViewController:momentsVC animated:YES];
    
    if (momentsVC.editEnable) {
        EzlUser *user = [EzlUser currentUser];
        [user didReadMomentForMarket:dic[@"id"]];
        user.numberOfUnreadMoment = 0;
        [_tableView reloadData];
    }
}


@end
