//
//  MomentsCellFrame.m
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentCellFrame.h"
#import "NSString+Size.h"
#import "RemarkModel.h"
#import "FabUserModel.h"

@implementation MomentCellFrame

- (void)setMoment:(MomentModel *)moment{
    _moment = moment;
    
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   50;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    
    _headFrame      =   CGRectMake(5, 10, 40, 40);
    _nameFrame      =   CGRectMake(CGRectGetMaxX(_headFrame) + 5, CGRectGetMinY(_headFrame), MainScreenWidth - CGRectGetMaxX(_headFrame) - 15 , 21);
    _leaderFrame    =   CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_nameFrame), CGRectGetWidth(_nameFrame), CGRectGetHeight(_nameFrame));
    
    UIFont *contentFont =   [MomentCellFrame contentFont];
    CGSize contentSize  =   [moment.content textSizeWithWidth:contentWidth Font:contentFont];
    CGFloat contentH    =   contentSize.height;
    CGFloat showAllBtnH =   0;
    
    NSInteger numberOfLine = fabs(contentH / contentFont.lineHeight);
    if (numberOfLine > 6) {
        _isLongContent = YES;
        if ( !_showAll ) {
            contentH = contentFont.lineHeight * 6;
        }
        showAllBtnH = 30;
    }
    else{
        _isLongContent = NO;
        _showAll = YES;
    }
    _contentFrame       =   CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_headFrame), contentWidth, contentH + 10);
    _showAllBtnFrame    =   CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_contentFrame), 50, showAllBtnH);
    
    CGFloat collectionX     =   CGRectGetMinX(_nameFrame);
    CGFloat collectionY     =   CGRectGetMaxY(_showAllBtnFrame) ;
    CGFloat collectionW     =   contentWidth;
    CGFloat collectionH     =   0;
    _colletionItemPad       =   5;
    _colletionItemW         =   0;
    _colletionItemH         =   0;
   
    NSInteger attchCount = moment.attch.count;
    if (attchCount > 0) {
        NSInteger currentLine =  1;
        if (attchCount == 1) {
            _colletionItemW  = contentWidth;
            _colletionItemH  =  MAIN_SCREEN_HEIGHT * 0.4;
            collectionH = _colletionItemH;
            _colletionItemPad = 0;
        }
        else if (attchCount == 2 || attchCount == 4) {
            _colletionItemW  = (contentWidth - _colletionItemPad) / 2;
            if (attchCount == 4 ) { currentLine = 2; }
            _colletionItemH  = _colletionItemW;
            collectionH = (_colletionItemW + _colletionItemPad ) * currentLine;
        }
        else{
            _colletionItemW  = (contentWidth - 4 * _colletionItemPad) / 3;
            currentLine = moment.attch.count / 3;
            if (moment.attch.count % 3 != 0) { currentLine++; }
            _colletionItemH  = _colletionItemW;
            collectionH = (_colletionItemW + _colletionItemPad ) * currentLine;
        }
        collectionY += 5;
    }
    _collectionFrame = CGRectMake(collectionX, collectionY, collectionW, collectionH);

    CGFloat btnW    =   60;
    CGFloat btnH    =   35;
    CGFloat btnY    =   CGRectGetMaxY(_collectionFrame) + 5;
    _dateFrame      =   CGRectMake(leftPading, btnY, 100, btnH);

    if (moment.editEnable) {
        _remarkBtnFrame =   CGRectMake(MainScreenWidth - rightPading - btnW, btnY, btnW, btnH);
        _fabBtnFrame    =   CGRectMake(CGRectGetMinX(_remarkBtnFrame) - btnW, btnY, btnW, btnH);
    }
    
    if (moment.deleteEnable) {
        _deleteBtnFrame =  CGRectMake(CGRectGetMaxX(_dateFrame) + 5, btnY, 50, btnH);
    }

    CGFloat fabH = 0;
    if (moment.fabs.count > 0) {
  
        UIFont *font = [FabUserModel fabFont];
        NSMutableString *mutableStr = [NSMutableString stringWithString:@"      "];
        NSMutableArray *array = [NSMutableArray array];
        [moment.fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            FabUserModel *user = [[FabUserModel alloc] init];
            NSString *name = [NSString stringWithFormat:@" %@ ",obj[@"name"]];
            user.name   = name;
            user.userId = obj[@"userId"];
            CGSize size = [name textSizeWithWidth:contentWidth Font:font];
            user.size = size;
            [array addObject:user];
            [mutableStr appendString:name];
        }];
        fabH = [mutableStr textSizeWithWidth:contentWidth - 10 Font:font].height + 10;
        _fabUsers = array;
    }

    _fabsFrame = CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_dateFrame),contentWidth, fabH > 0 ? fabH + 3 : 0);
    
    __block CGFloat viewH = 0;
    if (moment.remark.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        [moment.remark enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RemarkModel *remarkModel = [[RemarkModel alloc] init];
            remarkModel.maxWidth = contentWidth - 10;
            remarkModel.remarkDic = obj;
            viewH += remarkModel.viewHeight;
            [array addObject:remarkModel];
        }];
        _remarkModels = array;
    }
    _remarksFrame = CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_fabsFrame), contentWidth, viewH);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;
}

- (void)fitToCollectionSize:(CGSize)collectionSize{
    
    _collectionFrame.size    =   collectionSize;
    _colletionItemH                 =   collectionSize.height;
    _colletionItemW                 =   collectionSize.width;
    CGFloat dateY                   =   CGRectGetMaxY(_collectionFrame);
    _dateFrame.origin.y             =   dateY;
    _deleteBtnFrame.origin.y        =   dateY;
    _fabBtnFrame.origin.y           =   dateY;
    _remarkBtnFrame.origin.y        =   dateY;
    _fabsFrame.origin.y             =   CGRectGetMaxY(_dateFrame);
    _remarksFrame.origin.y          =   CGRectGetMaxY(_fabsFrame);
    _cellHeight                     =   CGRectGetMaxY(_remarksFrame) + 10;
}

- (void)resetFabs:(NSArray *)fabs{
    
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   50;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    _moment.fabs = fabs;
    _moment.isFabed = YES;
    CGFloat fabH = 0;
    if (_moment.fabs.count > 0) {
        
        UIFont *font = [FabUserModel fabFont];
        NSMutableString *mutableStr = [NSMutableString stringWithString:@"      "];
        NSMutableArray *array = [NSMutableArray array];
        [_moment.fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            FabUserModel *user = [[FabUserModel alloc] init];
            NSString *name = [NSString stringWithFormat:@" %@ ",obj[@"name"]];
            user.name   = name;
            user.userId = obj[@"userId"];
            CGSize size = [name textSizeWithWidth:contentWidth Font:font];
            user.size = size;
            [array addObject:user];
            [mutableStr appendString:name];
        }];
        fabH = [mutableStr textSizeWithWidth:contentWidth - 10 Font:font].height + 10;
        _fabUsers = array;
    }
    _fabsFrame = CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_dateFrame),contentWidth, fabH > 0 ? fabH + 3 : 0);
    _remarksFrame.origin.y = CGRectGetMaxY(_fabsFrame);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;
}

- (void)resetRemarks:(NSArray *)remarks{
    CGFloat MainScreenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat leftPading      =   50;
    CGFloat rightPading     =   10;
    CGFloat contentWidth    =   MainScreenWidth - leftPading - rightPading;
    _moment.remark = remarks;
    
    __block CGFloat viewH = 0;
    if (_moment.remark.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        [_moment.remark enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            RemarkModel *remarkModel = [[RemarkModel alloc] init];
            remarkModel.maxWidth = contentWidth - 10;
            remarkModel.remarkDic = obj;
            viewH += remarkModel.viewHeight;
            [array addObject:remarkModel];
        }];
        _remarkModels = array;
    }
    _remarksFrame = CGRectMake(CGRectGetMinX(_nameFrame), CGRectGetMaxY(_fabsFrame), contentWidth, viewH);
    _cellHeight = CGRectGetMaxY(_remarksFrame) + 10;

}



+ (UIFont *)nameFont{
    return [UIFont systemFontOfSize:16];
}

+ (UIFont *)leaderFont{
    return [UIFont systemFontOfSize:14];
}

+ (UIFont *)contentFont{
    return [UIFont systemFontOfSize:17];
}




@end








