//
//  MomentModel.h
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UDManager.h"

@interface MomentModel : NSObject

@property (nonatomic, weak) NSDictionary * dataDic;

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *leader;
@property (nonatomic, copy) NSString *eid;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSAttributedString *nameAttribute;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *momentId;
@property (nonatomic, copy) NSString *market;

@property (nonatomic, strong) NSArray * attch;
@property (nonatomic, strong) NSArray  *remark;
@property (nonatomic, strong) NSArray  *fabs;

@property (nonatomic, assign) BOOL isFabed;
@property (nonatomic, assign) BOOL editEnable;
@property (nonatomic, assign) BOOL deleteEnable;
@property (nonatomic, assign) BOOL uploaded;

- (void)setUserName:(NSString *)name role:(NSString *)role;
- (void)addSendingResultObserver;
@end
