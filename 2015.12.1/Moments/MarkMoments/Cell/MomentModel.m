//
//  MomentModel.m
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentModel.h"
#import "NSString+Json.h"
#import "NSDate+String.h"
#import "EzlUser.h"
#import "WebImage.h"
#import "MomentCellFrame.h"
#import "MomentShare.h"

@implementation MomentModel

- (void)setDataDic:(NSDictionary *)dataDic{

    EzlUser *user = [EzlUser currentUser];
    _content    =   dataDic[@"content"];
    _eid        =   dataDic[@"eid"];
    _momentId   =   dataDic[@"id"];
    _market     =   dataDic[@"market"];
 
    _uid        =   dataDic[@"userId"];
    _remark     =   [dataDic[@"remark"] toJsonObject];
    _fabs       =   [dataDic[@"fab"] toJsonObject];
    
    NSString *role =    [NSString stringWithFormat:@"(%@)",dataDic[@"role"]];
    NSString *name =    dataDic[@"name"];
    
    [self setUserName:name role:role];
    
    _deleteEnable = [_uid isEqualToString:user.userId];
    
    NSArray *attch =   [dataDic[@"attach"] toJsonObject];
    NSMutableArray *array = [NSMutableArray array];
    [attch enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WebImage *webImage = [[WebImage alloc] init];
        webImage.urlString = obj[@"url"];
        webImage.thumbUrlString = obj[@"thumbUrl"];
        [array addObject:webImage];
    }];
    _attch = array;
    
    
    if (_editEnable) {
        [_fabs enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj[@"userId"] isEqualToString:user.userId]) {
                _isFabed = YES;
                *stop = YES;
            }
        }];
    }

    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES];
    if (_fabs.count > 0) {
        _fabs = [_fabs sortedArrayUsingDescriptors:@[descriptor]];
    }
    if (_remark.count > 0) {
         _remark = [ _remark sortedArrayUsingDescriptors:@[descriptor] ];
    }

    NSMutableString *leader = [NSMutableString string];
    NSString *charge = dataDic[@"charge"];
    if (charge.length > 0) {
        [leader appendFormat:@"主管: %@       ",charge];
    }
    NSString *manager = dataDic[@"manager"];
    if (manager.length > 0) {
        [leader appendFormat:@"经理: %@",manager];
    }
    _leader = leader;

    
    NSTimeInterval timeSp = [dataDic[@"time"] doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeSp];
    _date = [date string];
}


- (void)setUploaded:(BOOL)uploaded{
    _uploaded = uploaded;
    if (!uploaded) {
        WEAKSELF
        [[NSNotificationCenter defaultCenter] addObserverForName:@"com.ezl.momentDidSendSucess" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            if (!weakSelf) {
                return ;
            }
            NSString *momentId = note.object;
            if ([momentId isEqualToString:weakSelf.momentId]) {
                weakSelf.uploaded = YES;
                [[NSNotificationCenter defaultCenter] removeObserver:self];
            }
        }];
    }
    
}

- (void)setUserName:(NSString *)name role:(NSString *)role{
    
    UIFont *font = [MomentCellFrame nameFont];
    NSAttributedString *roleAttribute = [[NSAttributedString alloc] initWithString:role attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor blackColor]}];
    NSAttributedString *nameAttribute = [[NSAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName : font , NSForegroundColorAttributeName : [UIColor colorWithRed:60.0/255.0 green:104.0/255.0 blue:161.0/255.0 alpha:1]}];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithAttributedString:nameAttribute];
    [attributeStr appendAttributedString:roleAttribute];
    _nameAttribute = attributeStr;
    _name = [NSString stringWithFormat:@"%@%@",name,role];
}

- (void)addSendingResultObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(momentDidSendSucess:) name:EzlMomentDidSendSucess object:nil];
}

- (void)momentDidSendSucess:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
    if (!dic) {
        return;
    }
    if ([dic[@"timeSp"] isEqualToString:_momentId]) {
        _momentId = dic[@"momentId"];
        _uploaded = YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:EzlMomentDidSendSucess object:nil];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
