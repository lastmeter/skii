//
//  MomentsCell.m
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentCell.h"
#import "MomentPortrait.h"
#import "RemarksView.h"
#import "FabsView.h"
#import "ImageShowCell.h"
#import "WebImage.h"
#import "WebImageBrowser.h"
#import "MomentShare.h"
#import "LrhAlertView.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"
#import "UIImage+Scale.h"

@interface MomentCell ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, RemarksViewDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (assign, nonatomic) BOOL loadData;
@end

@implementation MomentCell{
    
    MomentPortrait *_head;
    RemarksView *_remarks;
    FabsView *_fabs;
    UIButton *_remarkBtn, *_fabBtn, *_nameBtn;

    UILabel *_leader, *_date;
    UILabel *_content;
    
    UIButton *_deleteBtn, *_showAllBtn;
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.loadData = YES;
        
        _head = [[MomentPortrait alloc] init];
        [self.contentView addSubview:_head];
        
        _nameBtn = [[UIButton alloc] init];
        _nameBtn.titleLabel.font = [MomentCellFrame nameFont];
        _nameBtn.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        [_nameBtn addTarget:self action:@selector(nameBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_nameBtn];
        
        _leader = [[UILabel alloc] init];
        _leader.textColor = [UIColor grayColor];
        _leader.font = [MomentCellFrame leaderFont];
        [self.contentView addSubview:_leader];
        
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[ImageShowCell class] forCellWithReuseIdentifier:@"photoCell"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
        
        _content = [[UILabel alloc] init];
        _content.font = [MomentCellFrame contentFont];
        _content.numberOfLines = 0;
        [self.contentView addSubview:_content];
        
        _fabs = [[FabsView alloc] init];
        [self.contentView addSubview:_fabs];
        
        _remarks = [[RemarksView alloc] init];
        _remarks.delegate = self;
        [self.contentView addSubview:_remarks];

        _date = [[UILabel alloc] init];
        _date.font = [UIFont systemFontOfSize:16];
        _date.textColor = [UIColor grayColor];
        [self.contentView addSubview:_date];
        
        _deleteBtn = [[UIButton alloc] init];
        [self.contentView addSubview:_deleteBtn];
        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(deleteMoment:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setEditEnable:(BOOL)editEnable{
    _editEnable = editEnable;
    if (editEnable) {
        if (!_fabBtn) {
            _fabBtn = [[UIButton alloc] init];
            [_fabBtn addTarget:self action:@selector(addFab:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:_fabBtn];
        }
        if (!_remarkBtn) {
            _remarkBtn = [[UIButton alloc] init];
            [_remarkBtn setImage:[UIImage imageNamed:@"remark"] forState:UIControlStateNormal];
            [_remarkBtn addTarget:self action:@selector(addRemark:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:_remarkBtn];
        }
    }

}


- (void)setCellFrame:(MomentCellFrame *)cellFrame{
    _cellFrame = cellFrame;
     [_collectionView reloadData];
    _collectionView.frame = cellFrame.collectionFrame;
   
    _head.frame         =   cellFrame.headFrame;
    _nameBtn.frame      =   cellFrame.nameFrame;
    _leader.frame       =   cellFrame.leaderFrame;
    _content.frame      =   cellFrame.contentFrame;
    _fabs.frame         =   cellFrame.fabsFrame;
    _remarks.frame      =   cellFrame.remarksFrame;
    _date.frame         =   cellFrame.dateFrame;
   
    
    if (cellFrame.isLongContent) {
        if (!_showAllBtn) {
            _showAllBtn = [[UIButton alloc] init];
            _showAllBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [self.contentView addSubview:_showAllBtn];
            _showAllBtn.titleLabel.font = [UIFont systemFontOfSize:16];
            [_showAllBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [_showAllBtn addTarget:self action:@selector(showAllContent:) forControlEvents:UIControlEventTouchUpInside];
        }
        _showAllBtn.frame   =   cellFrame.showAllBtnFrame;
        _showAllBtn.hidden  =   NO;
        
        [_showAllBtn setTitle:cellFrame.showAll ? @"收起" : @"全文" forState:UIControlStateNormal];
    }
    else if (_showAllBtn){
        _showAllBtn.hidden = YES;
    }
    
    MomentModel * moment = cellFrame.moment;
    
    _head.uid       =   moment.uid;
    _head.eid       =   moment.eid;
    _head.name      =   moment.name;
    [_head fetchHeadPortrait];
  
    _leader.text    =   moment.leader;
    _content.text   =   moment.content;
    _date.text      =   moment.date;
    [_nameBtn setAttributedTitle:moment.nameAttribute forState:UIControlStateNormal];
    
    _remarks.remarksArray = cellFrame.remarkModels;
    _fabs.users = cellFrame.fabUsers;
    
    if (_editEnable) {
        _fabBtn.frame       =   cellFrame.fabBtnFrame;
        _remarkBtn.frame    =   cellFrame.remarkBtnFrame;
        
        if (moment.isFabed) {
            [_fabBtn setImage:[UIImage imageNamed:@"fabed"] forState:UIControlStateNormal];
        }
        else{
            [_fabBtn setImage:[UIImage imageNamed:@"nofab"] forState:UIControlStateNormal];
        }
    }
    
    if (moment.deleteEnable) {
        _deleteBtn.hidden = NO;
        _deleteBtn.frame = cellFrame.deleteBtnFrame;
    }
    else{
        _deleteBtn.hidden = YES;
    }

    
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.cellFrame.moment.attch.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageShowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    cell.needScale = !(self.cellFrame.moment.attch.count == 1);
    cell.imageView.hidden = self.cellFrame.moment.attch.count == 1;
    
    WebImage *webImage = self.cellFrame.moment.attch[indexPath.row];
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[webImage.urlString lastPathComponent]];
    if (image) {
        cell.imageView.image = image;
        if (self.cellFrame.moment.attch.count == 1) {
            
            if ( !self.cellFrame.loadImageFinish ) {
                [self fitToImageSize:image.size];
            }
            else{
                cell.imageView.hidden = NO;
            }
        }
    }
    else{
        WEAKSELF
        __weak ImageShowCell *weakCell = cell;
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:webImage.thumbUrlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (weakSelf.cellFrame.moment.attch.count == 1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ( !weakSelf.cellFrame.loadImageFinish ) {
                        [weakSelf fitToImageSize:image.size];
                    }
                    else{
                        weakCell.imageView.hidden = NO;
                    }
                });
               
            }
            else{
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
                UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, weakSelf.cellFrame.colletionItemW, weakSelf.cellFrame.colletionItemH)];
                view2.center = view.center;
                weakCell.imageView.image = [image subImageForRect:view2.frame];
            }
        }];
    }
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(_cellFrame.colletionItemW, _cellFrame.colletionItemH);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return _cellFrame.colletionItemPad;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return _cellFrame.colletionItemPad;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *attach = self.cellFrame.moment.attch;
    
    UITableView *tableView = (UITableView *)self.superview.superview ;
    CGPoint offset = tableView.contentOffset;
    
    [attach enumerateObjectsUsingBlock:^(WebImage *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ImageShowCell *cell = (ImageShowCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
        CGRect frame = [self convertRect:cell.realImageFrame fromView:_collectionView];
        frame.origin.y = frame.origin.y + CGRectGetMinY(self.frame) - offset.y + 64;
        obj.originFrame = frame;
        obj.image = cell.imageView.image;
    }];


    WebImageBrowser *imageBrowser = [[WebImageBrowser alloc] init];
    [imageBrowser showImageWithWebImages:attach startIndex:indexPath.row];
}

#pragma mark-
- (void)addRemark:(UIButton *)btn{
    if (!_cellFrame.moment.uploaded) { return; }
    CGFloat y = CGRectGetMaxY(btn.frame) + CGRectGetMinY(self.frame);
    [_delegate addRemarkToMomentId:_cellFrame.moment.momentId maxY:y cellFrame:self.cellFrame];
}

- (void)addFab:(UIButton *)btn{
    if (!_editEnable) { return; }
    if (!_cellFrame.moment.uploaded) { return; }
    if (_cellFrame.moment.isFabed)   { return; }
    [_delegate addFabToMomentId:_cellFrame.moment.momentId cellFrame:self.cellFrame];
}

- (void)nameBtnClick:(UIButton *)btn{
    NSDictionary *dic = @{ @"uid" : _cellFrame.moment.uid, @"name" : _cellFrame.moment.name };
    [[NSNotificationCenter defaultCenter] postNotificationName:EzlReadMomentsOfUser object:dic];
}

- (void)deleteMoment:(UIButton *)btn{
    if (!_cellFrame.moment.uploaded) { return; }
    if (!_editEnable) { return; }
    LrhAlertAction *action1 = [LrhAlertAction actionWithTitle:@"确定" handler:^{
         [_delegate deleteMomentId:_cellFrame.moment.momentId cellFrame:self.cellFrame];
    }];
    LrhAlertAction *action2 = [LrhAlertAction actionWithTitle:@"取消" handler:nil];
    [LrhAlertView showMessage:@"确定删除吗？" actions:@[action1, action2]];
}

- (void)showAllContent:(UIButton *)btn{
    if (_cellFrame.isLongContent) {
        _cellFrame.showAll = !_cellFrame.showAll;
        _cellFrame.moment = _cellFrame.moment;
        [_showAllBtn setTitle:_cellFrame.showAll ? @"取消" : @"全文" forState:UIControlStateNormal];
        UITableView *tableView = (UITableView *)self.superview.superview;
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)addReplyToRemark:(RemarkModel *)remark convertRect:(CGRect)rect{
    if (!_cellFrame.moment.uploaded) { return; }
    if (!_editEnable) { return; }
    
    CGRect converRect = [self convertRect:rect fromView:_remarks];
    CGFloat y = CGRectGetMaxY(converRect) + CGRectGetMinY(self.frame);
    NSString *replyId = remark.userId;
    NSString *replyName = remark.userName;
    NSString *momentId = self.cellFrame.moment.momentId;
    [_delegate addReplyToMomentId:momentId replyId:replyId replyName:replyName maxY:y cellFrame:_cellFrame];
}

#pragma mark-
- (void)fitToImageSize:(CGSize)imageSize{
    CGSize newSize = [self scaleSizeFormImageSize:imageSize];
    self.cellFrame.loadImageFinish = YES;
    [self.cellFrame fitToCollectionSize:newSize];
    UITableView *tableView = (UITableView *)self.superview.superview;
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:self.tag] withRowAnimation:UITableViewRowAnimationNone];
}

- (CGSize)scaleSizeFormImageSize:(CGSize)imageSize{
    CGFloat xScale = self.cellFrame.colletionItemW / imageSize.width;
    CGFloat yScale = self.cellFrame.colletionItemH / imageSize.height;

    CGFloat minScale = MIN(xScale, yScale);
    
    CGFloat maxScale = 2.0 / [[UIScreen mainScreen] scale];
    if (minScale > maxScale) {
        minScale = maxScale;
    }
    return CGSizeMake(imageSize.width * minScale, imageSize.height * minScale);
}

- (void)dealloc{
    DLOG(@"CELL dealloc");
}

@end
