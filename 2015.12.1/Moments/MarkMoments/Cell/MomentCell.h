//
//  MomentsCell.h
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MomentCellFrame.h"

@protocol MomentCellDelegate <NSObject>

- (void)addRemarkToMomentId:(NSString *)momentId maxY:(CGFloat)maxY cellFrame:(MomentCellFrame *)cellFrame;
- (void)addFabToMomentId:(NSString *)momentId cellFrame:(MomentCellFrame *)cellFrame;
- (void)addReplyToMomentId:(NSString *)momentId replyId:(NSString *)replyId replyName:(NSString *)replyName maxY:(CGFloat)maxY cellFrame:(MomentCellFrame *)cellFrame;
- (void)deleteMomentId:(NSString *)momentId cellFrame:(MomentCellFrame *)cellFrame;
@end

@interface MomentCell : UITableViewCell

@property (nonatomic, weak) MomentCellFrame *cellFrame;
@property (nonatomic, assign) BOOL editEnable;
@property (nonatomic, assign) BOOL loadImage;
@property (nonatomic, weak) id<MomentCellDelegate>delegate;

@end
