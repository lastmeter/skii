//
//  MomentsCellFrame.h
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MomentModel.h"

@interface MomentCellFrame : NSObject

@property(nonatomic, strong) MomentModel *moment;

@property(nonatomic, readonly) CGRect headFrame;
@property(nonatomic, readonly) CGRect nameFrame;
@property(nonatomic, readonly) CGRect leaderFrame;
@property(nonatomic, readonly) CGRect contentFrame;

@property(nonatomic, readonly) CGRect dateFrame;
@property(nonatomic, readonly) CGRect deleteBtnFrame;

@property(nonatomic, readonly) CGRect remarkBtnFrame;
@property(nonatomic, readonly) CGRect fabBtnFrame;
@property(nonatomic, readonly) CGRect fabsFrame;
@property(nonatomic, readonly) CGRect remarksFrame;
@property(nonatomic, readonly) CGFloat cellHeight;

@property(nonatomic, readonly) CGRect collectionFrame;
@property(nonatomic, readonly) CGFloat colletionItemPad;
@property(nonatomic, readonly) CGFloat colletionItemW;
@property(nonatomic, readonly) CGFloat colletionItemH;


@property(nonatomic, readonly) NSArray *remarkModels;
@property(nonatomic, readonly) NSArray *fabUsers;

@property(nonatomic, assign) BOOL showAll;
@property(nonatomic, readonly) BOOL isLongContent;
@property(nonatomic, readonly) CGRect showAllBtnFrame;
@property(nonatomic, assign) BOOL loadImageFinish;

+ (UIFont *)nameFont;
+ (UIFont *)leaderFont;
+ (UIFont *)contentFont;

- (void)fitToCollectionSize:(CGSize)collectionSize;
- (void)resetRemarks:(NSArray *)remarks;
- (void)resetFabs:(NSArray *)fabs;
@end
