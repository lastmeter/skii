//
//  MomentSendHelper.m
//  EZLearning
//
//  Created by xihan on 16/1/19.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "MomentSendHelper.h"
#import "EzlUser.h"
#import "MomentsService.h"
#import "MomentShare.h"
#import "MomentModel.h"
#import "WebImage.h"
#import "MomentCellFrame.h"
#import "SDImageCache.h"
#import "NSDate+String.h"

#import "PMomentCellFrame.h"
@implementation MomentSendHelper

+ (NSString *)p_key{
    return [NSString stringWithFormat:@"%@_uploadMoments",[EzlUser currentUser].eid];
}

+ (void)continueSendMoments{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [self p_key];
    NSDictionary *moments = [defaults objectForKey:key];
    WEAKSELF
    [moments enumerateKeysAndObjectsUsingBlock:^(NSString *  _Nonnull key, NSDictionary *  _Nonnull obj, BOOL * _Nonnull stop) {
        [weakSelf p_SendMomentWithContent:obj[@"content"] attach:obj[@"attach"] timeSp:key];
    }];
    
}

+ (void)sendMomentWithContent:(NSString *)content
                       attach:(NSArray *)attach
                       timeSp:(NSString *)timeSp{
    
    [self p_SaveUploadingMomentWithContent:content attach:attach timeSp:timeSp];
    [self p_SendMomentWithContent:content attach:attach timeSp:timeSp];
}

+ (void)p_SendMomentWithContent:(NSString *)content
                         attach:(NSArray *)attach
                         timeSp:(NSString *)timeSp{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSMutableArray *array = [NSMutableArray array];
        [attach enumerateObjectsUsingBlock:^(NSData *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *imgStr = [obj base64EncodedStringWithOptions:0];
            NSDictionary *dic = @{ @"base64" : imgStr, @"mime" : @"image/png" };
            [array addObject:dic];
        }];
        NSString *jsonStr = @"[]";
        if (array.count > 0) {
            NSData *jsonData  = [NSJSONSerialization dataWithJSONObject:array options:0 error:nil];
            jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            if (!jsonStr) {
                jsonStr = @"[]";
            }
        }
        [self p_SendMomentUntilSucessWithContent:content attach:jsonStr timeSp:timeSp];
    });
}

+ (void)p_SendMomentUntilSucessWithContent:(NSString *)content
                                    attach:(NSString *)attach
                                    timeSp:(NSString *)timeSp{
    WEAKSELF
    [MomentsService sendMomentsMessage:content attach:attach handler:^(HandleResult result, NSString *dataStr) {
        if (result == HandleResultSuccess && dataStr.length > 0) {
            [weakSelf p_MomentDidSendSucess:timeSp momentId:dataStr];
        }
        else{
            [weakSelf p_SendMomentUntilSucessWithContent:content attach:attach timeSp:timeSp];
        }
    }];
}

+ (void)p_MomentDidSendSucess:(NSString *)timeSp momentId:(NSString *)momentId{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [self p_key];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[defaults objectForKey:key]];
    [dic removeObjectForKey:timeSp];
    [defaults setObject:dic forKey:key];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:EzlMomentDidSendSucess object:@{ @"timeSp" : timeSp, @"momentId" : momentId }];
    NSLog(@"p_MomentDidSendSucess");
}

+ (void)p_SaveUploadingMomentWithContent:(NSString *)content
                                  attach:(NSArray *)attach
                                  timeSp:(NSString *)timeSp{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [self p_key];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:[defaults objectForKey:key]];
    dic[timeSp] = @{ @"content" : content, @"attach" : attach };
    [defaults setObject:dic forKey:key];
    [defaults synchronize];
}

+ (NSArray *)getUploadingMoment{
    NSMutableArray *array = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [self p_key];
    NSDictionary *moments = [defaults objectForKey:key];
    
    [moments enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSArray *attach = obj[@"attach"];
        NSMutableArray *images = [NSMutableArray array];
        [attach enumerateObjectsUsingBlock:^(NSData *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WebImage *webImage = [[WebImage alloc] init];
            NSTimeInterval timeSp = [[NSDate date] timeIntervalSince1970];
            webImage.urlString = [@(timeSp) stringValue];
            [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:obj] forKey:[@(timeSp) stringValue]];
            [images addObject:webImage];
        }];
        
        MomentModel *moment = [[MomentModel alloc] init];
        moment.content = obj[@"content"];
        moment.attch = images;
        
        EzlUser *user = [EzlUser currentUser];
        moment.momentId = key;
        [moment setUserName:user.name role:user.role];
        
        moment.eid = user.eid;
        moment.date = [[NSDate date] string];
        moment.uid = user.userId;
        moment.isFabed = NO;
        moment.editEnable = YES;
        moment.deleteEnable = YES;
        moment.uploaded = NO;
        [moment addSendingResultObserver];
        
        NSMutableString *leader = [NSMutableString string];
        if (user.charge.length > 0) {
            [leader appendFormat:@"主管: %@       ",user.charge];
        }
        if (user.manager.length > 0) {
            [leader appendFormat:@"经理: %@",user.manager];
        }
        moment.leader = leader;
        
        MomentCellFrame *cellFrame = [[MomentCellFrame alloc] init];
        cellFrame.moment = moment;
        [array addObject:cellFrame];
    }];
    return array;
}


+ (NSArray *)getUploadingPMoment{
    NSMutableArray *array = [NSMutableArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *key = [self p_key];
    NSDictionary *moments = [defaults objectForKey:key];
    
    [moments enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSArray *attach = obj[@"attach"];
        NSMutableArray *images = [NSMutableArray array];
        [attach enumerateObjectsUsingBlock:^(NSData *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            WebImage *webImage = [[WebImage alloc] init];
            NSTimeInterval timeSp = [[NSDate date] timeIntervalSince1970];
            webImage.urlString = [@(timeSp) stringValue];
            [[SDImageCache sharedImageCache] storeImage:[UIImage imageWithData:obj] forKey:[@(timeSp) stringValue]];
            [images addObject:webImage];
        }];
        
        PMomentModel *moment = [[PMomentModel alloc] init];
        moment.content = obj[@"content"];
        moment.attch = images;
        
        EzlUser *user = [EzlUser currentUser];
        moment.momentId = key;
        moment.date = [[NSDate date] string];
        moment.uid = user.userId;
        moment.isFabed = NO;
        moment.editEnable = YES;
        moment.deleteEnable = YES;
        moment.uploaded = NO;
        [moment addSendingResultObserver];
        
        NSMutableString *leader = [NSMutableString string];
        if (user.charge.length > 0) {
            [leader appendFormat:@"主管: %@       ",user.charge];
        }
        if (user.manager.length > 0) {
            [leader appendFormat:@"经理: %@",user.manager];
        }

        PMomentCellFrame *cellFrame = [[PMomentCellFrame alloc] init];
        cellFrame.pMoment = moment;
        [array addObject:cellFrame];
    }];
    return array;
}




@end
