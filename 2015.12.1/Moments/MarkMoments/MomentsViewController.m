//
//  MomentsViewController.m
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentsViewController.h"
#import "MomentsService.h"
#import "MomentCell.h"
#import "PMomentsViewController.h"
#import "MomentShare.h"
#import "RemarkInputBar.h"
#import "MomentEditController.h"
#import "ImageBrowser.h"
#import "RemarkModel.h"
#import "FabUserModel.h"
#import "NSDate+String.h"
#import "MomentModel.h"
#import "EzlUser.h"
#import "LoadingFootView.h"
#import "SDWebImageManager.h"
#import "MomentSendHelper.h"

@interface MomentsViewController ()<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, RemarkInputBarDelegate, MomentCellDelegate, MomentEditDelegate>
{

    RemarkInputBar *_inputBar;
    NSLayoutConstraint *_heightConstraint;
    NSLayoutConstraint *_bottomConstraint;
    CGFloat _offsetY;
    NSInteger _currentPage;
    BOOL _noMoreMoments, _isLoading;
    BOOL _loadImage;
}

@property (nonatomic, strong) NSMutableArray *moments;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MomentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _currentPage = 0;
    _noMoreMoments = NO;
    
    [self p_InitTableView];
    [self p_InitInputBar];

    [self p_LoadData];
    [self p_AddBackgroundTap];
    
    if (_editEnable) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addMoments)];
    }

    _loadImage = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self p_AddObserver];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_inputBar endEdit];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        [_tableView reloadData];
}
- (void)addMoments{
    
    MomentEditController *momentEditController = [[MomentEditController alloc] init];
    momentEditController.delegate = self;
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"新的信息";
    self.navigationItem.backBarButtonItem = backItem;
    [self.navigationController pushViewController:momentEditController animated:YES];
}

- (void)didSendMoment:(NSString *)momentId content:(NSString *)content attch:(NSArray *)attch{

    
    EzlUser *user = [EzlUser currentUser];
    
    MomentModel *moment = [[MomentModel alloc] init];
    moment.content = content;
    moment.momentId = momentId;
    moment.attch = attch;
    
    [moment setUserName:user.name role:user.role];
    moment.eid = user.eid;
    moment.date = [[NSDate date] string];
    moment.uid = user.userId;
    moment.isFabed = NO;
    moment.editEnable = YES;
    moment.deleteEnable = YES;
    moment.uploaded = NO;
    [moment addSendingResultObserver];
    
    NSMutableString *leader = [NSMutableString string];
    if (user.charge.length > 0) {
        [leader appendFormat:@"主管: %@       ",user.charge];
    }
    if (user.manager.length > 0) {
        [leader appendFormat:@"经理: %@",user.manager];
    }
    moment.leader = leader;
    
    MomentCellFrame *cellFrame = [[MomentCellFrame alloc] init];
    cellFrame.moment = moment;
    [self.moments insertObject:cellFrame atIndex:0];
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];



}

#pragma mark-
- (void)p_LoadData{
    
    if (_noMoreMoments) { return; }
    _isLoading = YES;
  
    
    if (_currentPage == 0) { [self waitting];  }
    else if (!_tableView.tableFooterView) {
        _tableView.tableFooterView = [[LoadingFootView alloc] init];
    }
    
    if (!_moments) {
        if (_editEnable) {
            _moments = [NSMutableArray arrayWithArray:[MomentSendHelper getUploadingMoment]];
        }
        else{
            _moments = [NSMutableArray array];
        }
    }
    
    __weak MomentsViewController *weakSelf = self;
    [MomentsService getMomentsBySectorId:_sectorId pageNo:_currentPage pageSize:NumberOfMomentsOnePage handler:^(HandleResult result, NSArray *dataArray) {
        
        if (!weakSelf) { return ; }
        [weakSelf stopWaitting];
        
        _isLoading = NO;
        if (result != HandleResultSuccess) { _tableView.tableFooterView = nil; return ; }
        _currentPage += 1;
        if (dataArray.count < NumberOfMomentsOnePage) {
            _noMoreMoments = YES;
            _tableView.tableFooterView = nil;
        }
        if (dataArray.count == 0) { return; }
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO];
        NSArray *sortArray = [dataArray sortedArrayUsingDescriptors:@[descriptor]];
        

        [sortArray enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MomentModel * model = [[MomentModel alloc] init];
            model.editEnable = self.editEnable;
            model.dataDic = obj;
            model.uploaded = YES;
            MomentCellFrame * cellFrame = [[MomentCellFrame alloc] init];
            cellFrame.moment = model;
            [_moments addObject:cellFrame];
        }];
     
        [_tableView reloadData];

    }];
}

#pragma mark- Input Bar
- (void)p_InitInputBar{
    if (!_editEnable) { return; }
    _inputBar = [[RemarkInputBar alloc] init];
    _inputBar.delegate = self;
    [self.view addSubview:_inputBar];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[inputBar]|" options:0 metrics:nil views:@{@"inputBar" : _inputBar}]];
    
    _heightConstraint = [NSLayoutConstraint constraintWithItem:_inputBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:[_inputBar barHeight]];
    [self.view addConstraint:_heightConstraint];
    
    _bottomConstraint = [NSLayoutConstraint constraintWithItem:_inputBar attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:[_inputBar barHeight]];
    [self.view addConstraint:_bottomConstraint];

}

- (void)InputBar_BarHeightChange:(CGFloat)originHeight newHeight:(CGFloat)newHeight{
    _heightConstraint.constant = newHeight;
    CGFloat addY = newHeight - originHeight;
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.25 animations:^{
        [_tableView setContentOffset:CGPointMake(0, _tableView.contentOffset.y + addY) animated:NO];
        [self.view layoutIfNeeded];
    }];
}

#pragma mark- Table View
- (void)p_InitTableView {
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
     _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:@{@"tableView" : _tableView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|" options:0 metrics:nil views:@{@"tableView" : _tableView}]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _moments.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentCellFrame *cellFrame = _moments[indexPath.section];
    return cellFrame.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MomentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"moment"];
    if ( cell == nil ) {
        cell = [[MomentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moment"];
        if (_editEnable) {
            cell.delegate = self;
            cell.editEnable = YES;
        }
    }
    cell.loadImage = _loadImage;
    cell.tag = indexPath.section;
    cell.cellFrame = _moments[indexPath.section];
    return cell;
}

#pragma mark-
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _loadImage = NO;
//    [[SDWebImageManager sharedManager] cancelAll];
//    [[SDImageCache sharedImageCache] clearMemory];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (_isLoading || _noMoreMoments) { return; }
    if ((_tableView.contentSize.height - _tableView.contentOffset.y) < 2 * self.view.frame.size.height ) {
        [self p_LoadData];
    }

}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if (_isLoading || _noMoreMoments) { return; }
    if ((_tableView.contentSize.height - _tableView.contentOffset.y) < 1.5 * self.view.frame.size.height ) {
        [self p_LoadData];
    }

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (_isLoading || _noMoreMoments) { return; }
    if ((_tableView.contentSize.height - _tableView.contentOffset.y) < 1.5 * self.view.frame.size.height ) {
        [self p_LoadData];
    }
}


#pragma mark- Moment Cell
- (void)addReplyToMomentId:(NSString *)momentId
                   replyId:(NSString *)replyId
                 replyName:(NSString *)replyName
                      maxY:(CGFloat)maxY
                 cellFrame:(MomentCellFrame *)cellFrame{
 
    _offsetY = maxY + 5;
    _inputBar.placeholder = [NSString stringWithFormat:@"回复 %@:",replyName];
    [_inputBar beginEditWithSendHandler:^(NSString *text) {
      
        [MomentsService addReplyToMomentsId:momentId replyUserId:replyId content:text handler:^(HandleResult result, BOOL dataResult) {
            if (!cellFrame) { [_inputBar endEdit]; return ; }
            
            if (!dataResult) {
                [LrhAlertView showMessage:@"回复失败"];
                [_inputBar endEdit];
                return ;
            }
            EzlUser *user = [EzlUser currentUser];
            MomentModel *moment = cellFrame.moment;
            NSDictionary *dic =
            @{
              @"name"          :   user.name,
              @"content"       :   text,
              @"userId"        :   user.userId,
              @"replyUserId"   :   replyId,
              @"replyName"     :   replyName
            };
            
            NSMutableArray *array = [NSMutableArray arrayWithArray:moment.remark];
            [array addObject:dic];
            moment.remark = array;
            cellFrame.moment = moment;
            cellFrame.loadImageFinish = NO;
            [_tableView reloadData];
            [_inputBar endEdit];
        }];
    }];
    
}

- (void)addRemarkToMomentId:(NSString *)momentId
                       maxY:(CGFloat)maxY
                  cellFrame:(MomentCellFrame *)cellFrame{
    
    _offsetY = maxY + 5;
    _inputBar.placeholder = @"评论";
    [_inputBar beginEditWithSendHandler:^(NSString *text) {
     
        [MomentsService addRemarkToMomentsId:momentId content:text handler:^(HandleResult result, BOOL dataResult) {
            if (!cellFrame) { [_inputBar endEdit];  return ; }
            
            
            if (!dataResult) {
                [LrhAlertView showMessage:@"评论失败"];
                [_inputBar endEdit];
                return ;
            }
            EzlUser *user = [EzlUser currentUser];
            NSDictionary *dic = @{ @"name" : user.name, @"userId" : user.userId, @"content" :text, };
            MomentModel *moment = cellFrame.moment;
            NSMutableArray *array = [NSMutableArray arrayWithArray:moment.remark];
            [array addObject:dic];
            [cellFrame resetRemarks:array];
            [_tableView reloadData];
            [_inputBar endEdit];
        }];
    }];
}

- (void)addFabToMomentId:(NSString *)momentId cellFrame:(MomentCellFrame *)cellFrame{

    [MomentsService addFabToMomentsId:momentId handler:^(HandleResult result, NSString *dataStr) {
        if (!cellFrame) {  return ; }

        if (result != HandleResultSuccess || [dataStr isEqualToString:@"-1"]) {
            [LrhAlertView showMessage:@"点赞失败"];
        }
        else if ([dataStr isEqualToString:@"0"]) {
            EzlUser *user = [EzlUser currentUser];
            NSDictionary *dic = @{ @"name" : user.name, @"userId" : user.userId };
            MomentModel *moment = cellFrame.moment;
            NSMutableArray *array = [NSMutableArray arrayWithArray:moment.fabs];
            [array addObject:dic];
            [cellFrame resetFabs:array];
            [_tableView reloadData];
        }
        else if ([dataStr isEqualToString:@"1"]){
            MomentModel *moment = cellFrame.moment;
            [cellFrame resetFabs:moment.fabs];
            [_tableView reloadData];
        }
       
    }];
}


- (void)deleteMomentId:(NSString *)momentId cellFrame:(MomentCellFrame *)cellFrame{
    
    [MomentsService deleteMomentsId:momentId handler:^(HandleResult result, BOOL dataResult) {
        if (!dataResult) { [LrhAlertView showMessage:@"删除失败"]; }
        else{
            [_moments removeObject:cellFrame];
            [_tableView reloadData];
        }
    }];
}



#pragma mark- Notification
- (void)p_AddObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(readUserMoments:) name:EzlReadMomentsOfUser object:nil];
    if (!_editEnable) {  return; }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


- (void)readUserMoments:(NSNotification *)noti{
    NSDictionary * user = noti.object;
    if (user == nil) { return; }
    PMomentsViewController *vc = [[PMomentsViewController alloc] init];
    vc.userId = user[@"uid"];
    vc.userName = user[@"name"];
    vc.editEnable = self.editEnable;
    dispatch_async(dispatch_get_main_queue(), ^{
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
        backItem.title = user[@"name"];
        self.navigationItem.backBarButtonItem = backItem;
        [self.navigationController pushViewController:vc animated:YES];
    });
}



#pragma mark- Keyboard
- (void)keyboardWillShow:(NSNotification *)noti{
    NSDictionary *userInfo = [noti userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    _bottomConstraint.constant = -keyboardEndFrame.size.height;
    
    CGFloat y = [UIScreen mainScreen].bounds.size.height - keyboardEndFrame.size.height - CGRectGetHeight(_inputBar.frame) - 64;
    CGFloat yy = _tableView.contentOffset.y + y;
    CGFloat addY =  _offsetY - yy;
    CGFloat offSetY = _tableView.contentOffset.y + addY;
    
    [self.view setNeedsUpdateConstraints];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
 
    [_tableView setContentOffset:CGPointMake(0, offSetY) animated:NO];

    _tableView.userInteractionEnabled = NO;
    [self.view layoutIfNeeded];
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)noti{
    NSDictionary *userInfo = [noti userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    _bottomConstraint.constant = _heightConstraint.constant;
    _tableView.userInteractionEnabled = YES;
    CGFloat MainScreenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [self.view setNeedsUpdateConstraints];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    [self.view layoutIfNeeded];

    if ( _tableView.contentSize.height  <= (MainScreenHeight + _tableView.contentOffset.y)) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:(_moments.count - 1)] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    else if (_tableView.contentOffset.y < 0){
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    [UIView commitAnimations];
}

#pragma mark - BackgroundTap
- (void)p_AddBackgroundTap{
    if (!_editEnable) {  return; }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.delegate = self;
    [tap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tap];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    return _inputBar.editing;
}

- (void)hideKeyboard{
    [_inputBar endEdit];
}

-(void)dealloc{
    NSLog(@"VC DEALLOC");
}



@end
