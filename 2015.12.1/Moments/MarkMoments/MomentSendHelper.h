//
//  MomentSendHelper.h
//  EZLearning
//
//  Created by xihan on 16/1/19.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MomentSendHelper : NSObject
+ (void)continueSendMoments;

+ (void)sendMomentWithContent:(NSString *)content
                       attach:(NSArray *)attach
                       timeSp:(NSString *)timeSp;

+ (NSArray *)getUploadingMoment;

+ (NSArray *)getUploadingPMoment;
@end
