//
//  MomentsViewController.h
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "NEzlViewController.h"

@interface MomentsViewController : NEzlViewController

@property (nonatomic, copy) NSString * sectorId;
@property (nonatomic, assign) BOOL editEnable;


@end
