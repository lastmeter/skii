//
//  EzlCurrentUser.h
//  EZLearning
//
//  Created by xihan on 15/12/18.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UserRole){
    UserRoleBC              =   3,
    UserRoleCharge          =   1,
    UserRoleManager         =   0,
    UserRoleUnknown         =   -1
};

@interface EzlUser : NSObject

+ (EzlUser *)currentUser;
- (void)loginIfNoActive;
- (void)getUserInfo;

@property (nonatomic, copy) NSString *eid;
@property (nonatomic, copy) NSString *pwd;
@property (nonatomic, copy) NSString *mail;
@property (nonatomic, copy) NSString *mobilePhone;


@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *charge;
@property (nonatomic, readonly) NSString *manager;
@property (nonatomic, readonly) NSString *credit;
@property (nonatomic, readonly) NSString *market;
@property (nonatomic, readonly) NSString *role;
@property (nonatomic, readonly) NSString *userId;
@property (nonatomic, readonly) NSString *city;

@property (nonatomic, assign) NSInteger numberOfUnreadMoment;
@property (nonatomic, assign, readonly) NSInteger numberOfUnreadStaff;
@property (nonatomic, retain) NSMutableDictionary *straffInfo;

@property (nonatomic, assign, readonly) NSInteger numberOfAllUnreadMessage;
@property (nonatomic, assign, readonly) UserRole userRole;

- (void)didGetStraffInfo:(void(^)(NSDictionary *dic))handler;
- (void)getUnreadMoment;
- (NSTimeInterval)getLastReadDateForMarket:(NSString *)marketId;
- (void)didReadMomentForMarket:(NSString *)marketId;
@end
