//
//  UserInfoViewController.m
//  EZLearning
//
//  Created by xihan on 16/1/5.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "UserInfoViewController.h"
#import "EzlUser.h"
#import "EzlWebImageManager.h"
#import "LrhAlertView.h"
#import "MLearningService.h"
#import "NSString+Size.h"

@interface UserInfoViewController ()<UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>
{
    UIButton *_iconBtn, *_sendBtn;
    UITableView *_tableView;
    UIImagePickerController *_picker;
    
    NSString *_mail, *_phone;
    
    BOOL _headPortraitChanged, _mailChanged, _mobileChanged;
    
    CGFloat _maxContentY;
}

@property (nonatomic, weak) EzlUser *currentUser;
@property (nonatomic, weak) UITextField *mobileTF;
@property (nonatomic, weak) UITextField *mailTF;
@property (nonatomic, strong) NSArray *tagArray;
@property (nonatomic, strong) NSArray *infoArray;
@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self p_InitNavigationBar];
    [self p_LoadData];
    [self p_InitBaseView];
    [self p_GetHeadPortrait];
    [self p_AddBackgroundTap];
    [self p_AddObserver];
 
}


- (void)p_LoadData{
    _currentUser = [EzlUser currentUser];
    _phone = _currentUser.mobilePhone;
    _mail = _currentUser.mail;
    switch (_currentUser.userRole) {
        case UserRoleBC:
        {
            _tagArray  = @[@"姓名：",@"市场：",@"城市：",@"主管：",@"积分：",@"电话：",@"邮箱："];
            _infoArray = @[_currentUser.name, _currentUser.market, _currentUser.city, _currentUser.charge, _currentUser.credit, _currentUser.mobilePhone, _currentUser.mail];
        }
            break;
        case UserRoleCharge:
        {
            _tagArray  = @[@"姓名：",@"市场：",@"城市：",@"经理：",@"积分：",@"电话：",@"邮箱："];
            _infoArray = @[_currentUser.name, _currentUser.market, _currentUser.city, _currentUser.manager, _currentUser.credit, _currentUser.mobilePhone, _currentUser.mail];
        }
            break;
        case UserRoleManager:
        {
            _tagArray  = @[@"姓名：",@"市场：",@"城市：",@"积分：",@"电话：",@"邮箱："];
            _infoArray = @[_currentUser.name, _currentUser.market, _currentUser.city, _currentUser.credit, _currentUser.mobilePhone, _currentUser.mail];
        }
            break;
        case UserRoleUnknown:
            
            break;
    }
}

- (void)p_InitNavigationBar{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0,0)];
    [btn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [btn setTitle:@"个人信息" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItems = @[backItem];
}

- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)p_InitBaseView{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(60, 0, self.view.bounds.size.width - 60, self.view.bounds.size.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.bounces = NO;
    [self.view addSubview:_tableView];
    

    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.bounds), 100)];
    _iconBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 80, 80)];
    _iconBtn.center = CGPointMake(CGRectGetMidX(headView.frame) - 30, CGRectGetMidY(_iconBtn.frame));
    [headView addSubview:_iconBtn];
    [_iconBtn addTarget:self action:@selector(changeHeadPortrait:) forControlEvents:UIControlEventTouchUpInside];
    _tableView.tableHeaderView = headView;
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_tableView.bounds), 50)];
    _sendBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 100, 40)];
    _sendBtn.center = CGPointMake(CGRectGetMidX(footView.frame) - 30, CGRectGetMidY(_sendBtn.frame));
    _sendBtn.titleLabel.font = [UIFont systemFontOfSize:20];
    [_sendBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sendBtn setBackgroundImage:[UIImage imageNamed:@"save_self_message"] forState:UIControlStateNormal];
    [footView addSubview:_sendBtn];
    _sendBtn.enabled = NO;
    [_sendBtn addTarget:self action:@selector(submitInfo:) forControlEvents:UIControlEventTouchUpInside];
    _tableView.tableFooterView = footView;
    
    CGRect converRect = [self.view convertRect:_sendBtn.frame fromView:footView];
    _maxContentY = CGRectGetMaxY(converRect);
}

- (void)p_GetHeadPortrait{
    [EzlWebImageManager fetchHeadPortraitByEid:_currentUser.eid uid:_currentUser.userId handler:^(UIImage *image, NSString *name) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (image) {
                [_iconBtn setImage:image forState:UIControlStateNormal];
            }
            else{
                [_iconBtn setImage:[UIImage imageNamed:@"article_photo_pressed"] forState:UIControlStateNormal];
            }
        });
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _tagArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSAttributedString *tagStr = [[NSAttributedString alloc] initWithString:_tagArray[indexPath.row] attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:20] , NSForegroundColorAttributeName : [UIColor blackColor]}];
       NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithAttributedString:tagStr];

    UITextField *textFiled = (UITextField *)[cell.contentView viewWithTag:99];
    if (!textFiled) {
        textFiled = [[UITextField alloc] initWithFrame:CGRectMake(75, 2, CGRectGetWidth(_tableView.bounds) - 100, 40)];
        textFiled.tag = 99 + indexPath.row;
        textFiled.delegate = self;
        textFiled.borderStyle = UITextBorderStyleRoundedRect;
        textFiled.font = [UIFont systemFontOfSize:20];
        textFiled.returnKeyType = UIReturnKeyDone;
        [cell.contentView addSubview:textFiled];
    }
    
    if (indexPath.row > _tagArray.count - 3) {
        if (indexPath.row == _tagArray.count - 1) {
            _mailTF = textFiled;
        }
        else{
            _mobileTF = textFiled;
        }
        textFiled.text = _infoArray[indexPath.row];
        textFiled.hidden = NO;
    }
    else{
        textFiled.hidden = YES;
        NSAttributedString *infoStr = [[NSAttributedString alloc] initWithString:_infoArray[indexPath.row] attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:20] , NSForegroundColorAttributeName : [UIColor blackColor]}];
        [content appendAttributedString:infoStr];
    }
    cell.textLabel.attributedText = content;
    
    if ([_tagArray[indexPath.row] isEqualToString:@"积分："]) {
        UIButton *btn = (UIButton *)[cell.contentView viewWithTag:438];
        if (!btn) {
            CGSize size = [content.string textSizeWithWidth:MAIN_SCREEN_WIDTH Font:[UIFont systemFontOfSize:20]];
            btn = [[UIButton alloc] initWithFrame:CGRectMake(size.width + 20, 2, 120, 44)];
            [btn setTitle:@"积分规则" forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btn setTitleColor:[UIColor colorWithRed:147.0/255.0 green:0 blue:0 alpha:1] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(getCreditRule:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btn];
            btn.tag = 438;
        }
    }
    return cell;
}

#pragma mark-
- (void)changeHeadPortrait:(UIButton *)btn{
    
    UIColor *color = [UIColor colorWithRed:18.0/255.0 green:183.0/255.0 blue:245.0/255.0 alpha:1];
    LrhAlertAction *action1 = [LrhAlertAction actionWithTitle:@"相册" titleColor:color handler:^{
        [self openPicLibrary];
    }];
    LrhAlertAction *action2 = [LrhAlertAction actionWithTitle:@"相机" titleColor:color handler:^{
        [self addCarema];
    }];
    LrhAlertAction *action3 = [LrhAlertAction actionWithTitle:@"取消" titleColor:color handler:^{
        
    }];
    [LrhAlertView showMessage:nil actions:@[action1, action2, action3]];
}

-(void)openPicLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        if (!_picker) {
            _picker = [[UIImagePickerController alloc] init];
        }
        _picker.allowsEditing = YES;
        _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:_picker animated:YES completion:^{ _picker.delegate = self; }];
    }
}

-(void)addCarema{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        if (!_picker) {
            _picker = [[UIImagePickerController alloc] init];
        }
        _picker.allowsEditing = YES;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        [self presentViewController:_picker animated:YES completion:^{ _picker.delegate = self;}];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *editImage  =   [info objectForKey:UIImagePickerControllerEditedImage];
    UIImage *cutImage   =   [self makeImage:editImage toScaleSize:CGSizeMake(80, 80)];
    [_iconBtn setImage:cutImage forState:UIControlStateNormal];
    [self dismissViewControllerAnimated:YES completion:nil];
    _sendBtn.enabled = YES;
    _headPortraitChanged = YES;

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark-

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == _mailTF){
        _mail = textField.text;
         NSString *str = _currentUser.mail;
        if (str.length == 0 && _phone.length == 0) {
            _mailChanged = NO;
        }
        else{
           _mailChanged = ![_mail isEqualToString:str];
        }
    }
    else{
        _phone = textField.text;
        NSString *str = _currentUser.mobilePhone;
        if (str.length == 0 && _phone.length == 0) {
            _mobileChanged = NO;
        }
        else{
           _mobileChanged = ![_phone isEqualToString:str];
        }
    }
    _sendBtn.enabled = _mailChanged || _mobileChanged;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)submitInfo:(UIButton *)btn{

    dispatch_group_t group = dispatch_group_create();
    __block BOOL modifyMailResult = !(_mobileChanged | _mailChanged);
    __block BOOL modifyHeadResult = !_headPortraitChanged;
    
    [self waitting];
    if (_mobileChanged | _mailChanged) {
        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_semaphore_t sem = dispatch_semaphore_create(0);
            [MLearningService changeEmail:_mail mobile:_phone handler:^(HandleResult result, BOOL dataResult) {
                modifyMailResult = dataResult;
                dispatch_semaphore_signal(sem);
            }];
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        });
    }
    if (_headPortraitChanged) {
        dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_semaphore_t sem = dispatch_semaphore_create(0);
            NSData *data = UIImageJPEGRepresentation(_iconBtn.imageView.image, 1);
            if (data.length > 500 * 1024) {
                data = UIImageJPEGRepresentation(_iconBtn.imageView.image, 500 * 1024 / data.length);
            }
            NSString *imageStr = [data base64EncodedStringWithOptions:0];
            NSString *imageName = [NSString stringWithFormat:@"%.f.jpg",[[NSDate date] timeIntervalSince1970] * 1000];
            [MLearningService setHeadPortraitWithStr:imageStr mimeType:@"image/jpeg" fileName:imageName handler:^(HandleResult result, BOOL dataResult) {
                modifyHeadResult = dataResult;
                _headPortraitChanged = !dataResult;
                dispatch_semaphore_signal(sem);
            }];
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        });
    }
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [self stopWaitting];
        if (modifyMailResult) {  
            _currentUser.mail = _mail;
            _currentUser.mobilePhone = _phone;
            _mailChanged = NO;
            _mobileChanged = NO;
        }
        if (modifyHeadResult) {
            [EzlWebImageManager addToCache:UIImageJPEGRepresentation(_iconBtn.imageView.image, 1) name:_currentUser.eid];
        }
        if (modifyHeadResult && modifyMailResult) {
            [LrhAlertView showMessage:@"保存成功"];
            _sendBtn.enabled = NO;
        }
        else{
            NSMutableString *failStr = [NSMutableString string];
            if (!modifyMailResult) {
                [failStr appendString:@"保存电话号码和邮箱失败!"];
            }
            if (!modifyHeadResult) {
                if (!modifyMailResult) {
                    [failStr appendString:@"\n"];
                }
                [failStr appendString:@"保存头像失败!"];
            }
            [LrhAlertView showMessage:failStr];
        }
    });
}


#pragma mark- Background Tap
- (void)p_AddBackgroundTap{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.delegate = self;
    [tap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tap];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UITextField class]]) {
        return NO;
    }
    [self hideKeyboard];
    return NO;
}

- (void)hideKeyboard{
    [_mobileTF resignFirstResponder];
    [_mailTF resignFirstResponder];
}

#pragma mark- Notification
- (void)p_AddObserver{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark- Keyboard
- (void)keyboardWillShow:(NSNotification *)noti{
    NSDictionary *userInfo = [noti userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    CGFloat y = [UIScreen mainScreen].bounds.size.height - keyboardEndFrame.size.height - 64;
    if (_maxContentY < y) { return; }
    
    CGFloat addY =  _maxContentY - y;
    CGFloat offSetY = _tableView.contentOffset.y + addY;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    [_tableView setContentOffset:CGPointMake(_tableView.contentOffset.x, offSetY) animated:NO];

    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)noti{
    NSDictionary *userInfo = [noti userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];

    [_tableView setContentOffset:CGPointMake(_tableView.contentOffset.x, 0) animated:NO];
    [UIView commitAnimations];
}


- (UIImage *)makeImage:(UIImage *)image toScaleSize:(CGSize )scaleSize{
    
    CGSize imageSize = image.size;
    
    CGFloat xScale = scaleSize.width  / imageSize.width;
    CGFloat yScale = scaleSize.height / imageSize.height;
    
    BOOL imagePortrait = imageSize.height > imageSize.width;
    BOOL phonePortrait = scaleSize.height > scaleSize.width;
    
    CGFloat scale = imagePortrait == phonePortrait ? xScale : MIN(xScale, yScale);
    if (scale >= 1)
    {
        return image;
    }
    else
    {
        CGSize newImageSize = CGSizeMake(imageSize.width * scale, imageSize.height * scale);
        UIGraphicsBeginImageContext(newImageSize);
        CGRect imageRect = CGRectMake(0.0, 0.0, newImageSize.width, newImageSize.height);
        [image drawInRect:imageRect];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
}

- (void)getCreditRule:(UIButton *)btn{
    [self waitting];
    [MLearningService getCreditRule:^(HandleResult result, NSString *dataStr) {
        [self stopWaitting];
        if ( result == HandleResultSuccess) {
            NSLog(@"%@",dataStr);
        }
    }];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
