//
//  XmlParser.m
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "XmlParser.h"

@interface XmlParser()< NSXMLParserDelegate >
{
    NSMutableString *_result;
}

@end

@implementation XmlParser


+ (NSString *)parserData:(NSData *)data{
    XmlParser *parser = [XmlParser new];
    return [parser parserData:data];
}

- (NSString *)parserData:(NSData *)data{
    
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    if (![parser parse]) {
        return  nil;
    }
    return  _result;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser{
    _result = [NSMutableString string];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    [_result appendString:string];
}


@end
