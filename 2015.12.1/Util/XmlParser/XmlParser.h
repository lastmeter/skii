//
//  XmlParser.h
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlParser : NSObject

+ (NSString *)parserData:(NSData *)data;

@end
