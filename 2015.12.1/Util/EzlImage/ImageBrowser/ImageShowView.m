//
//  ImageShowView.m
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ImageShowView.h"
#import "ImageZoomView.h"

@interface ImageShowView()

@property (nonatomic, strong) ImageZoomView *zoomView;
@property (nonatomic, strong) UIButton *tickBtn;

@end

@implementation ImageShowView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _zoomView = [[ImageZoomView alloc] initWithFrame:self.bounds];
        [self addSubview:_zoomView];
        
        UITapGestureRecognizer * singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
        [self addGestureRecognizer:singleRecognizer];
    }
    return self;
}

- (void)setImage:(UIImage *)image{
    _zoomView.image = image;
}

- (UIImage *)image{
    return _zoomView.image;
}

- (void)setTicked:(BOOL)ticked{
    _ticked = ticked;
    if (_tickBtn == nil) {
        _tickBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds) - 45, 10, 30, 30)];
        [_tickBtn addTarget:self action:@selector(tickImage:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_tickBtn];
    }
    [_tickBtn setImage:[UIImage imageNamed:ticked ? @"check_box_tick" : @"check_box_nor"] forState:UIControlStateNormal];
}

- (void)singleTap:(UIGestureRecognizer *)gesture{
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [_ezlDelegate imageDidTapped:self];
    }
}

- (void)tickImage:(UIButton *)btn{
    _ticked = !_ticked;
    [_tickBtn setImage:[UIImage imageNamed:_ticked ? @"check_box_tick" : @"check_box_nor"] forState:UIControlStateNormal];
    if (_ticked) {
        [_ezlDelegate imageDidSelect:self];
    }
    else{
        [_ezlDelegate imageDidDeselect:self];
    }
}

- (CGSize)currentSize{
    return _zoomView.currentSize;
}

- (CGSize)minimumSize{
    return _zoomView.minimumSize;
}

- (UIImage *)currentSizeImage{
    return _zoomView.currentSizeImage;
}

@end
