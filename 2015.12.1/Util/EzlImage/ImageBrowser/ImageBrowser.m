//
//  ImageBrowser.m
//  Moments
//
//  Created by xihan on 15/12/11.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ImageBrowser.h"
#import "ImageShowView.h"
#import "ShareAlertWindow.h"
#import "EzlPhoto.h"
#import "EzlPhotoManager.h"

@interface ImageBrowser()<EzlImageShowViewDelegate, UIScrollViewDelegate>
{
    UIScrollView *_scrolloView;
    NSMutableArray *_zoomViews;
    void(^_imageChangeHandler)();
}

@property(nonatomic, weak) EzlPhotoManager *photoManager;
@property(nonatomic, weak) ImageShowView *currentView;

@end


@implementation ImageBrowser


- (void)viewDidLoad{
    [super viewDidLoad];
    
   // self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    self.view.backgroundColor = [UIColor clearColor];
    
    _scrolloView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _scrolloView.pagingEnabled = YES;
    _scrolloView.delegate = self;
    _scrolloView.backgroundColor = [UIColor blackColor];
    _scrolloView.hidden = YES;
    [self.view addSubview:_scrolloView];
    
    self.photoManager = [EzlPhotoManager shareManager];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0;
    [self.view addSubview:bgView];

    EzlPhoto *photo = _images[_currentIndex];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:photo.originFrame];

    imageView.image = photo.fullScreenImage;
    [self.view addSubview:imageView];
    
    UIView *view = [[UIView alloc] init];
    view.frame = (CGRect){ CGPointZero, _currentView.currentSize };
    view.center = self.view.center;
    
    [UIView animateWithDuration:0.4 animations:^{
        imageView.frame = view.frame;
        bgView.alpha = 1;
    } completion:^(BOOL finished) {
        _scrolloView.hidden = NO;
        [imageView removeFromSuperview];
        [bgView removeFromSuperview];
    }];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _imageChangeHandler = nil;
}

- (void)imageDidTapped:(ImageShowView *)zoomView{
 
    EzlPhoto *photo = _images[_currentIndex];
    if (_images.count < 4) {
        photo = _images[zoomView.tag ];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:zoomView.currentSizeImage];
    imageView.center = self.view.center;
    imageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:imageView];
    
    [_scrolloView removeFromSuperview];
    
    [UIView animateWithDuration:0.4 animations:^{
        imageView.frame = photo.originFrame;
    } completion:^(BOOL finished) {
        [[ShareAlertWindow getInstance] dismissController];
    }];
}

- (void)imageDidDeselect:(ImageShowView *)view{
    
    EzlPhoto *photo = _images[_currentIndex];
    [self.photoManager removeFromSeleted:photo];

    if (_imageChangeHandler) {
        _imageChangeHandler();
    }
    if (_images.count == 1) {
        [UIView animateWithDuration:0.4 animations:^{
            self.view.alpha = 0;
        } completion:^(BOOL finished) {
            [[ShareAlertWindow getInstance] dismissController];
        }];
        return;
    }
    
    [_images removeObjectAtIndex:_currentIndex];

    [UIView animateWithDuration:0.4 animations:^{
        _currentView.alpha = 0;
    } completion:^(BOOL finished) {
        if (_currentIndex == _images.count) { _currentIndex--; }
        
        if (_images.count < 3) {
            ImageShowView *showView = (ImageShowView *) [_zoomViews lastObject];
            [showView removeFromSuperview];
            [_zoomViews removeLastObject];
            _scrolloView.contentSize = CGSizeMake(_images.count * CGRectGetWidth(_scrolloView.bounds) ,CGRectGetHeight(_scrolloView.bounds));
            _scrolloView.contentOffset = CGPointMake(_currentIndex * CGRectGetWidth(_scrolloView.bounds), 0);
        }
        
        [self displayImages];
        _currentView.alpha = 1;
    }];

    
}

- (void)showWithImages:(NSArray *)images startIndex:(NSInteger)startIndex{
    
    if (images.count == 0) { return; }
    _images         =   [images mutableCopy];
    _currentIndex   =   startIndex;
    
    CGFloat contentWidth        =   CGRectGetWidth(self.view.bounds);
    CGFloat contentHeight       =   CGRectGetHeight(self.view.bounds);
    NSInteger zoomViewCount     =   images.count > 3 ? 3 : images.count;
 
    _scrolloView.contentSize = CGSizeMake(contentWidth * zoomViewCount, contentHeight);
    

   _zoomViews = [NSMutableArray arrayWithCapacity:zoomViewCount];
    for (int i = 0; i < zoomViewCount; i++) {
        ImageShowView *zoomView = [[ImageShowView alloc] initWithFrame:CGRectMake(i * contentWidth, 0, contentWidth, contentHeight)];
        zoomView.ezlDelegate = self;
        zoomView.tag = i ;
        [_scrolloView addSubview:zoomView];
        [_zoomViews addObject:zoomView];
    }
    
    if (startIndex == images.count - 1 && images.count > 1) {
        _scrolloView.contentOffset = CGPointMake((zoomViewCount - 1) * contentWidth, 0);
    }

    [self displayImages];
    
    [[ShareAlertWindow getInstance] showController:self];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    if (_images.count < 4) { return; }

    CGFloat width = CGRectGetWidth(scrollView.bounds);
    CGFloat offsetX = scrollView.contentOffset.x;
    
    BOOL isFirstImage = _currentIndex == 0;
    BOOL isLastImage  = _currentIndex == _images.count - 1;
    
    BOOL indexChange = NO;

    if (offsetX == width ) {
        if ( isFirstImage){
            _currentIndex++;
            indexChange = YES;
        }
        else if ( isLastImage ) {
            _currentIndex--;
            indexChange = YES;
        }
    }
    else if (offsetX == 2 * width) {
        if (!isLastImage)  {
             indexChange = YES;
            _currentIndex++;
        }
    }
    else if (offsetX == 0) {
        if (!isFirstImage)  {
             indexChange = YES;
             _currentIndex--;
        }
    }

    if (indexChange) {
        [self displayImages];
    }
}

- (void)displayImages{
    
    if (_currentIndex == 0) { [self displayFirstImage]; return;}
    if (_currentIndex == _images.count - 1) { [self displayLastImage]; return;}
    
    _currentView = _zoomViews[1];
    NSInteger prevIndex = _currentIndex - 1;
    NSInteger nextIndex = _currentIndex + 1;
    NSArray *temp = @[ _images[prevIndex], _images[_currentIndex], _images[nextIndex] ];
    
    [_zoomViews enumerateObjectsUsingBlock:^(ImageShowView *  _Nonnull zoomView, NSUInteger idx, BOOL * _Nonnull stop) {
        EzlPhoto *photo = temp[idx];
        zoomView.image = photo.fullScreenImage;
        if (_editEnable) {
            zoomView.ticked = photo.ticked;
        }
    }];
    _scrolloView.contentOffset = CGPointMake(CGRectGetWidth(_scrolloView.bounds), 0);
}

- (void)displayFirstImage{
    _currentView = _zoomViews[0];
    EzlPhoto *photo = _images[0];
    _currentView.image = photo.fullScreenImage;
    if (_editEnable) {
        _currentView.ticked = photo.ticked;
    }
    
    if (_images.count > 1) {
        ImageShowView *nextView = _zoomViews[1];
        EzlPhoto *photo  = _images[1];
        nextView.image = photo.fullScreenImage;
        if (_editEnable) {
            nextView.ticked = photo.ticked;
        }
    }
    
    if (_images.count == 3) {
        ImageShowView *nextView = _zoomViews[2];
        EzlPhoto *photo = _images[2];
        nextView.image = photo.fullScreenImage;
        if (_editEnable) {
            nextView.ticked = photo.ticked;
        }
    }
}

- (void)displayLastImage{
    _currentView = [_zoomViews lastObject];
    EzlPhoto *photo = [_images lastObject];
    _currentView.image = photo.fullScreenImage;
    if (_editEnable) {
        _currentView.ticked = photo.ticked;
    }
    
    if (_images.count > 1) {
        ImageShowView *preView = _zoomViews[_zoomViews.count - 2];
         EzlPhoto *photo = _images[_currentIndex - 1];
        preView.image = photo.fullScreenImage;
        if (_editEnable) {
            preView.ticked = photo.ticked;
        }
    }
    
    if (_images.count == 3) {
        ImageShowView *preView = _zoomViews[0];
         EzlPhoto *photo = _images[0];
        preView.image = photo.fullScreenImage;
        if (_editEnable) {
            preView.ticked = photo.ticked;
        }
    }
}

- (void)imageDidChange:(void (^)())handler{
    _imageChangeHandler = handler;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
