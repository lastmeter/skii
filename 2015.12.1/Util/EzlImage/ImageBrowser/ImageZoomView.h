//
//  ImageZoomView.h
//  Moments
//
//  Created by xihan on 15/12/12.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ImageZoomView : UIScrollView

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, assign, getter=isTicked) BOOL ticked;

@property (nonatomic, strong) UIImage *currentSizeImage;

@property (nonatomic, assign) CGSize currentSize;

@property (nonatomic, assign) CGSize minimumSize;


@end
