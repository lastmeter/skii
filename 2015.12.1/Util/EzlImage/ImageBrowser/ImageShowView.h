//
//  ImageShowView.h
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageShowView;
@protocol EzlImageShowViewDelegate <NSObject>

- (void)imageDidTapped:(ImageShowView *)zoomView;

@optional
- (void)imageDidSelect:(ImageShowView *)view;
- (void)imageDidDeselect:(ImageShowView *)view;

@end

@interface ImageShowView : UIView

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, assign, getter=isTicked) BOOL ticked;

@property (nonatomic, strong) UIImage *currentSizeImage;

@property (nonatomic, assign) CGSize currentSize;

@property (nonatomic, assign) CGSize minimumSize;

@property (nonatomic, weak) id<EzlImageShowViewDelegate>ezlDelegate;

@end
