//
//  ImageBrowser.h
//  Moments
//
//  Created by xihan on 15/12/11.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageBrowser : UIViewController

@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, assign) BOOL editEnable;
@property (nonatomic, assign, readonly) NSInteger currentIndex;

- (void)showWithImages:(NSArray *)images startIndex:(NSInteger)startIndex;

- (void)imageDidChange:(void(^)())handler;

@end
