//
//  EzlImageModel.h
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface EzlPhoto : NSObject

@property (nonatomic, strong) ALAsset *asset;

@property (nonatomic, strong) UIImage *fullScreenImage;
@property (nonatomic, strong) UIImage *thumbnail;

@property (nonatomic, assign) CGRect originFrame;
@property (nonatomic, assign) BOOL ticked;

@property (nonatomic, readonly) NSData *imageData;
@property (nonatomic, copy) NSString *mimeType;

@end
