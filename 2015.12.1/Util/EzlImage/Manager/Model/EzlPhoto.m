//
//  EzlImageModel.m
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlPhoto.h"
#import "EzlPhotoManager.h"
#import <ImageIO/ImageIO.h>
#import "AppDelegate.h"

@implementation EzlPhoto


- (UIImage *)fullScreenImage{
    if (_asset) {
        ALAssetRepresentation *assetRepresentation = [_asset defaultRepresentation];
        return [UIImage imageWithCGImage:[assetRepresentation fullScreenImage]
                                   scale:[assetRepresentation scale]
                             orientation:UIImageOrientationUp];
    }
    return _fullScreenImage;
}

- (UIImage *)thumbnail{
    if (_asset) {
        CGImageRef thumbnailImageRef = [_asset aspectRatioThumbnail];
        return [UIImage imageWithCGImage:thumbnailImageRef];
    }
    return _thumbnail;
}



static size_t getAssetBytesCallback(void *info, void *buffer, off_t position, size_t count) {
    ALAssetRepresentation *rep = (__bridge id)info;
    
    NSError *error = nil;
    size_t countRead = [rep getBytes:(uint8_t *)buffer fromOffset:position length:count error:&error];
    
    if (countRead == 0 && error) {
        // We have no way of passing this info back to the caller, so we log it, at least.
        NSLog(@"thumbnailForAsset:maxPixelSize: got an error reading an asset: %@", error);
    }
    
    return countRead;
}
static void releaseAssetCallback(void *info) {
    // The info here is an ALAssetRepresentation which we CFRetain in thumbnailForAsset:maxPixelSize:.
    // This release balances that retain.
    CFRelease(info);
}

//压缩图片
- (UIImage *)editeImageForAsset:(ALAsset *)asset maxPixelSize:(NSUInteger)size
{
    NSParameterAssert(asset != nil);
    NSParameterAssert(size > 0);
    
    ALAssetRepresentation *rep = [asset defaultRepresentation];
    
    CGDataProviderDirectCallbacks callbacks =
    {
        .version = 0,
        .getBytePointer = NULL,
        .releaseBytePointer = NULL,
        .getBytesAtPosition = getAssetBytesCallback,
        .releaseInfo = releaseAssetCallback,
    };
    
    CGDataProviderRef provider = CGDataProviderCreateDirect((void *)CFBridgingRetain(rep), [rep size], &callbacks);
    
    CGImageSourceRef source = CGImageSourceCreateWithDataProvider(provider, NULL);
    
    CGImageRef imageRef = CGImageSourceCreateThumbnailAtIndex(source, 0, (__bridge CFDictionaryRef)
                                                              @{   (NSString *)kCGImageSourceCreateThumbnailFromImageAlways: @YES,
                                                                   (NSString *)kCGImageSourceThumbnailMaxPixelSize : @(size),
                                                                   (NSString *)kCGImageSourceCreateThumbnailWithTransform :@YES,
                                                                });
    
    CFRelease(source);
    CFRelease(provider);
    
    if (!imageRef) {
        return nil;
    }
    
    UIImage *toReturn = [UIImage imageWithCGImage:imageRef];
    
    CFRelease(imageRef);
    
    return toReturn;
}


- (NSData *)imageData{
    
    UIImage *image;
    if (_asset) {
        ALAssetRepresentation* representation = [_asset defaultRepresentation];
        NSMutableData *data = [NSMutableData data];
        
        // now loop, reading data into buffer and writing that to our data strea
        NSError *error;
        long long bufferOffset = 0ll;
        NSInteger bufferSize = 10000;
        long long bytesRemaining = [representation size];
        uint8_t buffer[bufferSize];
        NSUInteger bytesRead;
        while (bytesRemaining > 0)
        {
            bytesRead = [representation getBytes:buffer fromOffset:bufferOffset length:bufferSize error:&error];
            if (bytesRead == 0)
            {
                NSLog(@"error reading asset representation: %@", error);
                return nil;
            }
            bytesRemaining -= bytesRead;
            bufferOffset   += bytesRead;
            [data appendBytes:buffer length:bytesRead];
        }

        image = [UIImage imageWithData:data];
        image = [self fixOrientation:[UIImage imageWithData:data]];
    }
    else{
        image = [self fixOrientation:self.fullScreenImage];
    }

    return [self compressImage:image ];
   // return UIImageJPEGRepresentation(image, 1);
}

- (NSData *)compressImage:(UIImage *)image{
    
    //先调整分辨率
    CGSize imageSize = image.size;
    CGSize newSize = CGSizeMake(640, 960);
    if (imageSize.width > imageSize.height) {
        newSize = CGSizeMake(960, 640);
    }
    
    NSInteger quality = 10;
    UIImage * editImage = [self makeImage:image toScaleSize:newSize];
    NSData * data = UIImageJPEGRepresentation(editImage, quality * 0.1);
    NSLog(@"before compress：%@",@(data.length/1024));
    float maxSize =  512 * 1024;
    while (data.length > maxSize) {
        quality--;
        data = UIImageJPEGRepresentation(editImage, quality * 0.1);
        if (quality == 0) break;
    }
    NSLog(@"compress：%@",@(data.length/1024));
    return data;
}

- (NSString *)mimeType{
    NSString *mimeType = @"image/jpeg";
    if (_asset) {
        ALAssetRepresentation* representation = [_asset defaultRepresentation];
        NSString* filename = [representation filename];
        NSString *extension = [filename pathExtension];
        if ([extension isEqualToString:@"PNG"]) {
            mimeType = @"image/png";
        }
    }
    return mimeType;
}

- (UIImage *)makeImage:(UIImage *)image toScaleSize:(CGSize )scaleSize{

    //先调整分辨率
    
    CGSize newSize = CGSizeMake(image.size.width, image.size.height);
    
    CGFloat tempHeight = newSize.height / scaleSize.height;
    CGFloat tempWidth = newSize.width / scaleSize.width;
    
    if (tempWidth > 1.0 && tempWidth > tempHeight) {
        newSize = CGSizeMake(image.size.width / tempWidth, image.size.height / tempWidth);
    }
    else if (tempHeight > 1.0 && tempWidth < tempHeight){
        newSize = CGSizeMake(image.size.width / tempHeight, image.size.height / tempHeight);
    }
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)fixOrientation:(UIImage *)aImage {
    
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
