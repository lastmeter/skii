//
//  AlbumData.m
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlPhotoManager.h"

@interface EzlPhotoManager ()
@property (nonatomic, strong) NSMutableArray *mSeletedPhotos;
@property (nonatomic, strong) NSMutableArray *mSeletedAssests;
@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@end

@implementation EzlPhotoManager

+ (EzlPhotoManager *)shareManager{
    static dispatch_once_t once;
    static EzlPhotoManager *albumData;
    dispatch_once(&once, ^ {
        albumData = [[EzlPhotoManager alloc] init];
    });
    return albumData;
}


- (void)loadDataFinish:(void(^)())handler{
    if (!handler) { return; }
    

    if (!_assetsLibrary) {
       _assetsLibrary = [[ALAssetsLibrary alloc] init];
    }
    if (!_assetsGroups) {
        _assetsGroups = [NSMutableArray array];
    }
    else{
        [_assetsGroups removeAllObjects];
    }

   NSUInteger groupTypes = ALAssetsGroupLibrary | ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupFaces | ALAssetsGroupSavedPhotos | ALAssetsGroupPhotoStream | ALAssetsGroupAll;
//    
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        
        ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
        [group setAssetsFilter:onlyPhotosFilter];
        if ([group numberOfAssets] > 0)
        {
            [self.assetsGroups addObject:group];
        }
        else
        {
            if (self.assetsGroups.count > 1) {
                [self.assetsGroups sortUsingComparator:^NSComparisonResult(ALAssetsGroup *  _Nonnull obj1, ALAssetsGroup *  _Nonnull obj2) {
                    if ([obj1 numberOfAssets] > [obj2 numberOfAssets]) {
                        return NSOrderedAscending;
                    }
                    return NSOrderedDescending;
                }];
            }
            handler();
        }
    };
    
    // enumerate only photos
   // NSUInteger groupTypes = ALAssetsGroupAlbum | ALAssetsGroupEvent | ALAssetsGroupFaces | ALAssetsGroupSavedPhotos;
    [self.assetsLibrary enumerateGroupsWithTypes:groupTypes usingBlock:listGroupBlock failureBlock:^(NSError *error) {
        handler();
    }];
    
};

- (NSArray *)seletedPhotos{
    return _mSeletedPhotos;
}

- (NSMutableArray *)mSeletedPhotos{
    if (!_mSeletedPhotos) {
        _mSeletedPhotos = [NSMutableArray array];
    }
    return _mSeletedPhotos;
}

- (NSMutableArray *)mSeletedAssests{
    if (!_mSeletedAssests) {
        _mSeletedAssests = [NSMutableArray array];
    }
    return _mSeletedAssests;
}

- (void)addToSeleted:(EzlPhoto *)photo{
    photo.ticked = YES;
    [self.mSeletedPhotos addObject:photo];
    if (photo.asset) { [self.mSeletedAssests addObject:photo.asset]; }
}

- (void)removeFromSeleted:(EzlPhoto *)photo{
    photo.ticked = NO;
    if (photo.asset) { [self.mSeletedAssests removeObject:photo.asset];  }
    [self.mSeletedPhotos removeObject:photo];
}

- (BOOL)hadAddedToSeleted:(EzlPhoto *)photo{
    if (photo.asset) {
        if ([self.mSeletedAssests containsObject:photo.asset]) {
            return YES;
        }
    }
    return NO;
}

- (NSInteger)numberOfSeleted{
    return self.mSeletedPhotos.count;
}

- (EzlPhoto *)selectedPhotoWithIndex:(NSInteger)index{
    return self.mSeletedPhotos[index];
}

- (void)free{
    _assetsLibrary      = nil;
    _mSeletedAssests    = nil;
    _mSeletedPhotos     = nil;
    _assetsGroups       = nil;
}


@end
