//
//  AlbumData.h
//  Moments
//
//  Created by xihan on 15/12/14.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EzlPhoto.h"

#define MaxNumberOfPickImages 3

@interface EzlPhotoManager : NSObject

+ (EzlPhotoManager *)shareManager;

- (void)loadDataFinish:(void(^)())handler;

- (void)addToSeleted:(EzlPhoto *)photo;
- (void)removeFromSeleted:(EzlPhoto *)photo;
- (BOOL)hadAddedToSeleted:(EzlPhoto *)photo;

- (NSArray *)seletedPhotos;
- (NSInteger)numberOfSeleted;
- (EzlPhoto *)selectedPhotoWithIndex:(NSInteger)index;

- (void)free;

@property (nonatomic, strong) NSMutableArray *assetsGroups;


@end
