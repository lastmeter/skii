//
//  EzlWebImage.m
//  Moments
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlWebImageManager.h"
#import "MomentsService.h"

@interface EzlWebImageManager()

@property (nonatomic, copy) NSString *cachePath;
@property (nonatomic, strong) NSFileManager *fileManager;
@property (nonatomic, assign) BOOL folderExists;
@property (nonatomic, strong) NSMutableDictionary *handlerDic;
@property (nonatomic, strong) NSMutableArray *downloading;
@property (nonatomic, strong) dispatch_queue_t operation_queue;

@end

@implementation EzlWebImageManager

- (dispatch_queue_t)operation_queue{
    if (!_operation_queue) {
        _operation_queue = dispatch_queue_create("com.ezl.WebImage.OperationQueue", DISPATCH_QUEUE_SERIAL);
    }
    return _operation_queue;
}

- (NSString *)cachePath{
    if (_cachePath == nil) {
        NSString *document = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _cachePath = [document stringByAppendingPathComponent:@"do"];
        NSLog(@"%@",_cachePath);
    }
    return _cachePath;
}

- (NSFileManager *)fileManager{
    if (_fileManager == nil) {
        _fileManager = [NSFileManager defaultManager];
    }
    return _fileManager;
}

- (NSMutableDictionary *)handlerDic{
    if (_handlerDic == nil) {
        _handlerDic = [NSMutableDictionary dictionary];
    }
    return _handlerDic;
}

- (NSMutableArray *)downloading{
    if (_downloading == nil) {
        _downloading = [NSMutableArray array];
    }
    return _downloading;
}

#pragma mark-
- (void)p_CreateFolder{
    self.folderExists = YES;
    if (![self.fileManager fileExistsAtPath:self.cachePath]) {
         self.folderExists = [self.fileManager createDirectoryAtPath:self.cachePath withIntermediateDirectories:NO attributes:nil error:nil];
    }
}

- (NSString *)p_CacheFilePath:(NSString *)name{
    NSString *filePath = [self.cachePath stringByAppendingPathComponent:name];
    return filePath;
}

- (NSString *)p_GetFileName:(NSString *)url{
    return [url lastPathComponent];
}

- (BOOL)p_ImageIsInCache:(NSString *)name{
    
    return [self.fileManager fileExistsAtPath:[self p_CacheFilePath:name]];
}

#pragma mark-
- (void)p_DownloadImage:(NSString *)name urlString: (NSString *)urlString handler:(ImageFetchHandler)handler{
    
    self.handlerDic[name] = @[handler];
    [self.downloading addObject:name];
    
    NSString *encodeStr = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:urlString]];
    NSURL *url = [NSURL URLWithString:encodeStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];

    NSURLSessionDownloadTask *downLoad = [session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response,  NSError *error) {
        [self p_StartOperation:^{
            
            if (!self.folderExists) { [self p_CreateFolder]; }
            if (error) {
                NSLog(@"%@",error);
                [self.handlerDic removeObjectForKey:name];
                return ;
            }
            
            NSData *data = [NSData dataWithContentsOfURL:location];
            if (data == nil) { [self.handlerDic removeObjectForKey:name]; return ; }
            
            [data writeToFile:[self p_CacheFilePath:name] atomically:YES];
            
            UIImage *image = [UIImage imageWithData:data];
            
            NSArray *handlers = self.handlerDic[name];
            [handlers enumerateObjectsUsingBlock:^(ImageFetchHandler  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj(image, name);
            }];
            
            self.handlerDic[name] = nil;
            [session finishTasksAndInvalidate];

        }];
        [self.downloading removeObject:name];

    }];
    [downLoad resume];
}

#pragma mark-
- (void)fetchImageWithUrl:(NSString *)url handler:(ImageFetchHandler)handler{
    NSString *encodeStr1 = [url stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *imageName  = [self p_GetFileName:encodeStr1];

    if ([self p_ImageIsInCache:imageName]) {
        UIImage *image = [UIImage imageWithContentsOfFile:[self p_CacheFilePath:imageName]];
        handler(image, imageName);
        return;
    }
    
    if ([self.downloading containsObject:imageName]) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.handlerDic[imageName]];
        [array addObject:handler];
        self.handlerDic[imageName] = [array copy];
        return;
    }
    
    [self p_DownloadImage:imageName urlString:url handler:handler];
}

- (void)p_AddToCache:(NSData *)imageData name:(NSString *)name{
    [self.fileManager removeItemAtPath:[self p_CacheFilePath:name] error:nil];
    [imageData writeToFile:[self p_CacheFilePath:name] atomically:YES];
}

#pragma mark-
- (void)removeCache{
    if ([self.fileManager fileExistsAtPath:self.cachePath]) {
        [self.fileManager removeItemAtPath:self.cachePath error:nil];
    }
}

- (void)fetchHeadPortraitByEid:(NSString *)eid uid:(NSString *)uid handler:(ImageFetchHandler)handler{
    
    if ([self p_ImageIsInCache:eid]) {
        NSData *data = [NSData dataWithContentsOfFile:[self p_CacheFilePath:eid]];
        handler([UIImage imageWithData:data], eid);
        return;
    }
    
    if ([self.downloading containsObject:eid]) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.handlerDic[uid]];
        [array addObject:handler];
        self.handlerDic[eid] = [array copy];
        return;
    }
    
    self.handlerDic[eid] = @[handler];
    [self.downloading addObject:eid];

    [MomentsService getHeadPortraitByUserId:uid handler:^(HandleResult result, NSData *data) {
        [self.downloading removeObject:eid];
        if (result == HandleResultSuccess  && data) {
            if (!self.folderExists) { [self p_CreateFolder]; }
            
            [data writeToFile:[self p_CacheFilePath:eid] atomically:YES];
            UIImage *image = [UIImage imageWithData:data];
            NSError *error;
            if (error) {
                NSLog(@"%@",error);
            }
            NSArray *handlers = self.handlerDic[eid];
            [handlers enumerateObjectsUsingBlock:^(ImageFetchHandler  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj(image, eid);
            }];
        }
        [self.handlerDic removeObjectForKey:eid];
    }];
}

- (void)p_StartOperation:(void(^)())operaion{
    dispatch_async(self.operation_queue, ^{
        operaion();
    });
}

#pragma mark-

+ (EzlWebImageManager *)shareManager{
    static dispatch_once_t once;
    static EzlWebImageManager *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[EzlWebImageManager alloc] init];
    });
    return sharedView;
}

+ (void)fetchImageWithUrl:(NSString *)url handler:(ImageFetchHandler)handler{
    EzlWebImageManager *manager = [EzlWebImageManager shareManager];
    [manager p_StartOperation:^{
         [ manager fetchImageWithUrl:url handler:handler];
    }];
}

+ (void)removeCache{
    EzlWebImageManager *manager = [EzlWebImageManager shareManager];
    [manager p_StartOperation:^{
        [manager removeCache];
    }];
}

+ (void)fetchHeadPortraitByEid:(NSString *)eid uid:(NSString *)uid handler:(ImageFetchHandler)handler{
    EzlWebImageManager *manager = [EzlWebImageManager shareManager];
    [manager p_StartOperation:^{
        [manager fetchHeadPortraitByEid:eid uid:uid handler:handler];
    }];
}


+ (void)addToCache:(NSData *)imageData name:(NSString *)name{
    EzlWebImageManager *manager = [EzlWebImageManager shareManager];
    [manager p_StartOperation:^{
        [manager p_AddToCache:imageData name:name];
    }];
}
@end
