//
//  EzlWebImage.h
//  Moments
//
//  Created by xihan on 15/12/3.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ImageFetchHandler)(UIImage *image, NSString *name);

@interface EzlWebImageManager : NSObject

+ (EzlWebImageManager *)shareManager;

+ (void)fetchImageWithUrl:(NSString *)url handler:(ImageFetchHandler)handler;
+ (void)removeCache;
+ (void)fetchHeadPortraitByEid:(NSString *)eid uid:(NSString *)uid handler:(ImageFetchHandler)handler;
+ (void)addToCache:(NSData *)imageData name:(NSString *)name;
@end
