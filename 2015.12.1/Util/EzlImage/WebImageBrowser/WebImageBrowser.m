//
//  WebImageBrowser.m
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "WebImageBrowser.h"
#import "WebImageShowView.h"
#import "WebImage.h"
#import "ShareAlertWindow.h"
#import "UIImageView+EzlWeb.h"
#import "EzlWebImageManager.h"
#import "ImageZoomView.h"
#import "SDWebImageManager.h"

@interface WebImageBrowser() <EzlImageShowViewDelegate, UIScrollViewDelegate>
{
    UIScrollView *_scrolloView;
}

@property (nonatomic, weak) WebImage *showImage;
@property (nonatomic, weak) WebImageShowView *showView;
@property (nonatomic, weak) NSArray *webImages;
@end

@implementation WebImageBrowser

- (instancetype)init{
    self = [super init];
    if (self) {
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        _scrolloView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrolloView.pagingEnabled = YES;
        _scrolloView.delegate = self;
        _scrolloView.backgroundColor = [UIColor blackColor];
        _scrolloView.hidden = YES;
        [self.view addSubview:_scrolloView];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
   
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0;
    [self.view addSubview:bgView];
    
    WebImageShowView * zoomView = [[WebImageShowView alloc] initWithFrame:self.view.bounds];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:_showImage.originFrame];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    __weak WebImageBrowser *weakSelf = self;
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:_showImage.urlString] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (finished && !error && image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                zoomView.image = image;
                imageView.image = image;
                [weakSelf.view addSubview:imageView];
                
                UIView *view = [[UIView alloc] init];
                view.frame = (CGRect){ CGPointZero, zoomView.minimumSize };
                view.center = self.view.center;
                
                [UIView animateWithDuration:0.4 animations:^{
                    imageView.frame = view.frame;
                    bgView.alpha = 1;
                } completion:^(BOOL finished) {
                    _scrolloView.hidden = NO;
                    [imageView removeFromSuperview];
                    [bgView removeFromSuperview];
                }];
            });
        }
    }];
}

- (void)showImageWithWebImages:(NSArray *)webImages startIndex:(NSInteger)index{
    
    CGFloat contentWidth  =  CGRectGetWidth(_scrolloView.bounds);
    CGFloat contentHeight =  CGRectGetHeight(_scrolloView.bounds);
    
    _scrolloView.contentSize   = CGSizeMake(webImages.count * contentWidth, contentHeight);
    _scrolloView.contentOffset = CGPointMake(index * contentWidth , 0);

    _showImage = webImages[index];
    
    [webImages enumerateObjectsUsingBlock:^(WebImage *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WebImageShowView *showView = [[WebImageShowView alloc] initWithFrame:CGRectMake(idx * contentWidth, 0, contentWidth, contentHeight)];
       
        showView.imageUrlString = obj.urlString;
        showView.image = obj.image;
        showView.tag = idx;
        showView.ezlDelegate = self;
        [_scrolloView addSubview:showView];
        if (idx == index) {
            _showView = showView;
        }
    }];
    
    _webImages = webImages;
    [[ShareAlertWindow getInstance] showController:self];
}

- (void)imageDidTapped:(ImageShowView *)zoomView{
    WebImage* webImage = _webImages[zoomView.tag];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:zoomView.currentSizeImage];
    imageView.center = self.view.center;
    [self.view addSubview:imageView];
    
    [_scrolloView removeFromSuperview];
    
    [UIView animateWithDuration:0.4 animations:^{
        imageView.frame = webImage.originFrame;
    } completion:^(BOOL finished) {
        [[ShareAlertWindow getInstance] dismissController];
    }];
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
