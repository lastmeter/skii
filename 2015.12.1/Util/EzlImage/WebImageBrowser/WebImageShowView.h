//
//  WenImageShowView.h
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ImageShowView.h"
#import "EzlWebImageManager.h"

@interface WebImageShowView : ImageShowView

@property(nonatomic, copy)NSString *imageUrlString;


@end
