//
//  WebImageBrowser.h
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebImageBrowser :UIViewController
- (void)showImageWithWebImages:(NSArray *)webImages startIndex:(NSInteger)index;
@end
