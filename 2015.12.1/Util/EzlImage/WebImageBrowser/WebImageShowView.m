//
//  WenImageShowView.m
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "WebImageShowView.h"
#import "SDWebImageManager.h"
@implementation WebImageShowView

- (void)setImageUrlString:(NSString *)imageUrlString{
    __weak WebImageShowView *weakSelf = self;
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageUrlString] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (finished && !error && image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.image = image;
            });
        }
    }];
}

@end
