//
//  EzlWebImage.h
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebImage : NSObject

@property (nonatomic, copy) NSString *urlString;
@property (nonatomic, copy) NSString *thumbUrlString;
@property (nonatomic, assign) CGRect originFrame;
@property (nonatomic, weak) UIImage *image;


@end
