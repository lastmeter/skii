//
//  ImageGroupController.m
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "AssetsGroupController.h"
#import "AssetsContentsController.h"
#import "EzlPhotoManager.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AssetsGroupController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) EzlPhotoManager *photoManager;
@end

@implementation AssetsGroupController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择照片";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];

    
    self.photoManager = [EzlPhotoManager shareManager];
    
    [self p_InitTableView];
    [self p_LoadData];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}

- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)p_LoadData{

    [self waitting];
    [self.photoManager loadDataFinish:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopWaitting];
//            if (self.photoManager.assetsGroups.count > 0) {
//                AssetsContentsController *assetsContentsController = [[AssetsContentsController alloc] init];
//                assetsContentsController.assetsGroup = self.photoManager.assetsGroups[0];
//                [self.navigationController pushViewController:assetsContentsController animated:NO];
//            }
            [self.tableView reloadData];
        });
    }];
}

- (void)p_InitTableView{
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    UIView *view = [[UIView alloc] init];
    _tableView.tableFooterView = view;
    [self.view addSubview:_tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.photoManager.assetsGroups.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @" ";
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 3;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    ALAssetsGroup *group = self.photoManager.assetsGroups[indexPath.section];
    CGImageRef posterImageRef = [group posterImage];
    cell.imageView.image = [UIImage imageWithCGImage:posterImageRef];
    cell.detailTextLabel.text = [@([group numberOfAssets]) stringValue];
    cell.textLabel.text = [group valueForProperty:ALAssetsGroupPropertyName];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    AssetsContentsController *assetsContentsController = [[AssetsContentsController alloc] init];
    assetsContentsController.assetsGroup = self.photoManager.assetsGroups[indexPath.section];
    [self.navigationController pushViewController:assetsContentsController animated:YES];
}


@end
