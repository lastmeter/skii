//
//  AlbumContentsViewController.h
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALAssetsGroup;
@interface AssetsContentsController : UIViewController

@property (nonatomic, weak) ALAssetsGroup *assetsGroup;

@end
