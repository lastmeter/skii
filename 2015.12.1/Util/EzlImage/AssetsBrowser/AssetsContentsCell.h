//
//  AlbumContentCell.h
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetsContentsCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign) BOOL tick;
@end
