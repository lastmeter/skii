//
//  AlbumContentsViewController.m
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "AssetsContentsController.h"
#import "AssetsContentsCell.h"
#import "EzlPhotoManager.h"
#import "LrhAlertView.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AssetsContentsController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIButton *sendBtn;
@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, weak) EzlPhotoManager *photoManager;

@end

@implementation AssetsContentsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.photoManager = [EzlPhotoManager shareManager];
    self.title = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];
    
    [self p_InitCollectionView];
    [self p_InitBottomView];
    [self p_LoadData];
}


- (void)p_InitCollectionView
{
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 105) collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerClass:[AssetsContentsCell class] forCellWithReuseIdentifier:@"photoCell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [self.view addSubview:_collectionView];
}

- (void)p_InitBottomView{
    UIView* bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 114, CGRectGetWidth(self.view.frame), 50)];
    bottomView.backgroundColor = [UIColor colorWithRed:248/255.0 green:249/255.0 blue:250/250.0 alpha:1];
    [self.view addSubview:bottomView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(bottomView.frame), 1)];
    line.backgroundColor = [UIColor colorWithRed:207/255.0 green:207/255.0 blue:207/255.0 alpha:1];
    [bottomView addSubview:line];
    
    _sendBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(bottomView.frame) - 90, 8, 80, 34)];
    _sendBtn.backgroundColor = [UIColor grayColor];
    _sendBtn.enabled = NO;
    _sendBtn.layer.cornerRadius = 6;
    [_sendBtn setTitle:@"确定" forState:UIControlStateNormal];
    [_sendBtn addTarget:self action:@selector(addImage) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_sendBtn];
}

- (void)addImage{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pickImageFinish" object:nil];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)p_LoadData{
    if (!self.assets) {
        _assets = [[NSMutableArray alloc] init];
    } else {
        [self.assets removeAllObjects];
    }
    
    ALAssetsGroupEnumerationResultsBlock assetsEnumerationBlock = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result) {
            EzlPhoto *photo = [[EzlPhoto alloc] init];
            photo.asset = result;
            [self.assets addObject:photo];
        }
    };
    ALAssetsFilter *onlyPhotosFilter = [ALAssetsFilter allPhotos];
    [self.assetsGroup setAssetsFilter:onlyPhotosFilter];
    [self.assetsGroup enumerateAssetsUsingBlock:assetsEnumerationBlock];
    [self.assets sortUsingComparator:^NSComparisonResult(EzlPhoto *  _Nonnull obj1, EzlPhoto *  _Nonnull obj2) {
        NSDate *date1 = [ obj1.asset valueForProperty:ALAssetPropertyDate ] ;
        NSDate *date2 = [ obj2.asset valueForProperty:ALAssetPropertyDate ] ;
        if ([date1 compare:date2] == NSOrderedAscending) {
            return NSOrderedDescending;
        }
        return NSOrderedAscending;
    }];
    
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.assetsGroup numberOfAssets];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AssetsContentsCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    EzlPhoto *photo = self.assets[indexPath.row];
    cell.imageView.image = photo.thumbnail;
    cell.tick = [self.photoManager.seletedPhotos containsObject:photo];
    
    [cell updateConstraintsIfNeeded];
    [cell updateConstraints];
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat wh = ( [UIScreen mainScreen].bounds.size.width - 15 )* 0.25;
    return CGSizeMake(wh, wh);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AssetsContentsCell *cell = (AssetsContentsCell *)[collectionView cellForItemAtIndexPath:indexPath];

    EzlPhoto *photo = self.assets[indexPath.row];
    if ([self.photoManager hadAddedToSeleted:photo]) {
        [self.photoManager removeFromSeleted:photo];
        cell.tick = NO;
    }

    else{
        if ([self.photoManager numberOfSeleted] == MaxNumberOfPickImages) {
            NSString *tips = [NSString stringWithFormat:@"最多只能选择%@张",[@(MaxNumberOfPickImages) stringValue]];
            [LrhAlertView showMessage:tips];
            return;
        }
        [self.photoManager addToSeleted:photo];
        cell.tick = YES;
    }
    
    if ([self.photoManager numberOfSeleted] > 0) {
        if (!_sendBtn.enabled) {
            _sendBtn.backgroundColor = [UIColor colorWithRed:31/255.0 green:186/255.0 blue:243/255.0 alpha:1];
            _sendBtn.enabled = YES;
        }
        [_sendBtn setTitle:[NSString stringWithFormat:@"确定(%@)",@(self.photoManager.seletedPhotos.count)] forState:UIControlStateNormal];
    }
    else if (_sendBtn.enabled){
        _sendBtn.backgroundColor = [UIColor grayColor];
        [_sendBtn setTitle:@"确定" forState:UIControlStateNormal];
        _sendBtn.enabled = NO;
    }
    
}

@end
