//
//  AlbumContentCell.m
//  Moments
//
//  Created by xihan on 15/12/8.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "AssetsContentsCell.h"

@implementation AssetsContentsCell{
    UIImageView *_tickView;
    BOOL constraintsUpdate;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        
        _imageView = [[UIImageView alloc] init];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_imageView];
        
        _tickView = [[UIImageView alloc] init];
        _tickView.translatesAutoresizingMaskIntoConstraints = NO;
        _tickView.image = [UIImage imageNamed:@"check_box_nor"];
        [self.contentView addSubview:_tickView];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    if (constraintsUpdate) {
        return;
    }
    constraintsUpdate = YES;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"image":_imageView }]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"image":_imageView }]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[tick(30)]" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tick":_tickView }]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[tick(30)]-5-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tick":_tickView }]];
}

- (void)setTick:(BOOL)tick{
    _tick = tick;
    _tickView.image = [UIImage imageNamed: tick ? @"check_box_tick" : @"check_box_nor"];
}

@end
