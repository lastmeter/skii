//
//  ImagePicker.h
//  Locate
//
//  Created by xihan on 15/11/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EzlPhotoPicker : UIImagePickerController

+ (void)showInSuperVC:(UIViewController *)superVC
            pickImage:(void(^)(NSArray * images))handler;

@end
