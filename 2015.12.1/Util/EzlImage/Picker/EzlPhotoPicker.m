//
//  ImagePicker.m
//  Locate
//
//  Created by xihan on 15/11/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlPhotoPicker.h"
#import "AssetsGroupController.h"
#import "EzlPhotoManager.h"
#import "EzlPhoto.h"
#import "LrhAlertAction.h"
#import "LrhAlertView.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface EzlPhotoPicker()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    void(^_pickHandler)(NSArray *images);
    UIViewController *_superVC;
}
@end

@implementation EzlPhotoPicker

+ (void)showInSuperVC:(UIViewController *)superVC
            pickImage:(void(^)(NSArray * images))handler{
    
    EzlPhotoPicker *picker = [[EzlPhotoPicker alloc] init];
    [picker showInSuperVC:superVC pickImage:handler];
}

- (void)showInSuperVC:(UIViewController *)superVC
            pickImage:(void(^)(NSArray * images))handler{
    
    _superVC = superVC;
    _pickHandler = [handler copy];
    
    UIColor *color = [UIColor colorWithRed:18.0/255.0 green:183.0/255.0 blue:245.0/255.0 alpha:1];
    LrhAlertAction *action1 = [LrhAlertAction actionWithTitle:@"相册" titleColor:color handler:^{
        [self openPicLibrary];
    }];
    LrhAlertAction *action2 = [LrhAlertAction actionWithTitle:@"相机" titleColor:color handler:^{
        if ([EzlPhotoManager shareManager].seletedPhotos.count == MaxNumberOfPickImages) {
            [LrhAlertView showMessage:[NSString stringWithFormat:@"最多选择%@张",@(MaxNumberOfPickImages)]];
            return ;
        }
        [self addCarema];
    }];
    LrhAlertAction *action3 = [LrhAlertAction actionWithTitle:@"取消" titleColor:color handler:^{
        
    }];
    [LrhAlertView showMessage:nil actions:@[action1, action2, action3]];

}

-(void)openPicLibrary{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        AssetsGroupController *vc = [AssetsGroupController new];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        nav.navigationBar.translucent = NO;
        [_superVC presentViewController:nav animated:true completion:nil];
    }
}

-(void)addCarema{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
       
        self.allowsEditing = NO;
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
        _superVC.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        [_superVC presentViewController:self animated:YES completion:^{ self.delegate = self;}];
    }
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *editImage  =   [info objectForKey:UIImagePickerControllerOriginalImage];
    
    EzlPhoto *photo = [[EzlPhoto alloc] init];
    photo.fullScreenImage = editImage;
    photo.thumbnail = editImage;
    photo.ticked = YES;

    [[EzlPhotoManager shareManager] addToSeleted:photo];
    
    if (_pickHandler) {
        _pickHandler(@[editImage]);
    }
    _pickHandler = nil;
    [_superVC dismissViewControllerAnimated:YES completion:nil];
    _superVC = nil;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{

    [_superVC dismissViewControllerAnimated:YES completion:nil];
    _pickHandler = nil;
    _superVC = nil;
}

#pragma mark-

- (void)dealloc{
    _pickHandler = nil;
   _superVC = nil;
    NSLog(@"dealloc : %@",self);
}

@end
