//
//  UIImage+ScaleSize.h
//  Moments
//
//  Created by xihan on 15/12/12.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ScaleSize)

-(UIImage*)getSubImage:(CGRect)rect;
-(UIImage*)scaleToSize:(CGSize)size;
+ (UIImage *) captureScreen;
@end
