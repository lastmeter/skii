//
//  UIImage+EzlWeb.h
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (EzlWeb)
- (void)ezl_fetchImageWithUrl:(NSString *)url;
@end
