//
//  UIImage+EzlWeb.m
//  Moments
//
//  Created by xihan on 15/12/15.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "UIImageView+EzlWeb.h"
#import "EzlWebImageManager.h"

@implementation UIImageView (EzlWeb)

- (void)ezl_fetchImageWithUrl:(NSString *)url{
    __weak UIImageView *weakSelf = self;
    __weak NSString *weakName = [url lastPathComponent];
    [EzlWebImageManager fetchImageWithUrl:url handler:^(UIImage *image, NSString *name) {
        if ([weakName isEqualToString:name]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.image = image;
            });
        }
        
    }];
}

@end
