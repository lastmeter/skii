//
//  NEzlViewController.m
//  EZLearning
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "NEzlViewController.h"
#import "LoadingHUD.h"

@interface NEzlViewController ()
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, assign) BOOL wait;
@end

@implementation NEzlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (MBProgressHUD *)hud{
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _hud.labelText = @"请稍等";
    }
    return _hud;
}

- (void)waitting{
    if (_wait) {
        return;
    }
    [LoadingHUD show];
//    _wait = YES;
//    [self.view addSubview:self.hud];
//    [_hud show:YES];
}

- (void)stopWaitting{
    _wait = NO;
    [LoadingHUD hide];
}


@end
