//
//  ArticleHistoryCell.m
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ArticleHistoryCell.h"

@interface ArticleHistoryCell()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@property (nonatomic, strong) UILabel *statusLabel;

@end

@implementation ArticleHistoryCell
{
    BOOL _constraintsUpdated;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [AricleCellFrame titleFont];
        _titleLabel.numberOfLines = 0;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:_titleLabel];
        
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.textColor = [UIColor lightGrayColor];
        _subTitleLabel.font = [AricleCellFrame subTitleFont];
        _subTitleLabel.numberOfLines = 0;
        _subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:_subTitleLabel];
        
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.textColor = [UIColor blackColor];
        _statusLabel.font = [AricleCellFrame statusFont];
        _statusLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_statusLabel];
    }
    return self;
}

- (void)setCellFrame:(AricleCellFrame *)cellFrame{
    _titleLabel.frame       =   cellFrame.titleFrame;
    _subTitleLabel.frame    =   cellFrame.subTitleFrame;
    _statusLabel.frame      =   cellFrame.statusFrame;
    
    AricleModel *aricle = cellFrame.aricle;
    _titleLabel.text        =   [NSString stringWithFormat:@"标题:%@",aricle.title];
    _subTitleLabel.text     =   aricle.dateString;
    _statusLabel.text       =   aricle.status;
}
@end
