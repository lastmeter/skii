//
//  ArticleHistoryController.m
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ArticleHistoryController.h"
#import "ArticleHistoryCell.h"
#import "MLearningService.h"
#import "ArticleDetailController.h"
#import "AricleCellFrame.h"

@interface ArticleHistoryController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *atricles;

@end

@implementation ArticleHistoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_InitTableView];
    [self waitting];
    [MLearningService getHistoryArticleOfMeWithHandler:^(HandleResult result, NSArray *dataArray) {
        [self stopWaitting];
        if (result != HandleResultSuccess) { return ; }
        
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO];
        NSArray *sortArray = [dataArray sortedArrayUsingDescriptors:@[descriptor]];
        
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:dataArray.count];
        [sortArray enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            AricleModel *aricle = [[AricleModel alloc] init];
            aricle.aricleDic = obj;
            AricleCellFrame *cellFrame = [[AricleCellFrame alloc] init];
            cellFrame.aricle = aricle;
            [array addObject:cellFrame];
        }];
        self.atricles = array;
        [self.tableView reloadData];
    }];
}

- (void)p_InitTableView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"tableView":_tableView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"tableView":_tableView}]];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    AricleCellFrame *cellFrame = _atricles[indexPath.row];
    return cellFrame.cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _atricles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ArticleHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Article"];
    if (!cell) {
        cell = [[ArticleHistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Article"];
    }
    cell.cellFrame = _atricles[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ArticleDetailController *detailController = [[ArticleDetailController alloc] init];
    AricleCellFrame *cellFrame = _atricles[indexPath.row];
    detailController.aricle = cellFrame.aricle;
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"投稿详情";
    self.navigationItem.backBarButtonItem = backItem;
    
    [self.navigationController pushViewController:detailController animated:YES];
}
@end
