//
//  ArticleEditController.m
//  Moments
//
//  Created by xihan on 15/12/20.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ArticleEditController.h"
#import "ImageShowCell.h"
#import "EzlPhotoPicker.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MomentsService.h"
#import "ImageBrowser.h"
#import "EzlPhotoManager.h"
#import "UIView+Frame.h"
#import "WebImage.h"
#import "MLearningService.h"
#import "ArticleHistoryController.h"
#import <AssetsLibrary/AssetsLibrary.h>

const NSInteger kMaxArticleContentLen = 1024;
@interface ArticleEditController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>
{
    NSLayoutConstraint *_collectionH;
}
@property (nonatomic, strong) UILabel *infoLabel;
@property (nonatomic, strong) UITextField *titleTF;
@property (nonatomic, strong) UITextField *subTitleTF;
@property (nonatomic, strong) UITextView *contentTV;
@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;
@property (strong, nonatomic) UILabel *textLen;

@property (nonatomic, weak)   EzlPhotoManager *photoManager;
@property (strong, nonatomic) UIButton *sendBtn;

@end

@implementation ArticleEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.photoManager = [EzlPhotoManager shareManager];
    
    [self p_InitBaseView];
    [self p_InitConstraints];
    [self p_InitNavigationBar];
    [self p_AddBackgroundTap];
    
    [MLearningService getArticleInfoWithHandler:^(HandleResult result, NSString *dataStr) {
        if (result != HandleResultSuccess) { return ; }
        _infoLabel.text = dataStr;
        [self.view needsUpdateConstraints];
        [self.view updateConstraintsIfNeeded];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"pickImageFinish" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:) name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewTextDidChange:) name:UITextViewTextDidChangeNotification object:nil];

}



- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)articleHistroy{
    ArticleHistoryController *historyVC = [[ArticleHistoryController alloc] init];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"投稿历史";
    self.navigationItem.backBarButtonItem = backItem;
    [self.navigationController pushViewController:historyVC animated:YES];
}

- (void)p_InitNavigationBar{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [btn setTitle:@"投稿" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0,0)];
    [btn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = backItem;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"历史" style:UIBarButtonItemStylePlain target:self action:@selector(articleHistroy)];

}



- (void)p_InitBaseView{
    CGFloat width = self.view.frame.size.width;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.contentSize = CGSizeMake(width, 494);
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_scrollView];
    
    _contentView = [[UIView alloc] init];
    _contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [_scrollView addSubview:_contentView];
    
    _infoLabel = [[UILabel alloc] init];
    _infoLabel.textColor = [UIColor blackColor];
    _infoLabel.font = [UIFont systemFontOfSize:16];
    _infoLabel.numberOfLines = 0;
    _infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _infoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [_contentView addSubview:_infoLabel];
    
    _titleTF = [[UITextField alloc] init];
    _titleTF.placeholder = @"标题";
    _titleTF.textColor = [UIColor blackColor];
    _titleTF.font = [UIFont systemFontOfSize:16];
    _titleTF.translatesAutoresizingMaskIntoConstraints = NO;
    _titleTF.borderStyle = UITextBorderStyleRoundedRect;
    _titleTF.delegate = self;
    [_contentView addSubview:_titleTF];
    
    _subTitleTF = [[UITextField alloc] init];
    _subTitleTF.placeholder = @"副标题";
    _subTitleTF.textColor = [UIColor blackColor];
    _subTitleTF.font = [UIFont systemFontOfSize:16];
    _subTitleTF.translatesAutoresizingMaskIntoConstraints = NO;
    _subTitleTF.borderStyle = UITextBorderStyleRoundedRect;
    _subTitleTF.delegate = self;
    [_contentView addSubview:_subTitleTF];
    
    _contentTV = [[UITextView alloc] init];
    _contentTV.font = [UIFont systemFontOfSize:17];
    _contentTV.layer.masksToBounds = YES;
    _contentTV.layer.cornerRadius = 7;
    _contentTV.layer.borderWidth = 2;
    _contentTV.layer.borderColor = [[UIColor orangeColor] CGColor];
    _contentTV.textAlignment = NSTextAlignmentLeft;
    _contentTV.translatesAutoresizingMaskIntoConstraints = NO;
    _contentTV.delegate = self;
    [_contentView addSubview:_contentTV];
    
    _textLen = [[UILabel alloc] init];
    _textLen.font = _contentTV.font;
    _textLen.translatesAutoresizingMaskIntoConstraints = NO;
    _textLen.textColor = [UIColor grayColor];
    _textLen.text = [NSString stringWithFormat:@"0/%@",@(kMaxArticleContentLen)];
    [_contentView addSubview:_textLen];
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerClass:[ImageShowCell class] forCellWithReuseIdentifier:@"photoCell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [_contentView addSubview:_collectionView];
    
    _sendBtn = [[UIButton alloc] init];
    [_sendBtn setTitle:@"投稿" forState:UIControlStateNormal];
    [_sendBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    _sendBtn.layer.masksToBounds = YES;
    _sendBtn.layer.cornerRadius = 8;
    _sendBtn.layer.borderColor = [[UIColor blackColor] CGColor];
    _sendBtn.layer.borderWidth = 1;
    [_sendBtn addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_sendBtn addTarget:self action:@selector(btnTouchCancel:) forControlEvents:UIControlEventTouchUpOutside];
    [_sendBtn addTarget:self action:@selector(uploadArticle:) forControlEvents:UIControlEventTouchUpInside];
    _sendBtn.translatesAutoresizingMaskIntoConstraints = NO;
    _sendBtn.enabled = NO;
    [_contentView addSubview:_sendBtn];
}

- (void)p_InitConstraints{
    
    NSDictionary *views = @{ @"infoLabel" : _infoLabel, @"titleTF":_titleTF, @"subTitleTF":_subTitleTF, @"contentTV":_contentTV, @"collectionView" : _collectionView, @"sendBtn" : _sendBtn, @"textLen" : _textLen };
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"scrollView" : _scrollView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"scrollView" : _scrollView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentView(width)]" options:NSLayoutFormatDirectionLeftToRight metrics:@{@"width":@(self.view.frame.size.width)} views:@{@"contentView" : _contentView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contentView(494)]" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"contentView" : _contentView}]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[infoLabel]-20-[titleTF(40)]-5-[subTitleTF(40)]-10-[contentTV(180)]-10-[textLen]-10-[collectionView]-10-[sendBtn(40)]" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[infoLabel]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[textLen]" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[titleTF]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[subTitleTF]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[contentTV]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[collectionView]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[sendBtn]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];

    _collectionH = [NSLayoutConstraint constraintWithItem:_collectionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:70];
    [self.view addConstraint:_collectionH];
}

- (void)btnTouchDown:(UIButton *)btn{
    btn.backgroundColor = [UIColor lightGrayColor];
}

- (void)btnTouchCancel:(UIButton *)btn{
    btn.backgroundColor = [UIColor whiteColor];
}


- (void)uploadArticle:(UIButton *)btn{
    [self waitting];
    btn.backgroundColor = [UIColor whiteColor];
    
    NSMutableArray *array = [NSMutableArray array];
    if ([self.photoManager numberOfSeleted]> 0) {
        for (NSInteger i = 0; i< [self.photoManager numberOfSeleted]; i++) {
            EzlPhoto *photo = [self.photoManager selectedPhotoWithIndex:i];
            NSString *imgStr = [photo.imageData base64EncodedStringWithOptions:0];
            [array addObject:imgStr];
        }
    }

    [MLearningService uploadArticleWithTitle:_titleTF.text subTitle:_subTitleTF.text content:_contentTV.text images:array handler:^(HandleResult result, BOOL dataResult) {
        [self stopWaitting];
        if (result != HandleResultSuccess) {
            [LrhAlertView showMessage:@"获取服务器信息失败"];
        }
        else if (dataResult) {
            [LrhAlertView showMessage:@"投稿成功" handler:^{
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        else{
            [LrhAlertView showMessage:@"投稿失败，请删除部分文字后重试"];
        }
    }];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    NSInteger seletedPhotos = self.photoManager.seletedPhotos.count ;
    if (seletedPhotos < 3) {
        return seletedPhotos + 1;
    }
    return seletedPhotos;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageShowCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    NSInteger seletedPhotos = self.photoManager.seletedPhotos.count ;
    
    if (seletedPhotos < 3 && indexPath.row == seletedPhotos) {
        cell.imageView.image = [UIImage imageNamed:@"article_photo_pressed"];
    }
    else{
        EzlPhoto *photo = self.photoManager.seletedPhotos[indexPath.row];
        cell.imageView.image = photo.thumbnail;
        CGRect frame = [cv convertRect:cell.frame toView:self.view];
        frame.origin.y += 64;
        photo.originFrame = frame;
    }
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat wh = 60;
    return CGSizeMake(wh, wh);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    __unsafe_unretained ArticleEditController* unSelf = self;
    if (indexPath.row == [self.photoManager numberOfSeleted]) {
        [EzlPhotoPicker showInSuperVC:self pickImage:^(NSArray *images) {
            [unSelf reloadData];
        }];
    }
    else{
        ImageBrowser *browser = [[ImageBrowser alloc] init];
        browser.editEnable = YES;
        [browser imageDidChange:^{
            [unSelf reloadData];
        }];
        [browser showWithImages:self.photoManager.seletedPhotos startIndex:indexPath.row];
    }
}


- (void)reloadData{
    CGFloat btnWH = 60;
    CGFloat width = CGRectGetWidth(self.collectionView.bounds);
    NSInteger number = width / btnWH;
    NSInteger count = self.photoManager.seletedPhotos.count + 1;
    NSInteger currentLine = count / number;
    if (count % number != 0) { currentLine++; }
    _collectionH.constant = (btnWH + 6) * currentLine;
    [self.view needsUpdateConstraints];
    [self.view updateConstraintsIfNeeded];
    [self.collectionView reloadData];
}

#pragma mark-
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.25 animations:^{
        _scrollView.contentOffset = CGPointMake(0, textView.frame.origin.y);
    }];
    _scrollView.scrollEnabled = NO;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.25 animations:^{
        _scrollView.contentOffset = CGPointMake(0, 0);
    }];
    _scrollView.scrollEnabled = YES;
    return YES;
}

- (void)textViewTextDidChange:(NSNotification *)noti{
    [self submitEnable];
    
    NSInteger length = _contentTV.text.length;
    UITextRange *selectedRange = [_contentTV markedTextRange];
    UITextPosition *position = [_contentTV positionFromPosition:selectedRange.start offset:0];
    if (!position) {
        if (length > kMaxArticleContentLen) {
            _contentTV.text = [_contentTV.text substringToIndex:kMaxArticleContentLen];
            _textLen.text = [NSString stringWithFormat:@"%@/%@",@(kMaxArticleContentLen),@(kMaxArticleContentLen)];
        }
        else{
            _textLen.text = [NSString stringWithFormat:@"%@/%@",@(length),@(kMaxArticleContentLen)];
        }
        
    }
}


- (void)textFieldTextDidChange:(NSNotification *)noti{
    [self submitEnable];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)submitEnable{
    BOOL canSubmit = _contentTV.text.length > 0 && _titleTF.text.length > 0 && _subTitleTF.text.length > 0;
    if (canSubmit == _sendBtn.enabled) { return; }
    _sendBtn.enabled = canSubmit;
    [_sendBtn setTitleColor:canSubmit ? [UIColor orangeColor] : [UIColor lightGrayColor] forState:UIControlStateNormal];
}

#pragma mark- Background Tap
- (void)p_AddBackgroundTap{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tap.delegate = self;
    [tap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tap];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UITextView class]]) {
        return NO;
    }
    if ([touch.view isKindOfClass:[UITextField class]]) {
        return NO;
    }
    [self hideKeyboard];
    return NO;
}

- (void)hideKeyboard{
    [_contentTV resignFirstResponder];
    [_titleTF resignFirstResponder];
    [_subTitleTF resignFirstResponder];
}

- (void)dealloc{
    [[EzlPhotoManager shareManager] free];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
