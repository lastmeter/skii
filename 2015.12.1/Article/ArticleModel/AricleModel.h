//
//  AricleModel.h
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AricleModel : NSObject

@property (nonatomic, weak) NSDictionary *aricleDic;

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *subTitle;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSArray *attach;
@property (nonatomic, readonly) NSString *dateString;
@property (nonatomic, readonly) NSString *content;
@end
