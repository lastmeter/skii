//
//  AricleCellFrame.h
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AricleModel.h"

@interface AricleCellFrame : NSObject

@property (nonatomic, strong) AricleModel *aricle;

@property (nonatomic, readonly) CGRect titleFrame;
@property (nonatomic, readonly) CGRect subTitleFrame;
@property (nonatomic, readonly) CGRect statusFrame;
@property (nonatomic, readonly) CGFloat cellHeight;


+ (UIFont *)titleFont;
+ (UIFont *)subTitleFont;
+ (UIFont *)statusFont;

@end
