//
//  AricleModel.m
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "AricleModel.h"
#import "NSDate+String.h"
#import "NSString+Json.h"

@implementation AricleModel

- (void)setAricleDic:(NSDictionary *)aricleDic{
    _title      =   aricleDic[@"title"];
    
    NSTimeInterval timeSp = [aricleDic[@"time"] doubleValue] / 1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeSp];
    _dateString = [NSString stringWithFormat:@"时间:%@",[date string]];
    
    _subTitle   =   aricleDic[@"subTitle"];
    _status     =   [self statusTextWithStatus:[aricleDic[@"status"] integerValue]];
    _attach     =   [aricleDic[@"attach"] toJsonObject];
    _content    =   aricleDic[@"content"];
}

- (NSString *)statusTextWithStatus:(NSInteger)status{
    switch (status) {
        case 0:
            return @"待审核";
        case 1:
            return @"已采用";
        case -1:
            return @"已弃用";
    }
    return @"";
}

@end
