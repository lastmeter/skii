//
//  AricleCellFrame.m
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "AricleCellFrame.h"
#import "NSString+Size.h"
#import "NSString+Json.h"
#import "WebImage.h"
#import "NSDate+String.h"

@implementation AricleCellFrame


- (void)setAricleDic:(NSDictionary *)aricleDic{


//    NSArray *attach = [aricleDic[@"attach"] toJsonObject];
//    NSMutableArray *array = [NSMutableArray arrayWithCapacity:attach.count];
//    [attach enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        WebImage *image = [[WebImage alloc] init];
//        image.urlString = obj[@"url"];
//        [array addObject:image];
//    }];
}

- (void)setAricle:(AricleModel *)aricle{
    
    _aricle = aricle;
    
    CGFloat contentW = [UIScreen mainScreen].bounds.size.width - 15;
    CGFloat statusW = 80;
    
    CGSize titleSize = [[NSString stringWithFormat:@"标题:%@",aricle.title] textSizeWithWidth:contentW Font:[AricleCellFrame titleFont]];
    
    _titleFrame     =   (CGRect){ CGPointMake(15, 5), titleSize };
    _subTitleFrame  =   (CGRect){ CGPointMake(15, titleSize.height + 5), 200, 21 };
    _cellHeight     =   CGRectGetMaxY(_subTitleFrame) + 5;
    _statusFrame    =   CGRectMake(contentW - statusW, _cellHeight - 26, statusW, 21);
}


+ (UIFont *)titleFont{
    return [UIFont systemFontOfSize:16];
}

+ (UIFont *)subTitleFont{
    return [UIFont systemFontOfSize:15];
}

+ (UIFont *)statusFont{
    return [UIFont systemFontOfSize:15];
}


@end
