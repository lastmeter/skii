//
//  ArticleDetailController.m
//  Moments
//
//  Created by xihan on 15/12/21.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ArticleDetailController.h"
#import "ImageShowCell.h"
#import "NSString+Size.h"
#import "AricleModel.h"
#import "WebImage.h"
#import "WebImageBrowser.h"
#import "NSString+Size.h"

#import "UIImageView+WebCache.h"

@interface ArticleDetailController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@property (nonatomic, strong) UITextView *contentTV;
@property (nonatomic, strong) UIView *line1;
@property (nonatomic, strong) UIView *line2;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, copy) NSArray *attach;
@property (nonatomic, strong) NSLayoutConstraint *contentH;
@end

@implementation ArticleDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_InitBaseView];
    [self p_InitConstraints];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _titleLabel.text = _aricle.title;
    _subTitleLabel.text = _aricle.subTitle;
    _contentTV.text = _aricle.content;
    
    if (_aricle.subTitle.length == 0) {
        _line2.hidden = YES;
    }
    
    CGSize size = [_aricle.content textSizeWithWidth:_contentTV.frame.size.width Font:_contentTV.font];
    if (size.height > 70) {
        _contentH.constant = size.height + 16;
    }
    
    [_collectionView reloadData];
    [self.view needsUpdateConstraints];
    [self.view layoutIfNeeded];
    
}

- (void)p_InitBaseView{
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.font = [UIFont systemFontOfSize:19];
    _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _titleLabel.numberOfLines = 0;
    [self.view addSubview:_titleLabel];
    
    _subTitleLabel = [[UILabel alloc] init];
    _subTitleLabel.textColor = [UIColor blackColor];
    _subTitleLabel.font = [UIFont systemFontOfSize:17];
    _subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _subTitleLabel.numberOfLines = 0;
    _subTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_subTitleLabel];
    
    _line1 = [[UIView alloc] init];
    _line1.backgroundColor = [UIColor lightGrayColor];
    _line1.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_line1];
    
    _line2 = [[UIView alloc] init];
    _line2.backgroundColor = [UIColor lightGrayColor];
    _line2.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_line2];
    
    _contentTV = [[UITextView alloc] init];
    _contentTV.font = [UIFont systemFontOfSize:17];
    _contentTV.layer.masksToBounds = YES;
    _contentTV.layer.cornerRadius = 7;
    _contentTV.layer.borderWidth = 2;
    _contentTV.layer.borderColor = [[UIColor orangeColor] CGColor];
    _contentTV.textAlignment = NSTextAlignmentLeft;
    _contentTV.translatesAutoresizingMaskIntoConstraints = NO;
    _contentTV.textColor = [UIColor blackColor];
    [self.view addSubview:_contentTV];
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerClass:[ImageShowCell class] forCellWithReuseIdentifier:@"photoCell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_collectionView];
}

- (void)p_InitConstraints{
    NSDictionary *views = @{ @"titleLabel":_titleLabel, @"subTitleLabel":_subTitleLabel, @"contentTV":_contentTV, @"collectionView" : _collectionView, @"line1":_line1, @"line2":_line2};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[titleLabel]-3-[line1(1)]-15-[subTitleLabel]-3-[line2(1)]-30-[contentTV]-30-[collectionView(80)]" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[titleLabel]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[subTitleLabel]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[contentTV]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line1]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[line2]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[collectionView]-20-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:views]];

    
    _contentH = [NSLayoutConstraint constraintWithItem:_contentTV attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:190];
   [self.view addConstraint:_contentH];
    
    
}

- (void)setAricle:(AricleModel *)aricle{
    _aricle = aricle;
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:self.aricle.attach.count];
    [self.aricle.attach enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        WebImage *image = [[WebImage alloc] init];
        image.urlString = obj[@"url"];
        [array addObject:image];
    }];
    _attach = array;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return _attach.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageShowCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    WebImage *photo = _attach[indexPath.row];

    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:photo.urlString]];

    CGRect converRect = [cv convertRect:cell.frame toView:self.view];
    converRect.origin.y += 64;
    photo.originFrame = converRect;
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat wh = ( CGRectGetWidth(collectionView.frame) - 10 ) / 3;
    return CGSizeMake(wh, wh);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WebImageBrowser *imageBrowser = [[WebImageBrowser alloc] init];
    [imageBrowser showImageWithWebImages:self.attach startIndex:indexPath.row];
}

@end
