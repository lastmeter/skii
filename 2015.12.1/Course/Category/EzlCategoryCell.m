//
//  EzlCategoryCell.m
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlCategoryCell.h"

@implementation EzlCategoryCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *norBG = [self backgroundViewWithColor:[UIColor colorWithRed:184.0/255.0 green:184.0/255.0 blue:184.0/255.0 alpha:1]];
        norBG.frame = self.bounds;
        self.backgroundView = norBG;
        
        UIView *selBG = [self backgroundViewWithColor:[UIColor colorWithRed:183.0/255.0 green:171.0/255.0 blue:139.0/255.0 alpha:1]];
        selBG.frame = self.bounds;
        self.selectedBackgroundView = selBG;
        
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.font = [UIFont systemFontOfSize:16];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.contentView addSubview:_textLabel];

    }
    return self;
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
}

- (UIView *)backgroundViewWithColor:(UIColor *)color{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = color;
    view.layer.cornerRadius = 8;
    view.layer.masksToBounds = true;
    view.layer.borderWidth = 2;
    view.layer.borderColor = [[UIColor grayColor] CGColor];
    return view;
}

@end
