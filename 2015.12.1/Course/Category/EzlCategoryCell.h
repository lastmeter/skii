//
//  EzlCategoryCell.h
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface EzlCategoryCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *textLabel;

@end
