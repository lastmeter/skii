//
//  EzlCategoryView.m
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlCategoryView.h"
#import "EzlCategoryCell.h"

@interface EzlCategoryView ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger currentIndex;
@end

@implementation EzlCategoryView


- (instancetype)init{
    self = [super init];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[EzlCategoryCell class] forCellWithReuseIdentifier:@"category"];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:_collectionView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[collectionView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"collectionView":_collectionView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[collectionView]-10-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{@"collectionView":_collectionView}]];
        
        _currentIndex = 0;
    }
    return self;
}

- (void)reloadData{
    [_collectionView reloadData];
    if (_categorys.count > 0) {
        [_collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionTop];
    }
  
}

- (CGFloat)viewHeight{
    if (_categorys.count == 0) { return 0; }

    NSInteger numberOfLine = (_categorys.count + 1) / 3;
    if ((_categorys.count + 1) % 3 != 0) { numberOfLine++; }
    return 52 * numberOfLine;

}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (_categorys.count > 0) {
        return _categorys.count + 1;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EzlCategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"category" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"全部类型";
    }
    else{
        NSDictionary *dic = _categorys[indexPath.row - 1];
        cell.textLabel.text = dic[@"name"];
    }
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = CGRectGetWidth(self.collectionView.bounds) - 20;
    if (_categorys.count > 2) {
         return CGSizeMake(width / 3, 42);
    }
    return CGSizeMake(width / 2, 42);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != _currentIndex) {
        _currentIndex = indexPath.row;
        [_delegate didSelectCategoryWithIndex:indexPath.row];
    }
}
@end
