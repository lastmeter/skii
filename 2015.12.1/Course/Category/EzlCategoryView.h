//
//  EzlCategoryView.h
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EzlCategoryViewDelegate <NSObject>

- (void)didSelectCategoryWithIndex:(NSInteger)index;

@end

@interface EzlCategoryView : UIView

@property (nonatomic, weak) id<EzlCategoryViewDelegate>delegate;
@property (nonatomic, weak) NSArray *categorys;

@property (nonatomic, assign) CGFloat viewHeight;

- (void)reloadData;
@end
