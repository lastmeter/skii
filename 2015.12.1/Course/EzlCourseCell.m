//
//  EzlCourseCell.m
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlCourseCell.h"
#import "UIImageView+WebCache.h"

@implementation EzlCourseCell
{
    UILabel *_titleLabel;
    UIButton *_favoriteBtn;
    UIImageView *_imageView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        CGFloat favoriteBtnWH = 20;
        
        _imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, CGRectGetWidth(frame), 0.84 * CGRectGetHeight(frame))];
        _imageView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_imageView];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_imageView.frame), CGRectGetWidth(_imageView.frame) - favoriteBtnWH, CGRectGetHeight(frame) -  CGRectGetHeight(_imageView.frame))];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_titleLabel];
        
        _favoriteBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_titleLabel.frame), CGRectGetMinY(_titleLabel.frame), favoriteBtnWH,CGRectGetHeight(_titleLabel.frame))];
        [_favoriteBtn addTarget:self action:@selector(favoriteBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_favoriteBtn];
    }
    return self;
}

- (void)setCourse:(CourseModel *)course{
    _course = course;
    [_favoriteBtn setImage:[UIImage imageNamed:course.isCollected ? @"favorite_normal" : @"favorite_cancel_normal"] forState:UIControlStateNormal];
    _titleLabel.text = course.title;
    if (course.imgPath) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:course.imgPath]];
    }
}

- (void)favoriteBtnClick:(UIButton *)btn{
    if (_course.isCollected) {
        [_delegate removeFromFavorites:_course index:self.tag];
    }
    else{
        [_delegate addToFavorites:_course index:self.tag];
    }
}
@end
