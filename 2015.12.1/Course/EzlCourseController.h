//
//  EzlCourseController.h
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "NEzlViewController.h"

@interface EzlCourseController : NEzlViewController

- (instancetype)initWithSiteTitle:(NSString *)title siteId:(NSString *)siteId;

@end
