//
//  EzlCourseController.m
//  EZLearning
//
//  Created by xihan on 15/12/24.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlCourseController.h"
#import "EzlCourseCell.h"
#import "EzlCategoryView.h"
#import "MLearningService.h"

@interface EzlCourseController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, EzlCourseCellDelegate, EzlCategoryViewDelegate>

@property (nonatomic, copy) NSString *siteTitle;
@property (nonatomic, copy) NSString *siteId;

@property (nonatomic, strong) NSMutableArray *courseArray;
@property (nonatomic, strong) NSArray *categoryArray;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) EzlCategoryView *categoryView;
@property (nonatomic, strong) NSLayoutConstraint *heightContraint;

@end

@implementation EzlCourseController

- (instancetype)initWithSiteTitle:(NSString *)title siteId:(NSString *)siteId{
    self = [super init];
    if (self) {
        _siteId = siteId;
        _siteTitle = title;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [self p_InitNavigationBar];
    [self p_InitBaseView];
    [self p_InitConstraints];
    [self p_LoadData];
}


- (void)p_InitNavigationBar{

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0,0)];
    [btn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    

    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 140, 30)];
    [btn2 setTitle:self.siteTitle forState:UIControlStateNormal];
    [btn2 setTitleEdgeInsets:UIEdgeInsetsMake(0, -58, 0,0)];
    btn2.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem2 = [[UIBarButtonItem alloc] initWithCustomView:btn2];
    
    self.navigationItem.leftBarButtonItems = @[backItem, backItem2];
}

- (void)p_InitBaseView{
    _categoryView = [[EzlCategoryView alloc] init];
    _categoryView.delegate = self;
    [self.view addSubview:_categoryView];
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_collectionView registerClass:[EzlCourseCell class] forCellWithReuseIdentifier:@"search"];
    [self.view addSubview:_collectionView];
}

- (void)p_InitConstraints{
    _heightContraint = [NSLayoutConstraint constraintWithItem:_categoryView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:1];
    _heightContraint.priority = 780;
    [self.view addConstraint:_heightContraint];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[categoryView]-1-[collectionView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"collectionView" : _collectionView, @"categoryView" : _categoryView }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[categoryView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"categoryView" : _categoryView }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[collectionView]-5-|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"collectionView" : _collectionView }]];
    

}

- (void)p_LoadData{
    
    [self waitting];
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group,  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [MLearningService getCourseBySiteId:_siteId handler:^(HandleResult result, NSArray *dataArray) {
            if (result == HandleResultSuccess) { _courseArray = [NSMutableArray arrayWithArray:dataArray] ; }
            dispatch_semaphore_signal(sem);
        }];
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_async(group,  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_t sem = dispatch_semaphore_create(0);
        [MLearningService getCategoryBySiteId:_siteId handler:^(HandleResult result, NSArray *dataArray) {
           
            if (result == HandleResultSuccess) { _categoryArray = dataArray; }
            
            dispatch_semaphore_signal(sem);
        }];
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        _categoryView.categorys = _categoryArray;
        [_categoryView reloadData];
        
        _heightContraint.constant = _categoryView.viewHeight;
        
        [_collectionView reloadData];
        [self stopWaitting];
    });

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)dismiss{
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return _courseArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EzlCourseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"search" forIndexPath:indexPath];
    cell.course = _courseArray[indexPath.row];
    cell.delegate = self;
    cell.tag = indexPath.row;
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = CGRectGetWidth(_collectionView.bounds);
    return CGSizeMake(0.5 * width - 10, isPad ? 240 : 140);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 0, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CourseModel * tmp = _courseArray[indexPath.row];
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    [delegate pushLearningViewControllerWithTitle:tmp.title Url:tmp.url];
}

#pragma mark- Course Cell
- (void)addToFavorites:(CourseModel *)course index:(NSInteger)index{
    if (!_siteTitle) {
        return;
    }
    CourseModel * tmp = _courseArray[index];
    WEAKSELF
    [MLearningService addToFavorites:tmp siteTitle:_siteTitle handler:^(HandleResult result, BOOL dataResult) {
        if (!weakSelf) {
            return ;
        }
        if (dataResult) {
            tmp.collected = YES;
            _courseArray[index] = tmp;
            [_collectionView reloadData];
        }
    }];
}

- (void)removeFromFavorites:(CourseModel *)course index:(NSInteger)index{
    WEAKSELF
    CourseModel * tmp = _courseArray[index];
    [MLearningService removeFromFavorites:tmp handler:^(HandleResult result, BOOL dataResult) {
        if (!weakSelf) {
            return ;
        }
        if (dataResult) {
            tmp.collected = NO;
            _courseArray[index] = tmp;
            [_collectionView reloadData];
        }
    }];
}

#pragma mark- Category View
- (void)didSelectCategoryWithIndex:(NSInteger)index{
    [self waitting];
    if (index == 0) {
        [MLearningService getCourseBySiteId:_siteId handler:^(HandleResult result, NSArray *dataArray) {
            if (result == HandleResultSuccess) {
                _courseArray = [NSMutableArray arrayWithArray:dataArray];
            }
            else{
                _courseArray = nil;
            }
            [_collectionView reloadData];
            [self stopWaitting];
        }];
    }
    else{
        NSDictionary *dic = _categoryArray[index - 1];
        [MLearningService getCourseByCategoryId:dic[@"id"] siteId:_siteId handler:^(HandleResult result, NSArray *dataArray) {
            if (result == HandleResultSuccess) {
                _courseArray = [NSMutableArray arrayWithArray:dataArray];
            }
            else{
                _courseArray = nil;
            }
            [_collectionView reloadData];
            [self stopWaitting];
        }];
    }
}

@end
