//
//  EzlCourseCell.h
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseModel.h"

@protocol EzlCourseCellDelegate <NSObject>

- (void)addToFavorites:(CourseModel *)course index:(NSInteger)index;
- (void)removeFromFavorites:(CourseModel *)course index:(NSInteger)index;

@end

@interface EzlCourseCell : UICollectionViewCell

@property (nonatomic, weak) CourseModel *course;
@property (nonatomic, weak) id<EzlCourseCellDelegate>delegate;
@end
