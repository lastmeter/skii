//
//  SelectDateViewController.m
//  EZLearning
//
//  Created by widhor on 16/2/19.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "SelectDateViewController.h"
#import "ShareReportViewController.h"


@interface SelectDateViewController (){
    AppDelegate *appDelegate;
}
@property (weak, nonatomic) IBOutlet UIDatePicker *startDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *endDate;

@end

@implementation SelectDateViewController
- (IBAction)startValueChaged:(id)sender {
    appDelegate.startDate = [_startDate date];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)endValueChanged:(id)sender {
    appDelegate.endDate = [_endDate date];
}
- (IBAction)confirm:(id)sender {
    ShareReportViewController *shareReport = [[ShareReportViewController alloc] init ];
    [self.navigationController pushViewController:shareReport animated:YES];

//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.startDate = [NSDate date];
    appDelegate.endDate = [NSDate date];
}


@end
