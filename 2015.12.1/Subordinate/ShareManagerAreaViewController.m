
//
//  ShareManagerAreaViewController
//  EZLearning
//
//  Created by widhor on 16/2/19.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "ShareManagerAreaViewController.h"
#import "MomentsService.h"
#import "ShareManagerTableViewCell.h"


@interface ShareManagerAreaViewController ()<UITableViewDataSource, UITableViewDelegate>{
    AppDelegate *appDelegate;
    NSMutableArray *tempArray;
}
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ShareManagerAreaViewController
- (instancetype)init{
    self = [super init];
    if (self) {
        self.title = @"经理区域";
        self.tabBarItem.image = [UIImage imageNamed:@"managerArea"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"managerArea_click"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = view;
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-64-[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tableView" : _tableView }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tableView" : _tableView }]];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    g_ShowLoadingView();
    [MomentsService getMomentsStatByManager:[appDelegate.startDate timeIntervalSince1970] * 1000 beforeDate:[appDelegate.endDate timeIntervalSince1970] * 1000 handler:^(HandleResult result, NSArray *dataArray) {
        g_HideLoadingView();
        tempArray = [[NSMutableArray alloc] init];
        [tempArray addObject: [[NSDictionary alloc] init]];
        for(int i = 0; i < [dataArray count]; i++)
        {
            id obj = [[dataArray objectAtIndex:i] copy];
            [tempArray addObject: obj];
        }
        
        [self.tableView reloadData];
    }];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tempArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShareManagerTableViewCell *cell = [[ShareManagerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"market"];
    
    cell.dicData = tempArray[indexPath.row];
    if (indexPath.row == 0) {
        cell.title = YES;
    }else{
        cell.title = NO;
    }
    
    
    return cell;
}

@end

