//
//  ShareSuperTableViewCell.m
//  EZLearning
//
//  Created by widhor on 16/2/20.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "ShareSuperTableViewCell.h"
#define cellHeight 44
@interface ShareSuperTableViewCell()
{
    UILabel *market, *count, *charge;
    
}
@end
@implementation ShareSuperTableViewCell

- (void)setTitle:(BOOL)title{
    if (title) {
        market.font = [UIFont boldSystemFontOfSize:22];
        market.text = @"市场";
        count.font = [UIFont boldSystemFontOfSize:22];
        count.text = @"晒单数";
        charge.font = [UIFont boldSystemFontOfSize:22];
        charge.text = @"主管";
    }
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        const CGFloat labelHeight = 21;
        const CGFloat labelOriginY = (cellHeight - labelHeight)*0.5;
        UIFont *font = [UIFont boldSystemFontOfSize:17];
        charge = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH * 0.1, labelOriginY, 150, labelHeight)];
        charge.font = font;
        charge.textColor = [UIColor blackColor];
        [self.contentView addSubview:charge];
        
        market = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH * 0.45, labelOriginY, MAIN_SCREEN_WIDTH * 0.25, labelHeight)];
        market.font = font;
        market.textColor = [UIColor blackColor];
        [self.contentView addSubview:market];
        
        count = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH * 0.75, 0, MAIN_SCREEN_WIDTH * 0.4, 44)];
        count.font = font;
        count.numberOfLines =2;
        count.lineBreakMode = NSLineBreakByWordWrapping;
        count.textColor = [UIColor blackColor];
        [self.contentView addSubview:count];
    }
    return self;
}
- (void)setDicData:(NSDictionary *)dic{
    market.text = dic[@"market"];
    count.text = dic[@"count"];
    charge.text = dic[@"charge"];
}

@end
