//
//  ShareReportViewController.m
//  EZLearning
//
//  Created by widhor on 16/2/19.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "ShareReportViewController.h"
#import "SubordinateListController.h"
#import "ChatViewController.h"
#import "ReviewService.h"
#import "SubordinateRankViewController.h"
#import "SubordinateListViewController.h"
#import "ShareMarketViewController.h"
#import "ShareSuperAreaViewController.h"
#import "ShareManagerAreaViewController.h"
#import "SelectDateViewController.h"
@interface ShareReportViewController ()< UITabBarControllerDelegate>{
    UIView *navBar;
    UIButton *_searchBtn;
}
@end

@implementation ShareReportViewController
- (void)setTitle:(NSString *)title backClick:(BackBlock)block{
    
    EzlTopBar *topBar = [[EzlTopBar alloc] initWithTitle:title backClick:block];
    [self.view addSubview:topBar];
    
}
- (instancetype)init{
    self = [super init];
    if (self) {
        self.tabBar.tintColor = [UIColor colorWithRed:147.0/255.0 green:0 blue:0 alpha:1];
    }
    return self;
}
- (void)addNavBarView
{
    navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navBar.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(56, 25, self.view.frame.size.width-100, 36)];
    lable.font = [UIFont boldSystemFontOfSize:17];
    [navBar addSubview:lable];
    [self.view addSubview:navBar];
}
- (void)searchCourse:(UIButton *)btn{
    
    SelectDateViewController * selectDateViewController = [[SelectDateViewController alloc] init];
    [self presentViewController:selectDateViewController animated:YES completion:nil];

}


- (void)viewDidLoad {
    [super viewDidLoad];
   [self addNavBarView];
    WEAKSELF
    [self setTitle:@"时间选择" backClick:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];

    ShareMarketViewController *marketViewController = [[ShareMarketViewController alloc] init];
    ShareSuperAreaViewController * superAreaViewController = [[ShareSuperAreaViewController alloc] init];
    ShareManagerAreaViewController * managerAreaViewController = [[ShareManagerAreaViewController alloc] init];
    
//    _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 40, 25, 30, 30)];
//    [_searchBtn setImage:[UIImage imageNamed:@"check_score_normal"] forState:UIControlStateNormal];
//    [_searchBtn setImage:[UIImage imageNamed:@"check_score_pressed"] forState:UIControlStateHighlighted];
//    [self.view addSubview:_searchBtn];
//    [_searchBtn addTarget:self action:@selector(searchCourse:) forControlEvents:UIControlEventTouchUpInside];

    self.viewControllers = @[marketViewController, managerAreaViewController,superAreaViewController];
    
}
@end
