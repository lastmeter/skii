//
//  ShareMarketTableViewCell.m
//  EZLearning
//
//  Created by widhor on 16/2/20.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "ShareMarketTableViewCell.h"
#define cellHeight 44
@interface ShareMarketTableViewCell()
{
    UILabel *market, *count;

}
@end
@implementation ShareMarketTableViewCell

- (void)setTitle:(BOOL)title{
    if (title) {
        market.font = [UIFont boldSystemFontOfSize:22];
        market.text = @"市场";
        count.font = [UIFont boldSystemFontOfSize:22];
        count.text = @"晒单数";
    }
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        const CGFloat labelHeight = 21;
        const CGFloat labelOriginY = (cellHeight - labelHeight)*0.5;
        UIFont *font = [UIFont boldSystemFontOfSize:17];
        market = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH * 0.25, labelOriginY, MAIN_SCREEN_WIDTH * 0.25, labelHeight)];
        market.font = font;
        market.textColor = [UIColor blackColor];
        [self.contentView addSubview:market];
        
        count = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH * 0.65, 0, MAIN_SCREEN_WIDTH * 0.4, 44)];
        count.font = font;
        count.numberOfLines =2;
        count.lineBreakMode = NSLineBreakByWordWrapping;
        count.textColor = [UIColor blackColor];
        [self.contentView addSubview:count];
    }
    return self;
}
- (void)setDicData:(NSDictionary *)dic{
    market.text = dic[@"market"];
    count.text = dic[@"count"];
}

@end
