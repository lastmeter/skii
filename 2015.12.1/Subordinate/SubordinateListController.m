//
//  SubordinateListController.m
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "SubordinateListController.h"
#import "EzlUser.h"
#import "ChatViewController.h"
#import "ReviewService.h"
#import "SubordinateRankViewController.h"
#import "SubordinateListViewController.h"
#import "MomentsService.h"
#import "SelectDateViewController.h"


@interface SubordinateListController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) EzlUser *currentUser;
@property (nonatomic, strong) NSArray *contactArray;
@property (nonatomic, assign) BOOL dataLoaded;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SubordinateListController

- (instancetype)init{
    self = [super init];
    if (self) {
        self.title = @"我的团队";
        self.tabBarItem.image = [UIImage imageNamed:@"tab3_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"tab3_sel"];
        self.currentUser = [EzlUser currentUser];
        __unsafe_unretained SubordinateListController *unSelf = self;
        [_currentUser didGetStraffInfo:^(NSDictionary *dic) {
            NSInteger unread = unSelf.currentUser.numberOfUnreadStaff;
            unSelf.tabBarItem.badgeValue = unread ? [@(unread) stringValue] : nil;
        }];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = view;
    [self.view addSubview:_tableView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-64-[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tableView" : _tableView }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"tableView" : _tableView }]];
    
    if (!_isBC) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:@"com.ezl.login" object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!_dataLoaded) {
        [self loadData];
    }
    if (_isBC) {
      [_tableView reloadData];
    }
    NSInteger unread = self.currentUser.numberOfUnreadStaff;
    self.tabBarItem.badgeValue = unread ? [@(unread) stringValue] : nil;
}


- (void)loadData{
    _dataLoaded = YES;

    __unsafe_unretained UITableView *unTable = _tableView;
    __unsafe_unretained SubordinateListController *unSelf = self;
    [_currentUser didGetStraffInfo:^(NSDictionary *dic) {
        [unTable reloadData];
        NSInteger unread = unSelf.currentUser.numberOfUnreadStaff;
        unSelf.tabBarItem.badgeValue = unread ? [@(unread) stringValue] : nil;
    }];
    
    if (_currentUser.userRole == UserRoleBC) {
        _isBC = YES;
        [ReviewService getBossWithHandler:^(HandleResult result, NSArray *dataArray) {
            if (result != HandleResultSuccess) { return ; }
            self.contactArray = dataArray;
            [_tableView reloadData];
        }];
    }
    else{
        _isBC = NO;
        self.contactArray = @[ @"上级/下属列表", @"下属成绩", @"晒单分享统计报表" ];
        [_tableView reloadData];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isBC) {
        return 55;
    }
    return 48;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (!_isBC) {
        return 3;
    }
    return self.contactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contact"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contact"];
       // cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    if (_isBC) {
        
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:99];
        CGFloat padding = MAIN_SCREEN_WIDTH - (isPad ? 100 : 40);
        if (!imageView) {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(padding, 13, 30, 30)];
            imageView.image = [UIImage imageNamed:@"chat_unread"];
            imageView.tag = 99;
            [cell.contentView addSubview:imageView];
        }
        
        UILabel *label = (UILabel *)[cell.contentView viewWithTag:98];
        if (!label) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(padding, 13, 30, 30)];
            label.textColor = [UIColor whiteColor];
            label.tag = 98;
            label.textAlignment = NSTextAlignmentCenter;
            [cell.contentView addSubview:label];
        }
        
        UIImageView *headImageView = (UIImageView *)[cell.contentView viewWithTag:97];
        if (!headImageView) {
            headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 45, 45)];
            headImageView.image = [UIImage imageNamed:@"chat_unread"];
            headImageView.tag = 97;
            [cell.contentView addSubview:headImageView];
        }
        
        UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:96];
        if (!nameLabel) {
            nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(headImageView.frame) + 10, 0, 300, 55)];
            nameLabel.textColor = [UIColor blackColor];
            nameLabel.tag = 96;
            [cell.contentView addSubview:nameLabel];
        }

        
        imageView.hidden = YES;
        label.hidden = YES;
        
        NSDictionary *dic  = self.contactArray[indexPath.row];

        NSString *lastName = dic[@"lastName"];
        NSString *firstName = dic[@"firstName"];
        NSMutableString *name = [NSMutableString string];
        if (firstName) { [name appendString:firstName]; }
        if (lastName)  { [name appendString:lastName]; }
        nameLabel.text = name;
        
        NSDictionary *straffInfo = self.currentUser.straffInfo;
        if (straffInfo[dic[@"id"]]) {
            imageView.hidden = NO;
            label.hidden = NO;
            label.text = [NSString stringWithFormat:@"%@",straffInfo[dic[@"id"]]];
        }
        
        __weak UIImageView *weakView = headImageView;
         headImageView.image = [UIImage imageNamed:@"article_photo_pressed"];
        [MomentsService getHeadPortraitByUserId:dic[@"id"] handler:^(HandleResult result, NSData *data) {
            if (result == HandleResultSuccess) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    weakView.image = image;
                }
           
            }
        }];
        
    }
    else{
        cell.imageView.image = [UIImage imageNamed:indexPath.row == 0 ? @"contactList" : @"rankList"];
        cell.textLabel.text = self.contactArray[indexPath.row];
    }
   
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_isBC) {
        NSDictionary *dic  = self.contactArray[indexPath.row];
        ChatViewController *chat = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
        NSString *lastName = dic[@"lastName"];
        NSString *firstName = dic[@"firstName"];
        NSMutableString *name = [NSMutableString string];
        if (firstName) { [name appendString:firstName]; }
        if (lastName)  { [name appendString:lastName]; }
        
        chat.name = name;
        chat.userId = dic[@"id"];
        NSInteger unread = [self.currentUser.straffInfo[dic[@"id"]] integerValue];
        if (unread) {
            [self.currentUser.straffInfo removeObjectForKey:dic[@"id"]];
            [_tableView reloadData];
            NSInteger unread  = _currentUser.numberOfUnreadStaff;
            self.tabBarItem.badgeValue = unread ? [@(unread) stringValue] : nil;
            [UIApplication sharedApplication].applicationIconBadgeNumber = _currentUser.numberOfAllUnreadMessage;

        }
        [self.navigationController pushViewController: chat animated:YES];
    }
    else{
        if (indexPath.row == 0) {
            SubordinateListViewController *list = [[SubordinateListViewController alloc] initWithNibName:@"SubordinateListViewController" bundle:nil];
            [self.navigationController pushViewController:list animated:YES];

        }
        else if (indexPath.row == 1){
            SubordinateRankViewController *rank = [[SubordinateRankViewController alloc] initWithNibName:@"SubordinateRankViewController" bundle:nil];
            [self.navigationController pushViewController:rank animated:YES];
        }
        else if (indexPath.row == 2){
            SelectDateViewController *selectDate = [[SelectDateViewController alloc] init ];
            [self.navigationController pushViewController:selectDate animated:YES];
        }
       
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
