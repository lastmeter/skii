//
//  ShareSuperTableViewCell.h
//  EZLearning
//
//  Created by widhor on 16/2/20.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareSuperTableViewCell : UITableViewCell
@property (nonatomic, weak)NSDictionary *dicData;
@property (nonatomic, assign)   BOOL title;
@end
