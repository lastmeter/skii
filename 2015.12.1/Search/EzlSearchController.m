//
//  EzlSearchController.m
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlSearchController.h"
#import "EzlCourseCell.h"
#import "MLearningService.h"
#import "AppDelegate.h"

@interface EzlSearchController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>

@property (nonatomic, strong) NSArray *searchResult;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UISearchBar *searchBar;

@end

@implementation EzlSearchController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self p_InitNavigationBar];
    [self p_InitBaseView];
    [self p_InitConstraints];
}

- (void)p_InitNavigationBar{
    self.navigationController.navigationBarHidden = NO;
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [btn setTitle:@"搜索" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setImageEdgeInsets:UIEdgeInsetsMake(0, -10, 0,0)];
    [btn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)p_InitBaseView{
    _searchBar = [[UISearchBar alloc] init];
    _searchBar.delegate = self;
    _searchBar.placeholder = @"搜索";
    
    _searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:_searchBar];
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_collectionView registerClass:[EzlCourseCell class] forCellWithReuseIdentifier:@"search"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:_collectionView];
}

- (void)p_InitConstraints{
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchBar][collectionView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"collectionView" : _collectionView, @"searchBar" : _searchBar }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[searchBar]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"searchBar" : _searchBar }]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[collectionView]|" options:NSLayoutFormatDirectionLeftToRight metrics:nil views:@{ @"collectionView" : _collectionView }]];
}

- (void)dismiss{
    [_searchBar resignFirstResponder];
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
 
    return _searchResult.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EzlCourseCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"search" forIndexPath:indexPath];
    cell.course = _searchResult[indexPath.row];
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(0.5 * MAIN_SCREEN_WIDTH - 10, isPad ? 240 : 140);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 0, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CourseModel * tmp = _searchResult[indexPath.row];
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    [delegate pushLearningViewControllerWithTitle:tmp.title Url:tmp.url];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self waitting];
    [MLearningService getCourseBySearchKey:searchBar.text handler:^(HandleResult result, NSArray *dataArray) {
        [self stopWaitting];
        if (result != HandleResultSuccess) {
            [LrhAlertView showMessage:@"查找不到相关课件"];
            return ;
        }
        NSMutableArray *array = [NSMutableArray array];
        [dataArray enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CourseModel *course = [[CourseModel alloc] init];
            course.url = obj[@"url"];
            course.title = obj[@"title"];
            course.imgPath = obj[@"imagePath"];
            course.tag = idx;
            course.pass = [obj[@"isPassed"] boolValue];
            [array addObject:course];
        }];
        _searchResult = array;
        [_collectionView reloadData];
        
        if (array.count == 0) {
            [LrhAlertView showMessage:@"查找不到相关课件"];
        }
    }];
}


@end
