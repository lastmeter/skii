//
//  EzlCurrentUser.m
//  EZLearning
//
//  Created by xihan on 15/12/18.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "EzlUser.h"
#import "LoginService.h"
#import "MLearningService.h"
#import "ReviewService.h"
#import "UDManager.h"
#import "MomentsService.h"

@implementation EzlUser{
    void(^_straffBlock)(NSDictionary *dic);
}

+ (EzlUser *)currentUser{
    static dispatch_once_t once;
    static EzlUser *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[EzlUser alloc] init];
        [sharedView initAll];
    });
    return sharedView;
}


- (void)initAll{
    _charge         =   @"";
    _credit         =   @"";
    _mail           =   @"";
    _manager        =   @"";
    _market         =   @"";
    _role           =   @"";
    _userId         =   @"";
    _mobilePhone    =   @"";
    _city           =   @"";
    _name           =   @"";
    _userRole       =   UserRoleUnknown;
}

- (void)getUserInfo{
    [self p_GetUserInfo];
    [self p_GetUserRole];
    [self p_GetStraffInfo:nil];
}

- (void)p_GetUserInfo{
    __unsafe_unretained EzlUser *unUser = self;
    [MLearningService getUserInfoWithHandler:^(HandleResult result, NSDictionary *dataDic) {
        if (result == HandleResultSuccess) {
            if (dataDic) {
                [unUser setUserDic:dataDic];
            }
        }
        else if (result == HandleResultPostFail){
            [unUser p_GetUserInfo];
        }
    }];
}

- (void)timerRun{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSTimer *timer = [NSTimer timerWithTimeInterval:300 target:self selector:@selector(p_GetStraffInfo:) userInfo:nil repeats:NO];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    });
}

- (void)p_GetStraffInfo:(NSTimer *)timer{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    [ReviewService getUnreadStaffMsgWithArrayHandler:^(HandleResult result, NSDictionary *dataDic) {
        if (result == HandleResultSuccess) {
            _straffInfo = [dataDic mutableCopy];
            __block NSInteger unreadNumber = 0;
            [dataDic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                unreadNumber += [obj integerValue];
            }];
            [UDManager saveUnreadStaffInfo:dataDic];
            [UIApplication sharedApplication].applicationIconBadgeNumber = self.numberOfAllUnreadMessage;
            
            if (_straffBlock) {
                _straffBlock(dataDic);
            }
        }
        [self timerRun];
    }];
}

- (void)didGetStraffInfo:(void(^)(NSDictionary *dic))handler{
    _straffBlock = [handler copy];
}

- (void)p_GetUserRole{
    
    __unsafe_unretained EzlUser *unUser = self;
    [ReviewService getUserRoleWithHandler:^(HandleResult result, NSString *dataStr) {
        if (result == HandleResultSuccess) {
            if (dataStr) {
                _userRole = [dataStr integerValue];
            }
        }
        else if (result == HandleResultPostFail){
            [unUser p_GetUserRole];
        }
    }];
}

- (void)setUserDic:(NSDictionary *)userDic{
    
    _charge         =   [self objectForKey:@"charge" from:userDic];
    _credit         =   [self objectForKey:@"credit" from:userDic];
    _mail           =   [self objectForKey:@"mail" from:userDic];
    _manager        =   [self objectForKey:@"manager" from:userDic];
    _market         =   [self objectForKey:@"market" from:userDic];
    _role           =   [self objectForKey:@"role" from:userDic];
    _userId         =   [self objectForKey:@"userId" from:userDic];
    _mobilePhone    =   [self objectForKey:@"mobilePhone" from:userDic];
    _city           =   [self objectForKey:@"city" from:userDic];
    
    NSString *firstName = userDic[@"firstName"];
    NSString *lastName  = userDic[@"lastName"];
    NSMutableString *name = [NSMutableString string];
    if (firstName.length > 0) {
        [name appendString:firstName];
    }
    if (lastName.length > 0) {
        [name appendString:lastName];
    }
    if (name.length > 0) {
         _name = name;
    }
}



- (NSString *)objectForKey:(NSString *)key from:(NSDictionary *)dic{
    if (dic[key]) {
        return dic[key];
    }
    return @"";
}

#pragma mark-
- (void)loginIfNoActive{
      if (self.eid && self.pwd) {
        [LoginService isActiveWithHandler:^(HandleResult result, BOOL dataResult) {
            if (result != HandleResultSuccess) { return ; }
            if (!dataResult) {  [self autoLoginUtilSuccess]; }
        }];
    }
}

- (void)autoLoginUtilSuccess{
    [LoginService loginWithUid:self.eid pwd:self.pwd handler:^(HandleResult result, NSString *dataStr) {
        if (result == HandleResultPostFail ) {
            [self autoLoginUtilSuccess];
        }
    }];
}

- (void)getUnreadMoment{
    self.numberOfUnreadMoment = 0;
    [MomentsService getMarketWithHandler:^(HandleResult result, NSArray *dataArray) {
        if (result != HandleResultSuccess) { return ;  }
        [dataArray enumerateObjectsUsingBlock:^(NSDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *sectorId = obj[@"id"];
            [MomentsService getNewOfMomentsBySectorId:sectorId afterDate:[self getLastReadDateForMarket:sectorId] handler:^(HandleResult result, NSString *dataStr) {
                if (result == HandleResultSuccess && dataStr) {
                    self.numberOfUnreadMoment += [dataStr integerValue];
                }
            }];
        }];
    }];
}

#pragma mark-
//- (void)setNumberOfUnreadMoment:(NSInteger)numberOfUnreadMoment{
//    [UDManager saveUnreadMomentNumber:numberOfUnreadMoment];
//}

//- (NSInteger)numberOfUnreadMoment{
//    return [UDManager getUnreadMomentNumber];
//}

- (NSInteger)numberOfUnreadStaff{
    if (!_straffInfo) {
        return 0;
    }
    __block NSInteger unreadNum = 0;
    [_straffInfo enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        unreadNum += [obj integerValue];
    }];

    return unreadNum;
}

- (NSInteger)numberOfAllUnreadMessage{
    return self.numberOfUnreadMoment + self.numberOfUnreadStaff;
}

#pragma mark-
- (void)didReadMomentForMarket:(NSString *)marketId{
    NSString *key = [NSString stringWithFormat:@"%@_readMarketDate", _eid];
    NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:key]] ;
    dic[marketId] = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSTimeInterval)getLastReadDateForMarket:(NSString *)marketId {
    NSString *key = [NSString stringWithFormat:@"%@_readMarketDate", _eid];
    NSDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:key]];
    if (dic) {
        NSDate *date = dic[marketId];
        if (date) {
            return [date timeIntervalSince1970] * 1000;
        }
    }
    return 0;
}


@end
