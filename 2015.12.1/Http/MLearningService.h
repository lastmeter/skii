//
//  MLearningService.h
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "HttpManager.h"
@class CourseModel;
@interface MLearningService : HttpManager

+ (void)getUserInfoWithHandler:(DicHandler)handler;

#pragma mark-
+ (void)uploadArticleWithTitle:(NSString *)title
                      subTitle:(NSString *)subTitle
                       content:(NSString *)content
                        images:(NSArray *)images
                       handler:(BoolHandler)handler;

+ (void)uploadArticleWithTitle:(NSString *)title
                      subTitle:(NSString *)subTitle
                       content:(NSString *)content
                        attach:(NSString *)attach
                       handler:(BoolHandler)handler;

+ (void)getArticleInfoWithHandler:(StrHandler)handler;

+ (void)getHistoryArticleOfMeWithHandler:(ArrayHandler)handler;

#pragma mark-
+ (void)setHeadPortraitWithStr:(NSString *)imageStr
                      mimeType:(NSString *)mimeType
                      fileName:(NSString *)fileName
                       handler:(BoolHandler)handler;
+ (void)changeEmail:(NSString *)email mobile:(NSString *)mobile handler:(BoolHandler)handler;

#pragma mark-
+ (void)getCourseBySearchKey:(NSString *)key handler:(ArrayHandler)handler;

#pragma mark-

+ (void)getCourseBySiteId:(NSString *)siteId handler:(ArrayHandler)handler;
+ (void)getCategoryBySiteId:(NSString *)siteId handler:(ArrayHandler)handler;
+ (void)getCourseByCategoryId:(NSString *)categoryId siteId:(NSString *)siteId handler:(ArrayHandler)handler;

+ (void)addToFavorites:(CourseModel *)course siteTitle:(NSString *)siteTitle handler:(BoolHandler)handler;
+ (void)removeFromFavorites:(CourseModel *)course handler:(BoolHandler)handler;

+ (void)getCreditRule:(StrHandler)handler;
@end
