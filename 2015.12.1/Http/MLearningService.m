//
//  MLearningService.m
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MLearningService.h"
#import "CourseModel.h"

@implementation MLearningService


+ (NSString *)p_MLearningAddress{
    return [NSString stringWithFormat:@"%@MLearning.jws", ServiceAddress];
}

+ (void)getUserInfoWithHandler:(DicHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getUserInfo";
    [self dic_PostEzlRequest:request handler:handler];
}

#pragma mark-
+ (void)uploadArticleWithTitle:(NSString *)title
                      subTitle:(NSString *)subTitle
                       content:(NSString *)content
                        images:(NSArray *)images
                       handler:(BoolHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"uploadArticle";
    
    NSDictionary *params = @{ @"title" : title, @"subTitle" : subTitle, @"content" : content, @"img1Str":@"",  @"img2Str":@"",  @"img3Str":@"", @"mime" :  @"image/jpeg"};
    
    if (images.count > 0) {
        NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithDictionary:params];
        [images enumerateObjectsUsingBlock:^(NSString *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            mDic[ [NSString stringWithFormat:@"img%@Str", @(idx+1)] ] = obj ;
        }];
        params = mDic;
    }
    request.params =  params;
    [self bool_PostEzlRequest:request handler:handler];
    
}

+ (void)uploadArticleWithTitle:(NSString *)title
                      subTitle:(NSString *)subTitle
                       content:(NSString *)content
                        attach:(NSString *)attach
                       handler:(BoolHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"uploadArticleFull";
    request.params =  @{ @"title" : title, @"subTitle" : subTitle, @"content" : content, @"attach":attach};
    [self bool_PostEzlRequest:request handler:handler];
    
}


+ (void)getArticleInfoWithHandler:(StrHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getArticleIInfo";
    [self str_PostEzlRequest:request handler:handler];
}

+ (void)getHistoryArticleOfMeWithHandler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getHistoryArticleOfMe";
    [self array_PostEzlRequest:request handler:handler];
}
#pragma mark-
+ (void)setHeadPortraitWithStr:(NSString *)imageStr
                      mimeType:(NSString *)mimeType
                      fileName:(NSString *)fileName
                       handler:(BoolHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"setHeadPortrait";
    request.params      =   @{ @"imageStr": imageStr, @"mimeType" : mimeType, @"fileName":fileName };
    [self bool_PostEzlRequest:request handler:handler];
}

+ (void)changeEmail:(NSString *)email mobile:(NSString *)mobile handler:(BoolHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"changeEmailAndMobile";
    request.params      =   @{ @"email": email, @"mobile" : mobile };
    [self bool_PostEzlRequest:request handler:handler];

}

#pragma mark-
+ (void)getCourseBySearchKey:(NSString *)key handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getCourseBySearchOfMe";
    request.params      =   @{ @"search" : key };
    [self array_PostEzlRequest:request handler:handler];
}

#pragma mark-
+ (void)getCourseBySiteId:(NSString *)siteId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getCourseBySite";
    request.params      =   @{ @"siteId" : siteId };
    
    [self array_PostEzlRequest:request handler:^(HandleResult result, NSArray *dataArray) {
        if (result != HandleResultSuccess) {
            handler(result, dataArray);
            return ;
        }
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:dataArray.count];
        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CourseModel *course = [[CourseModel alloc] init];
            course.title = obj[@"title"];
            course.url = obj[@"url"];
            course.imgPath = obj[@"imagePath"];
            course.contentPackageId = obj[@"contentPackageId"];
            [array addObject:course];
        }];
        handler(result, array);
    }];
}

+ (void)getCourseByCategoryId:(NSString *)categoryId siteId:(NSString *)siteId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getCourseByCategory";
    request.params      =   @{ @"siteId" : siteId, @"categoryId" : categoryId };
    
    [self array_PostEzlRequest:request handler:^(HandleResult result, NSArray *dataArray) {
        if (result != HandleResultSuccess) {
            handler(result, dataArray);
            return ;
        }
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:dataArray.count];
        [dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            CourseModel *course = [[CourseModel alloc] init];
            course.title = obj[@"title"];
            course.url = obj[@"url"];
            course.imgPath = obj[@"imagePath"];
            course.contentPackageId = obj[@"contentPackageId"];
            [array addObject:course];
        }];
        handler(result, array);
    }];
}

+ (void)getCategoryBySiteId:(NSString *)siteId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getCategoryListBySite";
    request.params      =   @{ @"siteId" : siteId };
    [self array_PostEzlRequest:request handler:handler];
}


+ (void)addToFavorites:(CourseModel *)course siteTitle:(NSString *)siteTitle handler:(BoolHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"saveFavorites";
    request.params      =   @{ @"packageId" : course.contentPackageId, @"url" : course.url, @"title":course.title, @"siteTitle": siteTitle };
    [self bool_PostEzlRequest:request handler:handler];
}

+ (void)removeFromFavorites:(CourseModel *)course handler:(BoolHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"cancelFavorites";
    request.params      =   @{ @"packageId" : course.contentPackageId };
    [self bool_PostEzlRequest:request handler:handler];
}

+ (void)getCreditRule:(StrHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MLearningAddress];
    request.function    =   @"getCreditRule";
    [self str_PostEzlRequest:request handler:handler];
}
@end
