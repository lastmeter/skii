//
//  LoginManager.m
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LoginService.h"
#import "UDManager.h"
#import "MLearningService.h"
#import "EzlUser.h"
#import "MomentSendHelper.h"

@implementation LoginService

+ (NSString *)p_LoginAddress{
    return [NSString stringWithFormat:@"%@SakaiLogin.jws", ServiceAddress];
}


+ (void)loginWithUid:(NSString *)uid pwd:(NSString *)pwd handler:(StrHandler)handler{
    EzlRequest * request    =   [EzlRequest new];
    request.function        =   @"login";
    request.params          =   @{ @"id" : uid, @"pw" : pwd };
    request.urlString       =   [self p_LoginAddress];
    request.noSessionId     =   YES;
    
    [self str_PostEzlRequest:request handler:^(HandleResult result, NSString *dataStr) {
       
        if (result != HandleResultSuccess) { handler(result, dataStr); return ; }
        if ( [dataStr isEqualToString:@"-1"] || [dataStr isEqualToString:@"-2"]) { handler(result, dataStr); return;}
        
        [ UDManager saveSessionID:dataStr ];
        [ UDManager saveUsername:uid ];
        [ UDManager savePassword:pwd ];

        handler(result, dataStr);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.ezl.login" object:nil];
        
        __unsafe_unretained EzlUser *user= [EzlUser currentUser];
        user.eid = uid;
        user.pwd = pwd;
        [user getUserInfo];
        [user getUnreadMoment];
        [MomentSendHelper continueSendMoments];
    }];

}


+ (void)isActiveWithHandler:(BoolHandler)handler{
    EzlRequest * request    =   [EzlRequest new];
    request.function        =   @"isActive";
    request.urlString       =   [self p_LoginAddress];
    [self bool_PostEzlRequest:request handler:handler];
}




@end
