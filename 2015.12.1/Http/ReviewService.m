//
//  ReviewService.m
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "ReviewService.h"

@implementation ReviewService

+ (NSString *)p_ReviewAddress{
    return [NSString stringWithFormat:@"%@Review.jws", ServiceAddress];
}

+ (void)getContactsWithHandler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"getContacts";
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getBossWithHandler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"getBoss";
    [self array_PostEzlRequest:request handler:handler];
}


+ (void)sendChatWithContent:(NSString *)content
                         to:(NSString *)toId
                       date:(long)date
                    handler:(StrHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"sendChat";
    request.params      =   @{ @"content" : content, @"to" : toId, @"time" : @(date) };
    [self str_PostEzlRequest:request handler:handler];
}

+ (void)getSubordinateRankWithHandler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"getSubordinateRank";
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getUserRoleWithHandler:(StrHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"getRoleOfUser";
    [self str_PostEzlRequest:request handler:handler];
}

#pragma mark-
+ (void)getUnreadStaffMsgWithArrayHandler:(DicHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"getUnReadStaffMsgByUser";
    [self dic_PostEzlRequest:request handler:handler];
}

+ (void)deleteStaffMessageByID:(NSString *)messageId handler:(BoolHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_ReviewAddress];
    request.function    =   @"delStaffMessage";
    request.params      =   @{ @"messageId" : messageId };
    [self bool_PostEzlRequest:request handler:handler];
}
@end
