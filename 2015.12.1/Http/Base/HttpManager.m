//
//  HttpManager.m
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "HttpManager.h"
#import "XmlParser.h"
#import "EzlRequest.h"
#import "LoginService.h"

//NSString * const ServiceAddress = @"http://skii.lastmeter.com.cn/sakai-axis/";
NSString * const ServiceAddress = @"http://www.lastmeter.cn/sakai-axis/";


@implementation HttpManager


+ (void)p_ParseXml:(NSData *)data error:(NSError *)error handler:(void(^)(HandleResult result, NSString *dataStr))handler{
    if (error) {
        NSLog(@"%@",error);
        handler(HandleResultPostFail, nil);
        return;
    }
    
    if (!data) {
        handler(HandleResultEmptyData, nil);
        return;
    }
    
   // NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
  //  NSLog(@"%@",str);
    
    NSString *xmlString = [XmlParser parserData:data];
    if (!xmlString) {
        handler(HandleResultParserXmlFail, nil);
        return;
    }
//    NSLog(@"%@",xmlString);
    handler(HandleResultSuccess, xmlString);
}

+ (void)p_ParseJson:(NSData *)data error:(NSError *)error handler:(void(^)(HandleResult result, id jsonObject))handler{
    
    [self p_ParseXml:data error:error handler:^(HandleResult result, NSString *dataStr) {
        if (result != HandleResultSuccess){
            handler(result, nil);
            return ;
        }
        
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            handler( HandleResultParserJsonFail, nil );
            NSLog(@"%@",error);
            return;
        }
        if (!jsonObject){
            handler( HandleResultParserJsonFail, nil );
            return;
        }
        NSLog(@"%@",jsonObject);
        handler(HandleResultSuccess, jsonObject);
    }];
}

+ (void)p_PostEzlRequest:(EzlRequest *)ezlRequest handler:(void(^)(NSData *data, NSError *error))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:ezlRequest.urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (handler) {
            handler(data, error);
        }
        [session finishTasksAndInvalidate];
    }];
    
    [dataTask resume];
}


#pragma mark-
+ (void)str_PostEzlRequest:(EzlRequest *)ezlRequest handler:(StrHandler)handler{
    [self p_PostEzlRequest:ezlRequest handler:^(NSData *data, NSError *error) {
        [self p_ParseXml:data error:error handler:handler];
    }];
}

+ (void)dic_PostEzlRequest:(EzlRequest *)ezlRequest handler:(DicHandler)handler{
    
    [self p_PostEzlRequest:ezlRequest handler:^(NSData *data, NSError *error) {
        [self p_ParseJson:data error:error handler:handler];
    }];
}

+ (void)array_PostEzlRequest:(EzlRequest *)ezlRequest handler:(ArrayHandler)handler{
    
    [self p_PostEzlRequest:ezlRequest handler:^(NSData *data, NSError *error) {
        [self p_ParseJson:data error:error handler:handler];
    }];
}

+ (void)bool_PostEzlRequest:(EzlRequest *)ezlRequest handler:(BoolHandler)handler{
    [self p_PostEzlRequest:ezlRequest handler:^(NSData *data, NSError *error) {
        [self p_ParseXml:data error:error handler:^(HandleResult result, NSString *dataStr) {
            if (result != HandleResultSuccess) {
                handler( result, false );
                return ;
            }
            handler(result, [dataStr boolValue]);
        }];
    }];
}


@end
