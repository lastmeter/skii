//
//  HttpManager.h
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EzlRequest.h"

extern NSString * const ServiceAddress;

typedef NS_ENUM(NSInteger, HandleResult ) {
    HandleResultSuccess,
    HandleResultPostFail,
    HandleResultEmptyData,
    HandleResultParserXmlFail,
    HandleResultParserJsonFail
};

typedef void(^DicHandler)  (HandleResult result, NSDictionary *dataDic);
typedef void(^StrHandler)  (HandleResult result, NSString *dataStr);
typedef void(^ArrayHandler)(HandleResult result, NSArray *dataArray);
typedef void(^BoolHandler) (HandleResult result, BOOL dataResult);

@interface HttpManager : NSObject

+ (void)str_PostEzlRequest:  (EzlRequest *)ezlRequest handler:(StrHandler)handler;
+ (void)dic_PostEzlRequest:  (EzlRequest *)ezlRequest handler:(DicHandler)handler;
+ (void)bool_PostEzlRequest: (EzlRequest *)ezlRequest handler:(BoolHandler)handler;
+ (void)array_PostEzlRequest:(EzlRequest *)ezlRequest handler:(ArrayHandler)handler;


@end
