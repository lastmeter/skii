//
//  EzlRequest.m
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "EzlRequest.h"
#import "UDManager.h"

NSString * const xmlHead =
@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"<soapenv:Envelope "
"xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
"xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><soapenv:Body>";


NSString * const xmlTail = @"</soapenv:Body></soapenv:Envelope>";

@interface XMLBody : NSObject

@end

@implementation EzlRequest
{
    NSURL *_url;
}

- (void)setUrlString:(NSString *)urlString{
    NSString *encodeStr1 = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *encodeStr2 = [encodeStr1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:encodeStr1]];
    _url = [NSURL URLWithString:encodeStr2];
}


- (NSString *)p_XmlFormat:(NSString *)string{
    NSString *xmlString = [string stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"<"  withString:@"&lt;" ];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@">"  withString:@"&gt;" ];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"'"  withString:@"&apos;" ];
    xmlString = [xmlString stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;" ];
    return xmlString;
}

- (NSString *)p_XmlString{
    
    NSString *bodyHead = [NSString stringWithFormat:@"<%@>",_function];
    NSString *bodyTail = [NSString stringWithFormat:@"</%@>",_function];
    NSMutableString * body = [NSMutableString string];
    
    if (!_noSessionId){ [body appendFormat:@"<sessionId>%@</sessionId>",[UDManager getSessionID]]; }
    
    [_params enumerateKeysAndObjectsUsingBlock:^(NSString *  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSString class]]) {
            obj = [self p_XmlFormat:obj];
        }
        [body appendFormat:@"<%@>%@</%@>",key,obj,key ];
    }];

//NSLog(@"%@", [NSString stringWithFormat:@"%@%@%@%@%@", xmlHead, bodyHead, body, bodyTail, xmlTail]);
    return [NSString stringWithFormat:@"%@%@%@%@%@", xmlHead, bodyHead, body, bodyTail, xmlTail];
}

- (NSURLRequest *)urlRequest{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:_url];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@" " forHTTPHeaderField:@"SOAPAction"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[self p_XmlString] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:_timeOut ? _timeOut : 20];
    return request;
}

@end
