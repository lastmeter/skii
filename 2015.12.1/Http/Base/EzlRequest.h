//
//  EzlRequest.h
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EzlRequest : NSObject


@property (nonatomic, strong) NSDictionary * params;
@property (nonatomic, copy) NSString * urlString;
@property (nonatomic, copy) NSString * function;
@property (nonatomic, assign) BOOL noSessionId;
@property (nonatomic, assign) NSTimeInterval timeOut;
@property (nonatomic, readonly) NSURLRequest *urlRequest;

@end
