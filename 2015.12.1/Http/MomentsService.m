//
//  MomentsService.m
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "MomentsService.h"
#import <UIKit/UIKit.h>

@implementation MomentsService

+ (NSString *)p_MomentsAddress{
    return [NSString stringWithFormat:@"%@Moments.jws", ServiceAddress];
}

+ (void)sendMomentsMessage:(NSString *)content attach:(NSString *)attach handler:(StrHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"sendMomentsMessage";
    request.timeOut     =   50;
    request.params      =   @{ @"content" : content, @"attach" : attach };
    [self str_PostEzlRequest:request handler:handler];
}

+ (void)addFabToMomentsId:(NSString *)momentsId handler:(StrHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"addFabToMoments";
    request.params      =   @{ @"momentsId" : momentsId };
    [self str_PostEzlRequest:request handler:handler];
}

+ (void)addRemarkToMomentsId:(NSString *)momentsId content:(NSString *)content handler:(BoolHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"addRemarkToMoments";
    request.params      =   @{ @"momentsId" : momentsId, @"content" : content };
    [self bool_PostEzlRequest:request handler:handler];
}

+ (void)addReplyToMomentsId:(NSString *)momentsId
                replyUserId:(NSString *)replyUserId
                    content:(NSString *)content
                    handler:(BoolHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"addReplyToMoments";
    request.params      =   @{ @"momentsId" : momentsId, @"content" : content, @"replyUserId" : replyUserId };
    [self bool_PostEzlRequest:request handler:handler];

}


+ (void)getMarketWithHandler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMarketOfMoments";
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getMomentsBySectorId:(NSString *)sectorId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsOfSector";
    request.params      =   @{ @"sectorId" : sectorId };
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getMomentsByUserId:(NSString *)userId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsOfUser";
    request.params      =   @{ @"userId" : userId };
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getAttachByMomentsId:(NSString *)momentsId handler:(ArrayHandler)handler{
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getAttachOfMoments";
    request.params      =   @{ @"momentsId" : momentsId };
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getHeadPortraitByUserId:(NSString *)userId handler:(void(^)(HandleResult result, NSData *data))handler {
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getHeadPortraitOfUser";
    request.params      =   @{ @"userId" : userId };
    [self str_PostEzlRequest:request handler:^(HandleResult result, NSString *dataStr) {
        if ( result != HandleResultSuccess) {
            handler(result, nil);
            return ;
        }
        
        NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
        NSData *decodeImageData = [[NSData alloc]initWithBase64EncodedData:data options:NSDataBase64DecodingIgnoreUnknownCharacters];
        handler(result, decodeImageData);
    }];
}

+ (void)deleteMomentsId:(NSString *)momentsId handler:(BoolHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"delMomentsMessage";
    request.params      =   @{ @"momentsId" : momentsId };
    [self bool_PostEzlRequest:request handler:handler];
    
}

+ (void)getNewOfMomentsBySectorId:(NSString *)sectorId
                        afterDate:(NSTimeInterval)aferDate
                          handler:(StrHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getNewOfMomentsByMarket";
    NSString *sp = [NSString stringWithFormat:@"%.f",aferDate];
    request.params      =   @{ @"sectorId" : sectorId, @"afterDate" : sp };
    [self str_PostEzlRequest:request handler:handler];
}

+ (void)getMomentsBySectorId:(NSString *)sectorId
                      pageNo:(NSInteger)pageNo
                    pageSize:(NSInteger)pageSize
                     handler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsOfMarket";
    request.params      =   @{ @"sectorId" : sectorId, @"pageNo" : @(pageNo), @"pageSize" : @(pageSize) };
    [self array_PostEzlRequest:request handler:handler];
}

+ (void)getMomentsPageByUserId:(NSString *)userId
                        pageNo:(NSInteger)pageNo
                      pageSize:(NSInteger)pageSize
                       handler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsPageOfUser";
    request.params      =   @{ @"userId" : userId, @"pageNo" : @(pageNo), @"pageSize" : @(pageSize) };
    [self array_PostEzlRequest:request handler:handler];
}



+ (void)getMomentsStatByMarket:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsStatByMarket";
    request.params      =   @{ @"afterDate" : @(afterDate), @"beforeDate" : @(beforeDate) };
    [self array_PostEzlRequest:request handler:handler];
}
+ (void)getMomentsStatByCharge:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsStatByCharge";
    request.params      =   @{ @"afterDate" : @(afterDate), @"beforeDate" : @(beforeDate) };
    [self array_PostEzlRequest:request handler:handler];
}
+ (void)getMomentsStatByManager:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler{
    
    EzlRequest *request =   [EzlRequest new];
    request.urlString   =   [self p_MomentsAddress];
    request.function    =   @"getMomentsStatByManager";
    request.params      =   @{ @"afterDate" : @(afterDate), @"beforeDate" : @(beforeDate) };
    [self array_PostEzlRequest:request handler:handler];
}

@end
