//
//  ReviewService.h
//  EZLearning
//
//  Created by xihan on 15/12/22.
//  Copyright © 2015年 YXB. All rights reserved.
//

#import "HttpManager.h"

@interface ReviewService : HttpManager

+ (void)getContactsWithHandler:(ArrayHandler)handler;
+ (void)getBossWithHandler:(ArrayHandler)handler;
+ (void)getSubordinateRankWithHandler:(ArrayHandler)handler;
+ (void)getUserRoleWithHandler:(StrHandler)handler;

+ (void)deleteStaffMessageByID:(NSString *)messageId handler:(BoolHandler)handler;
+ (void)getUnreadStaffMsgWithArrayHandler:(DicHandler)handler;

+ (void)sendChatWithContent:(NSString *)content
                         to:(NSString *)toId
                       date:(long)date
                    handler:(StrHandler)handler;
@end
