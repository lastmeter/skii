//
//  MomentsService.h
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "HttpManager.h"
@class UIImage;

#define NumberOfMomentsOnePage 7

@interface MomentsService : HttpManager

+ (void)sendMomentsMessage:(NSString *)content attach:(NSString *)attach handler:(StrHandler)handler;
+ (void)addFabToMomentsId:(NSString *)momentsId handler:(StrHandler)handler;
+ (void)addRemarkToMomentsId:(NSString *)momentsId content:(NSString *)content handler:(BoolHandler)handler;
+ (void)getMarketWithHandler:(ArrayHandler)handler;
+ (void)getMomentsBySectorId:(NSString *)sectorId handler:(ArrayHandler)handler;
+ (void)getMomentsByUserId:(NSString *)userId handler:(ArrayHandler)handler;
+ (void)getAttachByMomentsId:(NSString *)momentsId handler:(ArrayHandler)handler;

+ (void)getHeadPortraitByUserId:(NSString *)userId handler:(void(^)(HandleResult result, NSData *data))handler;

+ (void)addReplyToMomentsId:(NSString *)momentsId
                replyUserId:(NSString *)replyUserId
                    content:(NSString *)content
                    handler:(BoolHandler)handler;

+ (void)deleteMomentsId:(NSString *)momentsId handler:(BoolHandler)handler;

+ (void)getNewOfMomentsBySectorId:(NSString *)sectorId
                        afterDate:(NSTimeInterval)aferDate
                          handler:(StrHandler)handler;

+ (void)getMomentsBySectorId:(NSString *)sectorId
                      pageNo:(NSInteger)pageNo
                    pageSize:(NSInteger)pageSize
                     handler:(ArrayHandler)handler;

+ (void)getMomentsPageByUserId:(NSString *)userId
                        pageNo:(NSInteger)pageNo
                      pageSize:(NSInteger)pageSize
                       handler:(ArrayHandler)handler;

+ (void)getMomentsStatByMarket:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler;

+ (void)getMomentsStatByCharge:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler;

+ (void)getMomentsStatByManager:(long long) afterDate
                    beforeDate:(long long)beforeDate
                       handler:(ArrayHandler)handler;
@end
