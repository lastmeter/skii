//
//  LoginManager.h
//  Moments
//
//  Created by xihan on 15/11/30.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "HttpManager.h"

@interface LoginService : HttpManager

+ (void)loginWithUid:(NSString *)uid pwd:(NSString *)pwd handler:(StrHandler)handler;
+ (void)isActiveWithHandler:(BoolHandler)handler;


@end
