//
//  NEzlNavigationController.m
//  EZLearning
//
//  Created by xihan on 16/1/15.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import "NEzlNavigationController.h"

@interface NEzlNavigationController ()

@end

@implementation NEzlNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
