//
//  UIView+Frame.m
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

- (void)changeX:(CGFloat)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}


- (void)changeY:(CGFloat)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}


- (void)changeWidth:(CGFloat)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}


- (void)changeHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}




@end
