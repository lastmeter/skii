//
//  UIImage+Scale.h
//  EZLearning
//
//  Created by xihan on 16/1/27.
//  Copyright © 2016年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Scale)

- (UIImage *)changeToScaleSize:(CGSize )scaleSize;
- (UIImage *)fixOrientation;
- (UIImage *)subImageForRect:(CGRect)subRect;
@end
