//
//  NSString+Size.h
//  Moments
//
//  Created by xihan on 15/12/2.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Size)

- (CGSize)textSizeWithWidth:(CGFloat)width Font:(UIFont *)font;

@end
