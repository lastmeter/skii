//
//  UIView+Animation.h
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animation)

- (void)addPopAnimation;
- (void)addPopFromFrame:(CGRect )frame;

@end
