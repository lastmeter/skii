//
//  NSDate+String.m
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "NSDate+String.h"

@implementation NSDate (String)

- (NSString *)string{
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"MM-dd HH:mm"];
    NSString *locationString=[dateformatter stringFromDate:self];
    return locationString;
}

- (NSString *)dateString{
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"MM-dd"];
    NSString *locationString=[dateformatter stringFromDate:self];
    return locationString;
}

@end
