//
//  NSString+Json.h
//  Moments
//
//  Created by xihan on 15/12/1.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Json)

- (id)toJsonObject;

@end
