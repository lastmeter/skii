//
//  UIView+Frame.h
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

- (void)changeX:(CGFloat)x;
- (void)changeY:(CGFloat)y;

- (void)changeWidth:(CGFloat)width;
- (void)changeHeight:(CGFloat)height;

@end
