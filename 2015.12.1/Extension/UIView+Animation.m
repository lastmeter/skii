//
//  UIView+Animation.m
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "UIView+Animation.h"

@implementation UIView (Animation)

- (void)addPopAnimation{
    self.alpha = 1;
    CAKeyframeAnimation *animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.4;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 0.9)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:@"easeInEaseOut"];
    [self.layer addAnimation:animation forKey:nil];
}

- (void)addPopFromFrame:(CGRect )frame{
    
    //1.创建动画
    CABasicAnimation *anima=[CABasicAnimation animationWithKeyPath:@"bounds"];
    //1.1设置动画执行时间
    anima.duration= 0.25;
    //1.2设置动画执行完毕后不删除动画
    anima.removedOnCompletion=NO;
    //1.3设置保存动画的最新状态
    anima.fillMode=kCAFillModeForwards;
    //1.4修改属性，执行动画
    anima.fromValue =   [NSValue valueWithCGRect:frame];
    anima.toValue   =   [NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]];
    //2.添加动画到layer
    [self.layer addAnimation:anima forKey:nil];
}


@end
