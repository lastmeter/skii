//
//  LoadingHUD.h
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface LoadingHUD : NSObject

+ (MBProgressHUD *)showInView:(UIView *)view;
+ (void)show;
+ (void)hide;
@end
