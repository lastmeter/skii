//
//  LoadingHUD.m
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LoadingHUD.h"
#import "ShareAlertWindow.h"

@interface LoadingHUD()

@property (nonatomic, strong) MBProgressHUD *hud;

@end


@implementation LoadingHUD

+ (void)show{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    hud.labelText = @"请稍等";
    [[ShareAlertWindow getInstance] showView:hud];
    [hud show:YES];
}

+ (void)hide{
    [[ShareAlertWindow getInstance] dismissView];
}

+ (MBProgressHUD *)showInView:(UIView *)view{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    hud.labelText = @"请稍等";
    [view addSubview:hud];
    [hud show:YES];
    return hud;
}


@end
