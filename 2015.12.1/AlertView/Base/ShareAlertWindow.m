//
//  ShareAlertWindow.m
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "ShareAlertWindow.h"
#import "AppDelegate.h"

@interface ShareAlertWindow()

@property (nonatomic, assign) BOOL hadController;

@end

@implementation ShareAlertWindow


+ (ShareAlertWindow *)getInstance{
    static dispatch_once_t once;
    static ShareAlertWindow *sharedView;
    dispatch_once(&once, ^ {
        sharedView = [[ShareAlertWindow alloc] init];
    });
    return sharedView;
}

- (UIWindow *)window{
    if (_window == nil) {
        _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _window.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _window.windowLevel = UIWindowLevelAlert;
        _window.userInteractionEnabled = true;
    }
    return _window;
}



- (void)showController:(UIViewController *)controller{
    if (self.window.rootViewController) {
        _hadController = YES;
        self.window.rootViewController = nil;
    }
    self.window.rootViewController = controller;
    [self.window makeKeyAndVisible];
}

- (void)dismissController{
    
    if (_window != nil) {
        if (_hadController) {
            _hadController = NO;
            return;
        }
        _window.rootViewController = nil;
        [_window resignKeyWindow];
        [_window removeFromSuperview];
        _window = nil;
    }
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.window makeKeyAndVisible];
}

- (void)showView:(UIView *)view{
    [self.window addSubview:view];
    [self.window makeKeyAndVisible];
    
}

- (void)dismissView{
     if (_window != nil) {
         [_window.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
             [obj removeFromSuperview];
         }];
         [_window resignKeyWindow];
         [_window removeFromSuperview];
         _window = nil;
     }
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate.window makeKeyAndVisible];

}


@end
