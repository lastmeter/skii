//
//  ShareAlertWindow.h
//  Moments
//
//  Created by xihan on 15/12/7.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareAlertWindow : NSObject

@property(nonatomic, strong) UIWindow *window;

+ (ShareAlertWindow *)getInstance;

- (void)showController:(UIViewController *)controller;
- (void)dismissController;

- (void)showView:(UIView *)view;
- (void)dismissView;

@end
