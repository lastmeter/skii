//
//  LrhAlertView.m
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LrhAlertView.h"
#import "LrhAlertViewController.h"
#import "ShareAlertWindow.h"

@interface LrhAlertView ()
@property (nonatomic, strong) NSMutableArray *actions;
@property (nonatomic, strong) LrhAlertViewController *alertController;
@property (nonatomic, weak) ShareAlertWindow *window;
@end

@implementation LrhAlertView


- (NSMutableArray *)actions{
    if (!_actions) {
        _actions = [NSMutableArray array];
    }
    return _actions;
}

- (LrhAlertViewController *)alertController{
    if (!_alertController) {
        _alertController = [[LrhAlertViewController alloc] init];
    }
    return _alertController;
}

- (ShareAlertWindow *)window{
    if (!_window) {
        _window = [ShareAlertWindow getInstance];
    }
    return _window;
}

- (void)addAlertAction:(LrhAlertAction *)action{
    [self.actions addObject:action];
}

- (void)showMessage:(NSString *)message{

    if (self.actions.count == 0){
        LrhAlertAction *action = [LrhAlertAction actionWithTitle:@"确定" handler:nil];
        [_actions addObject:action];
    }
    self.alertController.actions = _actions;
    self.alertController.message = message;
    [self.window showController:self.alertController];
    
    [self.actions removeAllObjects] ;
    self.alertController = nil;
}

+ (void)showMessage:(NSString *)message{
    LrhAlertView *alert = [[LrhAlertView alloc] init];
    LrhAlertAction *action = [LrhAlertAction actionWithTitle:@"确定" handler:nil];
    alert.alertController.actions = @[action];
    alert.alertController.message = message;
    [alert.window showController:alert.alertController];
}

+ (void)showMessage:(NSString *)message handler:(void(^)())handler{
    LrhAlertView *alert = [[LrhAlertView alloc] init];
    LrhAlertAction *action = [LrhAlertAction actionWithTitle:@"确定" handler:handler];
    alert.alertController.actions = @[action];
    alert.alertController.message = message;
    [alert.window showController:alert.alertController];
}

+ (void)showMessage:(NSString *)message actions:(NSArray *)array{
    LrhAlertView *alert = [[LrhAlertView alloc] init];
    alert.alertController.actions = array;
    alert.alertController.message = message;
    [alert.window showController:alert.alertController];
}



@end
