//
//  LrhAlertViewController.m
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LrhAlertViewController.h"
#import "LrhAlertCell.h"
#import "LrhAlertAction.h"
#import "NSString+Size.h"
#import "UIView+Frame.h"
#import "UIView+Animation.h"
#import "ShareAlertWindow.h"


const CGFloat kAlertItemHeight  =   45;

@interface LrhAlertViewController ()<UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UILabel *contentLabel;
@property (strong, nonatomic) UIView *alertView;

@end

@implementation LrhAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _alertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self alertWidth], 0)];
    _alertView.backgroundColor = [UIColor whiteColor];
    _alertView.layer.cornerRadius = 8;
    _alertView.layer.masksToBounds = true;
    _alertView.layer.borderWidth = 2;
    _alertView.layer.borderColor = [[UIColor grayColor] CGColor];
    
    _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, [self alertWidth] - 40, 0)];
    _contentLabel.font = [self contentFont];
    _contentLabel.textColor = [ UIColor blackColor];
    _contentLabel.userInteractionEnabled = NO;
    _contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _contentLabel.numberOfLines = 0;
    [_alertView addSubview:_contentLabel];
    
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0 , [self alertWidth], 0) collectionViewLayout:flowLayout];
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.scrollEnabled = NO;
    [_collectionView registerClass:[LrhAlertCell class] forCellWithReuseIdentifier:@"alert"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_alertView addSubview:_collectionView];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self alertWillShow];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self alertShow];
}

- (void)alertWillShow{
    
    CGFloat collectionY = 0;
    CGFloat collectionH = kAlertItemHeight;
    
    if (_message.length > 0) {
        _contentLabel.text = _message;
        CGSize size = [_message textSizeWithWidth:[self alertWidth] - 40 Font:[self contentFont]];
        [_contentLabel changeHeight:size.height];
        collectionY = CGRectGetMaxY(_contentLabel.frame) + 20;
        if ( _actions.count > 2) {
            collectionH = _actions.count * kAlertItemHeight;
        }
    }
    else{
        [_contentLabel changeHeight:0];
        collectionH = _actions.count * kAlertItemHeight;
        [self p_AddBackgroundTap];
    }
   
    _collectionView.frame = CGRectMake(0, collectionY, [self alertWidth], collectionH);

    [_alertView changeHeight:CGRectGetMaxY(_collectionView.frame)];
    _alertView.center = self.view.center;
    [_alertView addPopAnimation];
}

- (void)alertShow{
    [_collectionView reloadData];
    [self.view addSubview:_alertView];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return self.actions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    LrhAlertCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"alert" forIndexPath:indexPath];
    LrhAlertAction *action = self.actions[indexPath.row];
    cell.textLabel.textColor = action.titleColor;
    cell.textLabel.text = action.title;    
    return cell;
}


#pragma mark - UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.actions.count == 2 && _message.length > 0) {
        return CGSizeMake([self alertWidth] / 2, kAlertItemHeight);
    }
    return CGSizeMake([self alertWidth], kAlertItemHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LrhAlertAction *action = self.actions[indexPath.row];

    if (action.handler) {
        action.handler();
    }
    _actions = nil;
    [self dismiss];
    
}

- (void)dismiss{
    [UIView animateWithDuration:0.25 animations:^{
        _alertView.alpha = 0;
    } completion:^(BOOL finished) {
        [[ShareAlertWindow getInstance] dismissController];
    }];
}

- (UIFont *)contentFont{
    if (isPad) {
        return [UIFont systemFontOfSize:21];
    }
    return [UIFont boldSystemFontOfSize:17];
}

- (CGFloat)alertWidth{
    if (isPad) {
        return 300;
    }
    return 258;
}

#pragma mark- Background Tap
- (void)p_AddBackgroundTap{
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [tap setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tap];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{

    if (touch.view != self.view) {
        return NO;
    }
   
    return YES;
}



@end
