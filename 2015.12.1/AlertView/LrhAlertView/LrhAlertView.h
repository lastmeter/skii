//
//  LrhAlertView.h
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LrhAlertAction.h"

@interface LrhAlertView : NSObject

- (void)addAlertAction:(LrhAlertAction *)action;
- (void)showMessage:(NSString *)message;

+ (void)showMessage:(NSString *)message;
+ (void)showMessage:(NSString *)message handler:(void(^)())handler;
+ (void)showMessage:(NSString *)message actions:(NSArray *)array;

@end
