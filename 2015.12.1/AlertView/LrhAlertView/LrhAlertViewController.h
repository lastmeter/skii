//
//  LrhAlertViewController.h
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LrhAlertViewController : UIViewController


- (void)alertShow;

@property (nonatomic, copy) NSArray *actions;
@property (nonatomic, copy) NSString *message;
//@property (nonatomic, weak) id<LrhAlertViewDelegate>delegate;


@end
