//
//  LrhAlertCell.m
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LrhAlertCell.h"

@implementation LrhAlertCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *selectBG = [[UIView alloc] initWithFrame:self.bounds];
        selectBG.backgroundColor = [UIColor lightGrayColor];
        self.selectedBackgroundView = selectBG;
        
        UIView *norBG = [[UIView alloc] initWithFrame:self.bounds];
        norBG.backgroundColor = [UIColor clearColor];
        self.backgroundView = norBG;
        
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        _textLabel.textColor = [UIColor blackColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [self titleFont];
        [self.contentView addSubview:_textLabel];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), 2)];
        line1.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:line1];
        
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(frame), 0, 2, CGRectGetHeight(frame))];
        line2.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:line2];
    }
    return self;
}


- (UIFont *)titleFont{
    if (isPad) {
        return [UIFont systemFontOfSize:21];
    }
    return [UIFont boldSystemFontOfSize:17];
}

@end
