//
//  LrhAction.m
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import "LrhAlertAction.h"

@implementation LrhAlertAction

+ (LrhAlertAction *)actionWithTitle:(NSString *)title handler:(void(^)())handler{
    LrhAlertAction *action = [[LrhAlertAction alloc] init];
    action.title = title;
    action.handler = handler;
    action.titleColor = [UIColor blackColor];
    return action;
}
+ (LrhAlertAction *)actionWithTitle:(NSString *)title titleColor:(UIColor *)titleColor handler:(void(^)())handler{
    LrhAlertAction *action = [[LrhAlertAction alloc] init];
    action.title = title;
    action.handler = handler;
    action.titleColor = titleColor;
    return action;
}

@end
