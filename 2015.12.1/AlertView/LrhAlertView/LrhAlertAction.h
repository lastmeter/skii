//
//  LrhAction.h
//  Moments
//
//  Created by xihan on 15/12/17.
//  Copyright © 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LrhAlertAction : NSObject

@property(nonatomic, copy ) NSString *title;
@property(nonatomic, strong ) UIColor *titleColor;
@property(nonatomic, strong ) void(^handler)();

+ (LrhAlertAction *)actionWithTitle:(NSString *)title handler:(void(^)())handler;
+ (LrhAlertAction *)actionWithTitle:(NSString *)title titleColor:(UIColor *)titleColor handler:(void(^)())handler;

@end
