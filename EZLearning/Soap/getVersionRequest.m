//
//  getVersionRequest.m
//  EZLearning
//
//  Created by zwf on 15/7/5.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "getVersionRequest.h"

@implementation getVersionRequest

-(void)getCurrentVersion
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getCurVersion";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"os" andValue:@"ios"];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:UPDATE_SERVICE RequestType:ST_GET_CURVERSION];
}

-(void)getCurversionAck:(NSString *)result{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_CURVERSION];
        return;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_GET_CURVERSION];
}

-(void)getVersionInfo:(int)vcode
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getVersionInfo";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"vcode" andValue:[NSNumber numberWithInt:vcode]];
     [soapXmlUtil addParamKey:@"os" andValue:@"ios"];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:UPDATE_SERVICE RequestType:ST_GET_VERSION_INFO];
}

-(void)getVersionInfoAck:(NSString *)result{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_VERSION_INFO];
        return;
    }
    NSDictionary *dic = [JsonUtil json2Dictionary:result];
    [SoapNotiCenter postSoapNotificationWithInfo:dic SoapType:ST_GET_VERSION_INFO];
}

@end
