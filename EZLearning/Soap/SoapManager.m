//
//  SoapManager.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapManager.h"

#import "LoginRequest.h"
#import "LogoutRequest.h"

#import "FavoritesOperationRequest.h"
#import "GetCategoryRequest.h"
#import "GetAdvertRequest.h"
#import "GetCourseRequest.h"
#import "GetSitesRequest.h"
#import "GetMP4Request.h"
#import "GetMarkScoreRequest.h"
#import "GetUserInfoRequest.h"
#import "GetNoticeRequest.h"
#import "FeedbackRequest.h"

#import "ChatRequest.h"
#import "ContactsRequest.h"
#import "GetUserJobRequest.h"

#import "UserServiceRequest.h"

#import "AssessmentsRequest.h"
#import "AgentScoreRequest.h"
#import "ChoiceItemRequest.h"
#import "AssessmentGradingRequest.h"

#import "GetAllForumuserRequest.h"

#import "GetSelfinfoRequest.h"

#import "getVersionRequest.h"
@implementation SoapManager

+ (void)loginWithUserName:(NSString *)username andPassword:(NSString *)password
{
    LoginRequest *sLogin = [[LoginRequest alloc] init];
    [sLogin loginWihtUserName:username andPassword:password];
}

+ (void)logout
{
    LogoutRequest *sLogout = [[LogoutRequest alloc] init];
    [sLogout logout];
}

+ (void)cancelFavoritesByPackageId:(NSString *)packageId
{
    FavoritesOperationRequest *sCancelFavorites = [[FavoritesOperationRequest alloc] init];
    [sCancelFavorites cancelFavoritesByPackageId:packageId];
}

+ (void)getCategoryListBySiteId:(NSString *)siteId
{
    GetCategoryRequest *sGetCategoryListBySite = [[GetCategoryRequest alloc] init];
    [sGetCategoryListBySite getCategoryListBySiteId:siteId];
}

+ (void)getCompanyAdvert
{
    GetAdvertRequest *sGetCompanyAdvert = [[GetAdvertRequest alloc] init];
    [sGetCompanyAdvert getCompanyAdvert];
}

+ (void)getMyCourseByOffSet:(int)offSet
{
    GetCourseRequest *sGetMyCourse = [[GetCourseRequest alloc] init];
    [sGetMyCourse getMyCourseByOffSet:offSet];
}

+ (void)getCourseBySiteID:(NSString *)siteID andCategoryID:(NSString *)categoryID
{
    GetCourseRequest *sGetCourseByCategory = [[GetCourseRequest alloc] init];
    [sGetCourseByCategory getCourseBySiteID:siteID andCategoryID:categoryID];
}

+ (void)getCourseBySiteID:(NSString *)siteID
{
    GetCourseRequest *sGetCourseBySite = [[GetCourseRequest alloc] init];
    [sGetCourseBySite getCourseBySiteID:siteID];
}

+ (void)getFavoritesList
{
    FavoritesOperationRequest *sGetFavoritesList = [[FavoritesOperationRequest alloc] init];
    [sGetFavoritesList getFavoritesList];
}

+ (void)getHeadPortrait
{
    GetUserInfoRequest *sGetHeadPortrait = [[GetUserInfoRequest alloc] init];
    [sGetHeadPortrait getHeadPortrait];
}

+ (void)getMarketScore
{
    GetMarkScoreRequest *sGetMarketScore = [[GetMarkScoreRequest alloc] init];
    [sGetMarketScore getMarketScore];
}

+ (void)getMP4ByCourseTitle:(NSString *)title
{
    GetMP4Request *sGetMp4RelatedToCourse = [[GetMP4Request alloc] init];
    [sGetMp4RelatedToCourse getMP4ByCourseTitle:title];
}

+ (void)getMySites
{
    GetSitesRequest *sGetMySites = [[GetSitesRequest alloc] init];
    [sGetMySites getMySites];
}

+ (void)getNoticeListAfterTimeStamp:(long)timeSp
{
    GetNoticeRequest *sGetNoticeList = [[GetNoticeRequest alloc] init];
    [sGetNoticeList getNoticeListAfterTimeStamp:timeSp];
}


+ (void)getUserInfo
{
    GetUserInfoRequest *sGetUserInfo = [[GetUserInfoRequest alloc] init];
    [sGetUserInfo getUserInfo];
    [sGetUserInfo getHeadPortrait];
}

+ (void)saveFavoritesByCourseId:(NSString *)courseId courseUrl:(NSString *)url courseTitle:(NSString *)courseTitle siteTitle:(NSString *)siteTitle
{
    FavoritesOperationRequest *sSaveFavorites = [[FavoritesOperationRequest alloc] init];
    [sSaveFavorites saveFavoritesByCourseId:courseId courseUrl:url courseTitle:courseTitle siteTitle:siteTitle];
}

+ (void)sendQuestiontoAdminWithPhone:(NSString *)phone email:(NSString *)email content:(NSString *)content device:(NSString *)device andRelease:(NSString *)release
{
    FeedbackRequest *sSendQuestionToAdmin = [[FeedbackRequest alloc] init];
    [sSendQuestionToAdmin sendQuestiontoAdminWithPhone:phone email:email content:content device:device andRelease:release];
}

+ (void)getChatByUserId:(NSString *)userId AfterDate:(long)afterDate
{
    ChatRequest *sGetChatWithUser = [[ChatRequest alloc] init];
    [sGetChatWithUser getChatByUserId:userId AfterDate:afterDate];
}

+ (void)getContacts
{
    ContactsRequest *sGetContacts = [[ContactsRequest alloc] init];
    [sGetContacts getContacts];
}

+ (void)getSubordinateRank
{
    ContactsRequest *sGetSubordinateRank = [[ContactsRequest alloc] init];
    [sGetSubordinateRank getSubordinateRank];
}

+ (void)getSubordinateScoreByUserId:(NSString *)userId
{
    ContactsRequest *sGetSubordinateScore = [[ContactsRequest alloc] init];
    [sGetSubordinateScore getSubordinateScoreByUserId:userId];
}

+ (void)getUserJob
{
    GetUserJobRequest *sGetUserJob = [[GetUserJobRequest alloc] init];
    [sGetUserJob getUserJob];
}

+ (void)sendChatContent:(NSString *)content andDate:(long)date toUserId:(NSString *)userId
{
    ChatRequest *sSendChat = [[ChatRequest alloc] init];
    [sSendChat sendChatContent:content andDate:date toUserId:userId];
}

+ (void)changOldPassword:(NSString *)oldPwd ToNewPassword: (NSString *)newPwd
{
    UserServiceRequest *sChangePassword = [[UserServiceRequest alloc] init];
    [sChangePassword changOldPassword:oldPwd ToNewPassword:newPwd];
}

+ (void)getAllActivePublishedAssessments
{
    AssessmentsRequest *sGetAllActivePublishedAssessments = [[AssessmentsRequest alloc] init];
    [sGetAllActivePublishedAssessments getAllActivePublishedAssessments];
}

+ (void)getScoreOfAgent
{
    AgentScoreRequest *sGetScoreOfAgent = [[AgentScoreRequest alloc] init];
    [sGetScoreOfAgent getScoreOfAgent];
}

+(void)getAssessmentDescriptionByAssessmentId:(NSString *)assessmentId
{
     AssessmentsRequest *sGetAssessmentDescription = [[AssessmentsRequest alloc] init];
    [sGetAssessmentDescription getAssessmentDescriptionByAssessmentId:assessmentId];
}

+ (void)getAssessmentSectionByAssessmentId:(NSString *)assessmentId
{
    ChoiceItemRequest *sGetAssessmentSection = [[ChoiceItemRequest alloc] init];
    [sGetAssessmentSection getAssessmentSectionByAssessmentId:assessmentId];
}

+ (void)getItemBySectionBySectionId:(NSString *)sectionId
{
    ChoiceItemRequest *sGetItemBySection = [[ChoiceItemRequest alloc] init];
    [sGetItemBySection getItemBySectionBySectionId:sectionId];
}

+ (void)getAnswerByItemId:(NSString *)itemId
{
    ChoiceItemRequest *sGetAnswerByItemId = [[ChoiceItemRequest alloc] init];
    [sGetAnswerByItemId getAnswerByItemId:itemId];
}

+ (void)beginAssessmentGradingByAssessmentId:(NSString *)assessmentId
{
    AssessmentGradingRequest *sBeginAssessmentGrading = [[AssessmentGradingRequest alloc] init];
    [sBeginAssessmentGrading beginAssessmentGradingByAssessmentId:assessmentId];
}

+ (void)updateSingleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId AnswerId:(NSString *)answerId{
    AssessmentGradingRequest *sUpdateSingleGradingData = [[AssessmentGradingRequest alloc] init];
    [sUpdateSingleGradingData updateSingleGradingDataWithGradingId:gradingId ItemId:itemId AnswerId:answerId];
}

+ (void)updateMultipleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId Data:(NSString *)data
{
    AssessmentGradingRequest *sUpdateMultipleGradingData = [[AssessmentGradingRequest alloc] init];
    [sUpdateMultipleGradingData updateMultipleGradingDataWithGradingId:gradingId ItemId:itemId Data:data];
}

+ (void)submitAssessmentGradingByGradingId:(NSString *)gradingId
{
    AssessmentGradingRequest *sSubmitAssessment = [[AssessmentGradingRequest alloc] init];
    [sSubmitAssessment submitAssessmentGradingByGradingId:gradingId];
}

+ (void)getAllForumUser
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser getAllForumuser];
}
+ (void)getTopicForum:(NSString *)forumId
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser GetTopicOfForum:forumId];
}

+ (void)getmsglistbyTopic:(NSString *)topicId
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser Getmsglistbytopic:topicId];
}

+(void)getmsgandReplies:(NSString *)topicId MessageId:(NSString *)messageid
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser Getmsgandreplies:topicId messageid:messageid];
}
+ (void)getaddMsgtoTopic:(NSString *)topicID Msgtitle:(NSString *)title Msgbody:(NSString *)body MsgreplyTold:(NSString *)replyTold
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser GetaddmsgtoTopic:topicID Msgtitle:title Msgbody:body MsgreplyTold:replyTold];
}

+ (void)addpraisetoMSG:(NSString *)messageId
{
    GetAllForumuserRequest *sGetForumUser = [[GetAllForumuserRequest alloc] init];
    [sGetForumUser addPraisetoMSG:messageId];
}
+ (void)setHeadPortrait:(NSString *)imageStr MIMETYPE:(NSString *)mimeType FileName:(NSString *)filename
{
    GetSelfinfoRequest *sGetsetHeadPortrait = [[GetSelfinfoRequest alloc]init];
    [sGetsetHeadPortrait setHeadPortrait:imageStr MimeTYPE:mimeType FILEName:filename];
}
+ (void)changeEmailandMobile:(NSString *)email phone:(NSString *)mobile
{
    GetSelfinfoRequest *sGetsetHeadPortrait = [[GetSelfinfoRequest alloc]init];
    [sGetsetHeadPortrait changeEmailandMobile:email phoneNum:mobile];

}

+ (void)getcurrentVersion
{
    getVersionRequest *sGetversion = [[getVersionRequest alloc]init];
    [sGetversion getCurrentVersion];
}

+ (void)getversionInfo:(int)vcode{
    getVersionRequest *sGetversion = [[getVersionRequest alloc]init];
    [sGetversion getVersionInfo:vcode];
}
@end

