//
//  SoapManager.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoapManager : NSObject

+ (void)loginWithUserName:(NSString *)username andPassword:(NSString *)password;
+ (void)logout;

+ (void)changOldPassword:(NSString *)oldPwd ToNewPassword: (NSString *)newPwd;

+ (void)cancelFavoritesByPackageId:(NSString *)packageId;
+ (void)getCategoryListBySiteId:(NSString *)siteId;
+ (void)getCompanyAdvert;
+ (void)getMyCourseByOffSet:(int)offSet;
+ (void)getCourseBySiteID:(NSString *)siteID andCategoryID:(NSString *)categoryID;
+ (void)getCourseBySiteID:(NSString *)siteID;
+ (void)getFavoritesList;
+ (void)getHeadPortrait;
+ (void)getMarketScore;
+ (void)getMP4ByCourseTitle:(NSString *)title;
+ (void)getMySites;
+ (void)getNoticeListAfterTimeStamp:(long)timeSp;
+ (void)getUserInfo;
+ (void)saveFavoritesByCourseId:(NSString *)courseId courseUrl:(NSString *)url courseTitle:(NSString *)courseTitle siteTitle:(NSString *)siteTitle;
+ (void)sendQuestiontoAdminWithPhone:(NSString *)phone email:(NSString *)email content:(NSString *)content device:(NSString *)device andRelease:(NSString *)release;

+ (void)getChatByUserId:(NSString *)userId AfterDate:(long)afterDate;
+ (void)getContacts;
+ (void)getSubordinateRank;
+ (void)getSubordinateScoreByUserId:(NSString *)userId;
+ (void)getUserJob;
+ (void)sendChatContent:(NSString *)content andDate:(long)date toUserId:(NSString *)userId;

+ (void)getAllActivePublishedAssessments;
+ (void)getAssessmentDescriptionByAssessmentId:(NSString *)assessmentId;
+ (void)getAssessmentSectionByAssessmentId:(NSString *)assessmentId;
+ (void)getScoreOfAgent;
+ (void)getItemBySectionBySectionId:(NSString *)sectionId;
+ (void)getAnswerByItemId:(NSString *)itemId;
+ (void)beginAssessmentGradingByAssessmentId:(NSString *)assessmentId;
+ (void)updateSingleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId AnswerId:(NSString *)answerId;
+ (void)updateMultipleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId Data:(NSString *)data;
+ (void)submitAssessmentGradingByGradingId:(NSString *)gradingId;
+ (void)getAllForumUser;
+ (void)getTopicForum:(NSString *)forumId;
+ (void)getmsglistbyTopic:(NSString *)topicId;
+ (void)getmsgandReplies:(NSString *)topicId MessageId:(NSString *)messageid;
+ (void)getaddMsgtoTopic:(NSString *)topicID Msgtitle:(NSString *)title Msgbody:(NSString *)body MsgreplyTold:(NSString *)replyTold;
+ (void)addpraisetoMSG:(NSString *)messageId;

+ (void)setHeadPortrait:(NSString *)imageStr MIMETYPE:(NSString *)mimeType FileName:(NSString *)filename;
+ (void)changeEmailandMobile:(NSString *)email phone:(NSString *)mobile;

+ (void)getcurrentVersion;
+ (void)getversionInfo:(int)vcode;
@end

