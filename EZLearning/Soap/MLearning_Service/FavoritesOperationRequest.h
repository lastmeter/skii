//
//  FavoritesOperationRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface FavoritesOperationRequest : SoapRequest

- (void) saveFavoritesByCourseId:(NSString *)courseId courseUrl:(NSString *)url courseTitle:(NSString *)courseTitle siteTitle:(NSString *)siteTitle;

- (void) cancelFavoritesByPackageId:(NSString *)packageId;

- (void) getFavoritesList;

@end
