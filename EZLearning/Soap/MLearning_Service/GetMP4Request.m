//
//  GetMP4Request.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetMP4Request.h"

@implementation GetMP4Request

- (void)getMP4ByCourseTitle:(NSString *)title
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMp4RelatedtoCourse";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"title" andValue:title];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_MP4_FOR_COURSE];
}

-(void)getMP4ForCourseAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_MP4_FOR_COURSE];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_MP4_FOR_COURSE];
}


@end
