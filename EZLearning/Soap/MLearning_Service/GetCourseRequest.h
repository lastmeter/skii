//
//  GetCourseRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface GetCourseRequest : SoapRequest

- (void)getMyCourseByOffSet:(int)offSet;
- (void)getCourseBySiteID:(NSString *)siteID andCategoryID:(NSString *)categoryID;
- (void)getCourseBySiteID:(NSString *)siteID;
@end
