//
//  GetUserInfoRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetUserInfoRequest.h"
#import "PersonInfo.h"

@implementation GetUserInfoRequest

- (void)getUserInfo
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getUserInfo";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_USER_INFO];
}

-(void)getUserInfoAck:(NSString *)result
{
    NSDictionary *dic = [JsonUtil json2Dictionary:result];
    PersonInfo *person = [[PersonInfo alloc] init];
    NSString *lastName = dic[@"lastName"];
    // NSString *firstName = result[@"firstName"];
    person.name = lastName;
    person.city = dic[@"city"];
    person.manager = dic[@"boss"];
    person.market = dic[@"market"];
    person.rank = dic[@"star"];
    person.phonenum = dic[@"mobilePhone"];
    person.emailaddress = dic[@"mail"];
    person.credit = dic[@"credit"];
    person.star = dic[@"star"];
    //NSLog(@"person.name = %@,person.phonenum = %@",person.name,person.phonenum);
    [SoapNotiCenter postSoapNotificationWithInfo:person SoapType:ST_GET_USER_INFO];
}

#pragma mark

- (void)getHeadPortrait
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getHeadPortrait";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_HEAD_PORTRAIT];
}

- (void)getHeadPortraitAck:(NSString *)result
{
    NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
    [SoapNotiCenter postSoapNotificationWithInfo:data SoapType:ST_GET_HEAD_PORTRAIT];
}

@end
