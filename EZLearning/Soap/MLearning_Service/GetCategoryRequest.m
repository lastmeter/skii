//
//  GetCategoryRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetCategoryRequest.h"
#import "CourseModel.h"

@implementation GetCategoryRequest

- (void)getCategoryListBySiteId:(NSString *)siteId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getCategoryListBySite";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"siteId" andValue:siteId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_CATEGORY_LIST_BY_SITE];
}

- (void)getCategoryListBySiteAck:(NSString *)result
{
    if ([result isEqualToString:@"null"])
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_CATEGORY_LIST_BY_SITE];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_CATEGORY_LIST_BY_SITE];
}
@end
