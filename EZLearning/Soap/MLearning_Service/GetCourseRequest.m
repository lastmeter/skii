//
//  GetCourseRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetCourseRequest.h"
#import "CourseModel.h"

@implementation GetCourseRequest

- (void)getCourseBySiteID:(NSString *)siteID
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getCourseBySite";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"siteId" andValue:siteID];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_COURSE_BY_SITE];
}

- (void)getCourseBySiteAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *courses = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        CourseModel *course = [[CourseModel alloc] init];
        course.title = obj[@"title"];
        course.url = obj[@"url"];
        course.imgPath = obj[@"imagePath"];
        course.contentPackageId = obj[@"contentPackageId"];
        course.type = CET_SITE_VIEW;
        course.tag = idx;
        [courses addObject:course];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:courses SoapType:ST_GET_COURSE_BY_SITE];
     
}

#pragma mark

- (void)getCourseBySiteID:(NSString *)siteID andCategoryID:(NSString *)categoryID
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getCourseByCategory";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"siteId" andValue:siteID];
    [soapXmlUtil addParamKey:@"categoryId" andValue:categoryID];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_COURSE_BY_CATEGORY];
}

- (void)getCourseByCategoryAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *courses = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        CourseModel *course = [[CourseModel alloc] init];
        course.title = obj[@"title"];
        course.url = obj[@"url"];
        course.imgPath = obj[@"imagePath"];
        course.contentPackageId = obj[@"contentPackageId"];
        course.tag = idx;
        course.type = CET_SITE_VIEW;
        [courses addObject:course];
    }];
   
    [SoapNotiCenter postSoapNotificationWithInfo:courses SoapType:ST_GET_COURSE_BY_CATEGORY];
}

#pragma mark

-(void)getMyCourseByOffSet:(int)offSet
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMyCourse";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"offset" andValue:[NSNumber numberWithInt:offSet]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_MY_COURSE];
}

-(void)getMyCourseAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *courses = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        CourseModel *course = [[CourseModel alloc] init];
        course.url = obj[@"url"];
        course.title = obj[@"title"];
        course.imgPath = obj[@"imagePath"];
        course.tag = idx;
        course.pass = [obj[@"isPassed"] boolValue];
        course.type = CET_CURRICULUM_VIEW;
        [courses addObject:course];
    }];
    
    [SoapNotiCenter postSoapNotificationWithInfo:courses SoapType:ST_GET_MY_COURSE];
}
@end
