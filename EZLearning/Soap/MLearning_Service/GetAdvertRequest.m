//
//  GetAdvertRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetAdvertRequest.h"

@implementation GetAdvertRequest


- (void)getCompanyAdvert
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getCompanyAdvert";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_COMPANY_ADVERT];
}

-(void)getCompanyAdvertAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_COMPANY_ADVERT];
    }
    else
    {
        NSArray *array = [JsonUtil json2Array:result];
        [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_COMPANY_ADVERT];
    }
    
}


@end
