//
//  GetMarkScoreRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetMarkScoreRequest.h"
#import "RankModel.h"

@implementation GetMarkScoreRequest

- (void)getMarketScore
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMarketScore";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_MARKET_SCORE];
}

-(void)getMarketScoreAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_MARKET_SCORE];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *score = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        RankModel *rank = [[RankModel alloc] init];
        rank.name = obj[@"market"];
        rank.score = [NSString stringWithFormat:@"%.2f", [obj[@"score"]floatValue]];
        rank.rank = idx+1;
        [score addObject:rank];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:score SoapType:ST_GET_MARKET_SCORE];
}

@end
