//
//  GetSitesRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetSitesRequest.h"

@implementation GetSitesRequest

- (void)getMySites
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMySites";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_MY_SITES];
}


- (void)getMySitesAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_MY_SITES];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_MY_SITES];
}

@end
