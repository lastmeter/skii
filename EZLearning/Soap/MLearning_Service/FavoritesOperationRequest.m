//
//  FavoritesOperationRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "FavoritesOperationRequest.h"
#import "CourseModel.h"

@implementation FavoritesOperationRequest

- (void)saveFavoritesByCourseId:(NSString *)courseId courseUrl:(NSString *)url courseTitle:(NSString *)courseTitle siteTitle:(NSString *)siteTitle
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"saveFavorites";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"packageId" andValue:courseId];
    [soapXmlUtil addParamKey:@"url" andValue:[XMLUtil XMLStringFromString:url]];
    [soapXmlUtil addParamKey:@"title" andValue:courseTitle];
    [soapXmlUtil addParamKey:@"siteTitle" andValue:siteTitle];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_SAVE_FAVORITES];
}

-(void)saveFavoritesAck:(NSString *)result
{
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_SAVE_FAVORITES];
}

#pragma mark

- (void)cancelFavoritesByPackageId:(NSString *)packageId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"cancelFavorites";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"packageId" andValue:packageId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_CANCEL_FAVORITES];
}

-(void)cancelFavoritesAck:(NSString *)result
{
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_CANCEL_FAVORITES];
}

#pragma mark

- (void)getFavoritesList
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getFavoritesList";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_FAVORITES_LIST];
}

-(void)getFavoritesListAck:(NSString *)result
{

    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *list = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop)
    {
        CourseModel *course = [[CourseModel alloc] init];
        course.title = obj[@"title"];
        course.url = obj[@"url"];
        course.userId = obj[@"userId"];
        course.imgPath = obj[@"imagePath"];
        course.contentPackageId = obj[@"contentPackageId"];
        course.tag = idx;
        course.collected = YES;
        course.type = CET_FAVORITE_VIEW;
        [list addObject:course];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:list SoapType:ST_GET_FAVORITES_LIST];
    
}
@end
