//
//  FeedbackRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface FeedbackRequest : SoapRequest

- (void)sendQuestiontoAdminWithPhone:(NSString *)phone email:(NSString *)email content:(NSString *)content device:(NSString *)device andRelease:(NSString *)release;

@end
