//
//  FeedbackRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "FeedbackRequest.h"

@implementation FeedbackRequest

- (void)sendQuestiontoAdminWithPhone:(NSString *)phone email:(NSString *)email content:(NSString *)content device:(NSString *)device andRelease:(NSString *)release
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"sendQuestiontoAdmin";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"phone" andValue:phone];
    [soapXmlUtil addParamKey:@"mail" andValue:email];
    [soapXmlUtil addParamKey:@"content" andValue:content];
    [soapXmlUtil addParamKey:@"device" andValue:device];
    [soapXmlUtil addParamKey:@"release" andValue:release];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_SEND_QUESTION_TO_ADMIN];

}

-(void)sendQuestionToAdminAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_SEND_QUESTION_TO_ADMIN];
        return;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_SEND_QUESTION_TO_ADMIN];
}

@end
