//
//  GetNoticeRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetNoticeRequest.h"
#import "NotificationModel.h"
#import "DateUtil.h"

@implementation GetNoticeRequest

- (void)getNoticeListAfterTimeStamp:(long)timeSp
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getNoticeList";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"afterDate" andValue:[NSNumber numberWithLong:timeSp]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_GET_NOTICE_LIST];
    
}

- (void)getNoticeListAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *notice = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        NotificationModel *noti = [[NotificationModel alloc] init];
        noti.title = obj[@"subject"];
        noti.content = obj[@"body"];
        long time = ([obj[@"date"] longValue])/1000;
        noti.date = [DateUtil dateFromTimeSp:time];
        noti.btnTag = idx;
        noti.opened = NO;
        noti.isLoaded = NO;
        noti.cellHeight = 0;
        [notice addObject:noti];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:notice SoapType:ST_GET_NOTICE_LIST];
}


@end
