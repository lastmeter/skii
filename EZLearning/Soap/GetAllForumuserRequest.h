//
//  GetAllForumuserRequest.h
//  EZLearning
//
//  Created by zwf on 15/5/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "SoapRequest.h"

@interface GetAllForumuserRequest : SoapRequest

-(void)getAllForumuser;
-(void)GetTopicOfForum:(NSString *)forumID;
-(void)Getmsglistbytopic:(NSString *)topicId;
-(void)Getmsgandreplies:(NSString *)topicId messageid:(NSString *)megid;
- (void)GetaddmsgtoTopic:(NSString *)topicID Msgtitle:(NSString *)title Msgbody:(NSString *)body MsgreplyTold:(NSString *)replyTold;
- (void)addPraisetoMSG:(NSString *)messageId;
@end
