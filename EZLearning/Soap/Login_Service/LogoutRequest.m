//
//  LogoutRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "LogoutRequest.h"

@implementation LogoutRequest

- (void)logout
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"logout";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:LOGIN_SERVICE RequestType:ST_LOGOUT];
}

- (void)logoutAck:(NSString *)result
{
    NSLog(@"logout:%@",result);
}

@end
