//
//  LoginRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest

- (void)loginWihtUserName:(NSString *)userName andPassword:(NSString *)password
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"login";
    [soapXmlUtil addParamKey:@"id" andValue:userName];
    [soapXmlUtil addParamKey:@"pw" andValue:password];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:LOGIN_SERVICE RequestType:ST_LOGIN];
}

- (void)loginAck:(NSString *)result
{
    NSDictionary *userInfo;
   
    int loginResult = [result intValue];
    if (loginResult == -1)
    {
        userInfo = @{
          @"getResult" : @"YES",
          @"result" :    @"NO",
          @"info" :      @"不允许登陆"
        };
    }
    else if (loginResult == -2)
    {
        userInfo = @{
          @"getResult" : @"YES",
          @"result" : @"NO",
          @"info" :   @"登陆失败,请检查用户名和密码是否正确"
        };
    }
    else
    {
        userInfo = @{
          @"getResult" : @"YES",
          @"result" :    @"YES"
        };
        [UDManager saveSessionID:result];
        DLOG(@"sessionID:%@",result);
    }
    [SoapNotiCenter postSoapNotiWithUserInfo:userInfo SoapType:ST_LOGIN];
}

@end
