//
//  LoginRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface LoginRequest : SoapRequest

- (void)loginWihtUserName:(NSString *)userName andPassword:(NSString *)password;

@end
