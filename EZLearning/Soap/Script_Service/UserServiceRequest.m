//
//  UserServiceRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "UserServiceRequest.h"

@implementation UserServiceRequest

- (void)changOldPassword:(NSString *)oldPwd ToNewPassword: (NSString *)newPwd
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"changeUserPassword";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"eid" andValue:[UDManager getUsername]];
    [soapXmlUtil addParamKey:@"oldPwd" andValue:oldPwd];
    [soapXmlUtil addParamKey:@"newPwd" andValue:newPwd];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SCRIPT_SERVICE RequestType:ST_CHANGE_PASSWORD];
}


- (void)changPasswordAck:(NSString *)result
{
  
    
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_CHANGE_PASSWORD];
        return;
    }
    NSString *info;
    BOOL changeResult = NO;
    
    if ([result isEqualToString:@"success"])
    {
        changeResult = YES;
        info = @"修改成功";
    }
    else if ([result isEqualToString:@"-1"])
    {
        info = @"旧密码不对";
    }
    else if ([result isEqualToString:@"-2"])
    {
        info = @"修改失败";
    }
    
    NSDictionary *userInfo = @{
                               @"getResult" : @"YES",
                             @"changeResult" : [NSNumber numberWithInt:changeResult],
                                     @"info" :info
                               };

    [SoapNotiCenter postSoapNotiWithUserInfo:userInfo SoapType:ST_CHANGE_PASSWORD];
}



@end
