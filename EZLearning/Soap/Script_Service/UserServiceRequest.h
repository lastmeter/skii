//
//  UserServiceRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface UserServiceRequest : SoapRequest
- (void)changOldPassword:(NSString *)oldPwd ToNewPassword: (NSString *)newPwd;
@end
