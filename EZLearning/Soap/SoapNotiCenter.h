//
//  SoapNotiCenter.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapInfo.h"

@interface SoapNotiCenter : NSObject

+ (void)addSoapObserver:(id)context selector:(SEL)aSelector SoapType:(SOAP_TYPE) type;

+ (void)addSoapRequestResultObserver:(id)context selector:(SEL)aSelector;


+ (void)postSoapNotiWithUserInfo:(NSDictionary *)userInfo SoapType:(SOAP_TYPE)type;
+ (void)postSoapNotificationWithInfo:(id)info SoapType:(SOAP_TYPE)type;
+ (void)postSoapEmptyResultNotificationWithSoapType:(SOAP_TYPE)type;
+ (void)postSoapRequestResultNotification:(BOOL)result;


+ (id)  getInfoFromNotification:(NSNotification *)noti;
+ (BOOL)getSoapResult:(NSNotification *)noti;
+ (BOOL)getSoapRequestResult:(NSNotification *)noti;

+ (void)removeSoapObserver:(id)context SoapType:(SOAP_TYPE) type;
@end
