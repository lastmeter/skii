//
//  GetSelfinfoRequest.h
//  EZLearning
//
//  Created by zwf on 15/6/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "SoapRequest.h"

@interface GetSelfinfoRequest : SoapRequest

- (void)setHeadPortrait:(NSString *)imageStr MimeTYPE:(NSString *)mimeType FILEName:(NSString *)filename;
- (void)changeEmailandMobile:(NSString *)email phoneNum:(NSString *)mobile;
@end
