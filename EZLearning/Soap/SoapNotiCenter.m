//
//  SoapNotiCenter.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapNotiCenter.h"

@implementation SoapNotiCenter

//定义通知
+(void)addSoapObserver:(id)context selector:(SEL)aSelector SoapType:(SOAP_TYPE) type
{
    [[NSNotificationCenter defaultCenter] addObserver:context selector:aSelector name:[SoapNotiCenter getNotiNameBySoapType:type] object:self];
}

+(void)addSoapRequestResultObserver:(id)context selector:(SEL)aSelector
{
    [[NSNotificationCenter defaultCenter] addObserver:context selector:aSelector name:@"Connect" object:self];
}

//调用通知
+ (void)postSoapRequestResultNotification:(BOOL)result
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Connect" object:self userInfo:@{@"result":[NSNumber numberWithBool:result]}];
}

+(void)postSoapNotiWithUserInfo:(NSDictionary *)userInfo SoapType:(SOAP_TYPE)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[SoapNotiCenter getNotiNameBySoapType:type] object:self userInfo:userInfo];
}


+ (void)postSoapEmptyResultNotificationWithSoapType:(SOAP_TYPE)type
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[SoapNotiCenter getNotiNameBySoapType:type] object:self userInfo:@{@"getResult":@"NO"}];
}

+ (void)postSoapNotificationWithInfo:(id)info SoapType:(SOAP_TYPE)type
{
    if (info) {
        NSDictionary *userInfo = @{@"getResult":@"YES",@"info":info};
        [[NSNotificationCenter defaultCenter] postNotificationName:[SoapNotiCenter getNotiNameBySoapType:type] object:self userInfo:userInfo];
    }
   
}


+ (BOOL)getSoapResult:(NSNotification *)noti
{
    NSDictionary *dic= noti.userInfo;
    return [dic[@"getResult"] boolValue];
}


+ (id)getInfoFromNotification:(NSNotification *)noti
{
    NSDictionary *dic= noti.userInfo;
    return dic[@"info"];
}

+ (BOOL)getSoapRequestResult:(NSNotification *)noti
{
    NSDictionary *dic= noti.userInfo;
    return [dic[@"result"] boolValue];
}

+ (NSString *)getNotiNameBySoapType:(SOAP_TYPE)type
{
    NSString *notiName;
    
    switch (type)
    {
        case ST_LOGIN:
            notiName = @"login";
            break;
        case ST_LOGOUT:
            notiName = @"logout";
            break;
        case ST_CHANGE_PASSWORD:
            notiName = @"changePassword";
            break;
        case ST_GET_NOTICE_LIST:
            notiName = @"getNoticeList";
            break;
        case ST_GET_MY_COURSE:
            notiName = @"getMyCourse";
            break;
        case ST_GET_MY_SITES:
            notiName = @"getMySites";
            break;
        case ST_GET_COURSE_BY_SITE:
            notiName = @"getCourseBySite";
            break;
        case ST_GET_CATEGORY_LIST_BY_SITE:
            notiName = @"getCategoryListBySite";
            break;
        case ST_GET_COURSE_BY_CATEGORY:
            notiName = @"getCourseByCategory";
            break;
        case ST_GET_MP4_FOR_COURSE:
            notiName = @"getMP4ForCourse";
            break;
        case ST_GET_FAVORITES_LIST:
            notiName = @"getFavoritesList";
            break;
        case ST_SAVE_FAVORITES:
            notiName = @"saveFavorites";
            break;
        case ST_CANCEL_FAVORITES:
            notiName = @"cancelFavorites";
            break;
        case ST_GET_MARKET_SCORE:
            notiName = @"getMarketScore";
            break;
        case ST_GET_USER_INFO:
            notiName = @"getUserInfo";
            break;
        case ST_GET_HEAD_PORTRAIT:
            notiName = @"getHeadPortrait";
            break;
        case ST_GET_HOTLINE:
            notiName = @"getHotLine";
            break;
        case ST_GET_COMPANY_ADVERT:
            notiName = @"getCompanyAdvert";
            break;
        case ST_GET_VERSION_INFO:
            notiName = @"getVersionInfo";
            break;
        case ST_GET_CURVERSION:
            notiName = @"getCurversion";
            break;
        case ST_GET_CONTACTS:
            notiName = @"getContacts";
            break;
        case ST_GET_USER_JOB:
            notiName = @"getUserJob";
            break;
        case ST_GET_SUBORDINATE_RANK:
            notiName = @"loginAck";
            break;
        case ST_GET_SUBORDINATE_SCORE:
            notiName = @"getSubrdinateScore";
            break;
        case ST_GET_CHAT_WITH_USER:
            notiName = @"getChatWithUser";
            break;
        case ST_SEND_CHAT:
            notiName = @"sendChat";
            break;
        case ST_SEND_QUESTION_TO_ADMIN:
            notiName = @"senQuestion";
            break;
        case ST_GET_ALL_ASSESSMENTS:
            notiName = @"getAssessment";
            break;
        case ST_GET_SCORE_OF_AGENT:
            notiName = @"getScoreOfAgent";
            break;
        case ST_GET_ASSESSMENT_DESCRIPTION:
            notiName = @"getDescription";
            break;
        case ST_GET_ASSESSMENT_SECTION:
            notiName = @"getSection";
            break;
        case ST_GET_ITEM_BY_SECTION:
            notiName = @"getItem";
            break;
        case ST_GET_ANSWER_OF_ITEM:
            notiName = @"getAnswer";
            break;
        case ST_BEGIN_ASSESSMENT_GTADING:
            notiName = @"beginGtading";
            break;
        case ST_UPDATE_ASSESSMENT_DATA_SINGLE:
            notiName = @"updateSingle";
            break;
        case ST_UPDATE_ASSESSMENT_DATA_MULTIPLE:
            notiName = @"updateMultiple";
            break;
        case ST_SUBMIT_ASSESSMENT_GRADING:
            notiName = @"submitGrading";
            break;
        case ST_GET_ALLFORUM_OFUSER:
            notiName = @"getAllForumuser";
            break;
        case ST_GET_TOPIC_OF_FORUM:
            notiName = @"getTopicOfForum";
            break;
        case ST_GET_MSGSUMMARY_OF_TOPIC:
            notiName = @"getMsgsummaryOfTopic";
            break;
        case ST_GET_MSG_AND_REPLIES:
            notiName = @"getMsgandReplies";
            break;
        case ST_ADD_MSG_TO_TOPIC:
            notiName = @"getaddmsgtoTopic";
            break;
        case ST_ADD_PRAISE_TO_MSG:
            notiName = @"addPraisetoMsg";
            break;
        case ST_SET_HEAD_PORTRAIT:
            notiName = @"setheadportrait";
            break;
        case ST_CHANGE_EMAIL_AND_MOBILE:
            notiName = @"changeEmailandMobile";
            break;
        default:
            notiName = nil;
            break;
    }
    return notiName;
}


//移出通知
+(void)removeSoapObserver:(id)context SoapType:(SOAP_TYPE) type
{
    [[NSNotificationCenter defaultCenter] removeObserver:context name:[SoapNotiCenter getNotiNameBySoapType:type] object:self];
}
@end
