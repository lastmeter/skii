//
//  GetAllForumuserRequest.m
//  EZLearning
//
//  Created by zwf on 15/5/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "GetAllForumuserRequest.h"
@implementation GetAllForumuserRequest

-(void)getAllForumuser
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getAllForumofUser";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_GET_ALLFORUM_OFUSER];
}

-(void)getallForumofuserAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_ALLFORUM_OFUSER];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_ALLFORUM_OFUSER];
}

-(void)GetTopicOfForum:(NSString *)forumID
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getTopicOfForum";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"forumId" andValue:forumID];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_GET_TOPIC_OF_FORUM];
}

-(void)getTopicOfForumAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_TOPIC_OF_FORUM];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_TOPIC_OF_FORUM];
}
-(void)Getmsglistbytopic:(NSString *)topicId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMsgSummaryOfTopic";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"topicId" andValue:topicId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_GET_MSGSUMMARY_OF_TOPIC];
}

-(void)getMsglistbyTopicAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_MSGSUMMARY_OF_TOPIC];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_MSGSUMMARY_OF_TOPIC];
}


-(void)Getmsgandreplies:(NSString *)topicId messageid:(NSString *)megid
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getMsgAndReplies";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"topicId" andValue:topicId];
    [soapXmlUtil addParamKey:@"messageId" andValue:megid];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_GET_MSG_AND_REPLIES];
}

-(void)getMsgAndRepliesAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_MSG_AND_REPLIES];
        return;
    }
    NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:array SoapType:ST_GET_MSG_AND_REPLIES];
}

-(void)GetaddmsgtoTopic:(NSString *)topicId Msgtitle:(NSString *)title Msgbody:(NSString *)body MsgreplyTold:(NSString *)replyTold
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"addMsgToTopic";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"topicId" andValue:topicId];
    [soapXmlUtil addParamKey:@"title" andValue:title];
    [soapXmlUtil addParamKey:@"body" andValue:body];
    [soapXmlUtil addParamKey:@"replyTold" andValue:replyTold];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_ADD_MSG_TO_TOPIC];
}
-(void)getaddMsgtoTopicAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_ADD_MSG_TO_TOPIC];
        return;
    }
    //NSArray *array = [JsonUtil json2Array:result];
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_ADD_MSG_TO_TOPIC];
}

- (void)addPraisetoMSG:(NSString *)messageId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"addPraiseToMsg";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"messageId" andValue:messageId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:FORUM_SERVICE RequestType:ST_ADD_PRAISE_TO_MSG];
}

-(void)addPraisetoMsgAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_ADD_PRAISE_TO_MSG];
        return;
    }
    NSLog(@"result = %@",result);
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_ADD_PRAISE_TO_MSG];
}
@end
