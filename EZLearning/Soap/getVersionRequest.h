//
//  getVersionRequest.h
//  EZLearning
//
//  Created by zwf on 15/7/5.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "SoapRequest.h"

@interface getVersionRequest : SoapRequest

-(void)getCurrentVersion;
-(void)getVersionInfo:(int)vcode;
@end
