//
//  AssessmentsRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AssessmentsRequest.h"
#import "AssessmentModel.h"

@implementation AssessmentsRequest

- (void)getAllActivePublishedAssessments
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getAllActivePublishedAssessments";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_ALL_ASSESSMENTS];
}

- (void)getAllActivePublishedAssessmentAck:(NSString *)result
{
    DLOG(@"getAllActivePublishedAssessmentAck:\n{\n %@ \n}",result);
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *assessments = [NSMutableArray array];
    
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        AssessmentModel *assessment = [[AssessmentModel alloc] init];
        assessment.assessmentTitle = obj[@"title"];
        assessment.assessmentId = obj[@"id"];
        assessment.timeLimit = [obj[@"timeLimit"] intValue];
        assessment.submissionsAllowed = [obj[@"submissionsAllowed"] intValue];
        assessment.unlimitedSubmissions = [obj[@"unlimitedSubmissions"] boolValue];
        assessment.createTime =  obj[@"time"];
        assessment.grading = [obj[@"grading"] intValue];
        [assessments addObject:assessment];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:assessments SoapType:ST_GET_ALL_ASSESSMENTS];
}


#pragma mark
- (void)getAssessmentDescriptionByAssessmentId:(NSString *)assessmentId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getAssessmentDescription";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentId" andValue:assessmentId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_ASSESSMENT_DESCRIPTION];
}

-(void)getAssessmentDescriptionAck:(NSString *)result
{
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_GET_ASSESSMENT_DESCRIPTION];
}



@end
