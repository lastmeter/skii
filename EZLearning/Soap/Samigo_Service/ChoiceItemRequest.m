//
//  ChoiceItemRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChoiceItemRequest.h"
#import "ItemModel.h"
#import "AnswerModel.h"
#import "SectionModel.h"
#import "ChoiceCellFrameModel.h"

@implementation ChoiceItemRequest

#pragma mark
-(void)getAssessmentSectionByAssessmentId:(NSString *)assessmentId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getAssessmentSection";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentId" andValue:assessmentId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_ASSESSMENT_SECTION];
}

-(void)getAssessmentSectionAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *sectionsArray = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        SectionModel *section = [[SectionModel alloc] init];
        section.sectionTitle = obj[@"title"];
        section.sectionId = obj[@"id"];
        section.creatTime = [obj[@"time"] longValue];
        section.itemCount = [obj[@"size"] intValue];
        [sectionsArray addObject:section];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:sectionsArray SoapType:ST_GET_ASSESSMENT_SECTION];
}


#pragma mark
-(void)getItemBySectionBySectionId:(NSString *)sectionId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getItemBySection";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"section" andValue:sectionId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_ITEM_BY_SECTION];
}

-(void)getItemBySectionAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *itemsArray = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        ItemModel *item = [[ItemModel alloc] init];
        ChoiceCellFrameModel *cellFrame = [[ChoiceCellFrameModel alloc] init];
        item.score = obj[@"score"] ;
        item.itemId = obj[@"id"];
        item.itemType = [obj[@"typeId"] intValue];
        item.text = obj[@"text"];
        cellFrame.itemModel = item;
        [itemsArray addObject:cellFrame];
    }];
   
    [SoapNotiCenter postSoapNotificationWithInfo:itemsArray SoapType:ST_GET_ITEM_BY_SECTION];
}

#pragma mark
-(void)getAnswerByItemId:(NSString *)itemId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getAnswerOfItem";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"itemTextId" andValue:itemId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_ANSWER_OF_ITEM];
}

-(void)getAnswerOfItemAck:(NSString *)result
{
    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *answersArray = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        AnswerModel *answer = [[AnswerModel alloc] init];
        answer.label = obj[@"label"];
        answer.answerId = obj[@"answerId"];
        answer.text = obj[@"text"];
        answer.correct = [obj[@"isCorrect"] boolValue];
        [answersArray addObject:answer];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:answersArray SoapType:ST_GET_ANSWER_OF_ITEM];
}
@end
