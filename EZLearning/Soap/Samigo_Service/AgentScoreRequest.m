//
//  AgentScoreRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AgentScoreRequest.h"
#import "AgentScoreModel.h"

@implementation AgentScoreRequest

-(void)getScoreOfAgent
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getScoreOfAgent";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_GET_SCORE_OF_AGENT];
}

- (void)getScoreOfAgentAck:(NSString *)result
{

    NSArray *array = [JsonUtil json2Array:result];
    NSMutableArray *scores = [NSMutableArray array];
    [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
        AgentScoreModel *score = [[AgentScoreModel alloc] init];
        score.title = obj[@"title"];
        score.finalScore = obj[@"finalScore"];
        score.myScore = obj[@"totalAutoScore"];
        [scores addObject:score];
    }];
    [SoapNotiCenter postSoapNotificationWithInfo:scores SoapType:ST_GET_SCORE_OF_AGENT];
}

@end
