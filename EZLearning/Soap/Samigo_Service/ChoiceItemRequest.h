//
//  ChoiceItemRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface ChoiceItemRequest : SoapRequest

- (void)getAssessmentSectionByAssessmentId:(NSString *)assessmentId;
- (void)getItemBySectionBySectionId:(NSString *)sectionId;
- (void)getAnswerByItemId:(NSString *)itemId;

@end
