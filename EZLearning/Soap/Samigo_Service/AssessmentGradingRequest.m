//
//  AssessmentGradingRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AssessmentGradingRequest.h"

@implementation AssessmentGradingRequest

-(void)beginAssessmentGradingByAssessmentId:(NSString *)assessmentId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"beginAssessmentGrading";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentId" andValue:assessmentId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_BEGIN_ASSESSMENT_GTADING];
}

-(void)beginAssessmentGradingAck:(NSString *)result
{
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_BEGIN_ASSESSMENT_GTADING];
}

#pragma mark
-(void)updateSingleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId AnswerId:(NSString *)answerId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"updateAssessmentGradingDataSingle";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentGradingId" andValue:gradingId];
    [soapXmlUtil addParamKey:@"itemId" andValue:itemId];
    [soapXmlUtil addParamKey:@"answerId" andValue:answerId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_UPDATE_ASSESSMENT_DATA_SINGLE];
}

-(void)updateAssmentGradingDataSingleAck:(NSString *)result
{
    DLOG(@"updateAssmentGradingDataSingleAck : %@",result);
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_UPDATE_ASSESSMENT_DATA_SINGLE];
}

#pragma mark
-(void)updateMultipleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId Data:(NSString *)data
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"updateAssessmentGradingDataMultiple";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentGradingId" andValue:gradingId];
    [soapXmlUtil addParamKey:@"itemId" andValue:itemId];
    [soapXmlUtil addParamKey:@"data" andValue:data];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_UPDATE_ASSESSMENT_DATA_MULTIPLE];
}

-(void)updateAssmentGradingDataMultipleAck:(NSString *)result
{
    DLOG(@"updateAssmentGradingDataMultipleAck : %@",result);
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_UPDATE_ASSESSMENT_DATA_MULTIPLE];
}

#pragma mark
- (void)submitAssessmentGradingByGradingId:(NSString *)gradingId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"submitAssessmentGrading";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"assessmentGradingId" andValue:gradingId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:SAMIGO_SERVICE RequestType:ST_SUBMIT_ASSESSMENT_GRADING];
}

- (void)submitAssessmentGradingAck:(NSString *)result
{
     DLOG(@"submitAssessmentGradingAck : %@",result);
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_SUBMIT_ASSESSMENT_GRADING];
}


@end
