//
//  AssessmentGradingRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface AssessmentGradingRequest : SoapRequest

- (void)beginAssessmentGradingByAssessmentId:(NSString *)assessmentId;

-(void)updateSingleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId AnswerId:(NSString *)answerId;

- (void)updateMultipleGradingDataWithGradingId:(NSString *)gradingId ItemId:(NSString *)itemId Data:(NSString *)data;

- (void)submitAssessmentGradingByGradingId:(NSString *)gradingId;
@end
