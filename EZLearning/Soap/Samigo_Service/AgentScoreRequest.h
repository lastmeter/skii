//
//  AgentScoreRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface AgentScoreRequest : SoapRequest
-(void)getScoreOfAgent;
@end
