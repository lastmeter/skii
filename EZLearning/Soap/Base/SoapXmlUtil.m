//
//  SoapXmlUtil.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapXmlUtil.h"

@interface SoapXmlUtil()
{
    NSMutableDictionary *_paramsDic;
    NSMutableArray *_keyArray;
}
@end

@implementation SoapXmlUtil

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        _paramsDic = [NSMutableDictionary dictionary];
        _keyArray = [NSMutableArray array];
    }
    return self;
}

- (void)addParamKey:(NSString *)key andValue:(id)value
{
    if (_paramsDic == nil)
    {
        YXB_ASSERT;
        return;
    }
    if (_keyArray == nil)
    {
        YXB_ASSERT;
        return;
    }
    [_paramsDic setObject:value forKey:key];
    [_keyArray addObject:key];
}

- (NSString *)getSoapXmlString
{
    if (_paramsDic == nil)
    {
        YXB_ASSERT;
        return nil;
    }
    if (self.functionName == nil)
    {
        YXB_ASSERT;
        return nil;
    }
    
    NSMutableString *xmlString = [NSMutableString string];
    NSString *xmlHeader =
    @"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        "<soapenv:Envelope "
        "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
        "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
    [xmlString appendString:xmlHeader];
    
    NSString *xmlBodyHeader = [NSString stringWithFormat:
                               @"<soapenv:Body>"
                               "<%@>",self.functionName];
    [xmlString appendString:xmlBodyHeader];
    
    [_keyArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"<%@>%@</%@>",key,_paramsDic[key],key];
        [xmlString appendString:param];
    }];
    
    NSString *xmlBodyTail = [NSString stringWithFormat:
                             @"</%@>"
                             "</soapenv:Body>"
                             "</soapenv:Envelope>",self.functionName];
    
     [xmlString appendString:xmlBodyTail];
    
    return xmlString;
}

@end

