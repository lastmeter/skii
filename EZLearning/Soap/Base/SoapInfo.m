//
//  Soap.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//


#import "SoapInfo.h"


//NSString *const LOGIN_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/SakaiLogin.jws";
//
//NSString *const SCRIPT_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/SakaiScript.jws";
//
//NSString *const MLEARINF_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/MLearning.jws";
//
//NSString *const REVIEW_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/Review.jws";
//
//NSString *const UPDATE_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/ClientUpdate.jws";
//
//NSString *const SAMIGO_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/Samigo.jws";
//
//NSString *const FORUM_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/Forum.jws";



NSString *const LOGIN_SERVICE = @"http://www.lastmeter.cn/sakai-axis/SakaiLogin.jws";

NSString *const SCRIPT_SERVICE = @"http://www.lastmeter.cn/sakai-axis/SakaiScript.jws";

NSString *const MLEARINF_SERVICE = @"http://www.lastmeter.cn/sakai-axis/MLearning.jws";

NSString *const REVIEW_SERVICE = @"http://www.lastmeter.cn/sakai-axis/Review.jws";

NSString *const UPDATE_SERVICE = @"http://www.lastmeter.cn/sakai-axis/ClientUpdate.jws";

NSString *const SAMIGO_SERVICE = @"http://www.lastmeter.cn/sakai-axis/Samigo.jws";

NSString *const FORUM_SERVICE = @"http://www.lastmeter.cn/sakai-axis/Forum.jws";

NSString *const UPDATEVERSION_SERVICE = @"http://skii.lastmeter.com.cn/sakai-axis/ClientUpdate.jws";

