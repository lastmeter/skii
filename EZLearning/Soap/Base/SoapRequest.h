//
//  SoapRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonUtil.h"
#import "XMLUtil.h"
#import "SoapInfo.h"
#import "SoapConnect.h"
#import "SoapNotiCenter.h"
#import "SoapXmlUtil.h"
#import "UDManager.h"

@interface SoapRequest : NSObject <SoapAckDelegate>

@end
