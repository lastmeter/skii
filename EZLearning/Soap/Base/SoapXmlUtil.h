//
//  SoapXmlUtil.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoapXmlUtil : NSObject

@property (nonatomic, strong)NSString *functionName;

- (NSString *)getSoapXmlString;
- (void)addParamKey:(NSString *)key andValue:(id)value;

@end
