//
//  EZL_SoapRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/6.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "SoapInfo.h"

@protocol SoapAckDelegate <NSObject>

@optional

- (void)loginAck:(NSString *)result;
- (void)logoutAck:(NSString *)result;

- (void)changPasswordAck:(NSString *)result;

- (void)cancelFavoritesAck:(NSString *)result;
- (void)getCategoryListBySiteAck:(NSString *)result;
- (void)getCompanyAdvertAck:(NSString *)result;
- (void)getCourseByCategoryAck:(NSString *)result;
- (void)getCourseBySiteAck:(NSString *)result;
- (void)getFavoritesListAck:(NSString *)result;
- (void)getHeadPortraitAck:(NSString *)result;
- (void)getMarketScoreAck:(NSString *)result;
- (void)getMP4ForCourseAck:(NSString *)result;
- (void)getMyCourseAck:(NSString *)result;
- (void)getMySitesAck:(NSString *)result;
- (void)getNoticeListAck:(NSString *)result;
- (void)getUserInfoAck:(NSString *)result;
- (void)saveFavoritesAck:(NSString *)result;
- (void)getHotLineAck:(NSString *)result;
- (void)sendQuestionToAdminAck:(NSString *)result;

- (void)getContactsAck:(NSString *)result;
- (void)getUserJobAck:(NSString *)result;
- (void)getSubordinateRankAck:(NSString *)result;
- (void)getSubordinateScoreAck:(NSString *)result;
- (void)getChatWithUserAck:(NSString *)result;
- (void)sendChatAck:(NSString *)result;

- (void)getVersionInfoAck:(NSString *)result;
- (void)getCurversionAck:(NSString *)result;

- (void)getAllActivePublishedAssessmentAck:(NSString *)result;
- (void)getScoreOfAgentAck:(NSString *)result;
- (void)getAssessmentDescriptionAck:(NSString *)result;
- (void)getAssessmentSectionAck:(NSString *)result;
- (void)getItemBySectionAck:(NSString *)result;
- (void)getAnswerOfItemAck:(NSString *)result;
- (void)beginAssessmentGradingAck:(NSString *)result;
- (void)updateAssmentGradingDataSingleAck:(NSString *)result;
- (void)updateAssmentGradingDataMultipleAck:(NSString *)result;
- (void)submitAssessmentGradingAck:(NSString *)result;

- (void)getallForumofuserAck:(NSString *)result;
- (void)getTopicOfForumAck:(NSString *)result;
- (void)getMsglistbyTopicAck:(NSString *)result;
- (void)getMsgAndRepliesAck:(NSString *)result;
- (void)getaddMsgtoTopicAck:(NSString *)result;
- (void)addPraisetoMsgAck:(NSString *)result;
- (void)getsetHeadPortraitAck:(NSString *)result;
- (void)changeEmailandPhoneAck:(NSString *)result;
@end

@interface SoapConnect : NSObject <NSXMLParserDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate>

- (void)soapConnectWithXML:(NSString *)soapXML URL:(NSString *)url RequestType:(SOAP_TYPE)type;

@property (strong, nonatomic) id<SoapAckDelegate>delegate;

@end
