//
//  EZL_SoapRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/6.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapConnect.h"
#import "SoapNotiCenter.h"

@interface SoapConnect()

@property (copy, nonatomic) NSMutableString *soapResults;
@property (assign, nonatomic) SOAP_TYPE requestType;


@end

@implementation SoapConnect

- (void)soapConnectWithXML:(NSString *)soapXML URL:(NSString *)url RequestType:(SOAP_TYPE)type
{
    NSURL *URL = [NSURL URLWithString:url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@" " forHTTPHeaderField:@"SOAPAction"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[soapXML dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:20];
    
    self.requestType = type;
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (data)
         {
           //  DLOG(@"\nSoapRequestAck:\n{\n%@\n}", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
             [self parseXMLData:data];
         }
         else
         {
             DLOG(@"Soap with error:%@",connectionError);
             [SoapNotiCenter postSoapRequestResultNotification:NO];
         }
     }];
}

- (void)parseXMLData:(NSData *)data
{
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:data];//传入XML数据，创建解析器
    [xmlParser setDelegate: self];//设置代理，监听解析过程
    [xmlParser setShouldResolveExternalEntities:YES];
    [xmlParser parse];//开始解析
}


//当解析器对象遇到xml的开始标记时，调用这个方法。
//获得结点头的值
//解析到一个开始tag，开始tag中可能会有properpies，例如<book catalog="Programming">
//所有的属性都存储在attributeDict中
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    _soapResults = [NSMutableString string];
}

//当解析器找到开始标记和结束标记之间的字符时，调用这个方法。
//解析器，从两个结点之间读取具体内容
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [_soapResults appendString:string];
}

//xml解析结束后的一些操作可在此
- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    [SoapNotiCenter postSoapRequestResultNotification:YES];
    
    if (_soapResults == NULL || _soapResults.length <= 0)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:self.requestType];
    }
    else
    {
        [self procDataWithType:_requestType];
    }
}


- (void)procDataWithType:(SOAP_TYPE) type
{
    switch (type)
    {
        case ST_LOGIN:
            [_delegate loginAck:_soapResults];
            break;
        case ST_LOGOUT:
            [_delegate logoutAck:_soapResults];
            break;
        case ST_CHANGE_PASSWORD:
            [_delegate changPasswordAck:_soapResults];
             break;
        case ST_GET_NOTICE_LIST:
            [_delegate getNoticeListAck:_soapResults];
            break;
        case ST_GET_MY_COURSE:
            [_delegate getMyCourseAck:_soapResults];
            break;
        case ST_GET_MY_SITES:
            [_delegate getMySitesAck:_soapResults];
            break;
        case ST_GET_COURSE_BY_SITE:
            [_delegate getCourseBySiteAck:_soapResults];
            break;
        case ST_GET_CATEGORY_LIST_BY_SITE:
            [_delegate getCategoryListBySiteAck:_soapResults];
            break;
        case ST_GET_COURSE_BY_CATEGORY:
            [_delegate getCourseByCategoryAck:_soapResults];
            break;
        case ST_GET_MP4_FOR_COURSE:
            [_delegate getMP4ForCourseAck:_soapResults];
            break;
        case ST_GET_FAVORITES_LIST:
            [_delegate getFavoritesListAck:_soapResults];
            break;
        case ST_SAVE_FAVORITES:
            [_delegate saveFavoritesAck:_soapResults];
            break;
        case ST_CANCEL_FAVORITES:
            [_delegate cancelFavoritesAck:_soapResults];
            break;
        case ST_GET_MARKET_SCORE:
            [_delegate getMarketScoreAck:_soapResults];
            break;
        case ST_GET_USER_INFO:
            [_delegate getUserInfoAck:_soapResults];
            break;
        case ST_GET_HEAD_PORTRAIT:
            [_delegate getHeadPortraitAck:_soapResults];
            break;
        case ST_GET_HOTLINE:
            [_delegate getHotLineAck:_soapResults];
            break;
        case ST_GET_COMPANY_ADVERT:
            [_delegate getCompanyAdvertAck:_soapResults];
            break;
        case ST_GET_VERSION_INFO:
            [_delegate getVersionInfoAck:_soapResults];
            break;
        case ST_GET_CURVERSION:
            [_delegate getCurversionAck:_soapResults];
            break;
        case ST_GET_CONTACTS:
            [_delegate getContactsAck:_soapResults];
            break;
        case ST_GET_USER_JOB:
            [_delegate getUserJobAck:_soapResults];
            break;
        case ST_GET_SUBORDINATE_RANK:
            [_delegate getSubordinateRankAck:_soapResults];
            break;
        case ST_GET_SUBORDINATE_SCORE:
            [_delegate getSubordinateScoreAck:_soapResults];
            break;
        case ST_GET_CHAT_WITH_USER:
            [_delegate getChatWithUserAck:_soapResults];
            break;
        case ST_SEND_CHAT:
            [_delegate sendChatAck:_soapResults];
            break;
        case ST_SEND_QUESTION_TO_ADMIN:
            [_delegate sendQuestionToAdminAck:_soapResults];
            break;
        case ST_GET_ALL_ASSESSMENTS:
            [_delegate getAllActivePublishedAssessmentAck:_soapResults];
            break;
        case ST_GET_SCORE_OF_AGENT:
            [_delegate getScoreOfAgentAck:_soapResults];
            break;
        case ST_GET_ASSESSMENT_DESCRIPTION:
            [_delegate getAssessmentDescriptionAck:_soapResults];
            break;
        case ST_GET_ASSESSMENT_SECTION:
            [_delegate getAssessmentSectionAck:_soapResults];
            break;
        case ST_GET_ITEM_BY_SECTION:
            [_delegate getItemBySectionAck:_soapResults];
            break;
        case ST_GET_ANSWER_OF_ITEM:
            [_delegate getAnswerOfItemAck:_soapResults];
            break;
        case ST_BEGIN_ASSESSMENT_GTADING:
            [_delegate beginAssessmentGradingAck:_soapResults];
            break;
        case ST_UPDATE_ASSESSMENT_DATA_SINGLE:
            [_delegate updateAssmentGradingDataSingleAck:_soapResults];
            break;
        case ST_UPDATE_ASSESSMENT_DATA_MULTIPLE:
            [_delegate updateAssmentGradingDataMultipleAck:_soapResults];
            break;
        case ST_SUBMIT_ASSESSMENT_GRADING:
            [_delegate submitAssessmentGradingAck:_soapResults];
            break;
        case ST_GET_ALLFORUM_OFUSER:
            [_delegate getallForumofuserAck:_soapResults];
            break;
        case ST_GET_TOPIC_OF_FORUM:
            [_delegate getTopicOfForumAck:_soapResults];
            break;
        case ST_GET_MSGSUMMARY_OF_TOPIC:
            [_delegate getMsglistbyTopicAck:_soapResults];
            break;
        case ST_GET_MSG_AND_REPLIES:
            [_delegate getMsgAndRepliesAck:_soapResults];
            break;
        case ST_ADD_MSG_TO_TOPIC:
            [_delegate getaddMsgtoTopicAck:_soapResults];
            break;
        case ST_ADD_PRAISE_TO_MSG:
            [_delegate addPraisetoMsgAck:_soapResults];
            break;
        case ST_SET_HEAD_PORTRAIT:
            [_delegate getsetHeadPortraitAck:_soapResults];
            break;
        case ST_CHANGE_EMAIL_AND_MOBILE:
            [_delegate changeEmailandPhoneAck:_soapResults];
            break;
        default:
            YXB_ASSERT;
            DLOG(@"error request type :%d",type);
            break;
    }
}

@end
