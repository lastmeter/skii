//
//  Soap.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/16.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const LOGIN_SERVICE;
extern NSString *const SCRIPT_SERVICE;
extern NSString *const MLEARINF_SERVICE;
extern NSString *const REVIEW_SERVICE;
extern NSString *const UPDATE_SERVICE;
extern NSString *const SAMIGO_SERVICE;
extern NSString *const FORUM_SERVICE;
//extern NSString *const UPDATEVERSION_SERVICE;
typedef enum
{
    ST_LOGIN                            =   0,
    ST_LOGOUT                           =   1,
    ST_CHANGE_PASSWORD                  =   2,
    ST_GET_NOTICE_LIST                  =   3,
    ST_GET_MY_COURSE                    =   4,
    ST_GET_MY_SITES                     =   5,
    ST_GET_COURSE_BY_SITE               =   6,
    ST_GET_CATEGORY_LIST_BY_SITE        =   7,
    ST_GET_COURSE_BY_CATEGORY           =   8,
    ST_GET_MP4_FOR_COURSE               =   9,
    ST_GET_FAVORITES_LIST               =   10,
    ST_SAVE_FAVORITES                   =   11,
    ST_CANCEL_FAVORITES                 =   12,
    ST_GET_MARKET_SCORE                 =   13,
    ST_GET_USER_INFO                    =   14,
    ST_GET_HEAD_PORTRAIT                =   15,
    ST_GET_HOTLINE                      =   16,
    ST_GET_COMPANY_ADVERT               =   17,
    ST_GET_VERSION_INFO                 =   18,
    ST_GET_CURVERSION                   =   19,
    ST_GET_CONTACTS                     =   20,
    ST_GET_USER_JOB                     =   21,
    ST_GET_CHAT_WITH_USER               =   22,
    ST_GET_SUBORDINATE_RANK             =   23,
    ST_GET_SUBORDINATE_SCORE            =   24,
    ST_SEND_CHAT                        =   25,
    ST_SEND_QUESTION_TO_ADMIN           =   26,
    ST_GET_ALL_ASSESSMENTS              =   27,
    ST_GET_SCORE_OF_AGENT               =   28,
    ST_GET_ASSESSMENT_DESCRIPTION       =   29,
    ST_GET_ASSESSMENT_SECTION           =   30,
    ST_GET_ITEM_BY_SECTION              =   31,
    ST_GET_ANSWER_OF_ITEM               =   32,
    ST_BEGIN_ASSESSMENT_GTADING         =   33,
    ST_UPDATE_ASSESSMENT_DATA_SINGLE    =   34,
    ST_UPDATE_ASSESSMENT_DATA_MULTIPLE  =   35,
    ST_SUBMIT_ASSESSMENT_GRADING        =   36,
    ST_GET_ALLFORUM_OFUSER              =   37,
    ST_GET_TOPIC_OF_FORUM               =   38,
    ST_GET_MSGSUMMARY_OF_TOPIC          =   39,
    ST_GET_MSG_AND_REPLIES              =   40,
    ST_ADD_MSG_TO_TOPIC                 =   41,
    ST_ADD_PRAISE_TO_MSG                =   42,
    ST_SET_HEAD_PORTRAIT                =   43,
    ST_CHANGE_EMAIL_AND_MOBILE          =   44,
    ST_ERROR
} SOAP_TYPE;

typedef enum
{
    UJ_BOSS,
    UJ_BC,
    UJ_UNKONW
}USER_JOB_TYPE;

