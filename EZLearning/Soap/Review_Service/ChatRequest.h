//
//  ChatRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface ChatRequest : SoapRequest

- (void)getChatByUserId:(NSString *)userId AfterDate:(long)afterDate;
- (void)sendChatContent:(NSString *)content andDate:(long)date toUserId:(NSString *)userId;
@end
