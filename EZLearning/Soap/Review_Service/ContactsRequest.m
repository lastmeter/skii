//
//  ContactsRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ContactsRequest.h"
#import "ContactModel.h"
#import "SubordinateRankModel.h"
#import "SubordinateScoreModel.h"

@implementation ContactsRequest

- (void)getContacts
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getContacts";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_GET_CONTACTS];
}

-(void)getContactsAck:(NSString *)result
{
    if (result != nil )
    {
        NSArray *array = [JsonUtil json2Array:result];
        NSMutableArray *contacts = [NSMutableArray array];
        
        NSArray *sortArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *  _Nonnull obj1, NSDictionary *  _Nonnull obj2) {
            if ([obj1[@"boss"] boolValue]) {
                return NSOrderedAscending;
            }
            return NSOrderedDescending;
        }];
        
        [sortArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            ContactModel *contact = [[ContactModel alloc] init];
            contact.userId = obj[@"id"];
            contact.lastName = obj[@"lastName"];
            contact.firstName = obj[@"firstName"];
            contact.boss = [obj[@"boss"]boolValue];
            [contacts addObject:contact];
        }];
        
        [SoapNotiCenter postSoapNotificationWithInfo:contacts SoapType:ST_GET_CONTACTS];
    }
    else
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_CONTACTS];
    }
}

#pragma mark

- (void)getSubordinateRank
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getSubordinateRank";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_GET_SUBORDINATE_RANK];
}

-(void)getSubordinateRankAck:(NSString *)result
{
    if (result != nil)
    {
        NSArray *array = [JsonUtil json2Array:result];
        NSMutableArray *rank = [NSMutableArray array];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            SubordinateRankModel *subordinate = [[SubordinateRankModel alloc] init];
            subordinate.lastName = obj[@"lastName"];
            subordinate.userId = obj[@"id"];
            subordinate.account = obj[@"account"];
            subordinate.passNum = [obj[@"passedNum"]intValue];
            subordinate.score = obj[@"score"];
            subordinate.btnTag = idx;
            subordinate.credit = obj[@"credit"];
            subordinate.star = obj[@"star"];
            [rank addObject:subordinate];
        }];
        [SoapNotiCenter postSoapNotificationWithInfo:rank SoapType:ST_GET_SUBORDINATE_RANK];
    }
    else
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_SUBORDINATE_RANK];
    }
}

#pragma mark

- (void)getSubordinateScoreByUserId:(NSString *)userId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getSubordinateScore";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"user" andValue:userId];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_GET_SUBORDINATE_SCORE];
}

-(void)getSubordinateScoreAck:(NSString *)result
{
    if (result != nil)
    {
        NSArray *array = [JsonUtil json2Array:result];
        NSMutableArray *score = [NSMutableArray array];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            SubordinateScoreModel *subordinate = [[SubordinateScoreModel alloc] init];
            subordinate.title = obj[@"title"];
            subordinate.pass = [obj[@"status"] intValue] ? YES:NO;
            subordinate.score = [NSString stringWithFormat:@"%.1f", [obj[@"score"]floatValue]];
            [score addObject:subordinate];
        }];
        [SoapNotiCenter postSoapNotificationWithInfo:score SoapType:ST_GET_SUBORDINATE_SCORE];
    }
    else
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_SUBORDINATE_SCORE];
    }
}



@end
