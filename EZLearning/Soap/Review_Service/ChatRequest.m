//
//  ChatRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChatRequest.h"
#import "MessageModel.h"
#import "DateUtil.h"

@interface ChatRequest()
{
    NSString *_myUserId;
}
@end

@implementation ChatRequest

- (void)sendChatContent:(NSString *)content andDate:(long)date toUserId:(NSString *)userId
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"sendChat";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"content" andValue:content];
    [soapXmlUtil addParamKey:@"to" andValue:userId];
    [soapXmlUtil addParamKey:@"time" andValue:@(date)];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_SEND_CHAT];
}

-(void)sendChatAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_SEND_CHAT];
        return;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_SEND_CHAT];
}

#pragma mark

- (void)getChatByUserId:(NSString *)userId AfterDate:(long)afterDate
{
    _myUserId = userId;
    
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getChatWithUser";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"user" andValue:userId];
    [soapXmlUtil addParamKey:@"afterDate" andValue:[NSNumber numberWithLong:afterDate]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_GET_CHAT_WITH_USER];
}

-(void)getChatWithUserAck:(NSString *)result
{
    if (result!= nil )
    {
        NSArray *array = [JsonUtil json2Array:result];
        NSMutableArray *chat = [NSMutableArray array];
        [array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            MessageModel *message = [[MessageModel alloc] init];
            message.text = obj[@"content"];
            long long time = [obj[@"time"] longLongValue]/1000;
            message.timeSp = time;
            message.messageId = obj[@"id"];
            message.time = [DateUtil dateFromTimeSp:time];
            message.fromUserId = obj[@"from"];
            message.toUserId = obj[@"to"];
            NSString *userId = obj[@"from"];
            if ([userId isEqualToString:_myUserId])
            {
                message.type = kMessageModelTypeMe;
            }
            else
            {
                message.type = kMessageModelTypeOther;
            }
            message.showTime = YES;
            [chat addObject:message];
        }];
        NSArray *sort = [self messageArraySortByDate:chat];
        [SoapNotiCenter postSoapNotificationWithInfo:sort SoapType:ST_GET_CHAT_WITH_USER];
    }
    else
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_GET_CHAT_WITH_USER];
    }
    
}


- (NSArray *)messageArraySortByDate:(NSArray *)array
{
    NSArray *sortArray;
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"timeSp" ascending:YES];
    NSArray *descs = @[descriptor];
    sortArray = [array sortedArrayUsingDescriptors:descs];
    return sortArray;
}


@end
