//
//  ContactsRequest.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SoapRequest.h"

@interface ContactsRequest : SoapRequest
- (void)getSubordinateScoreByUserId:(NSString *)userId;
- (void)getSubordinateRank;
- (void)getContacts;
@end
