//
//  GetUserJobRequest.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "GetUserJobRequest.h"

@implementation GetUserJobRequest

- (void)getUserJob
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"getUserJob";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:REVIEW_SERVICE RequestType:ST_GET_USER_JOB];
}

-(void)getUserJobAck:(NSString *)result
{
    int type = UJ_UNKONW;
    
    if ([result isEqualToString:@"bc"])
    {
        type = UJ_BC;
    }
    else if ([result isEqualToString:@"leader"])
    {
        type = UJ_BOSS;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:[NSNumber numberWithInt:type] SoapType:ST_GET_USER_JOB];
}


@end
