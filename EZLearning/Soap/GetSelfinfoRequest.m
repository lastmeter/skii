//
//  GetSelfinfoRequest.m
//  EZLearning
//
//  Created by zwf on 15/6/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "GetSelfinfoRequest.h"

@implementation GetSelfinfoRequest

- (void)setHeadPortrait:(NSString *)imageStr MimeTYPE:(NSString *)mimeType FILEName:(NSString *)filename
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"setHeadPortrait";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"imageStr" andValue:imageStr];
    [soapXmlUtil addParamKey:@"mimeType" andValue:mimeType];
    [soapXmlUtil addParamKey:@"fileName" andValue:filename];
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_SET_HEAD_PORTRAIT];
}
- (void)getsetHeadPortraitAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_SET_HEAD_PORTRAIT];
        return;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_SET_HEAD_PORTRAIT];
    
}

- (void)changeEmailandMobile:(NSString *)email phoneNum:(NSString *)mobile
{
    SoapXmlUtil *soapXmlUtil = [[SoapXmlUtil alloc] init];
    soapXmlUtil.functionName = @"changeEmailAndMobile";
    [soapXmlUtil addParamKey:@"sessionId" andValue:[UDManager getSessionID]];
    [soapXmlUtil addParamKey:@"email" andValue:email];
    [soapXmlUtil addParamKey:@"mobile" andValue:mobile];
    
    NSString *soapXml = [soapXmlUtil getSoapXmlString];
    
    SoapConnect *connect = [[SoapConnect alloc] init];
    connect.delegate = self;
    [connect soapConnectWithXML:soapXml URL:MLEARINF_SERVICE RequestType:ST_CHANGE_EMAIL_AND_MOBILE];
}

- (void)changeEmailandPhoneAck:(NSString *)result
{
    if (result == nil)
    {
        [SoapNotiCenter postSoapEmptyResultNotificationWithSoapType:ST_CHANGE_EMAIL_AND_MOBILE];
        return;
    }
    [SoapNotiCenter postSoapNotificationWithInfo:result SoapType:ST_CHANGE_EMAIL_AND_MOBILE];
}
@end
