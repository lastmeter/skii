//
//  DBManager.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/7.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "UDManager.h"

@implementation UDManager

+ (void)savePassword:(NSString *)password
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:password forKey:@"password"];
    [defaults synchronize];//同步数据到磁盘
}

+ (void)saveUsername:(NSString *)username
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username forKey:@"username"];
    [defaults synchronize];
}

+(NSString *)getPassword
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"password"];
}

+ (NSString *)getUsername
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"username"];
}

+ (void) saveChangePasswordDate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    [defaults setObject:[NSDate date] forKey:username];
    [defaults synchronize];
}

+ (NSDate*)getChangePasswordDateByUsername:(NSString *)username
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:username];
}

+ (void)saveSessionID:(NSString *)sessionID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:sessionID forKey:@"sessionID"];
    [defaults synchronize];
}

+ (NSString *)getSessionID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"sessionID"];
}

+ (BOOL)getRememberPwdState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *result =  [defaults objectForKey:@"rememberPwd"];
    return [result boolValue];
}

+(void)saveRememberPwdState:(BOOL) state
{
    NSNumber *result = [NSNumber numberWithBool:state];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:result forKey:@"rememberPwd"];
    [defaults synchronize];
}


+ (void)saveLastCheckMomentsDate:(NSDate *)date{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_momnetDate",eid];
    [defaults setObject:date forKey:key];
    [defaults synchronize];
}

+ (NSDate *)getLastCheckMomentsDate{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_momnetDate",eid];
    return [defaults objectForKey:key];
}

+ (void)saveUnreadMomentNumber:(NSInteger)number{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_unread",eid];
    [defaults setObject:@(number) forKey:key];
    [defaults synchronize];
}

+ (NSInteger)getUnreadMomentNumber{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_unread",eid];
    NSNumber *number = [defaults objectForKey:key] ;
    if (number) {
        return [number integerValue];
    }
    return 0;
}

+ (void)saveUnreadStaffInfo:(NSDictionary *)dic{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_Staff",eid];
    [defaults setObject:dic forKey:key];
    [defaults synchronize];
}

+ (NSDictionary *)getUnreadStaffInfo{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *eid = [defaults objectForKey:@"username"];
    NSString *key = [NSString stringWithFormat:@"%@_Staff",eid];
    id dic = [defaults objectForKey:key] ;
    if (dic) {
        if ([dic isKindOfClass:[NSDictionary class]]) {
            return dic;
        }
        [defaults removeObjectForKey:key];
    }
    return nil;
}

@end
