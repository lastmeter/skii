//
//  DBManager.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/7.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UDManager : NSObject

+ (NSString *)getPassword;
+ (NSString *)getUsername;
+ (NSDate *)getChangePasswordDateByUsername:(NSString *)username;
+ (NSString *)getSessionID;
+ (BOOL)getRememberPwdState;

+ (void) saveChangePasswordDate;
+ (void) saveUsername:(NSString *)username;
+ (void) savePassword:(NSString *)password;
+ (void) saveSessionID:(NSString *)sessionID;
+ (void) saveRememberPwdState:(BOOL) state;


#pragma mark - Add In 2015.12.17
+ (void)saveLastCheckMomentsDate:(NSDate *)date;
+ (NSDate *)getLastCheckMomentsDate;

+ (void)saveUnreadMomentNumber:(NSInteger)number;
+ (NSInteger)getUnreadMomentNumber;

+ (void)saveUnreadStaffInfo:(NSDictionary *)dic;
+ (NSDictionary *)getUnreadStaffInfo;
@end
