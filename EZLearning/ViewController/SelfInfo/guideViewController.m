//
//  guideViewController.m
//  EZLearning
//
//  Created by zwf on 15/5/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "guideViewController.h"

@interface guideViewController ()<UIScrollViewDelegate>
{
    int pancount;
}

@property(nonatomic, strong)UIScrollView  *scrollView;
@property(nonatomic, strong)UIPageControl *pageControl;
@end

@implementation guideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showScrollView];
}

#pragma mark - 滑动图

-(void) showScrollView{
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    _scrollView.contentSize = CGSizeMake(MAIN_SCREEN_WIDTH*6, MAIN_SCREEN_HEIGHT);
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.alpha = 0.8;
    _scrollView.tag = 101;
    _scrollView.pagingEnabled = YES;
    _scrollView.bounces = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.delegate = self;
    for (int i = 0 ; i < 6; i ++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width* i , 0, self.view.frame.size.width, self.view.frame.size.height)];
        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"guide%d",i]];
        [_scrollView addSubview:imageView];
    }
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets =NO;
    }
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-25, self.view.frame.size.height-60, 50, 40)];
    _pageControl.numberOfPages = 6;
    _pageControl.tag = 201;
    [self.view addSubview:_scrollView];
    [self.view addSubview: _pageControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    int current = scrollView.contentOffset.x/self.view.frame.size.width;
    UIPageControl *page = (UIPageControl *)[self.view viewWithTag:201];
    CGPoint translation = [_scrollView.panGestureRecognizer translationInView:_scrollView];
    page.currentPage = current;
    if (page.currentPage != 5) {
        pancount = 0;
    }
    if (page.currentPage == 5&&translation.x<0) {
        pancount++;
        if (pancount==2) {
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
}

@end
