//
//  ExaminationCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AssessmentCell.h"
#import "AssessmentModel.h"

@interface AssessmentCell()
{
    UILabel *_title;
    UILabel *_time;
    UILabel *_count;
}
@end

@implementation AssessmentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 200, 21)];
        _title.textColor = [UIColor blackColor];
        _title.font = [UIFont boldSystemFontOfSize:17];
        [self.contentView addSubview: _title];
        
        UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 27, 70, 21)];
        lab1.textColor = [UIColor blackColor];
        lab1.font = [UIFont systemFontOfSize:15];
        lab1.text = @"考试时间:";
        [self.contentView addSubview:lab1];
        
        _time = [[UILabel alloc] initWithFrame:CGRectMake(lab1.frame.size.width+lab1.frame.origin.x+5, 27, 50, 21)];
        _time.textColor = [UIColor blackColor];
        _time.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_time];
        
        UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(0.5*MAIN_SCREEN_WIDTH+5, 27, 70, 21)];
        lab2.textColor = [UIColor blackColor];
        lab2.font = [UIFont systemFontOfSize:15];
        lab2.text = @"提交次数:";
        [self.contentView addSubview:lab2];
        
        _count = [[UILabel alloc] initWithFrame:CGRectMake(lab2.frame.origin.x+lab2.frame.size.width+5, 27, 80, 21)];
        _count.textColor = [UIColor blackColor];
        _count.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_count];
    }
    return self;
}

-(void)setAssessment:(AssessmentModel *)assessment
{
    _title.text = assessment.assessmentTitle;
    _time.text = assessment.timeLimit == 0 ? @"不限制" : [NSString stringWithFormat:@"%d分钟",assessment.timeLimit] ;
    _count.text = assessment.isUnlimitedSubmissions ? @"不限制" : [NSString stringWithFormat:@"仅限%d/%d次",assessment.grading ,assessment.submissionsAllowed];
}

+(CGFloat)cellHeight
{
    return 52;
}
@end
