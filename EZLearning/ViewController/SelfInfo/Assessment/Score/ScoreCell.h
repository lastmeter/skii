//
//  ScoreCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AgentScoreModel;

@interface ScoreCell : UITableViewCell

@property(nonatomic, strong)AgentScoreModel *scoreModel;

@end
