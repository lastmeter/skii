//
//  ScoreViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ScoreViewController.h"
#import "ScoreCell.h"
#import "AgentScoreModel.h"

@interface ScoreViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_scoreSource;
    LoadingView *_loadingView;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ScoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(getScore:) SoapType:ST_GET_SCORE_OF_AGENT];
    [SoapManager getScoreOfAgent];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
    [self makeExtraCellLineHidden];
}

- (void)getScore:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _scoreSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN( ^{
            [_tableView reloadData];
            [_loadingView stopLoading];
        });
    }
    [_loadingView removeFromSuperview];
    _loadingView = nil;
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_SCORE_OF_AGENT];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _scoreSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"score"];
    if (cell == nil)
    {
        cell = [[ScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"score"];
    }
    cell.scoreModel = _scoreSource[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselect];
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
