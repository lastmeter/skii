//
//  ScoreCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ScoreCell.h"
#import "AgentScoreModel.h"

@interface ScoreCell()
{
    UILabel *_scoreLabel;
}
@end

@implementation ScoreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 10 - 80, 11, 80, 21)];
        _scoreLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:_scoreLabel];
    }
    return self;
}

-(void)setScoreModel:(AgentScoreModel *)scoreModel
{
    self.textLabel.text = scoreModel.title;
    _scoreLabel.text = [NSString stringWithFormat:@"%@/%@",scoreModel.myScore,scoreModel.finalScore];
}

@end
