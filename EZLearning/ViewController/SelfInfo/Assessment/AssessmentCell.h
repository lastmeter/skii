//
//  ExaminationCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AssessmentModel;

@interface AssessmentCell : UITableViewCell

@property (nonatomic, strong)AssessmentModel *assessment;

+(CGFloat)cellHeight;

@end
