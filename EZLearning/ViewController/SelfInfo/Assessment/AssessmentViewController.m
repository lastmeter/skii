//
//  AssessmentViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/22.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "AssessmentViewController.h"
#import "ScoreViewController.h"
#import "AssessmentCell.h"
#import "AssessmentModel.h"
#import "SectionModel.h"
#import "ChoiceViewController.h"
#import "ExamViewController.h"

@interface AssessmentViewController ()<UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
{
    NSArray *_assessmentsSource;
    LoadingView *_loadingView;
    NSString *_assessmentGradingId;
    NSIndexPath *_selectedIndex;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AssessmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvAssessments:) SoapType:ST_GET_ALL_ASSESSMENTS];
   
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSections:) SoapType:ST_GET_ASSESSMENT_SECTION];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvExamInfo:) SoapType:ST_GET_ASSESSMENT_DESCRIPTION];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
    [self makeExtraCellLineHidden];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SoapManager getAllActivePublishedAssessments];
}

- (void) recvAssessments:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _assessmentsSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
        });
    }
}

- (void)recvSections:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        
        NSArray *array = [SoapNotiCenter getInfoFromNotification:noti];
        NSMutableArray *titleArray = [NSMutableArray array];
        NSMutableArray *viewArray = [NSMutableArray array];
        [array enumerateObjectsUsingBlock:^(SectionModel *obj, NSUInteger idx, BOOL *stop) {
            [titleArray addObject:obj.sectionTitle];
            
            ChoiceViewController *choiceViewController = [[ChoiceViewController alloc] initWithNibName:@"ChoiceViewController" bundle:nil];
            choiceViewController.sectionId = obj.sectionId;
            choiceViewController.assessmentGradingId = _assessmentGradingId;
            [viewArray addObject:choiceViewController];
            
        }];
        
        ExamViewController *examViewController =  [[ExamViewController alloc] init];
        AssessmentModel *assessment = _assessmentsSource[_selectedIndex.row];
        examViewController.examName = assessment.assessmentTitle;
        examViewController.assessmentGradingId = _assessmentGradingId;
        examViewController.timeLimit = assessment.timeLimit;
        [examViewController setItemNameWithArray:titleArray];
        [examViewController setTabViewControllers:viewArray];
        
        MAIN( ^{
            [self.navigationController pushViewController:examViewController animated:YES];
        });
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _assessmentsSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [AssessmentCell cellHeight];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssessmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exam"];
    if (cell == nil)
    {
        cell = [[AssessmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"exam"];
    }
    cell.assessment = _assessmentsSource[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndex = indexPath;
    
    AssessmentModel *assessment = _assessmentsSource[indexPath.row];
    if ( assessment.isUnlimitedSubmissions || assessment.grading < assessment.submissionsAllowed )
    {
        [SoapManager getAssessmentDescriptionByAssessmentId:assessment.assessmentId];
        [_loadingView startLoading];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"您已经参加完这个考试" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self deselect];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)recvExamInfo:(NSNotification *)noti
{
    NSString *info = [SoapNotiCenter getInfoFromNotification:noti];
    AssessmentModel *assessment = _assessmentsSource[_selectedIndex.row];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:assessment.assessmentTitle message:info delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"立即考试", nil];
    MAIN(^{
        [alert show];
    });
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self startExam];
    }
}

-(void)startExam
{
     AssessmentModel *assessmente = _assessmentsSource[_selectedIndex.row];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvAssessmentGradingId:) SoapType:ST_BEGIN_ASSESSMENT_GTADING];
    [SoapManager beginAssessmentGradingByAssessmentId:assessmente.assessmentId];
    [_loadingView startLoading];
}

- (void)recvAssessmentGradingId:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        
        NSString *gradingId = [SoapNotiCenter getInfoFromNotification:noti];
        if ([gradingId intValue] == -1)
        {
            YXB_ASSERT;
            DLOG(@"can not recvAssessmentGradingId");
            return;
        }
        _assessmentGradingId = gradingId;
        AssessmentModel *assessmente = _assessmentsSource[_selectedIndex.row];
        [SoapManager getAssessmentSectionByAssessmentId:assessmente.assessmentId];
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_BEGIN_ASSESSMENT_GTADING];
}


- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkScore:(id)sender
{
    ScoreViewController *scoreViewController = [[ScoreViewController alloc] initWithNibName:@"ScoreViewController" bundle:nil];
    [self.navigationController pushViewController:scoreViewController animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
