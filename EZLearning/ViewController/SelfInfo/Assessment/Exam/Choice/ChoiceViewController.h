//
//  SChoiceViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "EzlViewController.h"

@interface ChoiceViewController : EzlViewController

@property (nonatomic, strong)NSString *sectionId;
@property (nonatomic, strong)NSString *assessmentGradingId;
@property (nonatomic, assign)BOOL timeLimit;

@end
