//
//  ChoiceCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChoiceCell.h"
#import "ItemModel.h"
#import "ChoiceCellFrameModel.h"

@interface ChoiceCell()
{
    UITextView *_textView;
    UILabel *_text, *_score, *_answer;
}
@end

@implementation ChoiceCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        UIFont *detailFont = [UIFont systemFontOfSize:[ChoiceCellFrameModel choiceCellDetailFontSize]];
        
        _textView = [[UITextView alloc] init];
        _textView.scrollEnabled = NO;
        _textView.userInteractionEnabled = NO;
        _textView.textColor = [UIColor blackColor];
        _textView.font = [UIFont systemFontOfSize:[ChoiceCellFrameModel choiceCellTitleFontSize]];
        [self.contentView addSubview:_textView];
        
        _score = [[UILabel alloc] init];
        _score.font = detailFont;
        _score.textColor = [UIColor blackColor];
        [self.contentView addSubview:_score];
        
        _answer = [[UILabel alloc] init];
        _answer.font = detailFont;
        _answer.textColor = [UIColor blackColor];
        _answer.hidden = YES;
        [self.contentView addSubview:_answer];
    }
    return self;
}

- (void)setCellFrame:(ChoiceCellFrameModel *)cellFrame
{
    _cellFrame = cellFrame;
    _textView.frame = cellFrame.textFrame;
    _score.frame = cellFrame.scoreFrame;
    _answer.frame = cellFrame.answerFrame;
    
    ItemModel *item = cellFrame.itemModel;
    _textView.text = [NSString stringWithFormat:@"%@",item.text];
    _score.text = [NSString stringWithFormat:@"分值:%@分",item.score];
    _answer.hidden = !item.isChosen;
    _answer.text = [NSString stringWithFormat:@"选择:%@",item.userChoice];
}

@end
