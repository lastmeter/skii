//
//  ChoiceCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ChoiceCellFrameModel;

@interface ChoiceCell : UITableViewCell
@property(nonatomic, strong)ChoiceCellFrameModel *cellFrame;

@end
