//
//  SChoiceViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChoiceViewController.h"
#import "ChoiceCell.h"
#import "ItemModel.h"
#import "AnswerModel.h"
#import "AnswerViewModel.h"
#import "ChoiceCellFrameModel.h"
#import "AnswerCell.h"
#import "AnswerView.h"
#import "JsonUtil.h"

@interface ChoiceViewController ()<UITableViewDataSource, UITableViewDelegate,AnswerViewDelegate>
{
    NSMutableArray *_answersSource, *_itemsSource, *_selectedAnswersSource;
    LoadingView *_loadingView;
    NSInteger _selectedRow;
    AnswerViewModel *_avModel;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) AnswerView *answerView;
@end

@implementation ChoiceViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _answerView = [[AnswerView alloc] init];
    _answerView.tableView.delegate = self;
    _answerView.tableView.dataSource = self;
    _answerView.delegate = self;
    
    _avModel = [[AnswerViewModel alloc] init];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvItems:) SoapType:ST_GET_ITEM_BY_SECTION];
    [SoapManager getItemBySectionBySectionId:_sectionId];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvAnswersOfItem:) SoapType:ST_GET_ANSWER_OF_ITEM];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_ANSWER_OF_ITEM];
    [_answerView removeFromSuperview];
}

-(void)setTimeLimit:(BOOL)timeLimit
{
    _bottomConstraint.constant = timeLimit? 44:0;
}


- (void)recvItems:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _itemsSource = [SoapNotiCenter getInfoFromNotification:noti];
        if (_selectedAnswersSource == nil)
        {
            _selectedAnswersSource = [NSMutableArray array];
        }
        for (int i = 0; i<_itemsSource.count; i++)
        {
            NSDictionary *dic = @{@"empty":@"YES"};
            _selectedAnswersSource[i] = dic;
        }
        MAIN( ^{
            [_tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_ITEM_BY_SECTION];
}

- (void)recvAnswersOfItem:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _answersSource = [SoapNotiCenter getInfoFromNotification:noti];
      
        _avModel.answerArray = _answersSource;
        _selectedAnswersSource[_selectedRow] = @{@"empty":@"NO",@"source":_answersSource};
        MAIN( ^{
            _answerView.answerViewModel = _avModel;
            [_loadingView stopLoading];
            [self.navigationController.view addSubview:_answerView];
            [_answerView.tableView reloadData];
        });
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableView)
    {
        return _itemsSource.count;
    }
    return _answersSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        ChoiceCellFrameModel *frame = _itemsSource[indexPath.row];
        return frame.cellHeght;
    }
    AnswerModel *answer = _answersSource[indexPath.row];
    return answer.cellSize.height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        ChoiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"choice"];
        if (cell == nil)
        {
            cell = [[ChoiceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"choice"];
        }
        cell.cellFrame = _itemsSource[indexPath.row];
        _selectedRow = indexPath.row;
        return cell;
    }
    else
    {
        AnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"item"];
        if (cell == nil)
        {
            cell = [[AnswerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"item"];
        }
        ChoiceCellFrameModel *cellFrame = _itemsSource[_selectedRow];
        cell.type = cellFrame.itemModel.itemType;
        cell.answer = _answersSource[indexPath.row];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
    {
        _selectedRow = indexPath.row;
        [self setAnswerViewType];
        [self reSetAnswerView];
    }
    else
    {
        ChoiceCellFrameModel *cellFrame = _itemsSource[_selectedRow];
        AnswerModel *answer = _answersSource[indexPath.row];
        if (cellFrame.itemModel.itemType == MULTIPLE_CHOICE)
        {
            [_answersSource enumerateObjectsUsingBlock:^(AnswerModel *obj, NSUInteger idx, BOOL *stop) {
                obj.chosen = NO;
            }];
        }
        answer.chosen = !answer.chosen;
        _answersSource[indexPath.row] = answer;
        [tableView reloadData];
    }
    [self deselect];
}

-(void)choiceBtnClick:(CHOICE_BTN_TYPE)type
{

    if (type == CBT_CANCEL )
    {
        if (AVT_LAST == _avModel.type)
        {
            [self saveUserChoice];
        }
        return;
    }
    
    [self saveUserChoice];
    
    if ( type == CBT_NEXT )
    {
        if (_selectedRow >= _itemsSource.count - 1)
        {
            YXB_ASSERT;
        }
        _selectedRow ++;
    }
    else if ( type == CBT_PRE )
    {
        if (_selectedRow <= 0)
        {
            YXB_ASSERT;
        }
        _selectedRow--;
    }
 
    [self setAnswerViewType];
    [self reSetAnswerView];
}

- (void)reSetAnswerView
{
   ChoiceCellFrameModel *cellFrame = _itemsSource[_selectedRow];
    _avModel.questionName = cellFrame.itemModel.text;
    
    NSDictionary *dic = _selectedAnswersSource[_selectedRow];
    
    if ([dic[@"empty"] boolValue])
    {
        [SoapManager getAnswerByItemId:cellFrame.itemModel.itemId];
        [_loadingView startLoading];
    }
    else
    {
        _answersSource = dic[@"source"];
        _avModel.answerArray = _answersSource;
        _answerView.answerViewModel = _avModel;
        [self.navigationController.view addSubview: _answerView];
        [_answerView.tableView reloadData];
    }
}

- (void)setAnswerViewType
{
    if (_selectedRow ==  _itemsSource.count - 1)
    {
        _avModel.type = AVT_LAST;
    }
    else if (_selectedRow == 0)
    {
        _avModel.type = AVT_FIRST;
    }
    else
    {
        _avModel.type = AVT_MIDDLE;
    }
}

- (void)saveUserChoice
{
    _selectedAnswersSource[_selectedRow] = @{@"empty":@"NO",
                                             @"source":_answersSource};
    
    ChoiceCellFrameModel *cellFrame = _itemsSource[_selectedRow];
    
    if ( cellFrame.itemModel.itemType ==  MULTIPLE_CHOICE )
    {
        [self saveSingleChoiceByItemId:cellFrame.itemModel.itemId];
    }
    
    else if (cellFrame.itemModel.itemType == MULTIPLE_CORRECT)
    {
        [self saveMultipleChoicesByItemId:cellFrame.itemModel.itemId];
    }
}

- (void)saveSingleChoiceByItemId:(NSString *)itemId
{
    __block BOOL find = NO;
    __block NSString *userChoice;
    [_answersSource enumerateObjectsUsingBlock:^(AnswerModel *obj, NSUInteger idx, BOOL *stop) {
        if (obj.isChosen)
        {
            find = YES;
            *stop = YES;
            userChoice = obj.label;
            [SoapManager updateSingleGradingDataWithGradingId:_assessmentGradingId ItemId:itemId AnswerId:obj.answerId];
        }
    }];
    if (find)
    {
        [self reloadSelectedRowByUserChoice:userChoice];
    }
}

- (void)saveMultipleChoicesByItemId:(NSString *)itemId
{
    NSMutableString *userChoice = [NSMutableString string];
    NSMutableArray *jsonArray = [NSMutableArray array];
    __block BOOL find = NO;
    [_answersSource enumerateObjectsUsingBlock:^(AnswerModel *obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dic = @{@"answerId":[NSString stringWithFormat:@"%@",obj.answerId],@"isOk":[NSNumber numberWithBool:obj.isChosen]};
        [jsonArray addObject:dic];
        if (obj.isChosen)
        {
            find = YES;
            [userChoice appendString:obj.label];
        }
    }];
    if (find)
    {
        [self reloadSelectedRowByUserChoice:userChoice];
        [SoapManager updateMultipleGradingDataWithGradingId:_assessmentGradingId ItemId:itemId Data:[JsonUtil array2json:jsonArray]];
    }
}

- (void)reloadSelectedRowByUserChoice:(NSString *)userChoice
{
    ChoiceCellFrameModel *cellFrame = _itemsSource[_selectedRow];
    cellFrame.itemModel.userChoice = userChoice;
    cellFrame.itemModel.chosen = YES;
    _itemsSource[_selectedRow] = cellFrame;
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

@end
