//
//  TestViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ExamViewController : UITabBarController

@property (nonatomic, strong)NSString *examName;
@property (nonatomic, strong)NSString *assessmentGradingId;
@property (nonatomic, assign)int timeLimit;
- (void)setItemNameWithArray:(NSArray *)array;

- (void)setTabViewControllers:(NSArray *)viewControllers;
@end
