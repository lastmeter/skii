//
//  TestViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ExamViewController.h"
#import "SoapManager.h"
#import "SoapNotiCenter.h"
#import "LoadingView.h"

@interface ExamViewController ()<UIAlertViewDelegate>
{
    UILabel *_title, *_minLable, *_secLable;
    LoadingView *_loadingView;
    UIView *_timerView;
    int _min, _sec;
    NSTimer *_timer;
}
@end

@implementation ExamViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tabBar.hidden = YES;
    
    [self setNavBarView];
    
    
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if (_timeLimit > 0)
    {
        BACK(^{
            _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
            [[NSRunLoop currentRunLoop] run];
            
        });
    }
}

- (void)startTimer
{
    if (_sec == 0 && _min > 0)
    {
        _sec = 59;
        _min--;
    }
    else
    {
        _sec-- ;
    }
    if (_min > 9)
    {
        _minLable.text = [NSString stringWithFormat:@"%d:",_min];
    }
    else
    {
        _minLable.text = [NSString stringWithFormat:@"0%d:",_min];
    }
    if (_sec > 9)
    {
        _secLable.text = [NSString stringWithFormat:@"%d",_sec];
    }
    else
    {
        _secLable.text = [NSString stringWithFormat:@"0%d",_sec];

    }

    if (_sec == 0 & _min == 0)
    {
        [_loadingView startLoading];
        [SoapManager submitAssessmentGradingByGradingId:_assessmentGradingId];
        [_timer invalidate];
        _timer = nil;
    }
}

-(void)setTestName:(NSString *)testName
{
    _title.text = testName;
}

-(void)setTimeLimit:(int)timeLimit
{
    _timeLimit = timeLimit;
    if (timeLimit > 0)
    {
        if (_timerView == nil)
        {
            
            CGFloat VIEW_WIDTH = MAIN_SCREEN_WIDTH;
            CGFloat VIEE_HEIGHT = MAIN_SCREEN_HEIGHT;
            
            _timerView = [[UIView alloc] initWithFrame:CGRectMake(0, VIEE_HEIGHT - 24, VIEW_WIDTH, 44)];
            _timerView.backgroundColor = [UIColor groupTableViewBackgroundColor];
            
            _minLable = [[UILabel alloc] initWithFrame:CGRectMake(VIEW_WIDTH * 0.5 - 18, 10, 35, 21)];
            _minLable.text = _timeLimit > 9? [NSString stringWithFormat:@"%d:",_timeLimit] : [NSString stringWithFormat:@"0%d:",_timeLimit];
            _minLable.textColor = [UIColor blackColor];
            [_timerView addSubview:_minLable];
            
            _secLable = [[UILabel alloc] initWithFrame:CGRectMake(VIEW_WIDTH * 0.5+10, 10, 20, 21)];
            _secLable.text = @"00";
            _secLable.textColor = [UIColor blackColor];
            [_timerView addSubview:_secLable];
        }
        [self.view addSubview:_timerView];
        
        _min = timeLimit;
    }
    else
    {
        [_timerView removeFromSuperview];
    }
}

-(void)setTabViewControllers:(NSArray *)viewControllers
{
    self.viewControllers = viewControllers;
}

- (void)setItemNameWithArray:(NSArray *)array
{
    CGFloat btnWidth = MAIN_SCREEN_WIDTH / array.count;
    [array enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(btnWidth*idx, 64, btnWidth, 40)];
        btn.tag = idx;
        [btn setTitle:obj forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [btn setBackgroundImage:[UIImage imageNamed:@"login_input_back"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor greenColor] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(selectItem:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }];
}

- (void)setNavBarView
{
    UIView *navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navBar.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:navBar];
    
    UIImageView  *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(3, 37, 20, 20)];
    imgView.image = [UIImage imageNamed:@"backArrow"];
    [navBar addSubview:imgView];
    
    _title = [[UILabel alloc] initWithFrame:CGRectMake(23, 37, 119, 21)];
    _title.font = [UIFont boldSystemFontOfSize:17];
    [navBar addSubview:_title];
    
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(3, 30, 130, 33)];
    back.backgroundColor = [UIColor clearColor];
    [back addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:back];
    
    UIButton *submit = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH-10-50, 33, 50, 33)];
    [submit setTitle:@"提交" forState:UIControlStateNormal];
    [submit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    submit.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [submit addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:submit];
}

- (void)selectItem:(UIButton *)btn
{
    self.selectedIndex = btn.tag;
}

-(void)setExamName:(NSString *)examName
{
    _title.text = examName;
}

- (void)submit:(UIButton *)btn
{
    [_loadingView startLoading];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSubmitResult:) SoapType:ST_SUBMIT_ASSESSMENT_GRADING];
    [SoapManager submitAssessmentGradingByGradingId:_assessmentGradingId];
}

- (void)recvSubmitResult:(NSNotification *)noti
{
    [_loadingView stopLoading];
    if ([SoapNotiCenter getSoapResult:noti])
    {
        if ([[SoapNotiCenter getInfoFromNotification:noti] boolValue])
        {
            UIAlertView *alertView =[[UIAlertView alloc] initWithTitle: @"提交成功" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alertView.tag = 0;
            MAIN(^{
                [alertView show];
            });
        }
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_SUBMIT_ASSESSMENT_GRADING];
}

- (void)back:(UIButton *)btn
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"退出考试界面" message:@"考试已经开始，如果退出此界面，考试将需要重新开始，确定退出吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = 1;
    [alert show];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 1)
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (alertView.tag == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
