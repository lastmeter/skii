//
//  AnswerCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AnswerModel;

@interface AnswerCell : UITableViewCell

@property (nonatomic, strong)AnswerModel *answer;
@property (nonatomic, assign)int type;

@end
