//
//  ChoiceView.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AnswerViewModel;

typedef enum
{
    CBT_CANCEL,
    CBT_PRE,
    CBT_NEXT
}CHOICE_BTN_TYPE;

@protocol AnswerViewDelegate <NSObject>

-(void)choiceBtnClick:(CHOICE_BTN_TYPE)type;

@end

@interface AnswerView : UIView

@property(nonatomic, strong)AnswerViewModel *answerViewModel;
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)id<AnswerViewDelegate>delegate;
@property(nonatomic, assign)BOOL btnEnable;

@end
