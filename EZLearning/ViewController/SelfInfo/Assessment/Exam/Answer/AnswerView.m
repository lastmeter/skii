//
//  ChoiceView.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//


#import "AnswerView.h"
#import "AnswerModel.h"
#import "AnimationUtil.h"
#import "NSString+Extension.h"
#import "AnswerViewModel.h"

#define BtnViewHeight 40
@interface AnswerView()
{
    UIView *_selectView, *_buttonView;
    UITextView *_textView;
    UILabel  *_lin, *_lin1, *_lin2, *_lin3;
    UIButton *_btn1, *_btn2, *_btn3;
}
@end

@implementation AnswerView

-(instancetype)init
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self)
    {
        UIView *background = [[UIView alloc] initWithFrame:self.frame];
        background.backgroundColor = [UIColor grayColor];
        background.alpha = 0.8;
        [self addSubview:background];
        
        _selectView = [[UIView alloc] init];
        _selectView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_selectView];
        
        _textView = [[UITextView alloc] init];
        _textView.userInteractionEnabled = NO;
        _textView.textColor = [UIColor blackColor];
        _textView.scrollEnabled = NO;
        _textView.font = [UIFont boldSystemFontOfSize:[AnswerModel answerViewTitleFontSize]];
        [_selectView addSubview:_textView];
        
        self.tableView = [[UITableView alloc] init];
        _tableView.scrollEnabled = NO;
        [_selectView addSubview:_tableView];
        
        _buttonView = [[UIView alloc] init];
        _buttonView.backgroundColor = [UIColor clearColor];
        [_selectView addSubview:_buttonView];
        
        _lin = [[UILabel alloc] init];
        _lin.backgroundColor = [UIColor blackColor];
        [_selectView addSubview:_lin];
        
        _lin1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ChoiceViewWidth, 1)];
        _lin1.backgroundColor = [UIColor blackColor];
        [_buttonView addSubview:_lin1];
        
        _lin2 = [[UILabel alloc] initWithFrame:CGRectMake(0.33*ChoiceViewWidth, 0, 1, 35)];
        _lin2.backgroundColor = [UIColor blackColor];
        [_buttonView addSubview:_lin2];
        
        _lin3 = [[UILabel alloc] initWithFrame:CGRectMake(0.66*ChoiceViewWidth, 0, 1, 35)];
        _lin3.backgroundColor = [UIColor blackColor];
        [_buttonView addSubview:_lin3];
        
        UIFont *btnFont = [UIFont systemFontOfSize:[AnswerModel answerCellTextFontSize]];
        
        _btn1 = [[UIButton alloc] init];
        _btn1.tag = 0;
        _btn1.titleLabel.font = btnFont;
        [_btn1 setTitle:@"取消" forState:UIControlStateNormal];
        [_btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btn1 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonView addSubview:_btn1];
        
        
        _btn2 = [[UIButton alloc] init];
        _btn2.tag = 0;
        _btn2.titleLabel.font = btnFont;
        [_btn2 setTitle:@"下一题" forState:UIControlStateNormal];
        [_btn2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btn2 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonView addSubview:_btn2];
        
        _btn3 = [[UIButton alloc] init];
        _btn3.tag = 0;
        _btn3.titleLabel.font = btnFont;
        [_btn3 setTitle:@"上一题" forState:UIControlStateNormal];
        [_btn3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_btn3 addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonView addSubview:_btn3];
    }
    return self;
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [AnimationUtil addPopAnimationToView:_selectView duration:0.5];
}

-(void)setAnswerViewModel:(AnswerViewModel *)answerViewModel
{
    _textView.text = answerViewModel.questionName;
    _textView.frame = answerViewModel.questionFrame;
    _lin.frame = CGRectMake(0, CGRectGetMaxY(answerViewModel.questionFrame), ChoiceViewWidth, 1);
    _tableView.frame = CGRectMake(0, CGRectGetMaxY(answerViewModel.questionFrame), ChoiceViewWidth, answerViewModel.tableViewHeight);
    _buttonView.frame = CGRectMake(0, CGRectGetMaxY(_tableView.frame), ChoiceViewWidth, BtnViewHeight);
    _selectView.frame = CGRectMake(0, 0, ChoiceViewWidth, CGRectGetMaxY(_buttonView.frame) );
    _selectView.center =CGPointMake(MAIN_SCREEN_WIDTH * 0.5, MAIN_SCREEN_HEIGHT * 0.5);
    [self setBtnViewByType:answerViewModel.type];

}

-(void)setBtnViewByType:(ANSWER_VIEW_TYPE)type
{
    switch (type)
    {
        case AVT_FIRST:
        {
            _btn1.frame = CGRectMake(0, 0, 0.5*ChoiceViewWidth, BtnViewHeight);
            _btn2.frame = CGRectMake(0.5*ChoiceViewWidth, 0, 0.5*ChoiceViewWidth, BtnViewHeight);
            
            [_btn1 setTitle:@"取消" forState:UIControlStateNormal];
            [_btn2 setTitle:@"下一题" forState:UIControlStateNormal];
            
            _btn1.tag = CBT_CANCEL;
            _btn2.tag = CBT_NEXT;
            
            _lin2.frame = CGRectMake(0.5*ChoiceViewWidth, 0, 1, BtnViewHeight);
            
            _lin3.hidden = YES;
            _btn3.hidden = YES;
           
        }
            break;
            
        case AVT_MIDDLE:
        {
            _btn1.frame = CGRectMake(0, 0, 0.33*ChoiceViewWidth, BtnViewHeight);
            _btn2.frame = CGRectMake(0.33*ChoiceViewWidth, 0, 0.33*ChoiceViewWidth, BtnViewHeight);
            _btn3.frame = CGRectMake(0.66*ChoiceViewWidth, 0, 0.33*ChoiceViewWidth, BtnViewHeight);
            
            [_btn1 setTitle:@"上一题" forState:UIControlStateNormal];
            [_btn2 setTitle:@"取消" forState:UIControlStateNormal];
            [_btn3 setTitle:@"下一题" forState:UIControlStateNormal];
            
            _btn1.tag = CBT_PRE;
            _btn2.tag = CBT_CANCEL;
            _btn3.tag = CBT_NEXT;
            
            _lin2.frame = CGRectMake(0.33*ChoiceViewWidth, 0, 1, BtnViewHeight);
            _lin3.frame = CGRectMake(0.66*ChoiceViewWidth, 0, 1, BtnViewHeight);
            
            _lin3.hidden = NO;
            _btn3.hidden = NO;
        }
            break;
            
        case AVT_LAST:
        {
            _btn1.frame = CGRectMake(0, 0, 0.5*ChoiceViewWidth, BtnViewHeight);
            _btn2.frame = CGRectMake(0.5*ChoiceViewWidth, 0, 0.5*ChoiceViewWidth, BtnViewHeight);
            
            [_btn1 setTitle:@"上一题" forState:UIControlStateNormal];
            [_btn2 setTitle:@"确定" forState:UIControlStateNormal];
            
            _btn1.tag = CBT_PRE;
            _btn2.tag = CBT_CANCEL;

            _lin2.frame = CGRectMake(0.5*ChoiceViewWidth, 0, 1, BtnViewHeight);
            
            _btn3.hidden = YES;
            _lin3.hidden = YES;
        }
            break;
        default:
            break;
    }

}

-(void)setBtnEnable:(BOOL)btnEnable
{
    
}

#pragma 0 取消 1 上一题 2 下一题
-(void)btnClick:(UIButton *)btn
{
    [self removeFromSuperview];
    [_delegate choiceBtnClick:(int)btn.tag];
}


@end
