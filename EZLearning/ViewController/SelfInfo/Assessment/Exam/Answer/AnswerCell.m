//
//  AnswerCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AnswerCell.h"
#import "AnswerModel.h"

@interface AnswerCell()
{
    UITextView *_textView;
    UILabel *_textLabel;
    UIImageView *_imgView;
}
@end

@implementation AnswerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _textView = [[UITextView alloc] init];
        _textView.userInteractionEnabled = NO;
        _textView.scrollEnabled = NO;
        _textView.font = [UIFont systemFontOfSize:[AnswerModel answerCellTextFontSize]];
        _textView.textColor = [UIColor blackColor];
        [self.contentView addSubview:_textView];
        
        _imgView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imgView];
    }
    return self;
}

-(void)setAnswer:(AnswerModel *)answer
{
    _textView.frame = (CGRect){10, 0, ChoiceViewWidth-40, answer.cellSize.height};
    _textView.text = [NSString stringWithFormat:@"%@.%@",answer.label,answer.text];
    
    CGFloat imgWH = isPad ? 30:20;
    
    _imgView.frame = CGRectMake(ChoiceViewWidth- imgWH - 5, answer.cellSize.height *0.5 - 0.5 * imgWH, imgWH, imgWH);
    
    if (_type == 1)
    {
        _imgView.image = answer.isChosen ? [UIImage imageNamed:@"check_radio_check"] : [UIImage imageNamed:@"check_radio_nor"];
    }
    else if (_type == 2)
    {
        _imgView.image = answer.isChosen ? [UIImage imageNamed:@"checkBox_check"] : [UIImage imageNamed:@"checkBox_nor"];
    }
}

@end
