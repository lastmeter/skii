//
//  PersonInfoViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "PersonInfoViewController.h"
#import "PersonInfo.h"
#import "UDManager.h"
@interface PersonInfoViewController()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    MBProgressHUD *_HUD;
    UIImagePickerController* pickerlibrary;
    NSString *filename;
    NSString *mimeType;
    NSString *imageStr;
    
    dispatch_semaphore_t sem;
}

@property (strong, nonatomic) IBOutlet UIImageView *head;
@property (strong, nonatomic) IBOutlet UILabel *city;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *manager;
@property (strong, nonatomic) IBOutlet UILabel *market;
@property (strong, nonatomic) IBOutlet UILabel *rank;
@property (strong, nonatomic) IBOutlet UIButton *save;
@property (strong, nonatomic) IBOutlet UITextField *phonenum;
@property (strong, nonatomic) IBOutlet UITextField *mailaddress;
@property (retain, nonatomic) PersonInfo *person;
@end

@implementation PersonInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_save addTarget:self action:@selector(saveMessage:) forControlEvents:UIControlEventTouchUpInside];
 
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvPersonInfo:) SoapType:ST_GET_USER_INFO];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvHeadPortrait:) SoapType:ST_GET_HEAD_PORTRAIT];
    [SoapManager getUserInfo];
    LoadingView *loadingView = [[LoadingView alloc] init];
    [self.view addSubview:loadingView];
    [loadingView startLoading];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectheadimage)];
    [_head addGestureRecognizer:singleTap];
    [self registerForKeyboardNotifications];
}

-(IBAction)saveMessage:(id)sender
{
    sem = dispatch_semaphore_create(0);
    [self waittingUtilSettingFinish];

    [SoapNotiCenter addSoapObserver:self selector:@selector(changeEmailandMobile:) SoapType:ST_CHANGE_EMAIL_AND_MOBILE];
    [SoapManager changeEmailandMobile:_mailaddress.text phone:_phonenum.text];
    
   
    if (mimeType.length>0&&filename.length>0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *_headimagedata;
            if (UIImagePNGRepresentation(_head.image)==nil) {
                _headimagedata=UIImageJPEGRepresentation(_head.image, 1.0);
            }else{
                _headimagedata=UIImagePNGRepresentation(_head.image);
            }
            imageStr= [_headimagedata base64EncodedStringWithOptions:0];
            [SoapNotiCenter addSoapObserver:self selector:@selector(getsetHeadPortrait:) SoapType:ST_SET_HEAD_PORTRAIT];
            [SoapManager setHeadPortrait:imageStr MIMETYPE:mimeType FileName:filename ];
        });
    }
    else
    {
        dispatch_semaphore_signal(sem);
        NSLog(@"release");
        [self HUDshow:@"选择头像"];
    }
    
}

-(void)getsetHeadPortrait:(NSNotification *)noti{
    dispatch_semaphore_signal(sem);
    NSLog(@"release");
    if ([SoapNotiCenter getSoapResult:noti])
    {
       NSString * _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
        if ([_boolpost boolValue]) {
            [self HUDshow:@"已保存个人信息"];
        }
        else
        {
            [self HUDshow:@"头像保存不成功，请重试"];
        }
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_SET_HEAD_PORTRAIT];
}

-(void)changeEmailandMobile:(NSNotification *)noti
{
    dispatch_semaphore_signal(sem);
    NSLog(@"release");
    if ([SoapNotiCenter getSoapResult:noti])
    {
        NSString * _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
        if ([_boolpost boolValue]) {
            [self HUDshow:@"已保存个人信息"];
        }
        else
        {
            [self HUDshow:@"修改不成功"];
        }
        
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_CHANGE_EMAIL_AND_MOBILE];
}
-(void)recvHeadPortrait:(NSNotification *)noti
{
    NSDictionary *dic = noti.userInfo;

    NSData *decodeImageData = [[NSData alloc]initWithBase64EncodedData:dic[@"info"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *newjiaban=[[UIImage alloc] initWithData:decodeImageData];
    _head.image = newjiaban;
}

- (void)recvPersonInfo:(NSNotification *)noti
{
    NSDictionary *dic = noti.userInfo;
    PersonInfo *person = dic[@"info"];
    MAIN(^{
        self.person = person;
    });
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_USER_INFO];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setPerson:(PersonInfo *)person
{
    _city.text = person.credit;
    _name.text = person.name;
    _manager.text = person.manager;
    _market.text = person.market;
    _rank.text = person.star;
    _phonenum.text = person.phonenum;
    _mailaddress.text = person.emailaddress;
    
}


-(void)selectheadimage{
    pickerlibrary = [[UIImagePickerController alloc] init];
    BOOL isAlbumSupport =[UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    if (isAlbumSupport) {
        pickerlibrary.delegate = self;
        pickerlibrary.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        pickerlibrary.allowsEditing = YES;
        [self presentViewController:pickerlibrary animated:YES completion:nil];
    }else{
        NSLog(@"nonpic");
    }
   
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{

    _head.image = [info objectForKey: UIImagePickerControllerEditedImage];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
       
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        filename= [representation filename];
        NSArray *filenameSeparate = [filename componentsSeparatedByString:@"."];
        if (filenameSeparate.count>0) {

            NSMutableString *mimeString = [[NSMutableString alloc]initWithString:@"IMAGE/"];
            NSString *filesuffix = filenameSeparate[filenameSeparate.count-1];
            [mimeString appendString:filesuffix];
            mimeType =mimeString;
        }
        float size = [representation size];//图片资源容量大小(字节)
        if(size>2097152)
        {
            
        }
        //NSLog(@"filename =%@,%f,%@",filename,size,mimeType);
    };
    ALAssetsLibrary *assetslibrary = [[ALAssetsLibrary alloc]init];
    [assetslibrary assetForURL:[info valueForKey:UIImagePickerControllerReferenceURL] resultBlock:resultblock failureBlock:nil];
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (IBAction)submitpersonInfo:(id)sender
{
    [_phonenum resignFirstResponder];
    [_mailaddress resignFirstResponder];
}


-(CGFloat) moveY
{
    if (MAIN_SCREEN_HEIGHT <= 480 )
    {
        return  200;
    }
    else if (MAIN_SCREEN_HEIGHT <= 568)
    {
        return 90;
    }
    else
    {
        return 0;
    }
}

- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
}


- (void)keyboardWillShow:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    [UIView animateWithDuration:duration/2 animations:^{
        self.view.frame = CGRectMake(0, 0-[self moveY], self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    [UIView animateWithDuration:duration/2 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_phonenum resignFirstResponder];
    [_mailaddress resignFirstResponder];
}


-(void)HUDshow:(NSString *)title
{
    _HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _HUD.delegate=self;
    _HUD.labelText = title;
    _HUD.mode = MBProgressHUDModeText;
    _HUD.margin =10.0;
    _HUD.opacity =1.0;
    _HUD.labelFont = [UIFont boldSystemFontOfSize:13.0];
    _HUD.yOffset = MAIN_SCREEN_HEIGHT/2-60;
    [_HUD hide:YES afterDelay:1.0];
}

-(void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
    hud=nil;
}

- (void)waittingUtilSettingFinish{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self waitting];
    });

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"waitting");
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        NSLog(@"stop waitting");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopWaitting];
        });
    });
}

@end

