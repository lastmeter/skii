//
//  SelfInfoViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "SelfInfoViewController.h"
#import "PersonInfoViewController.h"
#import "SettingViewController.h"
#import "ServiceCenterViewController.h"
#import "AssessmentViewController.h"
#import "FavoriteViewController.h"
#import "NotificationViewController.h"
#import "ReviewController.h"
#import "SubordinateViewController.h"
#import "SubordinateListViewController.h"
#import "SubordinateRankViewController.h"
#import "guideViewController.h"

#import "ArticleEditController.h"
#import "UserInfoViewController.h"
#import "NEzlNavigationController.h"

@interface SelfInfoViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray *_titleSource;
    NSArray *_imgSource;
    USER_JOB_TYPE _userJob;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SelfInfoViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.title = @"我";
        self.tabBarItem.image = [UIImage imageNamed:@"tab4_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"tab4_sel"];
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvUserJob:) SoapType:ST_GET_USER_JOB];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self loadData];
    [self makeExtraCellLineHidden];
     //_userJob = UJ_UNKONW;
}

-(void)recvUserJob:(NSNotification *)noti
{
   
    _userJob = [[SoapNotiCenter getInfoFromNotification:noti] intValue];
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_USER_JOB];
}


- (void)loadData
{
    _titleSource = @[@"考试", @"我的投稿",@"收藏夹",@"通知",@"个人信息",@"故障报修",@"设置",@"使用指引"];
    
    _imgSource = @[[UIImage imageNamed:@"me_test"],
                   [UIImage imageNamed:@"me_article"],
                   [UIImage imageNamed:@"me_favorite"],
                   [UIImage imageNamed:@"me_notice"],
                   [UIImage imageNamed:@"me_user_info"],
                   [UIImage imageNamed:@"me_faultrepair"],
                   [UIImage imageNamed:@"me_setting"],
                   [UIImage imageNamed:@"me_guide"]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titleSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"info";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    cell.imageView.image = _imgSource [indexPath.row];
    cell.textLabel.text = _titleSource[indexPath.row];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    
    if (row == 0)
    {
        AssessmentViewController *assessmentViewController = [[AssessmentViewController alloc] initWithNibName:@"AssessmentViewController" bundle:nil];
        [self.navigationController pushViewController:assessmentViewController animated:YES];
    }
    
    //Add In 2015.12.20 By Lrh
    else if (row == 1){
        ArticleEditController *articleEditController = [[ArticleEditController alloc] init];
        NEzlNavigationController *nav = [[NEzlNavigationController alloc] initWithRootViewController:articleEditController];
        nav.navigationBar.translucent = NO;
        nav.navigationBar.tintColor = [UIColor blackColor];
        [self presentViewController:nav animated:YES completion:nil];
    }
    //Add End
    
    else if (row == 2)
    {//收藏
        FavoriteViewController *favoriteViewController = [[FavoriteViewController alloc] initWithNibName:@"FavoriteViewController" bundle:nil];
        [self.navigationController pushViewController:favoriteViewController animated:YES];
    }
    /* 2015.12.21 By Lrh
    else if (row == 99)
    {
        if (_userJob == UJ_BOSS)
        {
            SubordinateListViewController *list = [[SubordinateListViewController alloc] initWithNibName:@"SubordinateListViewController" bundle:nil];
            SubordinateRankViewController *rank = [[SubordinateRankViewController alloc] initWithNibName:@"SubordinateRankViewController" bundle:nil];
            ReviewController *reviewController = [[ReviewController alloc]init];
            [reviewController setTabViewControllers:@[list, rank]];
            [self.navigationController pushViewController:reviewController animated:YES];
        }
        else if (_userJob == UJ_BC)
        {
            SubordinateViewController *suboridnateController = [[SubordinateViewController alloc] initWithNibName:@"SubordinateViewController" bundle:nil];
            [self.navigationController pushViewController:suboridnateController animated:YES];
        }
    }
     */
    else if (row == 3)
    {//通知
        NotificationViewController *notification = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
        [self.navigationController pushViewController:notification animated:YES];
    }
    else if (row == 4)
    {
        /*
        PersonInfoViewController *personInfoViewController = [[PersonInfoViewController alloc] initWithNibName:@"PersonInfoViewController" bundle:nil];
        [self.navigationController pushViewController:personInfoViewController animated:YES];
         */
        
        
        UserInfoViewController *userInfoVC = [[UserInfoViewController alloc] init];
        NEzlNavigationController *nav = [[NEzlNavigationController alloc] initWithRootViewController:userInfoVC];
        nav.navigationBar.tintColor = [UIColor blackColor];
        nav.navigationBar.translucent = NO;
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if (row == 5)
    {
        ServiceCenterViewController *serviceCenterViewController = [[ServiceCenterViewController alloc] initWithNibName:@"ServiceCenterViewController" bundle:nil];
        [self.navigationController pushViewController:serviceCenterViewController animated:YES];
    }
    else if (row == 6)
    {
        SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
        [self.navigationController pushViewController:settingViewController animated:YES];
    }
    else if (row == 7)
    {
        guideViewController *guideview =[[guideViewController alloc]init];
        [self.navigationController pushViewController:guideview animated:NO];
    }
    [self deselect];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}


@end
