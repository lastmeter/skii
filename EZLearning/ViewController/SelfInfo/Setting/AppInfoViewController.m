//
//  appInfoViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/18.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AppInfoViewController.h"

@interface AppInfoViewController ()
@property (strong, nonatomic) IBOutlet UILabel *version;

@end

@implementation AppInfoViewController
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    self.version.text = [NSString stringWithFormat:@"V%@",version];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
