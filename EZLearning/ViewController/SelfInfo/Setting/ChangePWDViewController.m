//
//  ChangePWDViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/18.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChangePWDViewController.h"
#import "SoapNotiCenter.h"
#import "SoapManager.h"
#import "UDManager.h"
#import "EzlUser.h"

@interface ChangePWDViewController ()<UIAlertViewDelegate>
{
    BOOL _changResult;
}
@property (strong, nonatomic) IBOutlet UITextField *oldPWD;
@property (strong, nonatomic) IBOutlet UITextField *kNewPWD;
@property (strong, nonatomic) IBOutlet UITextField *cfNewPwd;
@end

@implementation ChangePWDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvChangeResult:)  SoapType:ST_CHANGE_PASSWORD];
  
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _cfNewPwd.delegate = self;
    _cfNewPwd.text = @"";
    _kNewPWD.delegate =self;
    _kNewPWD.text =@"";
    _oldPWD.delegate = self;
    _oldPWD.text = @"";
    _changResult = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag==00) {
        [_oldPWD resignFirstResponder];
        [_kNewPWD becomeFirstResponder];
    }
    if (textField.tag==01) {
        [_kNewPWD resignFirstResponder];
        [_cfNewPwd becomeFirstResponder];
    }
    if (textField.tag==02) {
        [_cfNewPwd resignFirstResponder];
    }
    return YES;
}


- (IBAction)back:(id)sender
{
    
    if (_mustChange == YES && !_changResult)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"警告！" message:@"请修改密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (IBAction)Comfirm:(id)sender
{
    [_oldPWD resignFirstResponder];
    [_kNewPWD resignFirstResponder];
    [_cfNewPwd resignFirstResponder];
    
    if (_kNewPWD.text.length <= 0 || _cfNewPwd.text.length <= 0 || _oldPWD.text.length <= 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改失败" message:@"密码不能为空" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![_kNewPWD.text isEqual:_cfNewPwd.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改失败" message:@"确认密码与输入的新密码不一致" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if ([_kNewPWD.text isEqualToString:_oldPWD.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改失败" message:@"新密码不能与旧密码重复" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        //[SoapNotiCenter addSoapObserver:self selector:@selector(recvChangeResult:)  SoapType:ST_CHANGE_PASSWORD];多次点击按钮会多次注册通知，导致通知里面的方法被执行多次
        [SoapManager changOldPassword:_oldPWD.text ToNewPassword:_kNewPWD.text];
        return;
    }
}

- (void)recvChangeResult:(NSNotification *)noti
{

    NSDictionary *dic = noti.userInfo;
    
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _changResult = [dic[@"changeResult"] boolValue];
        NSString *info = dic[@"info"];
        if (_changResult)
        {
            [UDManager savePassword:_kNewPWD.text];
            [UDManager saveChangePasswordDate];
            
            EzlUser *user= [EzlUser currentUser];
            user.pwd = _kNewPWD.text;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:info message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (_changResult && _mustChange)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    /*
    else if (_changResult)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
     */
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
