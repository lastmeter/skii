//
//  ChangePWDViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/18.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "EzlViewController.h"

@interface ChangePWDViewController : EzlViewController<UITextFieldDelegate>

@property (assign, nonatomic)BOOL mustChange;
@end
