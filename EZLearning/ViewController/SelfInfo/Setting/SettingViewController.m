//
//  SettingViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SettingViewController.h"
#import "AppInfoViewController.h"
#import "ChangePWDViewController.h"

@interface SettingViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self makeExtraCellLineHidden];
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *Identifier = @"Setting";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"关于";
        cell.imageView.image = [UIImage imageNamed:@"setting_about"];
    }
    if (indexPath.row == 1)
    {
        cell.textLabel.text = @"修改密码";
        cell.imageView.image = [UIImage imageNamed:@"setting_change_passwd"];
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return NO;
}

/*-----------------------------------------------------------
 Description:   普通模式的选中代理方法
 -----------------------------------------------------------*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselect];
    if (indexPath.row == 0)
    {
        
        AppInfoViewController *appInfo = [[AppInfoViewController alloc] initWithNibName:@"AppInfoViewController" bundle:nil];
        
        [self.navigationController pushViewController:appInfo animated:YES];
    }
    if ( indexPath.row == 1)
    {
        
        ChangePWDViewController *changePWD = [[ChangePWDViewController alloc] initWithNibName:@"ChangePWDViewController" bundle:nil];
        
        [self.navigationController pushViewController:changePWD animated:YES];
    }
}

/*-----------------------------------------------------------
 Description:  消除多余的线
 -----------------------------------------------------------*/
- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
    
}
/*-----------------------------------------------------------
 Description:  消除选中痕迹
 -----------------------------------------------------------*/
- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}


@end
