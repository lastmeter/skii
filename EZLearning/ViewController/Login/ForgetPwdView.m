//
//  ForgetPwdView.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/26.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ForgetPwdView.h"

@implementation ForgetPwdView

+ (ForgetPwdView *) getInstance
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ForgetPwdView" owner:self options:nil];
    return [nib objectAtIndex:0];
}

- (IBAction)diaPhoneCall:(id)sender
{
    [_delegate emailBtnClick:sender];
}

- (IBAction)yesBtn:(id)sender
{
    [_delegate yesBtnClick:sender];
}


@end
