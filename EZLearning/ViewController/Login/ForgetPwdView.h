//
//  ForgetPwdView.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/26.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ForgetPwdViewDelegate <NSObject>

@optional
- (void)yesBtnClick:(UIButton *)btn;
- (void)emailBtnClick:(UIButton *)btn;

@end

@interface ForgetPwdView : UIView

+ (ForgetPwdView *) getInstance;
- (IBAction)diaPhoneCall:(id)sender;
- (IBAction)yesBtn:(id)sender;

@property (retain, nonatomic) id<ForgetPwdViewDelegate>delegate;
@end
