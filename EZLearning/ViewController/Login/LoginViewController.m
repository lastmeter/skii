//
//  LoginViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "LoginViewController.h"
#import "ChangePWDViewController.h"
#import "AppDelegate.h"
#import "UDManager.h"
#import "DateUtil.h"
#import "ForgetPwdView.h"
#import "AnimationUtil.h"

//Add In 2015.12.17 By LRH
#import "LoginService.h"
#import "LrhAlertView.h"

@interface LoginViewController ()<ForgetPwdViewDelegate>
{
    UIView *_inputView;
    UITextField *_username, *_password;
    LoadingView *_loadingView;
    BOOL _check;
}
@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addInputView];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    
    NSString *userName =  [UDManager getUsername];
    _username.text = userName;
}


- (void)addInputView
{
    CGFloat VIEW_HEIGHT = MAIN_SCREEN_HEIGHT;
    CGFloat VIEW_WIDTH  = MAIN_SCREEN_WIDTH;
    
    CGFloat inputViewWidth =  VIEW_WIDTH - 20;
    CGFloat inputViewHeight = 0.3 *VIEW_HEIGHT + 30;
    
    UIFont *inputFont = [UIFont systemFontOfSize:isPad ? 26 : 18];
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:self.view.frame];
    background.image = [UIImage imageNamed:@"login_background"];
    [self.view addSubview:background];
    
    _inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, inputViewWidth, inputViewHeight)];
    _inputView.center = CGPointMake(0.5 * VIEW_WIDTH, 0.4 *VIEW_HEIGHT);
    _inputView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_inputView];
    
    UIImageView *inputBack = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, inputViewWidth , 0.5*inputViewHeight)];
    inputBack.image = [UIImage imageNamed:@"login_input_back"];
    [_inputView addSubview:inputBack];
    
    UILabel *lin1 = [[UILabel alloc] initWithFrame:CGRectMake(2, 0.25*inputViewHeight, inputViewWidth-2, 1)];
    lin1.backgroundColor = [UIColor blackColor];
    [_inputView addSubview:lin1];
    
    UILabel *lin2 = [[UILabel alloc] initWithFrame:CGRectMake(0.3 * inputViewWidth, 5, 1, 0.25 * inputViewHeight - 10)];
    lin2.backgroundColor = [UIColor blackColor];
    [_inputView addSubview:lin2];
    
    UILabel *lin3 = [[UILabel alloc] initWithFrame:CGRectMake(0.3 * inputViewWidth, 0.25 * inputViewHeight + 5, 1, 0.25 * inputViewHeight - 10)];
    lin3.backgroundColor = [UIColor blackColor];
    [_inputView addSubview:lin3];
    
    UILabel *lab1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0.3 * inputViewWidth, 0.25 * inputViewHeight)];
    lab1.text = @"账号";
    lab1.font = inputFont;
    lab1.textAlignment = NSTextAlignmentCenter;
    lab1.textColor = [UIColor blackColor];
    [_inputView addSubview:lab1];
    
    UILabel *lab2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0.25 * inputViewHeight, 0.3 * inputViewWidth, 0.25 * inputViewHeight)];
    lab2.text = @"密码";
    lab2.font = inputFont;
    lab2.textAlignment = NSTextAlignmentCenter;
    lab2.textColor = [UIColor blackColor];
    [_inputView addSubview:lab2];
    
    _username = [[UITextField alloc] initWithFrame:CGRectMake(0.3 * inputViewWidth + 5, 0, 0.7 * inputViewWidth - 10, 0.25 * inputViewHeight)];
    _username.placeholder = @"请输入账号";
    _username.font = inputFont;
    _username.textColor = [UIColor blackColor];
    [_inputView addSubview:_username];
    
    _password = [[UITextField alloc] initWithFrame:CGRectMake(0.3 * inputViewWidth + 5, 0.25 * inputViewHeight, 0.7 * inputViewWidth - 10, 0.25 * inputViewHeight)];
    _password.placeholder = @"请输入密码";
    _password.secureTextEntry = YES;
    _password.font = inputFont;
    _password.textColor = [UIColor blackColor];
    [_inputView addSubview:_password];
    
    UILabel *lab3 = [[UILabel alloc] initWithFrame:CGRectMake(35, 0.5*inputViewHeight + 10, 69, 21)];
    lab3.font = [UIFont systemFontOfSize:17];
    lab3.textColor = [UIColor whiteColor];
    lab3.text = @"记住密码";
    [_inputView addSubview:lab3];
    
    _check = [UDManager getRememberPwdState];
    if (_check)
    {
        NSString *password = [UDManager getPassword];
        _password.text = password;
    }
    
    NSString *imgName = _check?@"checkBox_check":@"checkBox_nor";
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 0.5*inputViewHeight + 10, 20, 20)];
    [btn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(remPwd:) forControlEvents:UIControlEventTouchUpInside];
    [_inputView addSubview:btn];
    
    UIButton *loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lab3.frame) + 10, inputViewWidth - 20, 0.2 * inputViewHeight)];
    [loginBtn setImage:[UIImage imageNamed:@"login_btn_nor"] forState:UIControlStateNormal];
    [loginBtn setImage:[UIImage imageNamed:@"login_btn_pre"] forState:UIControlStateHighlighted];
    [loginBtn addTarget: self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [_inputView addSubview:loginBtn];
    
    UILabel *lab4 = [[UILabel alloc] initWithFrame:CGRectMake((inputViewWidth - 70) *0.5, CGRectGetMaxY(lab3.frame) + 10, 70, 0.2 * inputViewHeight)];
    lab4.font = inputFont;
    lab4.textAlignment = NSTextAlignmentCenter;
    lab4.textColor = [UIColor whiteColor];
    lab4.text = @"登录";
    [_inputView addSubview:lab4];
    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(inputViewWidth - 100, CGRectGetMaxY(loginBtn.frame) + 10, 100, 22)];
    btn2.titleLabel.font = [UIFont systemFontOfSize:isPad ? 18 : 15];
    btn2.titleLabel.textAlignment = NSTextAlignmentRight;
    [btn2 setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(forgetPwd) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frame = _inputView.frame;
    frame.size.height = CGRectGetMaxY(btn2.frame);
    _inputView.frame = frame;
    [_inputView addSubview:btn2];
    
    NSLog(@"%f",_inputView.frame.size.height);
}


/*
- (void)login
{
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    
    if (delegate.isNetworkConnect)
    {
        [SoapManager loginWithUserName:_username.text andPassword:_password.text];
        [SoapNotiCenter addSoapObserver:self selector:@selector(recvLoginResult:) SoapType:ST_LOGIN];
        [_loadingView startLoading];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"登陆失败" message:@"无法连接网络" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }
}
*/



- (void)remPwd:(UIButton *)btn
{
    _check = !_check;
    [UDManager saveRememberPwdState:_check];
    NSString *imgName = _check?@"checkBox_check":@"checkBox_nor";
    [btn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
}

- (void)forgetPwd
{
    
    UIView *forgetView = [[UIView alloc] initWithFrame:self.view.frame];
    forgetView.tag = 1314;
    
    UIView *background = [[UIView alloc] initWithFrame:self.view.frame];
    background.backgroundColor = [UIColor grayColor];
    background.alpha = 0.8;
    [forgetView addSubview:background];
    
    ForgetPwdView *forgetPwdView = [ForgetPwdView getInstance];
    forgetPwdView.center = CGPointMake(self.view.frame.size.width *0.5, self.view.frame.size.height *0.5);
    forgetPwdView.layer.cornerRadius = 8;
    forgetPwdView.layer.masksToBounds = YES;
    forgetPwdView.layer.borderWidth = 1;
    forgetPwdView.layer.borderColor = [[UIColor grayColor] CGColor];
    forgetPwdView.delegate = self;
    [AnimationUtil addPopAnimationToView:forgetPwdView duration:0.5];
    [forgetView addSubview:forgetPwdView];
    [self.view addSubview:forgetView];
}

- (void)yesBtnClick:(UIButton *)btn
{
    
    [AnimationUtil fadeoutWithAnimation:[self.view viewWithTag:1314] duration:0.3];
}

- (void)emailBtnClick:(UIButton *)btn{
    
}

- (void)recvLoginResult:(NSNotification *)noti
{
    NSDictionary *dic = noti.userInfo;
    
    if ([SoapNotiCenter getSoapResult:noti])
    {
        if ([dic[@"result"] boolValue])
        {
            [SoapManager getMySites];
            [SoapManager getCompanyAdvert];
            [SoapManager getUserJob];
            
            [UDManager saveUsername:_username.text];
            [UDManager savePassword:_password.text];
            
            
            NSDate *date = [UDManager getChangePasswordDateByUsername:_username.text];
            if (date == NULL || 90 < [DateUtil dayFromNowToDate:date])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"修改密码" message:@"第一次登陆应用之后必须修改密码，否则无法使用，而且以后每三个月也需要修改一次" delegate:self cancelButtonTitle:@"立即修改" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else
        {
            NSString *info = [SoapNotiCenter getInfoFromNotification:noti];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:info message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"服务器连接错误" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_LOGIN];
}




-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ChangePWDViewController *changPWDViewController = [[ChangePWDViewController alloc] initWithNibName:@"ChangePWDViewController" bundle:nil];
    changPWDViewController.mustChange = YES;
    [self.navigationController pushViewController:changPWDViewController animated:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_username resignFirstResponder];
    [_password resignFirstResponder];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark- Add In 2015.12.17 By LRH
- (void)login{
    
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    
    if (!delegate.isNetworkConnect){
        [LrhAlertView showMessage:@"无法连接网络"];
        return;
    }

    [self waitting];
    [LoginService loginWithUid:_username.text pwd:_password.text handler:^(HandleResult result, NSString* dataResult) {
        [self stopWaitting];
        if (result != HandleResultSuccess) {
            [LrhAlertView showMessage:@"获取服务器信息失败"];
        }
        else if ([dataResult isEqualToString:@"-3"]) {
            [LrhAlertView showMessage:@"帐号被锁定"];
        }
        else if ([dataResult isEqualToString:@"-2"] || [dataResult isEqualToString:@"-1"]){
            [LrhAlertView showMessage:@"登录失败，请检查用户名密码"];
        }
        else{
            
//            NSDate *date = [UDManager getChangePasswordDateByUsername:_username.text];
//            if (date == NULL || 90 < [DateUtil dayFromNowToDate:date])
//            {
//                __unsafe_unretained LoginViewController *unSelf = self;
//                LrhAlertAction *action = [LrhAlertAction actionWithTitle:@"立即修改" handler:^{
//                    ChangePWDViewController *changPWDViewController = [[ChangePWDViewController alloc] initWithNibName:@"ChangePWDViewController" bundle:nil];
//                    changPWDViewController.mustChange = YES;
//                    [unSelf.navigationController pushViewController:changPWDViewController animated:YES];
//                }];
//                [LrhAlertView showMessage:@"第一次登陆应用之后必须修改密码，否则无法使用，而且以后每三个月也需要修改一次" actions:@[action]];
//            }
//            else{
//                [self.navigationController popViewControllerAnimated:YES];
//            }
//            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
       
    }];
}

@end
