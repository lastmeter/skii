//
//  loadingView.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "LoadingView.h"
#import "SoapNotiCenter.h"

@interface LoadingView()
{
    UIActivityIndicatorView *_activityIndicatorView;
}
@end

@implementation LoadingView

- (instancetype)init
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    
    UIView *background = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    background.backgroundColor = [UIColor grayColor];
    background.alpha = 0.5;
    [self addSubview:background];

    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH*0.5 - 15, MAIN_SCREEN_HEIGHT*0.5 - 50, 50, 50)];
    _activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    _activityIndicatorView.hidesWhenStopped = YES;
  
    [self addSubview:_activityIndicatorView];
    
    [SoapNotiCenter addSoapRequestResultObserver:self selector:@selector(recvSoapResult:)];
    self.hidden = YES;
    return self;
}

- (void)recvSoapResult:(NSNotification *)noti
{
    [self stopLoading];
}

- (void)startLoading
{
    self.hidden = NO;
    [_activityIndicatorView startAnimating];
}

- (void)stopLoading
{
    [_activityIndicatorView stopAnimating];
    self.hidden = YES;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
