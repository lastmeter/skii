//
//  MoviePlayerViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/27.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "MoviePlayerViewController.h"

@interface MoviePlayerViewController ()

@end

@implementation MoviePlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
    
    self.moviePlayer.controlStyle = MPMovieControlStyleFullscreen;
//    self.moviePlayer.view.frame = CGRectMake(0, 0, self.view.bounds.size.height, self.view.bounds.size.width);
//    self.moviePlayer.view.center = CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2);
    self.moviePlayer.fullscreen = YES;
    
}
-(void)setUrlString:(NSString *)urlString
{
    NSString *encodeedString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:encodeedString];
    self.moviePlayer.contentURL = url;
    [self.moviePlayer prepareToPlay];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.moviePlayer play];
}



- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}



@end
