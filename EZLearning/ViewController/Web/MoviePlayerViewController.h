//
//  MoviePlayerViewController.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/27.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface MoviePlayerViewController : MPMoviePlayerViewController
@property (nonatomic, strong)NSString *urlString;
@end
