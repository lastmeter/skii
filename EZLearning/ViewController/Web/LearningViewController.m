//
//  LearningViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/27.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "LearningViewController.h"
#import "LoadingView.h"
#import "MoviePlayerViewController.h"
#import "SoapManager.h"
#import "SoapNotiCenter.h"
#import "LoadingHUD.h"
#import <WebKit/WebKit.h>

#define LOG_FRAME(frame) NSLog(@"\n{\n    x:%f\n    y:%f\n    width%f\n    height:%f\n}",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height)

#define LOG_SCREEN_FRAME  NSLog(@"{\n    width%f\n    height:%f\n}",MAIN_SCREEN_WIDTH,MAIN_SCREEN_HEIGHT)

#define FULL_SCALE  1.1645  // 全屏:宽度/高度
#define WIN_SCALE   1.2235  // 窗口:宽度/高度

@interface LearningViewController ()<UIWebViewDelegate,UIGestureRecognizerDelegate>
{
    NSString *_urlString,*_courseTitle;
    NSArray *_vedioArray;
    UIView *_videoListView;
    CGRect  _fullWebFrame, _winWebFrame;
    CGPoint _fullWebCenter, _winWebCenter;
    LoadingView *_loadingView;
    BOOL _able, _full, _first;
    NSMutableData *receivedData;
    WKWebView *view;
}

@property (strong, nonatomic) IBOutlet UIView *navBar;
@property (strong, nonatomic) UIWebView *webView;

@end

@implementation LearningViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setCookie];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvVideosList:) SoapType:ST_GET_MP4_FOR_COURSE];
    _able = YES;
    _full = NO;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if (_first)
    {
        [self setWebView];
        [self loadData];
        _first = NO;
    }
}


- (void)loadData
{
    
    NSString *encodedString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:encodedString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    [_webView loadRequest:request];
    [SoapManager getMP4ByCourseTitle:_courseTitle];
}



- (void)setWebView
{
    if (_webView == nil)
    {

        CGFloat VIEW_WIDTH  = MAIN_SCREEN_WIDTH;
        CGFloat VIEW_HEIGHT = MAIN_SCREEN_HEIGHT;
        
        _winWebFrame    =   CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT-44);
        _fullWebFrame   =   CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT+20);
        
        _fullWebCenter  =   CGPointMake((VIEW_WIDTH)/2, VIEW_HEIGHT/2);
        _winWebCenter   =   CGPointMake(VIEW_WIDTH/2, (VIEW_HEIGHT - 44)/2 + 44);
        
        self.webView = [[UIWebView alloc] init];
        _webView.scalesPageToFit = NO;
        _webView.scrollView.scrollEnabled = NO;
        _webView.dataDetectorTypes = UIDataDetectorTypeAll;
        _webView.delegate = self;
        _webView.frame  = _winWebFrame;
        _webView.center = _winWebCenter;
        _webView.hidden = YES;
        [self.view addSubview:_webView];
        
        UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
        longPressGR.minimumPressDuration = 1.35;//默认长按时间
        longPressGR.delegate = self;
        [_webView addGestureRecognizer:longPressGR];
        longPressGR = nil;
    }
}


-(void)loadCourseWithTitle:(NSString *)courseTitle URL:(NSString *)urlString
{
    _courseTitle = courseTitle;
    _urlString = urlString;
    _first = YES;
    _webView.hidden = YES;
    [LoadingHUD show];
}



-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    webView.hidden = NO;
    [LoadingHUD hide];
}


- (void)longPress
{
    if (_able)
    {
        _able = NO;
        _full = !_full;
        _navBar.hidden = _full;
        _webView.frame = _full ? _fullWebFrame:_winWebFrame;
        _webView.center = _full? _fullWebCenter:_winWebCenter;
        [self startTimer];
    }
}


- (void)startTimer
{
    [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(delay) userInfo:nil repeats:NO];
}

- (void)delay
{
    _able = YES;
}


- (void)recvVideosList:(NSNotification *)noti
{
    NSDictionary *dic = noti.userInfo;
    _vedioArray = dic[@"info"];
    MAIN(^{
        [self addVideoListViewWithArray:_vedioArray];
    });
}

- (void)setCookie
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *sessionID = (NSString *)[defaults objectForKey:@"sessionID"];
    NSString *value = [NSString stringWithFormat:@"%@.localhost",sessionID];
    NSDictionary *cookieProperties = @{ NSHTTPCookieName     : @"JSESSIONID",
                                        NSHTTPCookieValue    : value,
                                        NSHTTPCookieDomain   : @"www.lastmeter.cn",
                                        NSHTTPCookiePath     : @"/"
                                        };
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
}


- (void)addVideoListViewWithArray: (NSArray *)nameArray
{
    CGFloat height =44;
    CGFloat menuWidth = MAIN_SCREEN_WIDTH*0.5;

    [_videoListView removeFromSuperview];
    _videoListView = nil;
    
    _videoListView = [[UIView alloc] init];
    _videoListView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    for (int i = 0; i<nameArray.count; i++)
    {
        NSDictionary *dic = nameArray[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, i*height, menuWidth, height)];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitle:dic[@"name"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTintColor:[UIColor redColor]];
        [btn setBackgroundImage:[UIImage imageNamed:@"MenuButton"] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        btn.tag = i;
        [btn addTarget:self action:@selector(videoSelected:) forControlEvents:UIControlEventTouchUpInside];
        [_videoListView addSubview:btn];
    }
    _videoListView.frame = CGRectMake(MAIN_SCREEN_WIDTH - menuWidth - 10, 64, menuWidth, height * nameArray.count);
    _videoListView.hidden = YES;
    [self.view addSubview:_videoListView];
}


- (void)videoSelected:(UIButton *)btn
{
    _videoListView.hidden = YES;
    
    NSDictionary *dic = _vedioArray[btn.tag];
    NSString *urlString = dic[@"url"];
    
    MoviePlayerViewController *moviePlayerViewController = [[MoviePlayerViewController alloc] init];
    moviePlayerViewController.urlString = urlString;
    [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];
    
}

- (IBAction)viedeoList:(UIButton *)sender
{
    _videoListView.hidden = !_videoListView.hidden;
}


- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeLeft;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (IBAction)back:(id)sender
{
    _webView.hidden = YES;
    _first = YES;
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
      [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
