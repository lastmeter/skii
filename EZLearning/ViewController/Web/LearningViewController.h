//
//  LearningViewController.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/27.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LearningViewController : UIViewController

@property(nonatomic,strong)NSString *courseTitle;
- (void)loadCourseWithTitle : (NSString *)courseTitle URL:(NSString *)urlString ;
@end
