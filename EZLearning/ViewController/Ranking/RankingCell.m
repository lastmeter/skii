//
//  RankingCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "RankingCell.h"
#import "RankModel.h"

@implementation RankingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    CGFloat kCellHeight = 44;
    CGFloat kLabelY = 11;
    
    CGFloat cellWidth = MAIN_SCREEN_WIDTH -20;
    CGFloat labelHeight = self.frame.size.height - 2*kLabelY;
    self.frame = CGRectMake(0, 0, cellWidth, kCellHeight);

    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellWidth, kCellHeight)];
    background.image = [UIImage imageNamed:@"Form"];
    [self.contentView addSubview:background];
    
    self.rank = [[UILabel alloc] initWithFrame:CGRectMake(0, kLabelY, 0.31*cellWidth, labelHeight)];
    _rank.textAlignment = NSTextAlignmentCenter;
    
    self.name = [[UILabel alloc] initWithFrame:CGRectMake(_rank.frame.size.width, kLabelY, 0.39*cellWidth, labelHeight)];
    _name.textAlignment = NSTextAlignmentCenter;
    
    self.score = [[UILabel alloc] initWithFrame:CGRectMake(_name.frame.size.width+_name.frame.origin.x, kLabelY, cellWidth-_name.frame.size.width-_rank.frame.size.width, labelHeight)];
    _score.textAlignment = NSTextAlignmentCenter;

    [self.contentView addSubview:_rank];
    [self.contentView addSubview:_name];
    [self.contentView addSubview:_score];
    
    return self;
}

+ (NSString *)ID
{
    return @"RankingCell";
}

+ (CGFloat) cellHeight
{
    return 42;
}

- (void)setPerson:(RankModel *)person
{
    _score.text = person.score;
    _name.text = person.name;
    _rank.text = [NSString stringWithFormat:@"%lu",(unsigned long)person.rank];
 
}

@end
