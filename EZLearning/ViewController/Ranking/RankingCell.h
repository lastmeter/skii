//
//  RankingCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RankModel;
@interface RankingCell : UITableViewCell
@property (strong, nonatomic) UILabel *rank;
@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *score;
@property (strong, nonatomic) RankModel *person;

//类方法
+ (NSString *)ID;
+ (CGFloat) cellHeight;
@end
