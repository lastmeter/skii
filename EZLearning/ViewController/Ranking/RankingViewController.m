//
//  RankingViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "RankingViewController.h"
#import "RankingCell.h"
//#import "RankModel.h"

@interface RankingViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_rankSource;
    LoadingView *_loadingView;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation RankingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self makeExtraCellLineHidden:_tableView];
    [SoapNotiCenter addSoapObserver:self selector:@selector(getMarketScore:) SoapType:ST_GET_MARKET_SCORE];
    [SoapManager getMarketScore];

    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
}


- (void) getMarketScore:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _rankSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_loadingView removeFromSuperview];
            _loadingView = nil;
            [_tableView reloadData];
        });
    }
}

/*-----------------------------------------------------------
 Description:  指定分区（section）的数目
 -----------------------------------------------------------*/

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [RankingCell cellHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    RankingCell *view = [[RankingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[RankingCell ID]];
    view.backgroundColor = [UIColor grayColor];
    view.name.text = @"市场";
    view.rank.text = @"名次";
    view.score.text = @"积分";
    return  view;
}
/*-----------------------------------------------------------
 Description:  指定每个分区（section）的行数
 -----------------------------------------------------------*/
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rankSource.count;
}
/*-----------------------------------------------------------
 Description:  指定row高度
 -----------------------------------------------------------*/
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [RankingCell cellHeight];
}
/*-----------------------------------------------------------
 Description:  绘制cell
 -----------------------------------------------------------*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RankingCell *cell = [tableView dequeueReusableCellWithIdentifier:[RankingCell ID]];
    if (cell == nil)
    {
        cell = [[RankingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[RankingCell ID]];
    }
    cell.person = _rankSource[indexPath.row];
    return  cell;
}

/*-----------------------------------------------------------
 Description:   cell可编辑状态
 -----------------------------------------------------------*/
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

/*-----------------------------------------------------------
 Description:   普通模式的选中代理方法
 -----------------------------------------------------------*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselect];
}

/*-----------------------------------------------------------
 Description:  消除选中痕迹
 -----------------------------------------------------------*/
- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

/*-----------------------------------------------------------
 Description:  消除多余的线
 -----------------------------------------------------------*/
- (void) makeExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
