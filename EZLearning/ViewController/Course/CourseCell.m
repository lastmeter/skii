//
//  CurriculumCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/18.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "CourseCell.h"
#import "CourseUnitView.h"
#import "CourseModel.h"
#import "UIImageView+WebCache.h"


@interface CourseCell()
{

    CGFloat _viewWidth;
    CourseUnitView *_leftView;
    CourseUnitView *_rightView;
}
@end

@implementation CourseCell

-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    
    CGFloat cellHeight = [CourseCell cellHeight];
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    _viewWidth = (MAIN_SCREEN_WIDTH - 6*kPadding)*0.5;
    CGFloat viewHeight = cellHeight-2*kPadding;
    
    CGRect leftUnitViewFrame = CGRectMake(2*kPadding, kPadding, _viewWidth, viewHeight);
    CGRect rightUnitViewFrame = CGRectMake(kPadding*4+_viewWidth, kPadding, _viewWidth, viewHeight);
    
    _leftView = [[CourseUnitView alloc] initWithFrame: leftUnitViewFrame];
    _rightView = [[CourseUnitView alloc] initWithFrame: rightUnitViewFrame];
    
    [_leftView.btn addTarget:self action:@selector(imgBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_rightView.btn addTarget:self action:@selector(imgBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    [_leftView.collectBtn addTarget:self action:@selector(collectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_rightView.collectBtn addTarget:self action:@selector(collectBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview: _leftView];
    [self addSubview: _rightView];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return  self;
}

-(void)setLeftCourse:(CourseModel *)leftCourse
{
    _rightView.hidden = YES;
    _leftCourse = leftCourse;
    [self setCourseUnitView:_leftView withCourse:leftCourse];
}

-(void)setRightCourse:(CourseModel *)rightCourse
{
    _rightView.hidden = NO;
    _rightCourse = rightCourse;
    [self setCourseUnitView:_rightView withCourse:rightCourse];
}

-(void)setLastCourse:(CourseModel *)lastCourse
{
    _rightView.hidden = YES;
    [self setCourseUnitView:_leftView withCourse:lastCourse];
}


- (void)setCourseUnitView:(CourseUnitView *)unitView withCourse:(CourseModel *)course
{
    [unitView.img sd_setImageWithURL:[NSURL URLWithString:course.imgPath]];
    if (course.type == CET_CURRICULUM_VIEW)
    {
        [unitView.collectBtn removeFromSuperview];
        CGRect frame = _leftView.title.frame;
        frame.size.width = _viewWidth;
        unitView.title.frame = frame;
        unitView.passImg.hidden = !course.isPass;
    }
    else
    {
        [unitView.passImg removeFromSuperview];
        unitView.passImg = nil;
        if (course.type == CET_SITE_VIEW)
        {
            NSString *imgName = course.isCollected ? @"favorite_cancel_normal" : @"favorite_normal";
            [unitView.collectBtn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
        }
        else
        {
            [unitView.collectBtn setImage:[UIImage imageNamed:@"favorite_normal"] forState:UIControlStateNormal];
        }
        unitView.collectBtn.tag = course.tag;
        
    }
    unitView.title.text = course.title;
    unitView.btn.tag = course.tag;
}

- (void)imgBtnClick:(UIButton *)btn
{
    [_delegate imgBtnClick:btn];
}

- (void)collectBtnClick:(UIButton *)btn
{
    [_delegate collectBtnClick:btn];
}

+ (CGFloat)cellHeight
{
    return isPad ? 240 : 140;
}
@end
