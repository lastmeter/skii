//
//  CurriculumCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/18.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CourseModel;


@protocol CourseButtonDelegate <NSObject>

@optional
-(void)imgBtnClick:(UIButton *)btn;
-(void)collectBtnClick:(UIButton *)btn;

@end

@interface CourseCell : UITableViewCell

@property (nonatomic, strong)CourseModel *leftCourse;
@property (nonatomic, strong)CourseModel *rightCourse;
@property (nonatomic, strong)CourseModel *lastCourse;

@property (nonatomic, assign)BOOL isLast;

@property (nonatomic, retain)id<CourseButtonDelegate>delegate;

+ (CGFloat)cellHeight;
-(id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
