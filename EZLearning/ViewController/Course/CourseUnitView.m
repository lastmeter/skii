//
//  CurriculumUnitView.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "CourseUnitView.h"


@implementation CourseUnitView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    UIFont *titleFont = [UIFont systemFontOfSize:isPad?16:13];
    CGFloat viewWidth = frame.size.width;
    CGFloat viewHeight = frame.size.height;
    
    self.img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 0.84*viewHeight)];
    [self addSubview: _img];
    
    self.btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 0.84*viewHeight)];
    [self addSubview: _btn];
    
    self.title = [[UILabel alloc] initWithFrame:CGRectMake(3, _img.frame.size.height+1, viewWidth - 13 - kPadding, viewHeight-3-_img.frame.size.height)];
    _title.font = titleFont;
    _title.textColor = [UIColor blackColor];
    [self addSubview: _title];
    
    CGFloat btnWH = isPad ? 25:15;
    self.collectBtn = [[UIButton alloc] initWithFrame:CGRectMake(viewWidth-kPadding-btnWH, _img.frame.size.height+3, btnWH, btnWH)];
    [_collectBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _collectBtn.titleLabel.font = titleFont;
    [self addSubview:_collectBtn];
    
    self.passImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pass"]];
    _passImg.alpha = 0.8;
    _passImg.frame = _btn.frame;
    [self addSubview:_passImg];
    return self;
}
@end
