//
//  CurriculumUnitView.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

static const CGFloat kPadding = 5;

@interface CourseUnitView : UIView

@property (nonatomic, strong) UIImageView *img;
@property (nonatomic, strong) UILabel  *title;
@property (nonatomic, strong) UIButton *collectBtn;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) UIImageView *passImg;
@end
