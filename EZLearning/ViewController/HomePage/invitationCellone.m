//
//  invitationCellone.m
//  EZLearning
//
//  Created by zwf on 15/6/17.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "invitationCellone.h"

@implementation invitationCellone

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat kCellHeight = 60;
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    self.author = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, cellWidth/2*3, kCellHeight/2)];
    _author.textColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
    [self.contentView addSubview:_author];
    
    self.time = [[UILabel alloc]initWithFrame:CGRectMake(cellWidth/3*2-5, 0,cellWidth/3, kCellHeight/2)];
    _time.textColor = [UIColor grayColor];
    _time.font = [UIFont systemFontOfSize:14.0 ];
    _time.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_time];
    
    self.title = [[UILabel alloc]initWithFrame:CGRectMake(5, kCellHeight /2, cellWidth-20, 0)];
    _title.numberOfLines = 0;
    _title.font = [UIFont systemFontOfSize:16.0];
    [self.contentView addSubview:_title];
    return self;
}
+ (NSString *)ID
{
    return @"invitationOne";
}

+ (CGFloat) cellHeight
{
    return 60;
}


+ (NSString *)timechange:(NSString *)time
{
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM-dd HH:mm:ss"];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    return [formatter stringFromDate:confromTimesp];
}

-(CGFloat)inittitleText:(NSString *)string
{
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    CGRect tmpRect = _title.frame;
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString: string];
    _title.attributedText =attrStr;
    CGRect bodySize =[_title.text boundingRectWithSize:CGSizeMake(cellWidth-10,8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_title.font,NSFontAttributeName, nil] context:nil];
    tmpRect.size.height =bodySize.size.height;
    _title.frame =tmpRect;
    return tmpRect.size.height;
}


@end
