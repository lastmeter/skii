//
//  bbsViewController.m
//  EZLearning
//
//  Created by zwf on 15/5/22.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "ForumViewController.h"
#import "forumCell.h"
#import "forumTopicViewController.h"
@interface ForumViewController ()<UITableViewDataSource,UITableViewDelegate>
{
      NSMutableArray *_forumSource;
      NSMutableArray *_topicOfForum;
      LoadingView *_loadingView;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ForumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self makeExtraCellLineHidden:_tableView];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    //有滑动视图，防止有下移
    if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
        self.automaticallyAdjustsScrollViewInsets =NO;
    }
    [SoapNotiCenter addSoapObserver:self selector:@selector(getAllForumUser:) SoapType:ST_GET_ALLFORUM_OFUSER];
    [SoapManager getAllForumUser];
}

-(void)getAllForumUser:(NSNotification *)noti{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _forumSource = [SoapNotiCenter getInfoFromNotification:noti];
        [SoapNotiCenter addSoapObserver:self selector:@selector(getTopicOfForum:) SoapType:ST_GET_TOPIC_OF_FORUM];
        [SoapManager getTopicForum:_forumSource[0][@"forumId"]];
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_ALLFORUM_OFUSER];
}

-(void)getTopicOfForum:(NSNotification *)noti{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _topicOfForum = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_loadingView removeFromSuperview];
            _loadingView = nil;
            _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [_tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_TOPIC_OF_FORUM];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_forumSource[0][@"forumId"] intValue];
  
}
/*-----------------------------------------------------------
 Description:  指定row高度
 -----------------------------------------------------------*/
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [forumCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    forumCell *cell = [tableView dequeueReusableCellWithIdentifier:[forumCell ID]];
    if (cell == nil)
    {
        cell = [[forumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[forumCell ID]];
    }
    cell.title.text =(NSString *)_topicOfForum[indexPath.row][@"title"];
    cell.head.image = [UIImage imageNamed:[forumCell headimage:[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"topicId"]]]];
    cell.unread.text =[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"unread"]];
    cell.count.text =[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"count"]];
    NSString *timestring =[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"time"]];
    cell.time.text = [forumCell  timechange:[timestring substringToIndex:10]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"title"]] forKey:@"topicBackButton"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"topicId"]] forKey:@"topicId"];
    [SoapManager getmsglistbyTopic:[NSString stringWithFormat:@"%@",_topicOfForum[indexPath.row][@"topicId"]]];
    forumTopicViewController *forumTopic = [[forumTopicViewController alloc]init];
    [self.navigationController pushViewController:forumTopic animated:YES];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*-----------------------------------------------------------
 Description:  消除多余的线
 -----------------------------------------------------------*/
- (void) makeExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
