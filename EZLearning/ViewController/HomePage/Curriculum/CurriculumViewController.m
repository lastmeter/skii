//
//  CurriculumViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "CurriculumViewController.h"
#import "CourseCell.h"
#import "CourseModel.h"
#import "AppDelegate.h"

@interface CurriculumViewController ()<UITableViewDataSource, UITableViewDelegate, CourseButtonDelegate>

{
    int _rowNumber;
    int _month;
    int _nowMonth;
    int _year;
    int _nowYear;
    
    NSArray *_courseSource;
    
    LoadingView *_loadingView;
 //   WebViewController *_web;
}

@property (strong, nonatomic) IBOutlet UIView *selectView;
//@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *monthTitle;

@end

@implementation CurriculumViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.title = @"课程表";
        self.tabBarItem.image = [UIImage imageNamed:@"tab2_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"tab2_sel"];
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getCurrentMonth];
    _monthTitle.text = [NSString stringWithFormat:@"%d月",_month];
    //_monthLabel.text = [NSString stringWithFormat:@"%d月",_month];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [SoapNotiCenter addSoapObserver:self selector:@selector(getCourse:) SoapType:ST_GET_MY_COURSE];
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    
    _selectView.hidden = NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SoapManager getMyCourseByOffSet:(_month - _nowMonth)];
    [_loadingView startLoading];
}

- (void) getCourse:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _courseSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
        });
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_courseSource.count %2 == 0)
    {
        
        _rowNumber = (int)_courseSource.count/2;
    }
    else
    {
        _rowNumber = (int)_courseSource.count /2 +1;
    }
    return _rowNumber;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Identifier = @"academy";
    int row = (int)indexPath.row;
    int count = row*2;
    CourseCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[CourseCell alloc] initWithReuseIdentifier:Identifier];
    }
    if (_courseSource.count %2 != 0 && row == _rowNumber-1)
    {
        cell.lastCourse = [_courseSource lastObject];
    }
    else
    {
        cell.leftCourse  = _courseSource[count];
        cell.rightCourse = _courseSource[count+1];
    }
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CourseCell cellHeight];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}
/*-----------------------------------------------------------
 Description:  消除选中痕迹
 -----------------------------------------------------------*/
- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}


-(void)imgBtnClick:(UIButton *)btn
{
    
    CourseModel * tmp = _courseSource[btn.tag];
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    [delegate pushLearningViewControllerWithTitle:tmp.title Url:tmp.url];
//    if (_web == nil)
//    {
//        _web = [[WebViewController alloc]  initWithNibName:@"WebViewController" bundle:nil];
//    }
//    _web.urlString = tmp.url;
//    _web.courseTitle = tmp.title;
//    [self.navigationController pushViewController:_web animated:YES];
    
}


- (void)getCurrentMonth
{
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSCalendarUnitMonth | NSCalendarUnitYear;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:now];
   
    _nowMonth = (int) [dateComponents month];
    _month = _nowMonth;
    
    _nowYear = (int)[dateComponents year];
    _year = _nowYear;
}

- (IBAction)changeMonth:(UIButton *)sender
{

    switch (sender.tag)
    {
        case 0:
            if (_month > 1)
            {
                _month = _month-1;
            }
            else
            {
                _year = _year -1;
                _month = 12;
            }
            break;
        case 1:
            if (_month < 12)
            {
                _month = _month+1;
            }
            else
            {
                _year = _year+1;
                _month = 1;
            }
            break;
        default:
            break;
    }
    int offset;
    if (_year > _nowYear)
    {
        offset = 12 - _nowMonth + _month + (_year - _nowYear -1 )*12;
         _monthTitle.text = [NSString stringWithFormat:@"%d年%d月", _year, _month];
        
    }
    else if (_year < _nowYear)
    {
        offset = 0 -( _nowMonth + (12 - _month) + (_nowYear - _year - 1)*12);
        _monthTitle.text = [NSString stringWithFormat:@"%d年%d月", _year, _month];
    }
    else
    {
        offset = _month-_nowMonth;
        _monthTitle.text = [NSString stringWithFormat:@"%d月",_month];
    }
    [SoapManager getMyCourseByOffSet:offset];
    [_loadingView startLoading];
}
- (IBAction)selectMonth:(id)sender
{
    _selectView.hidden = NO;
}

- (IBAction)quitSelect:(id)sender
{
    /*
    if (_year != _nowYear ) {
        _monthLabel.text = [NSString stringWithFormat:@"%d年%d月",_year,_month];
    }
    else
    {
        _monthLabel.text = [NSString stringWithFormat:@"%d月",_month];
    }
     */
    _selectView.hidden = YES;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
