//
//  forumTopicCell.h
//  EZLearning
//
//  Created by zwf on 15/6/15.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface forumTopicCell : UITableViewCell
@property (strong, nonatomic) UIImageView *head;

@property (strong, nonatomic) UILabel *author;
@property (strong, nonatomic) UILabel *title;

//@property (strong, nonatomic) UILabel *time;
@property (strong, nonatomic) UILabel *replyTime;
@property (strong, nonatomic) UILabel *reply;
@property (strong, nonatomic) UILabel *replycount;

//类方法
+ (NSString *)ID;
+ (CGFloat) cellHeight;
+ (NSString *)timechange:(NSString *)time;
-(CGFloat)inittitleText:(NSString *)string;
@end
