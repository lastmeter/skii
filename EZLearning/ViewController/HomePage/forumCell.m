//
//  forumCell.m
//  EZLearning
//
//  Created by zwf on 15/6/14.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "forumCell.h"

@implementation forumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat kCellHeight = 60;
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    self.frame = CGRectMake(0, 0, cellWidth, kCellHeight);
    self.head = [[UIImageView alloc] initWithFrame:CGRectMake(10, 11, 48, 48)];
    [self.contentView addSubview:_head];
    
    self.title = [[UILabel alloc]initWithFrame:CGRectMake(self.head.frame.origin.x+48+5, 5, cellWidth/2, 30)];
    _title.font = [UIFont systemFontOfSize:18.0];
    [self.contentView addSubview:_title];
    
    
    self.unreadimage = [[UIImageView alloc]initWithFrame:CGRectMake(self.head.frame.origin.x+48+5, 45, 21, 21)];
    _unreadimage.image = [UIImage imageNamed:@"unread"];
    [self.contentView addSubview:_unreadimage];
    
    
    self.unread = [[UILabel alloc]initWithFrame:CGRectMake(self.unreadimage.frame.origin.x+21, 45, 20, 21)];
    _unread.font = [UIFont systemFontOfSize:14.0];
    _unread.textColor = [UIColor grayColor];
    [self.contentView addSubview:_unread];
    
    self.countimage = [[UIImageView alloc]initWithFrame:CGRectMake(self.unread.frame.origin.x+20, 45, 21, 21)];
     _countimage.image = [UIImage imageNamed:@"topic_count"];
    [self.contentView addSubview:_countimage];
    
    self.count = [[UILabel alloc]initWithFrame:CGRectMake(self.countimage.frame.origin.x+21, 45, 20, 21)];
    _count.font = [UIFont systemFontOfSize:14.0];
    _count.textColor = [UIColor grayColor];
    [self.contentView addSubview:_count];
    
    self.time = [[UILabel alloc]initWithFrame:CGRectMake(cellWidth/2-10, 45, cellWidth/2, 21) ];
    _time.font = [UIFont systemFontOfSize:14.0];
    _time.textColor = [UIColor grayColor];
    _time.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_time];
    
    return self;
}

+ (NSString *)ID
{
    return @"forum";
}

+ (CGFloat) cellHeight
{
    return 70;
}

+ (NSString *)timechange:(NSString *)time
{
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    return [formatter stringFromDate:confromTimesp];
}

+ (NSString *)headimage:(NSString *)tag
{
    int ahead = [tag intValue];
    switch (ahead) {
        case 7:
            return @"cco";
            break;
        case 9:
            return @"west";
            break;
        case 10:
            return @"east";
            break;
        case 11:
            return @"south";
            break;
        case 12:
            return @"north";
            break;
        case 13:
            return @"gbl";
            break;
        case 14:
            return @"gsh";
            break;
        default:
            return nil;
            break;
    }
}

@end
