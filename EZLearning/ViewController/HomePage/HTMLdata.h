//
//  HTMLdata.h
//  EZLearning
//
//  Created by zwf on 15/6/28.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface HTMLdata : NSObject

+(CGFloat)getImageWidth:(NSString *)htmlString;
+(CGFloat)getImageHeight:(NSString *)htmlString;
+(NSString *)getbodyString:(NSString *)htmlString;
+(NSString *)getImageUrl:(NSString *)htmlString;
@end