//
//  invitationCellThree.m
//  EZLearning
//
//  Created by zwf on 15/6/18.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "invitationCellThree.h"
@implementation invitationCellThree
{
    MBProgressHUD *_HUD;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat kCellHeight = 60;
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    self.author = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, cellWidth/2*3, kCellHeight/2)];
    _author.textColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
    [self.contentView addSubview:_author];
    
    self.time = [[UILabel alloc]initWithFrame:CGRectMake(cellWidth/3*2-5, 0,cellWidth/3, kCellHeight/2)];
    _time.textColor = [UIColor grayColor];
    _time.font = [UIFont systemFontOfSize:14.0 ];
    _time.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_time];
    
    self.body = [[UILabel alloc]initWithFrame:CGRectMake(5, 30, cellWidth-10, 0)];
    _body.numberOfLines = 0;
    _body.font = [UIFont systemFontOfSize:16.0];
    [self.contentView addSubview:_body];
    
    self.messageId = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _messageId.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_messageId];
    
    self.me = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _me.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_me];
    self.praiseNum = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _praiseNum.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_praiseNum];
    
    self.sendview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    self.praiseview = [[UIView alloc]initWithFrame:CGRectMake(cellWidth-60, 0, 60, 60)];
    UIImageView *praiseImage = [[UIImageView alloc]initWithFrame:CGRectMake(14, 0, 32, 32)];
    praiseImage.image = [UIImage imageNamed:@"praise_normal"];
    praiseImage.userInteractionEnabled=YES;
    
    [_praiseview addSubview:praiseImage];
    self.praiseLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 60, 28)];
    _praiseLabel.font = [UIFont systemFontOfSize:13.0];
    _praiseLabel.text = @"赞(0)";
    _praiseLabel.textAlignment = NSTextAlignmentCenter;
    UITapGestureRecognizer *praiseTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectpraise)];
    [_praiseview addGestureRecognizer:praiseTap];
    [_praiseview addSubview:_praiseLabel];
    [_sendview addSubview:_praiseview];

    
    self.copyview = [[UIView alloc]initWithFrame:CGRectMake(cellWidth-100, 0, 40, 60)];
    
    UIImageView *copyImage = [[UIImageView alloc]initWithFrame:CGRectMake(4, 0, 32, 32)];
    copyImage.image = [UIImage imageNamed:@"copy_normal"];
    [_copyview addSubview:copyImage];
    
    UILabel *copyLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 40, 28)];
    copyLabel.font = [UIFont systemFontOfSize:13.0];
    copyLabel.text = @"复制";
    copyLabel.textAlignment = NSTextAlignmentCenter;
    [_copyview addSubview:copyLabel];
    
    UITapGestureRecognizer *copyTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectcopy)];
    [_copyview addGestureRecognizer:copyTap];
    [_sendview addSubview:_copyview];
    
    [self.contentView addSubview:_sendview];
    
    return self;
}

+ (NSString *)ID
{
    return @"invitationThree";
}

+ (CGFloat) cellHeight
{
    return 60;
}

+ (NSString *)timechange:(NSString *)time
{
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM-dd HH:mm:ss"];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    return [formatter stringFromDate:confromTimesp];
}

-(CGFloat)initbodyText:(NSString *)string
{
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    CGRect tmpRect = _body.frame;
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString: string];
    _body.attributedText =attrStr;
    CGRect bodySize =[_body.text boundingRectWithSize:CGSizeMake(cellWidth-10,8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_body.font,NSFontAttributeName, nil] context:nil];
    tmpRect.size.height =bodySize.size.height;
    _body.frame =tmpRect;
    _messageId.frame = CGRectMake(0, tmpRect.size.height+35, cellWidth/2, 20);
    _me.frame = CGRectMake(0, tmpRect.size.height+55, cellWidth/2, 20);
    _praiseNum.frame = CGRectMake(0, tmpRect.size.height+75, cellWidth/2, 20);

    _sendview.frame = CGRectMake(0, tmpRect.size.height+35, cellWidth, 60);
    return tmpRect.size.height;
}

-(void)selectpraise
{
    NSLog(@"praise");
    if ([_me.text isEqualToString:@"1"]) {
        [self HUDshow:@"您已经点过赞了"];
        
    }else{
        [SoapManager addpraisetoMSG:_messageId.text];
        [SoapNotiCenter addSoapObserver:self selector:@selector(getISpraise:) SoapType:ST_ADD_PRAISE_TO_MSG];
    }

}

-(void)getISpraise:(NSNotification *)noti
{
    NSLog(@"sdfgasdg");
    /*
     NSString * _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
     NSLog(@"_jdsl= %@",_boolpost);
     if ([SoapNotiCenter getSoapResult:noti])
     {
     
     }
     */
    
    NSString * _boolpost = @"true";
    if ([_boolpost boolValue]) {
        _me.text = @"1";
        NSString *praiseNum = _praiseNum.text;
        int num =[praiseNum intValue];
        _praiseNum.text = [NSString stringWithFormat:@"%d",++num];
        NSMutableString *praiseString = [[NSMutableString alloc]initWithString:@"赞("];
        [praiseString appendString:_praiseNum.text];
        [praiseString appendString:@")"];
        _praiseLabel.text = praiseString;
        _praiseLabel.textColor = [UIColor blueColor];
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_ADD_PRAISE_TO_MSG];
}



-(void)selectcopy
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _body.text;
    [self HUDshow:@"已经放到系统的剪贴板里"];
}

-(void)HUDshow:(NSString *)title
{
    _HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    _HUD.delegate=self;
    _HUD.labelText = title;
    _HUD.mode = MBProgressHUDModeText;
    _HUD.margin =10.0;
    _HUD.opacity =1.0;
    _HUD.labelFont = [UIFont boldSystemFontOfSize:13.0];
    [self.window addSubview:_HUD];
    [_HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    _HUD.yOffset = MAIN_SCREEN_HEIGHT/2-60;
}

-(void)myTask
{
    sleep(1.0);
}

-(void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
    hud=nil;
}
@end
