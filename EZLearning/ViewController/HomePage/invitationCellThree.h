//
//  invitationCellThree.h
//  EZLearning
//
//  Created by zwf on 15/6/18.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EZLViewController.h"
#import "hudView.h"
@interface invitationCellThree : UITableViewCell<MBProgressHUDDelegate>

@property (strong, nonatomic) UILabel *author;
@property (strong, nonatomic) UILabel *time;
@property (strong, nonatomic) UILabel *body;
@property (strong, nonatomic) UIView *sendview;
@property (strong, nonatomic) UIView *praiseview;
@property (strong, nonatomic) UIView *copyview;
@property (strong, nonatomic) UILabel *praiseLabel;
@property (strong, nonatomic) UILabel *messageId;
@property (strong, nonatomic) UILabel *me;
@property (strong, nonatomic) UILabel *praiseNum;

+ (NSString *)ID;
+ (CGFloat) cellHeight;
+ (NSString *)timechange:(NSString *)time;
-(CGFloat)initbodyText:(NSString *)string;

@end
