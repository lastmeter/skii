//
//  forumCell.h
//  EZLearning
//
//  Created by zwf on 15/6/14.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface forumCell : UITableViewCell

@property (strong, nonatomic) UIImageView *head;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *unread;
@property (strong, nonatomic) UIImageView *unreadimage;
@property (strong, nonatomic) UILabel *count;
@property (strong, nonatomic) UIImageView *countimage;
@property (strong, nonatomic) UILabel *time;

+ (NSString *)ID;
+ (CGFloat) cellHeight;
+ (NSString *)timechange:(NSString *)time;
+ (NSString *)headimage:(NSString *)tag;
@end
