//
//  forumTopicViewController.m
//  EZLearning
//
//  Created by zwf on 15/6/15.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "forumTopicViewController.h"
#import "forumTopicCell.h"
#import "postViewController.h"
#import "invitationViewController.h"
@interface forumTopicViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    LoadingView *_loadingView;
    NSMutableArray *_topicList;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *addButton;
@end

@implementation forumTopicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self makeExtraCellLineHidden:_tableView];
    
    [_backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [_backButton setTitle:[[NSUserDefaults standardUserDefaults]objectForKey:@"topicBackButton"] forState:UIControlStateNormal];
    
    [_addButton setImage:[UIImage imageNamed:@"add_presspostforum"] forState:UIControlStateSelected];
    [_addButton addTarget:self action:@selector(btn_click) forControlEvents:UIControlEventTouchUpInside];
    
    
}



-(void)btn_click
{
    postViewController *post = [[postViewController alloc]init];
    post.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:post animated:NO completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(getMsgList:) SoapType:ST_GET_MSGSUMMARY_OF_TOPIC];
    [SoapManager getmsglistbyTopic:[NSString stringWithFormat:@"%@",(NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"topicId"]]];
}

-(void)getMsgList:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _topicList = [SoapNotiCenter getInfoFromNotification:noti];
        //NSLog(@"_topicList = %@",_topicList);
        MAIN(^{
            [_loadingView removeFromSuperview];
            _loadingView = nil;
            _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [_tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_MSGSUMMARY_OF_TOPIC];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_topicList count];
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    forumTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:[forumTopicCell ID]];
    if (cell == nil)
    {
        cell = [[forumTopicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[forumTopicCell ID]];
    }
    CGFloat height = [cell inittitleText:_topicList[indexPath.row][@"title"]];
    return ([forumTopicCell cellHeight]+height);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    forumTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:[forumTopicCell ID]];
    if (cell == nil)
    {
        cell = [[forumTopicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[forumTopicCell ID]];
    }
    cell.author.text = _topicList[indexPath.row][@"author"];
    [cell inittitleText:_topicList[indexPath.row][@"title"]];
    cell.title.text = (NSString *)_topicList[indexPath.row][@"title"];
    NSString *replyTimestring =[NSString stringWithFormat:@"%@",_topicList[indexPath.row][@"replyTime"]];
    cell.replyTime.text = [forumTopicCell timechange:[replyTimestring substringToIndex:10]];
    cell.replycount.text = [NSString stringWithFormat:@"%@",_topicList[indexPath.row][@"count"]];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    [SoapManager getmsgandReplies:[[NSUserDefaults standardUserDefaults]objectForKey:@"topicId"] MessageId:[NSString stringWithFormat:@"%@",_topicList[indexPath.row][@"id"]]];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",_topicList[indexPath.row][@"id"]] forKey:@"id"];
    invitationViewController *invitation = [[invitationViewController alloc]init];
    [self.navigationController pushViewController:invitation animated:YES];
}


-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*-----------------------------------------------------------
 Description:  消除多余的线
 -----------------------------------------------------------*/
- (void) makeExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
