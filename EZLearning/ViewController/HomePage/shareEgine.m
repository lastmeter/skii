//
//  shareEgine.m
//  EZLearning
//
//  Created by zwf on 15/6/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "shareEgine.h"

@implementation shareEgine
static shareEgine *sharedSingleton_ = nil;
+ (shareEgine *) sharedInstance
{
    @synchronized(self){
        if (sharedSingleton_ == nil) {
            sharedSingleton_ = [[self alloc] init];
        }
    }
    return  sharedSingleton_;
}

- (void)registerApp
{
    //向微信注册
    [WXApi registerApp:@"wxcbae123b7afd6317"];
}
- (BOOL)handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}


#pragma mark - wechat delegate
/*
- (void)weChatPostStatus:(NSString*)message
{
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = YES;
    req.text = message;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

- (void)weChatFriendPostStatus:(NSString*)message
{
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = YES;
    req.text = message;
    req.scene = WXSceneTimeline;
    
    [WXApi sendReq:req];
}
*/

- (void)sendWeChatImage:(UIImage*)image WithType:(WeiboType)weiboType
{
    if(weChat == weiboType)
    {
        [self sendAppContentWithImage:image  WithScene:WXSceneSession];
        return;
    }
    else if(weChatFriend == weiboType)
    {
        [self sendAppContentWithImage:image WithScene:WXSceneTimeline];
        return;
    }

}

- (void)sendWeChatMessage:(NSString*)message WithType:(WeiboType)weiboType
{
    if(weChat == weiboType)
    {
        [self sendAppContentWithMessage:message  WithScene:WXSceneSession];
        return;
    }
    else if(weChatFriend == weiboType)
    {
        [self sendAppContentWithMessage:message  WithScene:WXSceneTimeline];
        return;
    }
}


-(void)sendAppContentWithImage:(UIImage*)image  WithScene:(int)scene{
    // 发送内容给微信
    WXMediaMessage *message = [WXMediaMessage message];
    NSData *thumbImgData = UIImageJPEGRepresentation(image, 0.1);
    NSData *thumbImgData_ = UIImageJPEGRepresentation(image, 0.05);
    NSData *thumbImgData__ = UIImageJPEGRepresentation(image, 0.01);
    if (thumbImgData.length<32*1000) {
        message.thumbData = thumbImgData;
    }else if(thumbImgData.length>=32*1000&&thumbImgData_.length<32*1000){
        message.thumbData = thumbImgData_;
    }else if(thumbImgData__.length<32*1000){
        message.thumbData = UIImageJPEGRepresentation(image, 0.01);
    }else{
    }
    WXImageObject *ext = [WXImageObject object];
    ext.imageData = UIImageJPEGRepresentation(image, 1.0);
    message.mediaObject = ext;
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}


- (void)sendAppContentWithMessage:(NSString*)appMessage  WithScene:(int)scene
{
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = YES;
    req.text = appMessage;
    req.scene = scene;
    [WXApi sendReq:req];
}



-(void) onSentTextMessage:(BOOL) bSent
{
    // 通过微信发送消息后， 返回本App
    //    NSString *strTitle = [NSString stringWithFormat:@"发送结果"];
    //    NSString *strMsg = [NSString stringWithFormat:@"发送文本消息结果:%u", bSent];
    //
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //    [alert show];
    //    [alert release];
}


-(void) onSentMediaMessage:(BOOL) bSent
{
    // 通过微信发送消息后， 返回本App
    NSString *strTitle = [NSString stringWithFormat:@"发送结果"];
    NSString *strMsg = [NSString stringWithFormat:@"发送媒体消息结果:%u", bSent];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

-(void) onSentAuthRequest:(NSString *) userName accessToken:(NSString *) token expireDate:(NSDate *) expireDate errorMsg:(NSString *) errMsg
{
    
}

-(void) onShowMediaMessage:(WXMediaMessage *) message
{
    // 微信启动， 有消息内容。
    //    WXAppExtendObject *obj = message.mediaObject;
    
    //    shopDetailViewController *sv = [[shopDetailViewController alloc] initWithNibName:@"shopDetailViewController" bundle:nil];
    //    sv.m_sShopID = obj.extInfo;
    //    [self.navigationController pushViewController:sv animated:YES];
    //    [sv release];
    
    //    NSString *strTitle = [NSString stringWithFormat:@"消息来自微信"];
    //    NSString *strMsg = [NSString stringWithFormat:@"标题：%@ \n内容：%@ \n附带信息：%@ \n缩略图:%u bytes\n\n", message.title, message.description, obj.extInfo, message.thumbData.length];
    //
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //    [alert show];
    //    [alert release];
}

-(void) onRequestAppMessage
{
    // 微信请求App提供内容， 需要app提供内容后使用sendRsp返回
}

-(void) onReq:(BaseReq*)req
{
    if([req isKindOfClass:[GetMessageFromWXReq class]])
    {
        [self onRequestAppMessage];
    }
    else if([req isKindOfClass:[ShowMessageFromWXReq class]])
    {
        ShowMessageFromWXReq* temp = (ShowMessageFromWXReq*)req;
        [self onShowMediaMessage:temp.message];
    }
    
}

-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        
        //        NSString *strTitle = [NSString stringWithFormat:@"发送结果"];
        //        NSString *strMsg = [NSString stringWithFormat:@"发送媒体消息结果:%d", resp.errCode];
        //
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //        [alert show];
        //        [alert release];
    }
    //    else if([resp isKindOfClass:[SendAuthResp class]])
    //    {
    //        NSString *strTitle = [NSString stringWithFormat:@"Auth结果"];
    //        NSString *strMsg = [NSString stringWithFormat:@"Auth结果:%d", resp.errCode];
    //
    //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    //        [alert release];
    //    }
}


@end
