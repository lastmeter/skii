//
//  CollectionViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "FavoriteViewController.h"
#import "CourseModel.h"
#import "CourseCell.h"
#import "AppDelegate.h"

@interface FavoriteViewController ()<CourseButtonDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_courseSource;
    LoadingView *_loadingView;
    int _rowNumber;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FavoriteViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvFavoriteList:) SoapType:ST_GET_FAVORITES_LIST];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SoapManager getFavoritesList];
    [_loadingView startLoading];
}

- (void)recvFavoriteList:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _courseSource = [SoapNotiCenter getInfoFromNotification:noti];
        dispatch_async(dispatch_get_main_queue(),^{
            [_tableView reloadData];
        });
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_courseSource.count %2 == 0)
    {
        
        _rowNumber = (int)_courseSource.count/2;
    }
    else
    {
        _rowNumber = (int)_courseSource.count /2 +1;
    }
    return _rowNumber;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)indexPath.row;
    int count = row*2;
    
    static NSString *Identifier = @"academy";
    
    CourseCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[CourseCell alloc] initWithReuseIdentifier:Identifier];
    }
    
    if (_courseSource.count %2 != 0 && row == _rowNumber-1)
    {
        cell.lastCourse = [_courseSource lastObject];
    }
    else
    {
        cell.leftCourse  = _courseSource[count];
        cell.rightCourse  = _courseSource[count+1];
    }
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CourseCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)imgBtnClick:(UIButton *)btn
{
    CourseModel * tmp = _courseSource[btn.tag];
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    [delegate pushLearningViewControllerWithTitle:tmp.title Url:tmp.url];

}

-(void)collectBtnClick:(UIButton *)btn
{
    CourseModel *model = _courseSource[btn.tag];
    [SoapManager cancelFavoritesByPackageId:model.contentPackageId];
    
    [_courseSource removeObjectAtIndex:btn.tag];
    [_courseSource enumerateObjectsUsingBlock:^(CourseModel * obj, NSUInteger index, BOOL *stop)
     {
         obj.tag = (int)index;
     }];
    
    NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:0];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
 
@end
