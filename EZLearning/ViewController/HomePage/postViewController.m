//
//  postViewController.m
//  EZLearning
//
//  Created by zwf on 15/6/16.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "postViewController.h"
#import "MBProgressHUD.h"
#import "forumTopicViewController.h"
@interface postViewController ()<UITextViewDelegate,MBProgressHUDDelegate>
{
    NSString *_boolpost;
    MBProgressHUD *_HUD;
}

@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *postButton;
@property (strong, nonatomic) IBOutlet UITextView *titleView;
@property (strong, nonatomic) IBOutlet UITextView *contentView;

@end

@implementation postViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [_postButton addTarget:self action:@selector(postinvitation:) forControlEvents:UIControlEventTouchUpInside];
    _titleView.delegate = self;
    _titleView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
    _titleView.tag = 00;
    _titleView.layer.borderWidth = 1.0;
    _titleView.layer.borderColor = [[UIColor grayColor]CGColor];
    
    _contentView.delegate = self;
    _contentView.tag = 01;
    _contentView.layer.borderWidth = 1.0;
    _contentView.layer.borderColor = [[UIColor grayColor]CGColor];
    [SoapNotiCenter addSoapObserver:self selector:@selector(addMsgofTopic:) SoapType:ST_ADD_MSG_TO_TOPIC];

    
}

-(void)addMsgofTopic:(NSNotification *)noti{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
        if ([_boolpost intValue]==-1) {
            [self HUDshow:@"发帖失败，请重试"];
        }
        else
        {
            [self HUDshow:@"发帖成功"];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_ADD_MSG_TO_TOPIC];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag==00) {
        _titleView.layer.borderColor = [[UIColor blueColor]CGColor];
        _contentView.layer.borderColor = [[UIColor grayColor]CGColor];
        if ([textView.text isEqualToString:@"标题(必填)"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }
    }
    if (textView.tag==01){
        _titleView.layer.borderColor = [[UIColor grayColor]CGColor];
        _contentView.layer.borderColor = [[UIColor orangeColor]CGColor];
        if ([textView.text isEqualToString:@"正文"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag==00 && textView.text.length<1) {
        textView.text = @"标题(必填)";
        textView.textColor = [UIColor grayColor];
    }
    if (textView.tag==01 && textView.text.length<1) {
        textView.text = @"正文";
        textView.textColor = [UIColor grayColor];
    }
}
/*
- (void)textViewDidChange:(UITextView *)textView
{
    CGRect tmpRect = textView.frame;
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString:textView.text];
    textView.attributedText =attrStr;
    CGRect bodySize =[textView.text boundingRectWithSize:CGSizeMake(textView.frame.size.width,8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:textView.font,NSFontAttributeName, nil] context:nil];
    if (bodySize.size.height>40&&textView.tag==00) {
        tmpRect.size.height =bodySize.size.height+20;
        textView.frame =tmpRect;
    }
}
*/

-(IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)HUDshow:(NSString *)title
{
    _HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    _HUD.delegate=self;
    _HUD.labelText = title;
    _HUD.mode = MBProgressHUDModeText;
    _HUD.margin =10.0;
    _HUD.opacity =1.0;
    _HUD.labelFont = [UIFont boldSystemFontOfSize:13.0];
    [self.view.window addSubview:_HUD];
    [_HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    _HUD.yOffset = MAIN_SCREEN_HEIGHT/2-40;
}

-(void)myTask
{
    sleep(2.0);
}


-(IBAction)postinvitation:(id)sender
{
    [SoapManager getaddMsgtoTopic:[[NSUserDefaults standardUserDefaults]objectForKey:@"topicId"] Msgtitle:_titleView.text Msgbody:_contentView.text MsgreplyTold:@""];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [_titleView resignFirstResponder];
    [_contentView resignFirstResponder];
    _titleView.layer.borderColor = [[UIColor grayColor]CGColor];
    _contentView.layer.borderColor = [[UIColor grayColor]CGColor];
}


-(void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
    hud=nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
