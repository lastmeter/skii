//
//  ParseHtml.h
//  EZLearning
//
//  Created by zwf on 15/6/29.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TFHpple.h"
@interface ParseHtml : NSObject
+ (NSArray*)parseData:(NSData *)htmlData;
+ (NSString *)getTextfromHtml:(NSString *)htmlString;
@end

