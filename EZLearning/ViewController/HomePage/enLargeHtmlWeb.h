//
//  enLargeHtmlWeb.h
//  EZLearning
//
//  Created by zwf on 15/6/30.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface enLargeHtmlWeb : UIView
- (instancetype)initWeb:(UIImage *)image;
- (void)statEnlarge;
@end
