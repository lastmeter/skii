//
//  ParseHtml.m
//  EZLearning
//
//  Created by zwf on 15/6/29.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "ParseHtml.h"
@implementation ParseHtml



//解析html数据
+ (NSArray*)parseData:(NSData *)htmlData
{
    
    TFHpple *doc = [[TFHpple alloc] initWithHTMLData:htmlData];
    
    //在页面中查找img标签
    NSArray *imagesArray = [doc searchWithXPathQuery:@"//img"];
    /*
    for (TFHppleElement *hppleElement in imagesArray) {
        NSLog(@"%@",hppleElement.raw);
        NSLog(@"%@",hppleElement.text);
    }*/
    return imagesArray;
}


+ (NSString *)getTextfromHtml:(NSString *)htmlString
{
    NSMutableString *htmlText = [[NSMutableString alloc]initWithString:@""];
    NSRegularExpression *regularExpretion = [NSRegularExpression regularExpressionWithPattern:@"<[^>]*>|\n" options:0 error:nil];
    htmlString = [regularExpretion stringByReplacingMatchesInString:htmlString options:NSMatchingReportProgress range:NSMakeRange(0,htmlString.length) withTemplate:@"-"];
    regularExpretion = [NSRegularExpression regularExpressionWithPattern:@"-{1,}" options:0 error:nil];
    htmlString = [regularExpretion stringByReplacingMatchesInString:htmlString options:NSMatchingReportProgress range:NSMakeRange(0, htmlString.length) withTemplate:@"-"];
    NSArray *arr = [NSArray array];
    htmlString = [NSString stringWithString:htmlString];
    arr = [htmlString componentsSeparatedByString:@"-"];
    NSMutableArray *marr = [NSMutableArray arrayWithArray:arr];
    [marr removeObject:@""];
    [marr removeObject:@" "];
    [marr removeObject:@"  "];
    NSInteger j;
    if (marr.count>0) {
        for (j =0;j < marr.count;j++) {
            [htmlText appendString:[NSString stringWithFormat:@"%@",marr[j]]];
        }
    }
    return htmlText;
}
@end