//
//  AdvertViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/27.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AdvertViewController.h"

@interface AdvertViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation AdvertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
- (IBAction)back:(id)sender
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    NSString *encodedString = [_urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:encodedString]];
    [_webView loadRequest:request];
}

-(void)setUrlString:(NSString *)urlString
{
    _urlString = urlString;
}

@end
