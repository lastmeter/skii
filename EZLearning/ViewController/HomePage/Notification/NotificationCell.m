//
//  NotificationCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "NotificationCell.h"
#import "NotificationModel.h"

@interface NotificationCell ()<UIWebViewDelegate>
{
    UILabel *_content;
    UIWebView *_webView;
    void(^_handler)();
}
@end

@implementation NotificationCell

+(CGFloat)cellHeight
{
    return  0.6*MAIN_SCREEN_HEIGHT;
}

+(instancetype)cellWithTableView:(UITableView *)tableView
{
    static NSString *Identifier = @"notification";
    NotificationCell *notificationCell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (notificationCell == nil)
    {
        notificationCell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    return notificationCell;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {        
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(20, 0, MAIN_SCREEN_WIDTH-40,  5)];
        [self.contentView addSubview:_webView];
        _webView.delegate = self;
    }
    return self;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    if (_notification.isLoaded) {
        return;
    }
    CGSize size = [webView sizeThatFits:CGSizeMake(1.0f, 1.0f)];
    _notification.cellHeight = size.height;
    _notification.isLoaded = YES;
    if (_handler) {
        _handler();
    }
}


- (void)setNotification:(NotificationModel *)notification
{
    NSLog(@"setNotification");
     _notification = notification;
    if (!notification.isLoaded) {
        _webView.frame = CGRectMake(20, 0, MAIN_SCREEN_WIDTH-40,  5.0f);
    }
    else{
        _webView.frame = CGRectMake(20, 0, MAIN_SCREEN_WIDTH-40,  notification.cellHeight);
    }
    [_webView loadHTMLString:notification.content baseURL:nil];
}

- (void)didFinishLoad:(void(^)())handler{
    _handler = [handler copy];
}


@end
