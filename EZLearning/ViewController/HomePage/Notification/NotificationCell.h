//
//  NotificationCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  NotificationModel;
@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) NotificationModel *notification;
+(instancetype)cellWithTableView:(UITableView *)tableView;
- (void)didFinishLoad:(void(^)())handler;

@end
