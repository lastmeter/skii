//
//  NotificationViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationCell.h"
#import "NotificationTitle.h"
#import "NotificationModel.h"

@interface NotificationViewController()<NotificationTitleDelegate,UITableViewDataSource,UITableViewDelegate>
{
    LoadingView *_loadingView;
}

@property (strong, nonatomic) NSArray *notificationSource;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation NotificationViewController

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [self makeExtraCellLineHidden:self.tableView];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(getNotice:) SoapType:ST_GET_NOTICE_LIST];
    [SoapManager getNoticeListAfterTimeStamp:0];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
}

- (void)getNotice:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _notificationSource = [SoapNotiCenter getInfoFromNotification:noti];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        dispatch_async(dispatch_get_main_queue(),^{
            [self.tableView reloadData];
            [self makeExtraCellLineHidden:self.tableView];
            [_loadingView removeFromSuperview];
            _loadingView = nil;
        });
    }
}

/*-----------------------------------------------------------
 Description:  指定分区（section）的数目
 -----------------------------------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _notificationSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [NotificationTitle titleHeight];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @" ";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NotificationTitle *titleView = [NotificationTitle titleViewWithTableView:tableView];
    
    titleView.delegate = self;
    titleView.notification = _notificationSource[section];
    
    return titleView;
}

/*-----------------------------------------------------------
 Description:   指定每个分区（section）的行数
 -----------------------------------------------------------*/
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NotificationModel *notification = _notificationSource[section];
    return notification.isOpened ? 1:0;
}
/*-----------------------------------------------------------
 Description:   指定row高度
 -----------------------------------------------------------*/
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationModel *model = _notificationSource[indexPath.section];
    return model.cellHeight;
}
/*-----------------------------------------------------------
 Description:   绘制cell
 -----------------------------------------------------------*/
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noti"];
    if (!cell) {
        cell = [[NotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noti"];
    }
    cell.notification = _notificationSource[indexPath.section];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    __weak UITableView *weakTV = _tableView;
    WEAKSELF
    [cell didFinishLoad:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NotificationModel *model = weakSelf.notificationSource[indexPath.section];
            model.isLoaded = YES;
            [weakTV reloadData];
        });
      
    }];
    return cell;
}

/*-----------------------------------------------------------
 Description:  消除多余的线
 -----------------------------------------------------------*/
- (void) makeExtraCellLineHidden: (UITableView *)tableView
{
    if ( 0 == _notificationSource.count )
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    else
    {
        UIView *view = [[UIView alloc] init];
        [view setBackgroundColor:[UIColor clearColor]];
        [self.tableView setTableFooterView:view];
    }
}

/*-----------------------------------------------------------
 Description:  点击标题栏，展开view
 -----------------------------------------------------------*/
-(void)titleViewClick:(UIButton *)btn
{
    NotificationModel *notification = _notificationSource[btn.tag];
    notification.opened = !notification.opened;
    NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:btn.tag];
    [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
