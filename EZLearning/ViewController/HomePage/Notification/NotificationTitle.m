//
//  NotificationTitle.m
//  e-Learning
//
//  Created by RenHongwei on 15/2/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "NotificationTitle.h"
#import "NotificationModel.h"



@interface NotificationTitle ()
{
    UILabel *_titleLabel;
    UILabel *_timeLabel;
    UIButton *_bgButton;
    UIImageView *_arrow;
}
@end

@implementation NotificationTitle

+ (instancetype)titleViewWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"header";
    
    NotificationTitle *titleView = (NotificationTitle *)[tableView dequeueReusableCellWithIdentifier:ID];
    if (titleView == nil)
    {
        titleView = [[NotificationTitle alloc] initWithReuseIdentifier:ID];
    }
    return titleView;
}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier])
    {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        UIButton *bgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bgButton addTarget:self action:@selector(titleViewClick:) forControlEvents:UIControlEventTouchUpInside];
        [bgButton setBackgroundColor: [UIColor clearColor]];
        [self.contentView addSubview:bgButton];
        
        UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(21, 20, 7, 11)];
        arrowView.image = [UIImage imageNamed:@"NotificationArrow"];
        [self.contentView addSubview:arrowView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, MAIN_SCREEN_WIDTH - 40, 21)];
        label.font = [UIFont systemFontOfSize:17];
        [self.contentView addSubview: label];
        
        UILabel  *time = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 200, 21)];
        time.font = [UIFont systemFontOfSize:17];
        [self.contentView addSubview:time];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(20, 63, MAIN_SCREEN_WIDTH - 40, 1)];
        line.backgroundColor = [UIColor grayColor];
        [self.contentView addSubview:line];
        
        _titleLabel = label;
        _timeLabel = time;
        _arrow = arrowView;
        _bgButton = bgButton;
    }
    return self;
}

-(void)setNotification:(NotificationModel *)notification
{
    _titleLabel.text = notification.title;
    _timeLabel.text = notification.date;
    _bgButton.tag = notification.btnTag;
    _arrow.transform = notification.isOpened ? CGAffineTransformMakeRotation(M_PI_2) : CGAffineTransformMakeRotation(0);
}

- (void)titleViewClick:(UIButton *)btn
{
    _notification.opened = !_notification.opened ;
    if ([_delegate respondsToSelector:@selector(titleViewClick:)])
    {
        [_delegate titleViewClick:btn];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _bgButton.frame = self.bounds;
}

+ (CGFloat)titleHeight
{
    return 64;
}
@end
