//
//  NotificationTitle.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotificationTitleDelegate <NSObject>

@optional
- (void)titleViewClick:(UIButton *)btn;

@end

@class NotificationModel;
@interface NotificationTitle : UITableViewHeaderFooterView

+ (instancetype)titleViewWithTableView:(UITableView *)tableView;
+ (CGFloat)titleHeight;

@property (nonatomic, strong) NotificationModel *notification;
@property (nonatomic, weak) id<NotificationTitleDelegate> delegate;

@end
