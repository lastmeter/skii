//
//  forumTopicCell.m
//  EZLearning
//
//  Created by zwf on 15/6/15.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "forumTopicCell.h"

@implementation forumTopicCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    CGFloat kCellHeight = 60;
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    self.frame = CGRectMake(0, 0, cellWidth, kCellHeight);
    self.head = [[UIImageView alloc] initWithFrame:CGRectMake(10, 6, 48, 48)];
    self.head.image = [UIImage imageNamed:@"topic_list"];
    [self.contentView addSubview:_head];
    
    self.author = [[UILabel alloc]initWithFrame:CGRectMake(self.head.frame.origin.x+48+5, 5,cellWidth-(self.head.frame.origin.x+48+5), 20)];
    _author.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_author];
    
    
    self.title = [[UILabel alloc]initWithFrame:CGRectMake(self.head.frame.origin.x+48+5, self.author.frame.origin.y+25, cellWidth-(self.head.frame.origin.x+48+15), 0)];
    _title.font = [UIFont systemFontOfSize:16.0];
    _title.numberOfLines = 0;
    [self.contentView addSubview:_title];
    
    /*
    self.time = [[UILabel alloc]initWithFrame:CGRectMake(cellWidth/3*2-5, 5, cellWidth/3, 30) ];
    _time.font = [UIFont systemFontOfSize:12.0];
    _time.textAlignment = NSTextAlignmentRight;
    _time.textColor = [UIColor grayColor];
    [self.contentView addSubview:_time];
    
    self.replyBody = [[UILabel alloc]initWithFrame:CGRectMake(self.head.frame.origin.x+48+5, 35, cellWidth/2, 15)];
    _replyBody.textColor = [UIColor grayColor];
    _replyBody.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_replyBody];
    */
    self.replyTime = [[UILabel alloc]initWithFrame:CGRectMake(0, 0 , 0, 0) ];
    _replyTime.font = [UIFont systemFontOfSize:12.0];
    //_replyTime.textAlignment = NSTextAlignmentLeft;
    _replyTime.textColor = [UIColor grayColor];
    [self.contentView addSubview:_replyTime];
    
    self.reply = [[UILabel alloc]initWithFrame:CGRectMake(0, 0 , 0, 0) ];
    _reply.text = @"回复";
    _reply.font = [UIFont systemFontOfSize:12.0];
    _reply.textColor = [UIColor grayColor];
    [self.contentView addSubview:_reply];
    
    self.replycount = [[UILabel alloc]initWithFrame:CGRectMake(0, 0 , 0, 0) ];
    _replycount.font = [UIFont systemFontOfSize:12.0];
    _replycount.textColor = [UIColor grayColor];
    _replycount.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_replycount];
    return self;
}

+ (NSString *)ID
{
    return @"forumTopic";
}

+ (CGFloat) cellHeight
{
    return 60;
}

+ (NSString *)timechange:(NSString *)time
{
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"MM-dd HH:mm:ss"];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
    return [formatter stringFromDate:confromTimesp];
}
-(CGFloat)inittitleText:(NSString *)string{
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    CGRect tmpRect = _title.frame;
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString: string];
    _title.attributedText =attrStr;
    CGRect bodySize =[_title.text boundingRectWithSize:CGSizeMake(cellWidth-(self.head.frame.origin.x+48+15),8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_title.font,NSFontAttributeName, nil] context:nil];
    tmpRect.size.height =bodySize.size.height;
    _title.frame =tmpRect;
    _replyTime.frame = CGRectMake(self.head.frame.origin.x+48+5, tmpRect.size.height+35, cellWidth/2, 20);
    _reply.frame = CGRectMake(cellWidth-40, tmpRect.size.height+35, 30, 20);
    _replycount.frame = CGRectMake(cellWidth-70, tmpRect.size.height+35, 30, 20);
    return tmpRect.size.height;
}


@end
