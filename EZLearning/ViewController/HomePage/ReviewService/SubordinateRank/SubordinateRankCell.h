//
//  SubordinateScoreCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SubordinateRankModel;

@protocol SubordinateRankDelegate <NSObject>

- (void)check:(UIButton *)btn;
- (void)chat:(UIButton *)btn;

@end

@interface SubordinateRankCell : UITableViewCell

@property (nonatomic, strong) id<SubordinateRankDelegate>delegate;
@property (nonatomic, strong)SubordinateRankModel *subordinate;

@end
