//
//  SubordinateScoreViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateRankViewController.h"
#import "SubordinateRankCell.h"
#import "SubordinateRankModel.h"
#import "ChatViewController.h"
#import "SubordinateScoreViewController.h"

@interface SubordinateRankViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_rankSource;
    LoadingView *_loadingView ;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SubordinateRankViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSubordinateRank:)  SoapType:ST_GET_SUBORDINATE_RANK];
    [SoapManager getSubordinateRank];
    [self initHeadView];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
}

- (void)initHeadView{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 64, MAIN_SCREEN_WIDTH, 44)];
    view.backgroundColor = [UIColor whiteColor];
    
    const CGFloat labelHeight = 21;
    const CGFloat labelOriginY = (44 - labelHeight)*0.5;
    
    const CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    const CGFloat labelWidth = ( cellWidth - 22 ) / 5;
    
    UIFont *font = [UIFont systemFontOfSize:16];
    
    UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(5, labelOriginY, labelWidth, labelHeight)];
    name.font = font;
    name.text = @"姓名";
    [view addSubview:name];
    
    UILabel *account = [[UILabel alloc] initWithFrame:CGRectMake(labelWidth + 3, labelOriginY, labelWidth, labelHeight)];
    account.text = @"账号";
    account.font = font;
    [view addSubview:account];
    
    UILabel * star = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(account.frame)+3, labelOriginY, labelWidth, labelHeight)];
    star.font = font;
    star.text = @"星级";
    [view addSubview:star];
    
    UILabel * passNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(star.frame)+3, labelOriginY, labelWidth, labelHeight)];
    passNum.font = font;
    passNum.text = @"通过数";
    [view addSubview:passNum];
    
    UILabel * credit = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(passNum.frame)+3, labelOriginY, labelWidth, labelHeight)];
    credit.font = font;
    credit.text = @"积分";
    [view addSubview:credit];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(20, 43, cellWidth, 1)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [view addSubview:line];
    
    [self.view addSubview:view];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)recvSubordinateRank:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _rankSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_SUBORDINATE_RANK];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _rankSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubordinateRankCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rank"];
    if (cell == nil)
    {
        cell = [[SubordinateRankCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"rank"];
    }
    cell.subordinate = _rankSource[indexPath.row];
  //  cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SubordinateRankModel *subordinate = _rankSource[indexPath.row];
    SubordinateScoreViewController *score = [[SubordinateScoreViewController alloc] initWithNibName:@"SubordinateScoreViewController" bundle:nil];
    score.name = subordinate.lastName;
    score.userId = subordinate.userId;
    [self.navigationController pushViewController:score animated:YES];
}


/*
-(void)check:(UIButton *)btn
{
    SubordinateRankModel *subordinate = _rankSource[btn.tag];;
    SubordinateScoreViewController *score = [[SubordinateScoreViewController alloc] initWithNibName:@"SubordinateScoreViewController" bundle:nil];
    score.name = subordinate.lastName;
    score.userId = subordinate.userId;
    [self.navigationController pushViewController:score animated:YES];
}

-(void)chat:(UIButton *)btn
{
    ChatViewController *chatView = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    SubordinateRankModel *subordinate = _rankSource[btn.tag];
    chatView.name = subordinate.lastName;
    chatView.userId = subordinate.userId;
    [self.navigationController pushViewController:chatView animated:YES];
}
 */

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
