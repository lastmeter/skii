//
//  SubordinateScoreCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateRankCell.h"
#import "SubordinateRankModel.h"

#define cellHeight 44

@interface SubordinateRankCell()
{
    UILabel *_name, *_account, *_passedNum, *_star, *_credit;
   // UIButton *_check, *_chat;
}
@end

@implementation SubordinateRankCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        const CGFloat labelHeight = 21;
        const CGFloat labelOriginY = (cellHeight - labelHeight)*0.5;
 
        const CGFloat cellWidth = MAIN_SCREEN_WIDTH;
        const CGFloat labelWidth = (cellWidth - 22) / 5;
//        CGFloat btnWidth = 30;
//        CGFloat btnHeight = 30;
//        CGFloat btnY = (cellHeight - btnHeight)*0.5;
//        
        UIFont *font = [UIFont systemFontOfSize:15];
        

//        UIButton *chat = [[UIButton alloc] initWithFrame:CGRectMake(cellWidth-10-btnWidth, btnY, btnWidth, btnHeight)];
//        [chat setImage:[UIImage imageNamed:@"comment_normal"] forState:UIControlStateNormal];
//        [chat setImage:[UIImage imageNamed:@"comment_pressed"] forState:UIControlStateSelected];
//        [chat addTarget:self action:@selector(chat:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:chat];
//        _chat = chat;
//        
//        UIButton *check = [[UIButton alloc] initWithFrame:CGRectMake(chat.frame.origin.x-10-btnWidth, btnY, btnWidth, btnHeight)];
//        [check setImage:[UIImage imageNamed:@"check_score_normal"] forState:UIControlStateNormal];
//        [check setImage:[UIImage imageNamed:@"check_score_pressed"] forState:UIControlStateSelected];
//        [check addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:check];
//        _check = check;
        
        _name = [[UILabel alloc] initWithFrame:CGRectMake(5, labelOriginY, labelWidth, labelHeight)];
        _name.font = font;
        [self.contentView addSubview:_name];
        
        _account = [[UILabel alloc] initWithFrame:CGRectMake(labelWidth + 3, labelOriginY, labelWidth, labelHeight)];
        _account.font = font;
        [self.contentView addSubview:_account];
        
        _star = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_account.frame) + 3, labelOriginY, labelWidth, labelHeight)];
        _star.font = font;
        [self.contentView addSubview:_star];
        
        _passedNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_star.frame) + 3, labelOriginY, labelWidth, labelHeight)];
        _passedNum.font = font;
        [self.contentView addSubview:_passedNum];
        
        _credit = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_passedNum.frame) + 3, labelOriginY, labelWidth, labelHeight)];
        _credit.font = font;
        [self.contentView addSubview:_credit];
        
//        UILabel *passNum = [[UILabel alloc] initWithFrame:CGRectMake(check.frame.origin.x - 30 - 10, labelOriginY, 20, labelHeight)];
//        passNum.font = font;
//        passNum.textColor = [UIColor blackColor];
//        passNum.textAlignment = NSTextAlignmentCenter;
//        [self.contentView addSubview:passNum];
//        _passedNum = passNum;
//        
//        UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(10, labelOriginY, 100, labelHeight)];
//        name.font = font;
//        name.textColor = [UIColor blackColor];
//        [self.contentView addSubview:name];
//        
//        _star = [[UILabel alloc] initWithFrame:CGRectMake(name.frame.size.width+10+20, labelOriginY, 100, labelHeight)];
//        _star.font = font;
//        _star.textColor = [UIColor blackColor];
//        [self.contentView addSubview:_star];
//
//        
//        UILabel *account = [[UILabel alloc] initWithFrame:CGRectMake(_star.frame.size.width+10+20, labelOriginY, 100, labelHeight)];
//        account.font = font;
//        account.textColor = [UIColor blackColor];
//        [self.contentView addSubview:account];
//        _account = account;
//        _name = name;
//        
       // self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

-(void)setSubordinate:(SubordinateRankModel *)subordinate
{
    _name.text = subordinate.lastName;
    _account.text = subordinate.account;
    _passedNum.text = [NSString stringWithFormat:@"%d", subordinate.passNum];
//    _chat.tag = subordinate.btnTag;
//    _check.tag = subordinate.btnTag;
    _star.text = subordinate.star;
    _credit.text = [subordinate.credit stringValue];
}

- (void)check:(UIButton *)btn
{
    [_delegate check:btn];
}

- (void)chat:(UIButton *)btn
{
    [_delegate chat:btn];
}

@end
