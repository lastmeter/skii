//
//  SubordinateListViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateListViewController.h"
#import "ChatViewController.h"
#import "ContactModel.h"
#import "EzlUser.h"
#import "MomentsService.h"
@interface SubordinateListViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    LoadingView *_loadingView ;
    NSArray *_contactSource;
    __weak EzlUser *_currentUser;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SubordinateListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self makeExtraCellLineHidden];
    
    _currentUser = [EzlUser currentUser];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvContacts:) SoapType:ST_GET_CONTACTS];
    [SoapManager getContacts];
    
    _loadingView =[[LoadingView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
}

- (void)recvContacts:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _contactSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
            [_loadingView removeFromSuperview];
            _loadingView = nil;
        });
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _contactSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactModel *contact  = _contactSource[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contact"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contact"];
    }
    
    UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:99];
    if (!imageView) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 40, 13, 30, 30)];
        imageView.image = [UIImage imageNamed:@"chat_unread"];
        imageView.tag = 99;
        [cell.contentView addSubview:imageView];
    }
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:98];
    if (!label) {
        label = [[UILabel alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 40, 13, 30, 30)];
        label.textColor = [UIColor whiteColor];
        label.tag = 98;
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
    }
    
    imageView.hidden = YES;
    label.hidden = YES;
    
    if (_currentUser.straffInfo[contact.userId]) {
        imageView.hidden = NO;
        label.hidden = NO;
        label.text = [NSString stringWithFormat:@"%@",_currentUser.straffInfo[contact.userId]];
    }

    UIImageView *headImageView = (UIImageView *)[cell.contentView viewWithTag:97];
    if (!headImageView) {
        headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 45, 45)];
        headImageView.tag = 97;
        [cell.contentView addSubview:headImageView];
    }
    
    UILabel *nameLabel = (UILabel *)[cell.contentView viewWithTag:96];
    if (!nameLabel) {
        nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(headImageView.frame) + 10, 0, 300, 55)];
        nameLabel.textColor = [UIColor blackColor];
        nameLabel.tag = 96;
        [cell.contentView addSubview:nameLabel];
    }
    
    
    __weak UIImageView *weakView = headImageView;
    headImageView.image = [UIImage imageNamed:@"article_photo_pressed"];
    [MomentsService getHeadPortraitByUserId:contact.userId handler:^(HandleResult result, NSData *data) {
        if (result == HandleResultSuccess) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
               weakView.image = image;
            }
        }
    }];
    
    nameLabel.text = contact.lastName;
    return cell;
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
    
}
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactModel *contact = _contactSource[indexPath.row];
    ChatViewController *chat = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    chat.name = contact.lastName;
    chat.userId = contact.userId;
    [self deselect];

    [self.navigationController pushViewController: chat animated:YES];
    
    NSInteger unread = [_currentUser.straffInfo[contact.userId] integerValue];
    if (unread) {
        [_currentUser.straffInfo removeObjectForKey:contact.userId];
        [_tableView reloadData];
        unread  =  _currentUser.numberOfUnreadStaff;
        self.tabBarItem.badgeValue = unread ? [@(unread) stringValue] : nil;
        [UIApplication sharedApplication].applicationIconBadgeNumber = _currentUser.numberOfAllUnreadMessage;
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}
@end
