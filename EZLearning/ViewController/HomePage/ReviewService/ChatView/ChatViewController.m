//
//  ChatViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChatViewController.h"
#import "MessageModel.h"
#import "MessageCell.h"
#import "CellFrameModel.h"
#import "ReviewService.h"
#import "LrhAlertView.h"
#import "EzlUser.h"

@interface ChatViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *_cellFrameDatas;
    LoadingView *_loadingView ;
    UIButton *_sendBtn;
    UIView *_sendBar;
    UITextView *_textView;
    IBOutlet NSLayoutConstraint *bottomConstraint;
    NSDate *_refreshDate;
    
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation ChatViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.view.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    _tableView.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    
    [self addSendBarView];
    [self addObservers];
    _cellFrameDatas =[NSMutableArray array];
    
    _loadingView = [[LoadingView alloc] init];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteMessage:) name:@"deleteMsg" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
     self.nameLabel.text = self.name;
    [SoapManager getChatByUserId:self.userId AfterDate:0];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
  
}

- (void)getChat:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        NSArray * array = [SoapNotiCenter getInfoFromNotification:noti];
        [_cellFrameDatas removeAllObjects];
        [array enumerateObjectsUsingBlock:^(MessageModel *obj, NSUInteger idx, BOOL *stop) {
            CellFrameModel *cellFrame = [[CellFrameModel alloc] init];
            cellFrame.message = obj;
           [_cellFrameDatas addObject:cellFrame];
        }];
              _refreshDate = [NSDate date];
        dispatch_async(dispatch_get_main_queue(),^{
            if (_cellFrameDatas.count > 0) {
                [_tableView reloadData];
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_cellFrameDatas.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        });
    }
}

- (void)addObservers
{
    [SoapNotiCenter addSoapObserver:self selector:@selector(getChat:) SoapType:ST_GET_CHAT_WITH_USER];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSendResult:) SoapType:ST_SEND_CHAT];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void) keyboardWillShow:(NSNotification *) note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    CGRect keyFrame = [userInfo[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    CGFloat moveY  = keyFrame.size.height;
    
    
    CGRect sendBarFrame = _sendBar.frame;
    sendBarFrame.origin.y = sendBarFrame.origin.y - moveY;
    

     bottomConstraint.constant = bottomConstraint.constant + moveY;
    [self.view needsUpdateConstraints];
    [UIView animateWithDuration:duration animations:^{
        _sendBar.frame = sendBarFrame;
        [self.view layoutIfNeeded];
        if (_cellFrameDatas.count > 0) {
             [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_cellFrameDatas.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];

    
    CGRect sendBarFrame = _sendBar.frame;
    sendBarFrame.origin.y = MAIN_SCREEN_HEIGHT + 20 - sendBarFrame.size.height;

    bottomConstraint.constant = _sendBar.frame.size.height;
    [self.view needsUpdateConstraints];
    [UIView animateWithDuration:duration animations:^{
        _sendBar.frame = sendBarFrame;
        [self.view layoutIfNeeded];

    }];
}


- (void)addSendBarView
{
    CGFloat viewWidth = MAIN_SCREEN_WIDTH;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, MAIN_SCREEN_HEIGHT-32, viewWidth, 57)];
    [view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    _sendBar = view;
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(15, 10, 0.78*viewWidth, 37)];
    textView.font = [UIFont systemFontOfSize:17];
    textView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    textView.layer.borderColor = [UIColor grayColor].CGColor;
    textView.layer.borderWidth = 0.5;
    textView.layer.cornerRadius = 7.0;
    [view addSubview: textView];
    _textView = textView;
    
    
    [_textView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChanged:) name:UITextViewTextDidChangeNotification object:_textView];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(textView.frame.origin.x+textView.frame.size.width+5, 10, 46, 34)];
    btn.titleLabel.font = [UIFont systemFontOfSize:18];
    [btn setTitle:@"发送" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [btn setTitleShadowColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(send_click) forControlEvents:UIControlEventTouchUpInside];
    btn.enabled = NO;
    [view addSubview:btn];
    _sendBtn = btn;
    
    [self.view addSubview: view];
}

- (void)send_click
{
    if (_textView.text.length == 0 &&[_textView.text isEqualToString:@""])
    {
        _sendBtn.enabled = NO;
        return;
    }
    //1.获得时间
    NSDate *senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
        
    //2.创建一个MessageModel类
    MessageModel *message = [[MessageModel alloc] init];
    message.text = _textView.text;
    message.time = locationString;
    message.type = 0;
    message.fromUserId = [EzlUser currentUser].userId;
        
    //3.创建一个CellFrameModel类

        
    //4.添加进去，并且刷新数据
   // [_cellFrameDatas addObject:cellFrame];
   // [_tableView reloadData];
        
    //5.自动滚到最后一行
//    NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_cellFrameDatas.count - 1 inSection:0];
//    [_tableView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    NSTimeInterval time = [senddate timeIntervalSince1970];
    long timeStamp = [[NSNumber numberWithDouble:time] longValue];
    //[SoapManager sendChatContent:_textView.text andDate:timeStamp toUserId:_userId];
   
    WEAKSELF
    
    [ReviewService sendChatWithContent:_textView.text to:_userId date:timeStamp handler:^(HandleResult result, NSString *dataStr) {
        if (!weakSelf) return;
        
        if (result == HandleResultSuccess && dataStr.length > 0) {
            message.messageId = dataStr;
            CellFrameModel *cellFrame = [[CellFrameModel alloc] init];
            CellFrameModel *lastCellFrame = [_cellFrameDatas lastObject];
            message.showTime = ![lastCellFrame.message.time isEqualToString:message.time];
            cellFrame.message = message;
            [_cellFrameDatas addObject:cellFrame];
            [_tableView reloadData];
            
            NSIndexPath *lastPath = [NSIndexPath indexPathForRow:_cellFrameDatas.count - 1 inSection:0];
            [_tableView scrollToRowAtIndexPath:lastPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
        else{
            [LrhAlertView showMessage:@"信息发送失败"];
        }
    }];
    
    _textView.text = @"";
    _sendBtn.enabled = NO;
    [_textView resignFirstResponder];
    
    

}


- (void)textDidChanged:(NSNotification *)notif
{
    _sendBtn.enabled = _textView.text.length > 0;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object != _textView && ![keyPath isEqualToString:@"contentSize"]) {
        return;
    }
    CGSize contentSize = _textView.contentSize;
    NSInteger line = fabs(contentSize.height / _textView.font.lineHeight);
    if (line > 4)
    {
        return;
    }

    CGRect barFrame = _sendBar.frame;
    CGFloat barHeight = contentSize.height+20;
    CGFloat moveY = barHeight - barFrame.size.height;
    barFrame.origin.y = _sendBar.frame.origin.y-moveY;
    barFrame.size.height = barHeight;
    _sendBar.frame = barFrame;
    
    CGRect btnFrame = _sendBtn.frame;
    btnFrame.origin.y = _sendBtn.frame.origin.y+moveY;
    _sendBtn.frame = btnFrame;
    
    bottomConstraint.constant = MAIN_SCREEN_HEIGHT - barFrame.origin.y + 10;
    [self.view needsUpdateConstraints];
    [self.view layoutIfNeeded];
    if (_cellFrameDatas.count > 0) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_cellFrameDatas.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
}




- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cellFrameDatas.count;
}

- (MessageCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.cellFrame = _cellFrameDatas[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellFrameModel *cellFrame = _cellFrameDatas[indexPath.row];
    return cellFrame.cellHeght;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}

- (void)recvSendResult:(NSNotification *)noti
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopWaitting];
        NSDictionary *dic = noti.userInfo;
        NSString *result = dic[@"info"];
        if ([result boolValue]) {
            [SoapManager getChatByUserId:self.userId AfterDate:0];
        }
        else{
            [LrhAlertView showMessage:@"发送失败!"];
        }
    });

}


- (void)deleteMessage:(NSNotification *)noti{
    CellFrameModel *cellFrame = noti.object;
    if (cellFrame && [_cellFrameDatas containsObject:cellFrame]) {
        
        [ReviewService deleteStaffMessageByID:cellFrame.message.messageId handler:^(HandleResult result, BOOL dataResult) {
            if (!dataResult) {
                [LrhAlertView showMessage:@"删除失败!"];
            }
            else{
                [_cellFrameDatas removeObject:cellFrame];
                [_tableView reloadData];
            }
        }];
    }
}

-(void)dealloc
{
    [_textView removeObserver:self forKeyPath:@"contentSize"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
