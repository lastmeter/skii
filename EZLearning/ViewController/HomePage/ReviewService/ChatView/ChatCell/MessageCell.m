//
//  MessageCell.m
//  QQ聊天布局
//
//  Created by TianGe-ios on 14-8-19.
//  Copyright (c) 2014年 TianGe-ios. All rights reserved.
//

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

#import "MessageCell.h"
#import "CellFrameModel.h"
#import "MessageModel.h"
#import "UIImage+ResizeImage.h"
#import "MomentsService.h"

@interface MessageCell()
{
    UILabel *_timeLabel;
    UIImageView *_iconView;
    UIButton *_textView;
}
@end

@implementation MessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = nil;
        self.backgroundColor = [UIColor clearColor];
        
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        _timeLabel.textColor = [UIColor grayColor];
        _timeLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_timeLabel];
        
        _iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
        
        _textView = [UIButton buttonWithType:UIButtonTypeCustom];
        _textView.titleLabel.numberOfLines = 0;
        _textView.titleLabel.font = [UIFont systemFontOfSize:MessageModelTextFont];
        _textView.contentEdgeInsets = UIEdgeInsetsMake(textPadding, textPadding, textPadding, textPadding);
        [self.contentView addSubview:_textView];
        
        
        UILongPressGestureRecognizer *longPressGrstureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [self addGestureRecognizer:longPressGrstureRecognizer];
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (BOOL)canBecomeFirstResponder{
    return YES;
}

- (void)longPress:(UILongPressGestureRecognizer *)longPressGrstureRecognizer{
    [self becomeFirstResponder];
    if (longPressGrstureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIMenuItem *flag1 = [[UIMenuItem alloc] initWithTitle:@"撤销" action:@selector(deleteText:)];
        UIMenuItem *flag2 = [[UIMenuItem alloc] initWithTitle:@"拷贝" action:@selector(copyText:)];
        UIMenuController *menu = [UIMenuController sharedMenuController];
        [menu setMenuItems:[NSArray arrayWithObjects:flag1, flag2, nil]];
        [menu setTargetRect:_textView.frame inView:self];
        [menu setMenuVisible:YES animated:YES];
    
    }
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    if(action == @selector(copyText:)){
        return YES;
    }
    if (action == @selector(deleteText:)) {
        return YES;
    }
    return  NO;
}

-(void)copyText:(id)sender{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = _textView.titleLabel.text;
}

- (void)deleteText:(id)sender{
    [ [NSNotificationCenter defaultCenter] postNotificationName:@"deleteMsg" object:_cellFrame userInfo:nil];
}



- (void)setCellFrame:(CellFrameModel *)cellFrame
{
    _cellFrame = cellFrame;
    MessageModel *message = cellFrame.message;
    
    _timeLabel.frame = cellFrame.timeFrame;
    _timeLabel.text = message.time;
    
    _iconView.image = [UIImage imageNamed:@"article_photo_pressed"];
    _iconView.frame = cellFrame.iconFrame;
    _textView.frame = cellFrame.textFrame;
   
        [MomentsService getHeadPortraitByUserId:message.fromUserId handler:^(HandleResult result, NSData *data) {
            if (result == HandleResultSuccess) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    _iconView.image = image;
                }
                
            }
        }];

    
    NSString *textBg = message.type ? @"chat_recive_nor" : @"chat_send_nor";
    UIColor *textColor = message.type ? [UIColor blackColor] : [UIColor whiteColor];
    [_textView setTitleColor:textColor forState:UIControlStateNormal];
    [_textView setBackgroundImage:[UIImage resizeImage:textBg] forState:UIControlStateNormal];
    [_textView setTitle:message.text forState:UIControlStateNormal];
}

@end
