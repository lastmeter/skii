//
//  ChatViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "NEzlViewController.h"

@interface ChatViewController : NEzlViewController

@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *userId;
@property (nonatomic, strong)UIImage *chatImage;

@end
