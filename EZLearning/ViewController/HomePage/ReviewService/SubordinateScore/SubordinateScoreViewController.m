//
//  SubordinateScoreViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateScoreViewController.h"
#import "SubordinateScoreCell.h"
#import "SubordinateScoreModel.h"
#import "ChatViewController.h"

@interface SubordinateScoreViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *_scoreSource;
    LoadingView *_loadingView;
}
@property (strong, nonatomic) IBOutlet UIButton *nameBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation SubordinateScoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _nameLabel.text = _name;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self makeExtraCellLineHidden];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSubordinateScore:) SoapType:ST_GET_SUBORDINATE_SCORE];
    [SoapManager getSubordinateScoreByUserId:_userId];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
}

- (void)recvSubordinateScore:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _scoreSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
        });
    }
    [_loadingView removeFromSuperview];
    _loadingView = nil;
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_SUBORDINATE_SCORE];
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (IBAction)chat:(id)sender
{
    ChatViewController *chat = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    chat.userId = self.userId;
    chat.name = self.name;
    [self.navigationController pushViewController:chat animated:YES];
}
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _scoreSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubordinateScoreModel *score = _scoreSource[indexPath.row];
    SubordinateScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"score"];
    if (cell == nil) {
        cell = [[SubordinateScoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"score"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.subordinate = score;
    return cell;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
