//
//  SubordinateScoreCell.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateScoreCell.h"
#import "SubordinateScoreModel.h"

@interface SubordinateScoreCell()
{
    UILabel *_titleLabel, *_scoreLabel;
    UIImageView *_pass;
}
@end

@implementation SubordinateScoreCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 180, 24)];
        title.font = [UIFont systemFontOfSize:15];
        title.lineBreakMode = NSLineBreakByWordWrapping;
        title.numberOfLines = 0;
        title.textColor = [UIColor blackColor];
        [self addSubview:title];
        _titleLabel = title;
        
        UILabel *score = [[UILabel alloc] initWithFrame:CGRectMake(title.frame.origin.x+title.frame.size.width+20, 5, 50, 21)];
        score.textColor = [UIColor blackColor];
        score.font = [UIFont systemFontOfSize:15];
        [self addSubview: score];
        _scoreLabel = score;
        
        UIImageView *passView = [[UIImageView alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 10 -24, 5, 24, 24)];
        [self addSubview:passView];
        _pass = passView;
    }
    return self;
}

- (void)setSubordinate:(SubordinateScoreModel *)subordinate
{
    _pass.image = [UIImage imageNamed:subordinate.isPass?@"score_pass":@"score_not_pass"];
    _titleLabel.text = subordinate.title;
    _scoreLabel.text = subordinate.score;
}
@end
