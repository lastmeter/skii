//
//  SubordinateScoreCell.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  SubordinateScoreModel;

@interface SubordinateScoreCell : UITableViewCell

@property (nonatomic, strong)SubordinateScoreModel *subordinate;

@end
