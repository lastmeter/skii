//
//  SubordinateScoreViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "EzlViewController.h"

@interface SubordinateScoreViewController : EzlViewController

@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *userId;

@end
