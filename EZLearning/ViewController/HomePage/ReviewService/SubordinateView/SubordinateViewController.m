//
//  SubordinateViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/24.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SubordinateViewController.h"
#import "ContactModel.h"
#import "ChatViewController.h"

@interface SubordinateViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_contactsSource;
    LoadingView *_loadingView;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation SubordinateViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self makeExtraCellLineHidden];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvContactList:) SoapType:ST_GET_CONTACTS];
    [SoapManager getContacts];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
}

- (void)recvContactList:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _contactsSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
            
        });
    }
    [_loadingView removeFromSuperview];
    _loadingView = nil;
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_CONTACTS];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _contactsSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contact"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contact"];
    }
    ContactModel *contact  = _contactsSource[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:contact.isBoss?@"boss":@"subordinate"];
    cell.textLabel.text = contact.lastName;
    return cell;
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactModel *contact = _contactsSource[indexPath.row];
    
    ChatViewController *chat = [[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    chat.name = contact.lastName;
    chat.userId = contact.userId;
    [self deselect];
    [self.navigationController pushViewController: chat animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}


@end
