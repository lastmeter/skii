//
//  ReviewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ReviewController.h"
#import "EZLTabBar.h"


@interface ReviewController ()<EZLTabBarDelegate>

@property(nonatomic, weak)UIButton *selectedBtn;
@end

@implementation ReviewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)setTabViewControllers:(NSArray *)viewControllers
{
    self.viewControllers = viewControllers;
    
    CGRect rect = self.tabBar.bounds;
    EZLTabBar *myTabBar = [[EZLTabBar alloc] init];
    myTabBar.delegate = self;
    myTabBar.frame = rect;
    [self.tabBar addSubview:myTabBar];
    
    for (int i = 0; i < self.viewControllers.count; i++)
    {
        NSString *imageName = [NSString stringWithFormat:@"ReviewTab%d",i+1];
        NSString *imageNameSel = [NSString stringWithFormat:@"ReviewTabSel%d",i+1];
        
        UIImage *image = [UIImage imageNamed:imageName];
        UIImage *imagesel = [UIImage imageNamed:imageNameSel];
        
        [myTabBar setItemWithImage:image selectedImage:imagesel];
    }
}

- (void)tabBar:(EZLTabBar *)tabBar selectedFrom:(NSInteger)from to:(NSInteger)to
{
    self.selectedIndex = to;
}


@end
