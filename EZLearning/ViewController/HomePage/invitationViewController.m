//
//  invitationViewController.m
//  EZLearning
//
//  Created by zwf on 15/6/17.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "invitationViewController.h"
#import "invitationCellone.h"
#import "invitationCelltwo.h"
#import "invitationCellThree.h"
#import "UIImageView+WebCache.h"
#import "UDManager.h"
#import "SDWebImageDownloader.h"
#import "ParseHtml.h"
#import "enLargeHtmlWeb.h"
#import "HTMLdata.h"

@interface invitationViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate>
{
    LoadingView *_loadingView;
    enLargeHtmlWeb *_largeWeb;
    NSArray *_MsgandReplies;
    NSMutableArray *_misorderMsgandReplies;
    NSString *_boolpost;
    UIImage *orignImg;
    NSMutableData *receivedData;
    NSInteger cellRefreshCount;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UITextView *input;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@end

@implementation invitationViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self setCookie];
    cellRefreshCount = 0;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self makeExtraCellLineHidden:_tableView];
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
    [self initView];
    [self registerForKeyboardNotifications];
    [SoapNotiCenter addSoapObserver:self selector:@selector(getMsgandreplies:) SoapType:ST_GET_MSG_AND_REPLIES];
}

-(void)initView
{
    [_sendButton addTarget:self action:@selector(sendreply:) forControlEvents:UIControlEventTouchUpInside];
    [_backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    _input.layer.borderColor = [[UIColor colorWithWhite:0.8 alpha:1.0]CGColor];
    _input.layer.borderWidth = 1.0;
    _input.layer.cornerRadius = 5;
    _input.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _input.delegate = self;
}

-(void)addMsgofTopic:(NSNotification *)noti{
    
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
        if ([_boolpost intValue]==-1) {
        }
        else
        {
            [SoapNotiCenter addSoapObserver:self selector:@selector(getMsgandreplies:) SoapType:ST_GET_MSG_AND_REPLIES];
            [SoapManager getmsgandReplies:[[NSUserDefaults standardUserDefaults]objectForKey:@"topicId"] MessageId:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
        }
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_ADD_MSG_TO_TOPIC];
}

-(IBAction)sendreply:(id)sender
{
    if(_MsgandReplies.count>0){
        [SoapNotiCenter addSoapObserver:self selector:@selector(addMsgofTopic:) SoapType:ST_ADD_MSG_TO_TOPIC];
        [SoapManager getaddMsgtoTopic:[[NSUserDefaults standardUserDefaults]objectForKey:@"topicId"] Msgtitle:_MsgandReplies[0][@"title"] Msgbody:_input.text MsgreplyTold:[[NSUserDefaults standardUserDefaults]objectForKey:@"id"]];
    }else
    {
        
    }
    _input.text = @"";
    [_input resignFirstResponder];
}



-(void)getMsgandreplies:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _misorderMsgandReplies = [SoapNotiCenter getInfoFromNotification:noti];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"time" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
        _MsgandReplies =[_misorderMsgandReplies sortedArrayUsingDescriptors:sortDescriptors];
        NSLog(@"_MsgandReplies = %@",_MsgandReplies);
        MAIN(^{
            [_loadingView removeFromSuperview];
            _loadingView = nil;
            _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            [_tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_MSG_AND_REPLIES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        invitationCellone *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCellone ID]];
        if (cell == nil)
        {
            cell = [[invitationCellone alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCellone ID]];
        }
       CGFloat height = [cell inittitleText:_MsgandReplies[0][@"title"]];
        return ([invitationCellone cellHeight]/2+height+10);
    }
    else if (indexPath.section==1) {
        invitationCelltwo *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCelltwo ID]];
        NSInteger countNum;
        countNum = 0;
        if (cell == nil)
        {
            cell = [[invitationCelltwo alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCelltwo ID]];
        }
        NSData *htmlData = [_MsgandReplies[countNum][@"body"] dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *imgArray = [ParseHtml parseData:htmlData];
        if (imgArray.count>0)
        {
            NSString *textString = [ParseHtml getTextfromHtml:_MsgandReplies[countNum][@"body"]];
            [cell initbodyView:textString];
            CGFloat height = [cell initbodyView:textString];;
            return (height+300);
        }else
        {
            CGFloat height = [cell initbodyText:_MsgandReplies[countNum][@"body"]];
            return (80.0+height);
        }
        
    }else{
        invitationCellThree *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCellThree ID]];
        NSInteger replyNum;
        replyNum = indexPath.row+1;

        if (cell == nil)
        {
            cell = [[invitationCellThree alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCellThree ID]];
        }
        CGFloat height = [cell initbodyText:_MsgandReplies[replyNum][@"body"]];
        return (100+height);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_MsgandReplies.count==1) {
        if (section==0||section==1) {
            return 1;
        }
        else return 0;
    }else if(_MsgandReplies.count>1)
    {
        if (section==0||section==1) {
            return 1;
        }else return _MsgandReplies.count-1;
    }
    else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger section =indexPath.section;
    
    if (section==0){
        
        invitationCellone *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCellone ID]];
        if (cell == nil)
        {
            cell = [[invitationCellone alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCellone ID]];
        }
        
        cell.author.text = _MsgandReplies[0][@"author"];
        NSString *timestring =[NSString stringWithFormat:@"%@",_MsgandReplies[0][@"time"]];
        cell.time.text =[invitationCellone timechange:[timestring substringToIndex:10]];
        [cell inittitleText:_MsgandReplies[0][@"title"]];
        cell.title.text = _MsgandReplies[0][@"title"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone; 
        return cell;
    }else if(section==1)
    {
        invitationCelltwo *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCelltwo ID]];
        if (cell == nil)
        {
            cell = [[invitationCelltwo alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCelltwo ID]];
        }
    
        NSData *htmlData = [_MsgandReplies[0][@"body"] dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *imgArray = [ParseHtml parseData:htmlData];
        if (imgArray.count>0) {
            cell.body.hidden = YES;
            cell.bodyImageView.hidden = NO;
            cell.bodyviewLabel.hidden = NO;
            NSString *textString = [ParseHtml getTextfromHtml:_MsgandReplies[0][@"body"]];
            [cell initbodyView:textString];
            cell.bodyviewLabel.text = textString;
            NSString *imgUrl = [HTMLdata getImageUrl:_MsgandReplies[0][@"body"]];
            cell.bodyImageView.image = orignImg;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
            [cell.bodyImageView addGestureRecognizer:singleTap];
            if (cellRefreshCount==0) {
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:imgUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
                NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
                if (connection) {
                    receivedData = [[NSMutableData alloc] init];
                }
            }
            
        }else
        {
            [cell initbodyText:_MsgandReplies[0][@"body"]];
            cell.body.hidden = NO;
            cell.bodyviewLabel.hidden = YES;
            cell.bodyImageView.hidden = YES;
            cell.body.text = _MsgandReplies[0][@"body"];
        }
        
        NSString *praiseNum = [NSString stringWithFormat:@"%@",_MsgandReplies[0][@"praise"]];
        NSInteger praiseInt =[praiseNum intValue];
        if (praiseInt!=0) {
            NSMutableString *praiseString = [[NSMutableString alloc]initWithString:@"赞("];
            [praiseString appendString:praiseNum];
            [praiseString appendString:@")"];
            cell.praiseLabel.textColor = [UIColor blueColor];
            cell.praiseLabel.text = praiseString;
        }
        cell.messageId.text = [NSString stringWithFormat:@"%@",_MsgandReplies[0][@"id"]];
        cell.me.text = [NSString stringWithFormat:@"%@",_MsgandReplies[0][@"me"]];
        cell.praiseNum.text = [NSString stringWithFormat:@"%@",_MsgandReplies[0][@"praise"]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        invitationCellThree *cell = [tableView dequeueReusableCellWithIdentifier:[invitationCellThree ID]];
        if (cell == nil)
        {
            cell = [[invitationCellThree alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[invitationCellThree ID]];
        }
        NSInteger replyNum;
        replyNum = indexPath.row+1;
        cell.author.text = _MsgandReplies[replyNum][@"author"];
        NSString *timestring =[NSString stringWithFormat:@"%@",_MsgandReplies[replyNum][@"time"]];
        cell.time.text =[invitationCellone timechange:[timestring substringToIndex:10]];
        [cell initbodyText:_MsgandReplies[replyNum][@"body"]];
        cell.body.text = _MsgandReplies[replyNum][@"body"];
        NSString *praiseNum = [NSString stringWithFormat:@"%@",_MsgandReplies[replyNum][@"praise"]];
        NSInteger praiseInt =[praiseNum intValue];
        if (praiseInt!=0) {
            NSMutableString *praiseString = [[NSMutableString alloc]initWithString:@"赞("];
            [praiseString appendString:praiseNum];
            [praiseString appendString:@")"];
            cell.praiseLabel.textColor = [UIColor blueColor];
            cell.praiseLabel.text = praiseString;
        }
        cell.messageId.text = [NSString stringWithFormat:@"%@",_MsgandReplies[replyNum][@"id"]];
        cell.me.text = [NSString stringWithFormat:@"%@",_MsgandReplies[replyNum][@"me"]];
        cell.praiseNum.text = [NSString stringWithFormat:@"%@",_MsgandReplies[replyNum][@"praise"]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [receivedData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    orignImg = nil;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSData *imgdata = [NSData dataWithData:receivedData];
    orignImg = [UIImage imageWithData:imgdata ];
    cellRefreshCount++;
    if (cellRefreshCount==1) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:1];
        NSArray *paths = [NSArray arrayWithObjects:path,nil];
        [_tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
    }
    
}

-(void)back
{
    _largeWeb= [[enLargeHtmlWeb alloc] initWeb:orignImg];
    [self.view addSubview:_largeWeb];
    [_largeWeb statEnlarge];
}

- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
}


- (void)keyboardWillShow:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    NSDictionary *keyboardInfo = [note userInfo];
    NSValue *aValue = [keyboardInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    [UIView animateWithDuration:duration/2 animations:^{
        self.view.frame = CGRectMake(0, 0-keyboardRect.size.height, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *userInfo = note.userInfo;
    CGFloat duration = [userInfo[@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    
    [UIView animateWithDuration:duration/2 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}



-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) makeExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_input resignFirstResponder];
}


- (void)setCookie
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *sessionID = (NSString *)[defaults objectForKey:@"sessionID"];
    NSString *value = [NSString stringWithFormat:@"%@.localhost",sessionID];
    NSDictionary *cookieProperties = @{ NSHTTPCookieName     : @"JSESSIONID",
                                        NSHTTPCookieValue    : value,
                                        NSHTTPCookieDomain   : @"www.lastmeter.cn",
                                        NSHTTPCookiePath     : @"/"
                                        };
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
}

@end
