//
//  invitationCelltwo.h
//  EZLearning
//
//  Created by zwf on 15/6/18.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EZLViewController.h"
#import "hudView.h"
#import "shareEgine.h"
@interface invitationCelltwo : UITableViewCell<MBProgressHUDDelegate>

@property (strong, nonatomic) UILabel *body;
@property (strong, nonatomic) UILabel *bodyviewLabel;
//@property (strong, nonatomic) UIWebView *bodyviewimg;

@property (strong, nonatomic) UIImageView *bodyImageView;
@property (strong, nonatomic) UIButton *webButton;
@property (strong, nonatomic) UIView *sendview;
@property (strong, nonatomic) UIView *sharetoview;
@property (strong, nonatomic) UIView *friendscircleview;
@property (strong, nonatomic) UIView *friendsview;
@property (strong, nonatomic) UIView *praiseview;
@property (strong, nonatomic) UIView *copyview;
@property (strong, nonatomic) UILabel *praiseLabel;
@property (strong, nonatomic) UILabel *messageId;
@property (strong, nonatomic) UILabel *me;
@property (strong, nonatomic) UILabel *praiseNum;


+ (NSString *)ID;
+ (CGFloat) cellHeight;
-(CGFloat)initbodyText:(NSString *)string;
-(CGFloat)initbodyView:(NSString *)string;
@end
