//
//  invitationCellone.h
//  EZLearning
//
//  Created by zwf on 15/6/17.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface invitationCellone : UITableViewCell

@property (strong, nonatomic) UILabel *author;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *time;
//类方法
+ (NSString *)ID;
+ (CGFloat) cellHeight;
+ (NSString *)timechange:(NSString *)time;
-(CGFloat)inittitleText:(NSString *)string;
@end
