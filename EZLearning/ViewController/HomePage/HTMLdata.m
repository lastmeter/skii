//
//  HTMLdata.m
//  EZLearning
//
//  Created by zwf on 15/6/28.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "HTMLdata.h"
#import "ParseHtml.h"
@implementation HTMLdata

+(CGFloat)getImageWidth:(NSString *)htmlString
{
    NSRange range = [htmlString rangeOfString:@"width:.*?;height" options:NSRegularExpressionSearch];
    CGFloat width;
    width = MAIN_SCREEN_WIDTH;
    if (range.location != NSNotFound)
    {
        NSString *widthString =[htmlString substringWithRange:range];
        NSString *_widthString = [widthString substringFromIndex:6];
        NSString *widthString_ = [_widthString substringToIndex:_widthString.length-9];
        width = [widthString_ floatValue];
    }
    return width;
}

+(CGFloat)getImageHeight:(NSString *)htmlString
{
    NSRange range = [htmlString rangeOfString:@"height:.*?px;" options:NSRegularExpressionSearch];
    CGFloat height;
    height = MAIN_SCREEN_WIDTH;
    if (range.location != NSNotFound)
    {
        NSString *heightString =[htmlString substringWithRange:range];
        NSString *_heightString = [heightString substringFromIndex:7];
        NSString *heightString_ = [_heightString substringToIndex:_heightString.length-3];
        height = [heightString_ floatValue];
    }
    return height;
}

+(NSString *)getbodyString:(NSString *)htmlString
{
    return @"nil";
}

+(NSString *)getImageUrl:(NSString *)htmlString
{
     NSRange range = [htmlString rangeOfString:@"src=.*?style=" options:NSRegularExpressionSearch];
    if (range.location != NSNotFound)
    {
        NSString *imageUrl =[htmlString substringWithRange:range];
        NSString *_imageUrl = [imageUrl substringFromIndex:5];
        NSString *imageUrl_ = [_imageUrl substringToIndex:_imageUrl.length-8];
        return imageUrl_;
    }else {
        NSData *htmlData = [htmlString dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *imgArray = [ParseHtml parseData:htmlData];
        if (imgArray.count>0) {
            TFHppleElement *hppleElement = imgArray[0];
            NSString *raw = hppleElement.raw;
             NSRange rangeraw = [raw rangeOfString:@"http:.*?.jpg" options:NSRegularExpressionSearch];
            if (rangeraw.location != NSNotFound)
            {
                NSString *imageUrl =[raw substringWithRange:rangeraw];
                return imageUrl;
            }else{
                return nil;
            }
            
            
        }else{
            return nil;
        }
    }
}

@end