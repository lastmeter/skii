//
//  enLargeHtmlWeb.m
//  EZLearning
//
//  Created by zwf on 15/6/30.
//  Copyright (c) 2015年 YXB. All rights reserved.
//


#import "enLargeHtmlWeb.h"


@implementation enLargeHtmlWeb

- (instancetype)initWeb:(UIImage *)image
{
    self = [super initWithFrame:[UIScreen mainScreen].bounds];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = YES;
    imageView.backgroundColor = [UIColor blackColor];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(back)];
    [imageView addGestureRecognizer:singleTap];
    [self addSubview:imageView];
    self.hidden = YES;
    return self;
}

-(void)back
{
    self.hidden = YES;
}
-(void)statEnlarge
{
    self.hidden = NO;
}
@end
