//
//  HomePageViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "HomePageViewController.h"
#import "NotificationViewController.h"
#import "CurriculumViewController.h"
#import "JXBAdPageView.h"
#import "AdvertViewController.h"
#import "ForumViewController.h"
#import "SiteCourseViewController.h"
#import "MomentsMarkController.h"
#import "UIImageView+WebCache.h"
#import "EzlCourseController.h"
#import "NEzlNavigationController.h"
@interface HomePageViewController () <JXBAdPageViewDelegate>
{
    NSArray *_adverSource;
    CGRect _bannerFrame;
    NSMutableArray *_sitesSource;
    NSString *version;
    BOOL _first;
    
    JXBAdPageView *_adView;
}

@end

@implementation HomePageViewController


- (instancetype)init{
    self = [super init];
    if (self) {
        self.title = @"首页";
        self.tabBarItem.image = [UIImage imageNamed:@"tab1_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"tab1_sel"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvAdver:) SoapType:ST_GET_COMPANY_ADVERT];
    _bannerFrame = CGRectMake(0, 20+44, MAIN_SCREEN_WIDTH, 0.33*MAIN_SCREEN_HEIGHT) ;
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSites:) SoapType:ST_GET_MY_SITES];
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:@"com.ezl.login" object:nil];
}

- (void)loadData{
    [SoapManager getMySites];
    [SoapManager getCompanyAdvert];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(getVersion:) SoapType:ST_GET_CURVERSION];
    [SoapManager getcurrentVersion];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!_first) {
        _first = YES;
        [self addView];
    }
    [self.view bringSubviewToFront:_adView];
}


- (void) getVersion:(NSNotification *)noti{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        version = [SoapNotiCenter getInfoFromNotification:noti];
        float myVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        float _curVersion = [version floatValue];
        if (myVersion < _curVersion) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"有新的版本，是否前往更新" message:nil delegate:self cancelButtonTitle:@"前往更新" otherButtonTitles:@"取消", nil];
            alert.tag = 99;
            [alert show];
        }
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_CURVERSION];
}


- (void) getversionInfo:(NSNotification *)noti{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        NSDictionary *versiondic= [SoapNotiCenter getInfoFromNotification:noti];
        NSString *versionurl = versiondic[@"url"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:versionurl]];
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_VERSION_INFO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag==99 && buttonIndex == 0) {
        [SoapNotiCenter addSoapObserver:self selector:@selector(getversionInfo:) SoapType:ST_GET_VERSION_INFO];
        [SoapManager getversionInfo:[version intValue]];
    }
}


- (void) recvSites:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _sitesSource = [SoapNotiCenter getInfoFromNotification:noti];
    }
    
}


-(void)recvAdver:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _adverSource = [SoapNotiCenter getInfoFromNotification:noti];
        if (_adverSource.count == 0) { return ; }
        
        NSMutableArray *imgArray = [NSMutableArray array];
        NSMutableArray *adArray = [NSMutableArray array];
        [_adverSource enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
            [imgArray addObject:[obj[@"path"] stringByReplacingOccurrencesOfString:@" " withString:@""]];
            [adArray addObject:[obj[@"url"] stringByReplacingOccurrencesOfString:@" " withString:@""]];
            NSLog(@"%@",obj[@"path"]);
        }];
        
        if (_adView == nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                _adView = [[JXBAdPageView alloc] initWithFrame:_bannerFrame];
                _adView.delegate = self;
                _adView.bWebImage = YES;
                _adView.iDisplayTime = 5;
                [self.view addSubview:_adView];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            WEAKSELF;
            [_adView startAdsWithBlock:imgArray block:^(NSInteger clickIndex){
                AdvertViewController *adverController = [[AdvertViewController alloc] initWithNibName:@"AdvertViewController" bundle:nil];
                adverController.urlString = adArray[clickIndex];
                [weakSelf.navigationController pushViewController:adverController animated:YES];
            }];
        });
        

    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_COMPANY_ADVERT];
}

- (void)setWebImage:(UIImageView *)imgView imgUrl:(NSString *)imgUrl{
    [imgView sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
}

- (void)addView
{
    CGFloat viewWidth = MAIN_SCREEN_WIDTH;
    CGFloat viewHeight = MAIN_SCREEN_HEIGHT;
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, viewHeight+21 - 49)];
    background.image = [UIImage imageNamed:@"home_background"];
    [self.view addSubview:background];
    
    CGFloat Padding = 0.03*viewWidth;
    CGFloat BtnWidth = viewWidth*0.5;
    CGFloat BtnHeight = (viewHeight-_bannerFrame.size.height - 44 - Padding*2 -44 )*0.5;
    
    //账户判断
   //UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(Padding, _bannerFrame.origin.y + _bannerFrame.size.height+Padding*2, BtnWidth, BtnHeight)];
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, _bannerFrame.origin.y + _bannerFrame.size.height+Padding, BtnWidth, BtnHeight)];
    btn1.layer .borderWidth = 2.0;
    btn1.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1.0]CGColor];
    btn1.tag = 1;
    [btn1 setImage:[UIImage imageNamed:@"home_curriculum_normal"] forState:UIControlStateNormal];
    [btn1 setImage:[UIImage imageNamed:@"home_curriculum_pressed"] forState:UIControlStateSelected];
    [btn1 addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(BtnWidth+2*Padding, _bannerFrame.size.height + _bannerFrame.origin.y +Padding*2, BtnWidth, BtnHeight)];
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(BtnWidth, _bannerFrame.size.height + _bannerFrame.origin.y +Padding, BtnWidth, BtnHeight)];
    btn2.layer .borderWidth = 2.0;
    btn2.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1.0]CGColor];
    btn2.tag = 2;
    [btn2 setImage:[UIImage imageNamed:@"home_feedback_normal"] forState:UIControlStateNormal];
    [btn2 setImage:[UIImage imageNamed:@"home_feedback_pressed"] forState:UIControlStateSelected];
    [btn2 addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    //UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(Padding, btn1.frame.origin.y+Padding+BtnHeight, BtnWidth, BtnHeight)];
    UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(0, btn1.frame.origin.y+BtnHeight, BtnWidth, BtnHeight)];
    btn3.layer .borderWidth = 2.0;
    btn3.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1.0]CGColor];
    btn3.tag = 3;
    [btn3 setImage:[UIImage imageNamed:@"home_notice_normal"] forState:UIControlStateNormal];
    [btn3 setImage:[UIImage imageNamed:@"home_notice_pressed"] forState:UIControlStateSelected];
    [btn3 addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    //UIButton *btn4 = [[UIButton alloc] initWithFrame:CGRectMake(btn2.frame.origin.x, btn3.frame.origin.y, BtnWidth, BtnHeight)];
    UIButton *btn4 = [[UIButton alloc] initWithFrame:CGRectMake(btn2.frame.origin.x, btn3.frame.origin.y, BtnWidth, BtnHeight)];
    btn4.layer .borderWidth = 2.0;
    btn4.layer.borderColor = [[UIColor colorWithWhite:0.9 alpha:1.0]CGColor];
    btn4.tag = 4;
    //[btn4 setBackgroundColor:[UIColor blackColor]];
    [btn4 setImage:[UIImage imageNamed:@"home_favorite_normal"] forState:UIControlStateNormal];
    [btn4 setImage:[UIImage imageNamed:@"home_favorite_pressed"] forState:UIControlStateSelected];
    [btn4 addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn1];
    [self.view addSubview:btn2];
    [self.view addSubview:btn3];
    [self.view addSubview:btn4];
}

- (void)btn_click:(UIButton *)btn{
    if (btn.tag == 4) {
        MomentsMarkController *momentsMarkController = [[MomentsMarkController alloc] init];
        NEzlNavigationController *nav = [[NEzlNavigationController alloc] initWithRootViewController:momentsMarkController];
        nav.navigationBar.tintColor = [UIColor blackColor];
        nav.navigationBar.translucent = NO;
        [self presentViewController:nav animated:YES completion:nil];
    }
    else{
        if (_sitesSource.count < btn.tag) {
            return;
        }
        NSDictionary *dic1 = _sitesSource[btn.tag - 1];
        EzlCourseController *courseVC = [[EzlCourseController alloc] initWithSiteTitle:dic1[@"title"] siteId:dic1[@"siteId"]];
        [self.navigationController pushViewController:courseVC animated:YES];
    }

}

/*
- (void)btn_click:(UIButton *)sender
{

    switch (sender.tag)
    {
        case 3:
        {
 
            NotificationViewController *notification = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
            [self.navigationController pushViewController:notification animated:YES];

//            NSString *siteName = dic1[@"title"];
//            NSString *siteId = dic1[@"siteId"];
//            SiteCourseViewController *courseView = [[SiteCourseViewController alloc] initWithNibName:@"SiteCourseViewController" bundle:nil];
//            courseView.titleString = siteName;
//            courseView.siteId = siteId;
//            
//            [self.navigationController pushViewController:courseView animated:YES];
//            


        }
            break;
        case 1:
        {
            
            CurriculumViewController * curriculum = [[CurriculumViewController alloc] initWithNibName:@"CurriculumViewController" bundle:nil];
            [self.navigationController pushViewController:curriculum animated:YES];
            
            if (_sitesSource.count == 0 ) {
                return;
            }
            NSDictionary *dic1 = _sitesSource[0];
            NSString *siteName = dic1[@"title"];
            NSString *siteId = dic1[@"siteId"];
            
          
            SiteCourseViewController *courseView = [[SiteCourseViewController alloc] initWithNibName:@"SiteCourseViewController" bundle:nil];
            courseView.titleString = siteName;
            courseView.siteId = siteId;
            
        }
            break;
        case 4:
        {
            //论坛
//            ForumViewController *forum = [[ForumViewController alloc] initWithNibName:@"ForumViewController" bundle:nil];
//            [self.navigationController pushViewController:forum animated:YES];
            
            //Add In 2015.12.17 By LRH
 
            MomentsMarkController *momentsMarkController = [[MomentsMarkController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:momentsMarkController];
            nav.navigationBar.tintColor = [UIColor blackColor];
            nav.navigationBar.translucent = NO;
            [self presentViewController:nav animated:YES completion:nil];
            
        }
            break;
        case 2:
        {
            if (_sitesSource.count < 2 ) {
                return;
            }
            NSDictionary *dic1 = _sitesSource[1];
            NSString *siteName = dic1[@"title"];
            NSString *siteId = dic1[@"siteId"];
            SiteCourseViewController *courseView = [[SiteCourseViewController alloc] initWithNibName:@"SiteCourseViewController" bundle:nil];
            courseView.titleString = siteName;
            courseView.siteId = siteId;
            
            
            EzlCourseController *courseVC = [[EzlCourseController alloc] initWithSiteTitle:dic1[@"title"] siteId:dic1[@"siteId"]];
            [self.navigationController pushViewController:courseVC animated:YES];
            //[self.navigationController pushViewController:courseView animated:YES];
        }
            break;
        default:
            break;
    }
}
*/

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
