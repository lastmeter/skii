//
//  shareEgine.h
//  EZLearning
//
//  Created by zwf on 15/6/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"
@protocol ShareEngineDelegate;

typedef enum
{
    weChat,
    weChatFriend
}WeiboType;

@interface shareEgine:NSObject<WXApiDelegate>
@property (nonatomic, assign) id<ShareEngineDelegate> delegate;
+ (shareEgine *) sharedInstance;
- (BOOL)handleOpenURL:(NSURL *)url;
- (void)registerApp;
- (void)sendWeChatMessage:(NSString*)message WithType:(WeiboType)weiboType;
- (void)sendWeChatImage:(UIImage*)image WithType:(WeiboType)weiboType;
@end
