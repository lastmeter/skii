//
//  invitationCelltwo.m
//  EZLearning
//
//  Created by zwf on 15/6/18.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "invitationCelltwo.h"

@implementation invitationCelltwo
{
    MBProgressHUD *_HUD;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    
    self.body = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, cellWidth-10, 0)];
    _body.numberOfLines = 0;
    _body.font = [UIFont systemFontOfSize:16.0];
    [self.contentView addSubview:_body];
    
    self.messageId = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _messageId.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_messageId];
    
    self.me = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _me.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_me];
    self.praiseNum = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    _praiseNum.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_praiseNum];
    
    self.sendview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.sharetoview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    UIImageView *sharetoImage  = [[UIImageView alloc]initWithFrame:CGRectMake(14, 0, 32, 32)];
    sharetoImage.image = [UIImage imageNamed:@"share_to"];
    [_sharetoview addSubview:sharetoImage];
    
    
    UILabel *sharetoLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 60, 28)];
    sharetoLabel.font = [UIFont systemFontOfSize:13.0];
    sharetoLabel.text = @"分享到:";
    sharetoLabel.textAlignment = NSTextAlignmentCenter;
    [_sharetoview addSubview:sharetoLabel];
    
    [_sendview addSubview:_sharetoview];
    
    
    self.friendscircleview = [[UIView alloc]initWithFrame:CGRectMake(60, 0, 40, 60)];
    
    UIImageView *friendscircleImage = [[UIImageView alloc]initWithFrame:CGRectMake(4, 0, 32, 32)];
    friendscircleImage.image = [UIImage imageNamed:@"friends_circle_normal"];
    [_friendscircleview addSubview:friendscircleImage];
    
    UILabel *friendscircleLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 40, 28)];
    friendscircleLabel.font = [UIFont systemFontOfSize:13.0];
    friendscircleLabel.text = @"朋友圈";
    friendscircleLabel.textAlignment = NSTextAlignmentCenter;
    [_friendscircleview addSubview:friendscircleLabel];

    UITapGestureRecognizer *circlefriendsTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectcirclefriends)];
    [_friendscircleview addGestureRecognizer:circlefriendsTap];
    [_sendview addSubview:_friendscircleview];
    
    
    self.friendsview = [[UIView alloc]initWithFrame:CGRectMake(100, 0, 60, 60)];

    UIImageView *friendsImage = [[UIImageView alloc]initWithFrame:CGRectMake(14, 0, 32, 32)];
    friendsImage.image = [UIImage imageNamed:@"goodfriends_normal"];
    [_friendsview addSubview:friendsImage];
    
    UILabel *friendsLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 60, 28)];
    friendsLabel.font = [UIFont systemFontOfSize:13.0];
    friendsLabel.text = @"好友";
    friendsLabel.textAlignment = NSTextAlignmentCenter;
    [_friendsview addSubview:friendsLabel];
    
    UITapGestureRecognizer *friendsTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectfriends)];
    [_friendsview addGestureRecognizer:friendsTap];
    [_sendview addSubview:_friendsview];
    
    self.bodyviewLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, cellWidth-10, 20)];
    _bodyviewLabel.numberOfLines = 0;
    _bodyviewLabel.font = [UIFont systemFontOfSize:16.0];
    [self.contentView addSubview:_bodyviewLabel];
    /*
    self.bodyviewimg = [[UIWebView alloc]initWithFrame:CGRectMake(5, _bodyviewLabel.frame.size.height+20, 200, 200)];
    self.webButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 200, 200)];
    [_bodyviewimg addSubview:_webButton];
    _bodyviewimg.scalesPageToFit = YES;
    _bodyviewimg.scrollView.scrollEnabled = NO;
    [_bodyviewimg sizeToFit];
    [self.contentView addSubview:_bodyviewimg];
    */
    
    self.bodyImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, _bodyviewLabel.frame.size.height+20, 200, 200)];
    _bodyImageView.userInteractionEnabled = YES;
    _bodyImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_bodyImageView];

    
    self.praiseview = [[UIView alloc]initWithFrame:CGRectMake(cellWidth-60, 0, 60, 60)];
    
    UIImageView *praiseImage = [[UIImageView alloc]initWithFrame:CGRectMake(14, 0, 32, 32)];
    praiseImage.image = [UIImage imageNamed:@"praise_normal"];
    [_praiseview addSubview:praiseImage];
    
   self.praiseLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 60, 28)];
    _praiseLabel.font = [UIFont systemFontOfSize:13.0];
    _praiseLabel.text = @"赞(0)";
    _praiseLabel.textAlignment = NSTextAlignmentCenter;
    [_praiseview addSubview:_praiseLabel];
    
    UITapGestureRecognizer *praiseTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectpraise)];
    [_praiseview addGestureRecognizer:praiseTap];
    [_sendview addSubview:_praiseview];
    
    self.copyview = [[UIView alloc]initWithFrame:CGRectMake(cellWidth-100, 0, 40, 60)];
    
    UIImageView *copyImage = [[UIImageView alloc]initWithFrame:CGRectMake(4, 0, 32, 32)];
    copyImage.image = [UIImage imageNamed:@"copy_normal"];
    [_copyview addSubview:copyImage];
    
    UILabel *copyLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 32, 40, 28)];
    copyLabel.font = [UIFont systemFontOfSize:13.0];
    copyLabel.text = @"复制";
    copyLabel.textAlignment = NSTextAlignmentCenter;
    [_copyview addSubview:copyLabel];
    
    UITapGestureRecognizer *copyTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectcopy)];
    [_copyview addGestureRecognizer:copyTap];
    [_sendview addSubview:_copyview];
    
    [self.contentView addSubview:_sendview];
    
    
    return self;
}



+ (NSString *)ID
{
    return @"invitationTwo";
    
}
+ (CGFloat) cellHeight
{
    return 60;
}

-(CGFloat)initbodyText:(NSString *)string{
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    CGRect tmpRect = _body.frame;
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString: string];
    _body.attributedText =attrStr;
    CGRect bodySize =[_body.text boundingRectWithSize:CGSizeMake(cellWidth-10,8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_body.font,NSFontAttributeName, nil] context:nil];
    tmpRect.size.height =bodySize.size.height;
    _body.frame =tmpRect;
    _messageId.frame = CGRectMake(cellWidth/2, tmpRect.size.height+20, cellWidth/2, 20);
    _me.frame = CGRectMake(cellWidth/2, tmpRect.size.height+40, cellWidth/2, 20);
    _praiseNum.frame = CGRectMake(cellWidth/2, tmpRect.size.height+60, cellWidth/2, 20);
    _sendview.frame = CGRectMake(0, tmpRect.size.height+20, cellWidth, 60);
    return tmpRect.size.height;
}

-(CGFloat)initbodyView:(NSString *)string
{
    CGFloat cellWidth = MAIN_SCREEN_WIDTH;
    
    NSAttributedString *attrStr =[[NSAttributedString alloc]initWithString: string];
    _bodyviewLabel.attributedText =attrStr;
    CGRect bodySize =[_bodyviewLabel.text boundingRectWithSize:CGSizeMake(cellWidth-10,8000) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObjectsAndKeys:_bodyviewLabel.font,NSFontAttributeName, nil] context:nil];
    
    CGRect LabelRect = _bodyviewLabel.frame;
    LabelRect.size.height = bodySize.size.height;
    _bodyviewLabel.frame = LabelRect;
    
    /*
    CGRect ImgRect = _bodyviewimg.frame;
    ImgRect.origin.y = bodySize.size.height+30;
    _bodyviewimg.frame = ImgRect;
    */
    CGRect ImgViewRect = _bodyImageView.frame;
    ImgViewRect.origin.y = bodySize.size.height+30;
    _bodyImageView.frame = ImgViewRect;
    
    
    _messageId.frame = CGRectMake(cellWidth/2, bodySize.size.height+20, cellWidth/2, 20);
    _me.frame = CGRectMake(cellWidth/2, bodySize.size.height+40, cellWidth/2, 20);
    _praiseNum.frame = CGRectMake(cellWidth/2, bodySize.size.height+60, cellWidth/2, 20);
    
    _sendview.frame = CGRectMake(0, bodySize.size.height+240, cellWidth, 60);
    return bodySize.size.height;
}


-(void)selectcirclefriends
{
    if (_body.text.length==0) {
        [[shareEgine sharedInstance]sendWeChatImage:_bodyImageView.image WithType:weChatFriend];
    }else{
        [[shareEgine sharedInstance] sendWeChatMessage:_body.text  WithType:weChatFriend];
    }
    
}

-(void)selectfriends
{
    if (_body.text.length==0) {
        [[shareEgine sharedInstance]sendWeChatImage:_bodyImageView.image WithType:weChat];
        
    }else{
        [[shareEgine sharedInstance] sendWeChatMessage:_body.text  WithType:weChat];
    }
}

-(void)selectpraise
{
    if ([_me.text isEqualToString:@"1"]) {
        [self HUDshow:@"您已经点过赞了"];
        
    }else{
    [SoapManager addpraisetoMSG:_messageId.text];
    [SoapNotiCenter addSoapObserver:self selector:@selector(getISpraise:) SoapType:ST_ADD_PRAISE_TO_MSG];
    }
}

-(void)getISpraise:(NSNotification *)noti
{
    
    if ([SoapNotiCenter getSoapResult:noti])
    {
        NSString * _boolpost = [SoapNotiCenter getInfoFromNotification:noti];
        if ([_boolpost boolValue]) {
            _me.text = @"1";
            NSString *praiseNum = _praiseNum.text;
            int num =[praiseNum intValue];
            _praiseNum.text = [NSString stringWithFormat:@"%d",++num];
            NSMutableString *praiseString = [[NSMutableString alloc]initWithString:@"赞("];
            [praiseString appendString:_praiseNum.text];
            [praiseString appendString:@")"];
            _praiseLabel.text = praiseString;
            _praiseLabel.textColor = [UIColor blueColor];
        }else{
            [self HUDshow:@"点赞不成功，请重试"];
        }
    }
        [SoapNotiCenter removeSoapObserver:self SoapType:ST_ADD_PRAISE_TO_MSG];
}


-(void)selectcopy
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _body.text;
    [self HUDshow:@"已经放到系统的剪贴板里"];

}

-(void)HUDshow:(NSString *)title
{
    _HUD = [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    _HUD.delegate=self;
    _HUD.labelText = title;
    _HUD.mode = MBProgressHUDModeText;
    _HUD.margin =10.0;
    _HUD.opacity =1.0;
    _HUD.labelFont = [UIFont boldSystemFontOfSize:13.0];
    [self.window addSubview:_HUD];
    [_HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    _HUD.yOffset = MAIN_SCREEN_HEIGHT/2-60;
}

-(void)myTask
{
    sleep(1.0);
}

-(void)hudWasHidden:(MBProgressHUD *)hud{
    [hud removeFromSuperview];
    hud=nil;
}



@end
