//
//  CourseViewController.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/8.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "EzlViewController.h"

@interface SiteCourseViewController : EzlViewController
@property (nonatomic, copy)   NSString *titleString;
@property (nonatomic, copy)   NSString *siteId;


@end
