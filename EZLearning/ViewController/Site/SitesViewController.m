//
//  SitesViewController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "SitesViewController.h"
#import "SiteCourseViewController.h"

@interface SitesViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *_sitesSource;
    LoadingView *_loadingView;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SitesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSites:) SoapType:ST_GET_MY_SITES];
    [SoapManager getMySites];
    
    _loadingView = [[LoadingView alloc] init];
    [self.tableView addSubview:_loadingView];
    [_loadingView startLoading];
    
    [self makeExtraCellLineHidden];
}

- (void) recvSites:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _sitesSource = [SoapNotiCenter getInfoFromNotification:noti];
        MAIN(^{
            [_tableView reloadData];
        });
    }
    [_loadingView removeFromSuperview];
    _loadingView = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/*-----------------------------------------------------------
 Description:  row数目
 -----------------------------------------------------------*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _sitesSource.count;
}

/*-----------------------------------------------------------
 Description:   编辑cell
 -----------------------------------------------------------*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Identifier = @"academy";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
        cell.imageView.image = [UIImage imageNamed:@"site"];
    }
    NSDictionary *dic = _sitesSource[indexPath.row];
    cell.textLabel.text = dic[@"title"];;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dic1 = _sitesSource[indexPath.row];
    NSString *siteName = dic1[@"title"];
    NSString *siteId = dic1[@"siteId"];
    
    SiteCourseViewController *courseView = [[SiteCourseViewController alloc] initWithNibName:@"SiteCourseViewController" bundle:nil];
    courseView.titleString = siteName;
    courseView.siteId = siteId;
    
    [self.navigationController pushViewController:courseView animated:YES];
    
    [self deselect];
    
}


/*-----------------------------------------------------------
 Description:  不可编辑
 -----------------------------------------------------------*/
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void) makeExtraCellLineHidden
{
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setTableFooterView:view];

}
- (void)deselect
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
