//
//  CourseViewController.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/8.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "SiteCourseViewController.h"
#import "CourseModel.h"
#import "CourseCell.h"
#import "AppDelegate.h"

@interface SiteCourseViewController ()<UITableViewDelegate,UITableViewDataSource, CourseButtonDelegate>
{
    int _rowNumber;
    NSArray *_courseSource;
    NSArray *_categorySource;
    
    LoadingView *_loadingView;
    UIView *_categoryView;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SiteCourseViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleLabel.text = _titleString;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _loadingView = [[LoadingView alloc] init];
    [self.view addSubview:_loadingView];
    [_loadingView startLoading];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvCourseBySite:)  SoapType:ST_GET_COURSE_BY_SITE];
    [SoapManager getCourseBySiteID:_siteId];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvCategoryList:)  SoapType:ST_GET_CATEGORY_LIST_BY_SITE];
    [SoapManager getCategoryListBySiteId:_siteId];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvCourseByCategory:)  SoapType:ST_GET_COURSE_BY_CATEGORY];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvSaveResult:)  SoapType:ST_SAVE_FAVORITES];
    
    [SoapNotiCenter addSoapObserver:self selector:@selector(recvCancelResult:)  SoapType:ST_CANCEL_FAVORITES];
}


- (void)recvCourseBySite:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _courseSource = [SoapNotiCenter getInfoFromNotification:noti];
        NSLog(@"_courseSource = %@",_courseSource);
        MAIN(^{
            
            [_loadingView stopLoading];
            [self.tableView reloadData];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_COURSE_BY_SITE];
}


//分类数据
- (void)recvCourseByCategory:(NSNotification *)noti
{
    
    if ([SoapNotiCenter getSoapResult:noti])
    {
        _courseSource = [SoapNotiCenter getInfoFromNotification:noti];
        
        MAIN(^{
            [_tableView reloadData];
        });
    }
    //[SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_COURSE_BY_CATEGORY];//移除
}


//分类列表名称
- (void)recvCategoryList:(NSNotification *)noti
{
  
    if (![SoapNotiCenter getSoapResult:noti])
    {
        MAIN(^{
            [self addCategoryViewWithArray:nil];
        });
    }
    else
    {
        _categorySource = [SoapNotiCenter getInfoFromNotification:noti];
        NSMutableArray *categoryNameArray = [NSMutableArray array];
        [_categorySource enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            [categoryNameArray addObject:obj[@"name"]];
        }];
        MAIN(^{
            [self addCategoryViewWithArray:categoryNameArray];
        });
    }
    [SoapNotiCenter removeSoapObserver:self SoapType:ST_GET_CATEGORY_LIST_BY_SITE];
}

-(void)recvSaveResult:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        MAIN(^{
            [self.tableView reloadData];
        });
    }
}

-(void)recvCancelResult:(NSNotification *)noti
{
    if ([SoapNotiCenter getSoapResult:noti])
    {
        MAIN(^{
            [self.tableView reloadData];
        });
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

/*-----------------------------------------------------------
 Description:  row数目
 -----------------------------------------------------------*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_courseSource.count % 2 == 0)
    {
        
        _rowNumber = (int)_courseSource.count/2;
    }
    else
    {
        _rowNumber = (int)_courseSource.count /2 +1;
    }
    return _rowNumber;
}

/*-----------------------------------------------------------
 Description:   编辑cell
 -----------------------------------------------------------*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Identifier = @"academy";
    int row = (int)indexPath.row;
    int count = row*2;
    CourseCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[CourseCell alloc] initWithReuseIdentifier:Identifier];
    }
    if (_courseSource.count %2 != 0 && row == _rowNumber-1)
    {
        cell.lastCourse = [_courseSource lastObject];
    }
    else
    {
        cell.leftCourse  = _courseSource[count];
        cell.rightCourse = _courseSource[count+1];
    }
    cell.delegate = self;
    return cell;
}

/*-----------------------------------------------------------
 Description:  row高度
 -----------------------------------------------------------*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CourseCell cellHeight];
}

/*-----------------------------------------------------------
 Description:  不可编辑
 -----------------------------------------------------------*/
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

/*-----------------------------------------------------------
 Description:  图片按钮
 -----------------------------------------------------------*/
-(void)imgBtnClick:(UIButton *)btn
{
    
    CourseModel * course = _courseSource[btn.tag];
    AppDelegate *delegate = [AppDelegate getAppDelegate];
    [delegate pushLearningViewControllerWithTitle:course.title Url:course.url];
}

/*-----------------------------------------------------------
 Description:  收藏按钮
 -----------------------------------------------------------*/
-(void)collectBtnClick:(UIButton *)btn
{
    CourseModel * course = _courseSource[btn.tag];
    if (course.isCollected)
    {
        [SoapManager cancelFavoritesByPackageId:course.contentPackageId];
    }
    else
    {
        [SoapManager saveFavoritesByCourseId:course.contentPackageId courseUrl:course.url courseTitle:course.title siteTitle:self.titleString];
    }
    course.collected = !course.collected;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag/2 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)categoryBtn:(id)sender
{
    _categoryView.hidden = !_categoryView.hidden;
}

-(void)setTitleString:(NSString *)titleString
{
    _titleString = titleString;
}

- (void)addCategoryViewWithArray: (NSArray *)nameArray
{
    CGFloat height =44;
    CGFloat VIEW_WIDTH = MAIN_SCREEN_WIDTH;
    CGFloat menuWidth = VIEW_WIDTH * 0.26;
    
    if (nameArray == nil)
    {
        _categoryView = [[UIView alloc] initWithFrame:CGRectMake(VIEW_WIDTH-20-menuWidth, 64, menuWidth, height)];
        [_categoryView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, menuWidth, height)];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn setTitle:@"全部类别" forState:UIControlStateNormal];
        
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTintColor:[UIColor redColor]];
        [btn setBackgroundImage:[UIImage imageNamed:@"MenuButton"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(categorySelected:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 9999;
        [_categoryView addSubview:btn];
        _categoryView.hidden = YES;
        [self.view addSubview:_categoryView];
        return;
    }

    _categoryView = [[UIView alloc] initWithFrame:CGRectMake(VIEW_WIDTH-20-menuWidth, 64, menuWidth, height*(nameArray.count+1))];

    [_categoryView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, menuWidth, height)];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn setTitle:@"全部类别" forState:UIControlStateNormal];
    
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTintColor:[UIColor redColor]];
    [btn setBackgroundImage:[UIImage imageNamed:@"MenuButton"] forState:UIControlStateNormal];
    btn.tag = 9999;
    [btn addTarget:self action:@selector(categorySelected:) forControlEvents:UIControlEventTouchUpInside];
    [_categoryView addSubview:btn];
    
    for (int i = 0; i<nameArray.count; i++)
    {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, (i+1)*height, menuWidth, height)];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];

        [btn setTitle:nameArray[i] forState:UIControlStateNormal];

        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTintColor:[UIColor redColor]];
        [btn setBackgroundImage:[UIImage imageNamed:@"MenuButton"] forState:UIControlStateNormal];

        btn.tag = i;
        [btn addTarget:self action:@selector(categorySelected:) forControlEvents:UIControlEventTouchUpInside];
        [_categoryView addSubview:btn];
    }
    _categoryView.hidden = YES;
    [self.view addSubview:_categoryView];

}

- (void)categorySelected:(UIButton *)btn
{
    _categoryView.hidden = YES;
    if (btn.tag != 9999)
    {
        NSDictionary *dic = _categorySource[btn.tag];
        [SoapManager getCourseBySiteID:_siteId andCategoryID:dic[@"id"]];
        [_loadingView startLoading];
    }
    else if(btn.tag == 9999)
    {
        [SoapNotiCenter addSoapObserver:self selector:@selector(recvCourseBySite:)  SoapType:ST_GET_COURSE_BY_SITE];
        [SoapManager getCourseBySiteID:_siteId];
        [_loadingView startLoading];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
