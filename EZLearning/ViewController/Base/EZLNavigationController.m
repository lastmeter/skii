//
//  EZLNavigationController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "EZLNavigationController.h"
#import "SoapNotiCenter.h"

@interface EZLNavigationController ()

@end

@implementation EZLNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    [SoapNotiCenter addSoapRequestResultObserver:self selector:@selector(recvSoapResult:)];
}


-(void)recvSoapResult:(NSNotification *)noti
{
    if (![SoapNotiCenter getSoapRequestResult:noti])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"无法连接服务器，请检查网络状况后重试" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        MAIN((^{
            [alert show];
        }));
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


-(BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;//竖屏显示
}

@end
