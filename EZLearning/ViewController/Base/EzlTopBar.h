//
//  EzlTopBar.h
//  NewEzl
//
//  Created by xihan on 15/8/30.
//  Copyright (c) 2015年 lrh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BackBlock)();

@interface EzlTopBar : UIView

- (instancetype)initWithTitle:(NSString *)title
                    backClick:(BackBlock)block;

@end
