//
//  EzlTopBar.m
//  NewEzl
//
//  Created by xihan on 15/8/30.
//  Copyright (c) 2015年 lrh. All rights reserved.
//

#import "EzlTopBar.h"


@implementation EzlTopBar{
    BackBlock _backBlock;
}

- (instancetype)initWithTitle:(NSString *)title
                    backClick:(BackBlock)block{
    self = [super initWithFrame:CGRectMake(0, 0, MAIN_SCREEN_WIDTH, 64)];
    if (self) {
        
        UIImageView *background = [[UIImageView alloc] initWithFrame:self.bounds];
        background.image = [UIImage imageNamed:@"actionbar_background"];
        [self addSubview:background];
        
        UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(15, 32, 10, 18)];
        arrow.image = [UIImage imageNamed:@"arrow"];
        [self addSubview:arrow];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(30, 30, 200, 21)];
        label.text = title;
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont systemFontOfSize:17];
        [self addSubview:label];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 150, 40)];
        [btn addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
        btn.backgroundColor = [UIColor clearColor];
        [self addSubview:btn];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 64, MAIN_SCREEN_WIDTH, 1)];
        line.backgroundColor = [UIColor grayColor];
        [self addSubview:line];
        
        _backBlock = block;
    }
    return self;
}

- (void)back:(UIButton *)btn{
    if (_backBlock) {
        _backBlock();
    }
    _backBlock = nil;
}

- (void)dealloc{
    _backBlock = nil;
}
@end
