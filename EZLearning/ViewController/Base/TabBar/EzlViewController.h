//
//  EzlViewController.h
//  EZLearning
//
//  Created by xihan on 15/5/30.
//  Copyright (c) 2015年 STWX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableView+Extension.h"
#import "EzlTopBar.h"
#import "MBProgressHUD.h"
#import "LoadingView.h"
#import "SoapManager.h"
#import "SoapNotiCenter.h"
#import "AppDelegate.h"

@interface EzlViewController : UIViewController


- (void)setTitle:(NSString *)title backClick:(BackBlock)block;

@end
