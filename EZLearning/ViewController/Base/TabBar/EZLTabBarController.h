//
//  YXBTabBarController.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EZLTabBarController : UITabBarController

- (void)setTabViewControllers:(NSArray *)viewControllers;

@end
