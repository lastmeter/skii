//
//  EzlViewController.m
//  EZLearning
//
//  Created by xihan on 15/5/30.
//  Copyright (c) 2015年 STWX. All rights reserved.
//

#import "EzlViewController.h"
@interface EzlViewController ()

@end

@implementation EzlViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
   self =  [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    self.edgesForExtendedLayout =UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    return self;
}

- (instancetype)init{
   self = [super init];
    if (self) {
        self.edgesForExtendedLayout =UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}


- (void)setTitle:(NSString *)title backClick:(BackBlock)block{
    EzlTopBar *topBar = [[EzlTopBar alloc] initWithTitle:title backClick:block];
    [self.view addSubview:topBar];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



@end
