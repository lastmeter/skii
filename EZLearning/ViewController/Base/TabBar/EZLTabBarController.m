//
//  YXBTabBarController.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "EZLTabBarController.h"
#import "EZLTabBar.h"

#import "EzlSearchController.h"

@interface EZLTabBarController ()<EZLTabBarDelegate, UITabBarControllerDelegate>{
    UIView *navBar;
    UIButton *_searchBtn;
    BOOL _first;
}
@end

@implementation EZLTabBarController

- (instancetype)init{
    self = [super init];
    if (self) {
        self.tabBar.tintColor = [UIColor colorWithRed:147.0/255.0 green:0 blue:0 alpha:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addNavBarView];
    self.delegate= self;
    _first = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    if (_first) {
//        _first = NO;
//        EZLTabBar *myTabBar = [[EZLTabBar alloc] init];
//        myTabBar.delegate = self;
//        
//        CGRect rect = self.tabBar.bounds;
//        myTabBar.frame = rect;
//        [self.tabBar addSubview:myTabBar];
//        
//        for (int i = 0; i < self.viewControllers.count; i++)
//        {
//            NSString *imageName = [NSString stringWithFormat:@"TabBar%d",i+1];
//            NSString *imageNameSel = [NSString stringWithFormat:@"TabBarSel%d",i+1];
//            
//            UIImage *image = [UIImage imageNamed:imageName];
//            UIImage *imagesel = [UIImage imageNamed:imageNameSel];
//            
//            [myTabBar setItemWithImage:image selectedImage:imagesel];
//        }
//
//    }
}

- (void)setTabViewControllers:(NSArray *)viewControllers
{
    self.viewControllers = viewControllers;
}

- (void)addNavBarView
{
    navBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navBar.backgroundColor = [UIColor groupTableViewBackgroundColor];
    UIImageView  *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 25, 34, 34)];
    imgView.image = [UIImage imageNamed:@"logo"];
    [navBar addSubview:imgView];
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(56, 25, self.view.frame.size.width-100, 36)];
    lable.text = @"SK-II在线培训平台";
    lable.font = [UIFont boldSystemFontOfSize:17];
    [navBar addSubview:lable];
    [self.view addSubview:navBar];
    
    //Add In 2015.12.21 By Lrh
    _searchBtn = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SCREEN_WIDTH - 40, 25, 30, 30)];
    [_searchBtn setImage:[UIImage imageNamed:@"check_score_normal"] forState:UIControlStateNormal];
    [_searchBtn setImage:[UIImage imageNamed:@"check_score_pressed"] forState:UIControlStateHighlighted];
    [navBar addSubview:_searchBtn];
    [_searchBtn addTarget:self action:@selector(searchCourse:) forControlEvents:UIControlEventTouchUpInside];
    //End
}

//Add In 2015.12.21 By Lrh

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    _searchBtn.hidden = self.selectedIndex != 0;
    navBar.hidden = self.selectedIndex == 1;
}


- (void)searchCourse:(UIButton *)btn{
    EzlSearchController *searchController = [[EzlSearchController alloc] init];
    [self.navigationController pushViewController:searchController animated:YES];
}
//End


//实现协议方法
- (void)tabBar:(EZLTabBar *)tabBar selectedFrom:(NSInteger)from to:(NSInteger)to
{
    self.selectedIndex = to;
    if(to==1){//显示导航栏
        [navBar removeFromSuperview];
    }else if(self.view.subviews.count!=3){
       [self addNavBarView];
    }
}



@end
