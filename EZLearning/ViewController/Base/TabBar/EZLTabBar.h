//
//  YXBTabBar.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EZLTabBar;

@protocol EZLTabBarDelegate <NSObject>

- (void)tabBar:(EZLTabBar *)tabBar selectedFrom:(NSInteger) from to: (NSInteger)to;

@end

@interface EZLTabBar : UIView

@property (nonatomic, weak) id<EZLTabBarDelegate> delegate;

- (void)setItemWithImage:(UIImage *)image selectedImage:(UIImage *)selectedImage;

@end
