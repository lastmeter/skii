//
//  YXBTabBar.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "EZLTabBar.h"

@interface EZLTabBar()

@property (nonatomic, weak) UIButton *selectedBtn;//设置之前选中的按钮

@end

@implementation EZLTabBar


/*
 *  使用特定的图片来创建按钮
 *  @param image            普通状态下的图片
 *  @param selectedImage    选中状态下的图片
 */
- (void)setItemWithImage:(UIImage *)image selectedImage:(UIImage *)selectedImage
{
    UIButton *btn = [[UIButton alloc] init];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:selectedImage forState:UIControlStateSelected];
    [self addSubview:btn];
    
    [btn addTarget:self action:@selector(btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    //如果是第一个按钮，则选中
    if (self.subviews.count == 1)
    {
        [self btn_click:btn];
    }
}



//布局子视图
- (void)layoutSubviews
{
    [super layoutSubviews];
    NSUInteger count = self.subviews.count;
    for (int i = 0; i < count; i++)
    {
        UIButton *btn = self.subviews[i];
        
        btn.tag = i; //设置按钮tag，方便索引当前的按钮，并跳转到相应的视图
        
        CGFloat x = i *self.bounds.size.width / count;
        CGFloat y = 0;
        CGFloat width = self.bounds.size.width / count;
        CGFloat height = self.bounds.size.height;
        btn.frame = CGRectMake(x, y, width, height);
    }
}

- (void)btn_click:(UIButton *)button
{
    _selectedBtn.selected = NO; //之前选中的按钮设置为未选中
    button.selected = YES;      //当前按钮设置为选中
    self.selectedBtn = button;  //将当前按钮赋值为之前选中的按钮
    
    //跳转视图交给LRHTabBarController做
    //判断_delegate实例是否实现协议方法
    if ([self.delegate respondsToSelector:@selector(tabBar:selectedFrom:to:)])
    {
        [self.delegate tabBar:self selectedFrom:_selectedBtn.tag to:button.tag];
    }
}


@end
