//
//  RankModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RankModel : NSObject

@property(nonatomic, copy) NSString *score;
@property(nonatomic, copy)  NSString *name;
@property(nonatomic, assign) NSUInteger rank;

@end
