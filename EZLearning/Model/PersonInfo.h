//
//  PersonInfo.h
//  e-Learning
//
//  Created by RenHongwei on 15/2/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonInfo : NSObject

@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *manager;
@property (copy, nonatomic) NSString *market;
@property (copy, nonatomic) NSString *rank;
@property (copy, nonatomic) NSString *phonenum;
@property (copy, nonatomic) NSString *emailaddress;
@property (copy, nonatomic) NSString *credit;
@property (copy, nonatomic) NSString *star;
@end
