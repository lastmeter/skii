//
//  ExaminationModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssessmentModel : NSObject

@property (copy, nonatomic)NSString *assessmentTitle;
@property (assign, nonatomic)NSString *createTime;
@property (assign, nonatomic)NSString *assessmentId;
@property (assign, nonatomic)int timeLimit;
@property (assign, nonatomic)int submissionsAllowed;
@property (assign, nonatomic)int grading;
@property (assign, nonatomic, getter=isUnlimitedSubmissions)BOOL unlimitedSubmissions;


@end
