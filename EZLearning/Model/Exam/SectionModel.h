//
//  SectionModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SectionModel : NSObject

@property(nonatomic, copy)NSString *sectionTitle;
@property(nonatomic, copy)NSString *sectionId;
@property(nonatomic, assign)long creatTime;
@property(nonatomic, assign)int itemCount;

@end
