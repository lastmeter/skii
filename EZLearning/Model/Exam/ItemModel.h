//
//  ChoiceModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    MULTIPLE_CHOICE     =   1,
    MULTIPLE_CORRECT    =   2,
    TRUE_FALSE          =   4,
    DEFAULT_SECTION     =   21
}CHOICE_TYPE;

@interface ItemModel : NSObject

@property(nonatomic, copy)NSString *text;
@property(nonatomic, copy)NSString *score;
@property(nonatomic, copy)NSString *userChoice;
@property(nonatomic, copy)NSString *itemId;

@property(nonatomic, assign) CHOICE_TYPE itemType;
@property(nonatomic, assign, getter=isChosen) BOOL chosen;


@end
