//
//  ChoiceCellFrameModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class  ItemModel;

@interface ChoiceCellFrameModel : NSObject

@property (nonatomic, strong) ItemModel *itemModel;

@property (nonatomic, assign, readonly) CGRect textFrame;
@property (nonatomic, assign, readonly) CGRect scoreFrame;
@property (nonatomic, assign, readonly) CGRect answerFrame;
@property (nonatomic, assign, readonly) CGFloat cellHeght;

+(CGFloat)choiceCellTitleFontSize;
+(CGFloat)choiceCellDetailFontSize;

@end
