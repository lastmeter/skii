//
//  ChoiceViewModel.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AnswerViewModel.h"
#import "NSString+Extension.h"
#import "AnswerModel.h"

@implementation AnswerViewModel

-(void)setQuestionName:(NSString *)questionName
{
    _questionName = questionName;
    CGSize questionSize = [questionName sizeWithFont:[UIFont systemFontOfSize:[AnswerModel answerViewTitleFontSize]] andWidth:ChoiceViewWidth-20];
    _questionFrame = (CGRect){10,0,questionSize};
}

-(void)setAnswerArray:(NSArray *)answerArray
{
    __block CGFloat tableViewHeight = 0;
    [answerArray enumerateObjectsUsingBlock:^(AnswerModel *obj, NSUInteger idx, BOOL *stop) {
        
        tableViewHeight = tableViewHeight + obj.cellSize.height;
    }];
    _tableViewHeight = tableViewHeight;
}


@end
