//
//  ChoiceCellFrameModel.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "ChoiceCellFrameModel.h"
#import "ItemModel.h"
#import "NSString+Extension.h"

#define textPadding 5

@implementation ChoiceCellFrameModel

-(void)setItemModel:(ItemModel *)itemModel
{
    _itemModel = itemModel;
    CGFloat viewWidth = MAIN_SCREEN_WIDTH;
    
    CGSize textSize = [itemModel.text sizeWithFont:[UIFont systemFontOfSize:[ChoiceCellFrameModel choiceCellTitleFontSize]] andWidth:viewWidth-20];
    _textFrame = (CGRect){10, 0, textSize};
    
    _answerFrame = CGRectMake(viewWidth*0.5+30,_textFrame.size.height, (viewWidth-20)*0.5, 21);
    
    _scoreFrame = CGRectMake(20, _textFrame.size.height, (viewWidth-20)*0.5, 21);
    
    _cellHeght = CGRectGetMaxY(_answerFrame)+textPadding;
}

- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width
{
    UITextView *detailTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, width, 0)];
    detailTextView.font = [UIFont systemFontOfSize:fontSize];
    detailTextView.text = value;
    CGSize deSize = [detailTextView sizeThatFits:CGSizeMake(width,CGFLOAT_MAX)];
    return deSize.height;
}


+(CGFloat)choiceCellTitleFontSize
{
    return isPad ? 20:16;
}

+(CGFloat)choiceCellDetailFontSize
{
    return isPad ? 18:14;
}

@end
