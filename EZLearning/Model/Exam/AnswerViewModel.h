//
//  ChoiceViewModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/19.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum
{
    AVT_FIRST,
    AVT_MIDDLE,
    AVT_LAST
}ANSWER_VIEW_TYPE;

@interface AnswerViewModel : NSObject

@property(nonatomic, strong)NSString *questionName;
@property(nonatomic, strong)NSArray *answerArray;
@property(nonatomic, assign)ANSWER_VIEW_TYPE type;

@property (nonatomic, assign, readonly) CGRect questionFrame;
@property (nonatomic, assign, readonly) CGFloat tableViewHeight;
@end
