//
//  AgentScoreModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AgentScoreModel : NSObject

@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy) NSString *finalScore;
@property(nonatomic, copy) NSString *myScore;

@end
