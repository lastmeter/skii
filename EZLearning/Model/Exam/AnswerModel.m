//
//  ChoiceItemModel.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AnswerModel.h"
#import "NSString+Extension.h"


@implementation AnswerModel

-(void)setText:(NSString *)text
{
    _text = text;
    CGFloat pad = isPad ? 80 : 60;
    _cellSize = [text sizeWithFont:[UIFont systemFontOfSize:[AnswerModel answerCellTextFontSize]] andWidth:ChoiceViewWidth - pad];
}

+ (CGFloat)answerViewTitleFontSize
{
    return isPad ? 25:16;
}

+ (CGFloat)answerCellTextFontSize
{
    return isPad? 23:15;
}

@end
