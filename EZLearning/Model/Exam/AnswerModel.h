//
//  ChoiceItemModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/15.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ChoiceViewWidth (0.8)*(([UIScreen mainScreen].applicationFrame.size.width))

@interface AnswerModel : NSObject

@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *answerId;
@property (nonatomic, copy) NSString *text;

@property (nonatomic, assign, readonly) CGSize cellSize;
@property (nonatomic, assign, getter=isCorrect)  BOOL correct;
@property (nonatomic, assign, getter=isChosen)   BOOL chosen;

+ (CGFloat)answerViewTitleFontSize;
+ (CGFloat)answerCellTextFontSize;

@end
