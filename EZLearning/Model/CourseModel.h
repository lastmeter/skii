//
//  CourseModel.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseModel : NSObject

typedef enum
{
    CET_SITE_VIEW,
    CET_FAVORITE_VIEW,
    CET_CURRICULUM_VIEW
}COURSR_CELL_TYPE;

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *imgPath;
@property(nonatomic, copy) NSString *contentPackageId;
@property(nonatomic, copy) NSString *url;
@property(nonatomic, copy) NSString *userId;


@property(nonatomic, assign) COURSR_CELL_TYPE type;
@property(nonatomic, assign, getter = isCollected) BOOL collected;
@property(nonatomic, assign, getter = isPass) BOOL pass;

@property(nonatomic, assign) NSInteger tag; //标记位

@end
