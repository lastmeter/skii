//
//  NotificationModel.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationModel : NSObject

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *date;

@property (nonatomic, assign, getter = isOpened) BOOL opened;
@property (nonatomic, assign) NSUInteger btnTag;
@property (nonatomic, assign) BOOL isLoaded;
@property (nonatomic, assign) CGFloat cellHeight;

@end
