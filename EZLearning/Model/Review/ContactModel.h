//
//  ContactModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/27.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactModel : NSObject

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, assign, getter=isBoss) BOOL boss;
@end
