//
//  MessageModel.h
//  QQ聊天布局
//
//  Created by TianGe-ios on 14-8-19.
//  Copyright (c) 2014年 TianGe-ios. All rights reserved.
//

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

static const CGFloat MessageModelTextFont = 18;

typedef enum {
    kMessageModelTypeOther,
    kMessageModelTypeMe
} MessageModelType;

@interface MessageModel : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *messageId;
@property (nonatomic, copy) NSString *fromUserId;
@property (nonatomic, copy) NSString *toUserId;
@property (nonatomic, assign) long long timeSp;
@property (nonatomic, assign) MessageModelType type;

@property (nonatomic, assign) BOOL showTime;


@end
