//
//  SubordinateScoreModel.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubordinateScoreModel : NSObject
@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign,getter=isPass) BOOL pass;
@property (nonatomic,copy)NSString *score;
@end
