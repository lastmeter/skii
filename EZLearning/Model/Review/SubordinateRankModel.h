//
//  SubordinateRank.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubordinateRankModel : NSObject

@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *lastName;
@property (nonatomic,copy) NSString *firstName;
@property (nonatomic,copy) NSString *score;
@property (nonatomic,copy) NSString *account;
@property (nonatomic,copy) NSString *star;
@property (nonatomic, copy)NSNumber *credit;

@property (nonatomic, assign) NSUInteger btnTag;
@property (nonatomic, assign) int passNum;


@end
