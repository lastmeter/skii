//
//  XMLUtil.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLUtil : NSObject
+(NSString *)XMLStringFromString:(NSString *)string;
@end
