//
//  DateUtil.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "DateUtil.h"

@implementation DateUtil

+(NSString *)dateFromTimeSp:(NSTimeInterval )timeSp
{
    NSDate *date= [NSDate dateWithTimeIntervalSince1970:timeSp];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    return [formatter stringFromDate:date];
}

+(NSString *)dateNow
{
    NSDate *senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"yy-MM-dd HH:mm:ss"];
    return [dateformatter stringFromDate:senddate];
}

+(int)dayFromNowToDate:(NSDate *)date
{
    if (date == nil)
    {
        return 0;
    }
    NSDate *nowDate = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSCalendarUnitDay;
    NSDateComponents *d = [cal components:unitFlags fromDate:date toDate:nowDate options:0];
    int day = (int) [d day];
    return day;
}
@end
