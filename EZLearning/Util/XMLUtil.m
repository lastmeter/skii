//
//  XMLUtil.m
//  e-Learning
//
//  Created by RenHongwei on 15/3/23.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "XMLUtil.h"

@implementation XMLUtil

+(NSString *)XMLStringFromString:(NSString *)string
{
    NSString *xmlString = [XMLUtil replaceString:@"&" toString:@"&amp;" inString:string];
    xmlString = [XMLUtil replaceString:@"<" toString:@"&lt;" inString:xmlString];
    xmlString = [XMLUtil replaceString:@">" toString:@"&gt;" inString:xmlString];
    xmlString = [XMLUtil replaceString:@"'" toString:@"&apos;" inString:xmlString];
    xmlString = [XMLUtil replaceString:@"\"" toString:@"&quot;" inString:xmlString];
    return xmlString;
}

+(NSString *)replaceString:(NSString *)raplaceString toString:(NSString *)toString inString:(NSString *)mainString
{
    NSMutableString *xmlString = [NSMutableString string];
    while (1)
    {
        NSRange range = [mainString rangeOfString:raplaceString];
        if (NSNotFound == range.location)
        {
            [xmlString appendString:mainString];
            break;
        }
        [xmlString appendString:[mainString substringToIndex:range.location]];
        [xmlString appendString:toString];
        mainString = [mainString substringFromIndex:NSMaxRange(range)];
    }
    return xmlString;
}

@end
