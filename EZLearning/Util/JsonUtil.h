//
//  JsonUtil.h
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonUtil : NSObject

+(NSArray *)json2Array:(NSString *)jsonString;
+(NSDictionary *)json2Dictionary:(NSString *)jsonString;
+(NSString *)dictionary2Json:(NSDictionary *)dic;
+(NSString *)array2json:(NSArray *)array;

@end
