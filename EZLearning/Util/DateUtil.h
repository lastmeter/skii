//
//  DateUtil.h
//  e-Learning
//
//  Created by RenHongwei on 15/3/22.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtil : NSObject
+(NSString *)dateFromTimeSp:(NSTimeInterval )timeSp;
+(NSString *)dateNow;
+(int) dayFromNowToDate:(NSDate *)date;
@end
