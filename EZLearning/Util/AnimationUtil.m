//
//  AnimationUtil.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/13.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "AnimationUtil.h"

@implementation AnimationUtil

+(void)addPopAnimationToView:(UIView *)view duration:(CFTimeInterval)dur
{
    view.alpha = 1;
    CAKeyframeAnimation *animation;
    animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = dur;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 0.9)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:@"easeInEaseOut"];
    [view.layer addAnimation:animation forKey:nil];
}

+(void)fadeoutWithAnimation:(UIView *) view duration :(CFTimeInterval)dur
{
    [UIView animateWithDuration:dur animations:^{
        view.alpha = 0;
    }completion:^(BOOL finished) {
        view.alpha = 1;
        [view removeFromSuperview];
    }];
}

@end
