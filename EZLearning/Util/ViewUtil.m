//
//  ViewUtil.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "ViewUtil.h"

@implementation ViewUtil

+(CGPoint)centerPointByFrame:(CGRect)frame
{
    return CGPointMake(0.5 * frame.size.width,  0.5 * frame.size.height);
}

@end
