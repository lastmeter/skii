//
//  JsonUtil.m
//  e-Learning
//
//  Created by RenHongwei on 15/4/17.
//  Copyright (c) 2015年 RenHongwei. All rights reserved.
//

#import "JsonUtil.h"

@implementation JsonUtil

+ (NSArray *)json2Array:(NSString *)jsonString
{
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    if (error != nil)
    {
        DLOG(@"JsonUtil json2Array json: %@",jsonString);
        DLOG(@"JsonUtil json2Array error happend:%@",error);
        return  nil;
    }
    NSArray *array = (NSArray *)jsonObject;
    return array;
}

+ (NSDictionary *)json2Dictionary:(NSString *)jsonString
{
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    if (error != nil)
    {
        DLOG(@"JsonUtil json2Dictionary error happend:%@",error);
        return  nil;
    }
    NSDictionary *dic = (NSDictionary *)jsonObject;
    return dic;
}

+ (NSString *)dictionary2Json:(NSDictionary *)dic
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic
                                                       options:0
                                                         error:&error];
    if ([jsonData length] > 0 && error == nil)
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    else if ([jsonData length] == 0 && error == nil)
    {
        DLOG(@"JsonUtil dictionary2Json: No data");
    }
    else if (error != nil)
    {
        DLOG(@"JsonUtil dictionary2Json error happend: %@",error);
    }
    return  nil;
}

+(NSString *)array2json:(NSArray *)array
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:0
                                                         error:&error];
    if ([jsonData length] > 0 && error == nil)
    {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    else if ([jsonData length] == 0 && error == nil)
    {
        DLOG(@"JsonUtil dictionary2Json: No data");
    }
    else if (error != nil)
    {
        DLOG(@"JsonUtil dictionary2Json error happend: %@",error);
    }
    return  nil;
}

@end
