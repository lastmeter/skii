//
//  AppDelegate.m
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HomePageViewController.h"
#import "SitesViewController.h"
#import "RankingViewController.h"
#import "SelfInfoViewController.h"
#import "EZLTabBarController.h"
#import "EZLNavigationController.h"
#import "Reachability.h"
#import "LearningViewController.h"
#import "CurriculumViewController.h"
#import "guideViewController.h"
#import "shareEgine.h"
#import "EzlUser.h"
#import "SubordinateListController.h"
#import "EzlWebImageManager.h"
#import "SDImageCache.h"
#import "SDWebImageManager.h"

@interface AppDelegate ()
{
    MBProgressHUD *_loadingHUD;
}
@property (nonatomic, strong) Reachability *reach;
@property (nonatomic, strong) LearningViewController *learningViewController;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [EzlWebImageManager removeCache];
    [[SDImageCache sharedImageCache] setMaxMemoryCost:1024 * 1024 * 30];
    
    [[shareEgine sharedInstance]registerApp];
    int cacheSizeMemory = 200*1024*1024;//4MB
    //int cacheSizeDisk = 5*1024*1024;//32MB
    int cacheSizeDisk = 100*1024*1024;//32MB
    NSURLCache *shareCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:shareCache];
    
    HomePageViewController *homePageViewController = [[HomePageViewController alloc] init];
    CurriculumViewController * curriculum = [[CurriculumViewController alloc] initWithNibName:@"CurriculumViewController" bundle:nil];
 
    SelfInfoViewController *selfInfofViewController = [[SelfInfoViewController alloc] initWithNibName:@"SelfInfoViewController" bundle:nil];
    EZLTabBarController *tabBarController = [[EZLTabBarController alloc] init];
   
    
    //Add In 2015.12.21 By Lrh
      // RankingViewController *rankingViewController = [[RankingViewController alloc] initWithNibName:@"RankingViewController" bundle:nil];
      // [tabBarController setTabViewControllers:@[homePageViewController,curriculum, rankingViewController, selfInfofViewController]];
    SubordinateListController *subordinateListController = [[SubordinateListController alloc] init];
    [tabBarController setTabViewControllers:@[homePageViewController,curriculum, subordinateListController, selfInfofViewController]];
    //Add End
    
    self.navigationController = [[EZLNavigationController alloc] initWithRootViewController:tabBarController];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _window.rootViewController = _navigationController;
    
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    [_navigationController pushViewController:loginViewController animated:NO];
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"isLunach"]) {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isLunach"];
        guideViewController *guideView = [[guideViewController alloc]init];
        [_navigationController pushViewController:guideView animated:NO];
    }
    //注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    self.reach = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    
    if ([[[UIDevice currentDevice]systemVersion ] floatValue] >= 8.0) {
        UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert| UIUserNotificationTypeBadge categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:setting];
       // [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
 
    
    [_window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
     [UIApplication sharedApplication].applicationIconBadgeNumber = [EzlUser currentUser].numberOfAllUnreadMessage;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.reach stopNotifier];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[EzlUser currentUser] loginIfNoActive];
    [self.reach startNotifier];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [[shareEgine sharedInstance]handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[shareEgine sharedInstance]handleOpenURL:url];
}

//通知
-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    NSParameterAssert([reach isKindOfClass: [Reachability class]]);
    NetworkStatus status = [reach currentReachabilityStatus];
    
    //	//用于检测是否是WIFI
    //	NSLog(@"%d",([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] != NotReachable));
    //	//用于检查是否是3G
    //	NSLog(@"%d",([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable));
    
    if (status == NotReachable)
    {
        _isNetworkConnect = NO;
        _isWifi = NO;
        DLOG(@"Notification Says Unreachable");
    }
    else if(status == ReachableViaWWAN)
    {
        _isNetworkConnect = YES;
         _isWifi = NO;
    }
    else if(status == ReachableViaWiFi)
    {
        _isNetworkConnect = YES;
        _isWifi = YES;
    }
}


-(void)pushLearningViewControllerWithTitle:(NSString *)courseTitle Url:(NSString *)urlString
{
    if (_learningViewController == nil)
    {
        self.learningViewController = [[LearningViewController alloc] init];
    }
    [_learningViewController loadCourseWithTitle:courseTitle URL:urlString];
    [_navigationController presentViewController:_learningViewController animated:YES completion:nil];
    //[self.navigationController pushViewController:_learningViewController animated:nil];
}


+(AppDelegate *)getAppDelegate
{
    static AppDelegate *delegate = nil;
    static dispatch_once_t predocate;
    dispatch_once(&predocate, ^{
        delegate = [UIApplication sharedApplication].delegate;
    });
    return delegate;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)p_showLoadingView{
    
    if (_loadingHUD == nil) {
        _loadingHUD = [[MBProgressHUD alloc] initWithView:_window];
        [_window addSubview:_loadingHUD];
        _loadingHUD.labelText = @"加载中，请稍候...";
    }
    [_loadingHUD show:YES];
}
- (void)p_hideLoadingView{
    [_loadingHUD hide:YES];
}


@end


void g_ShowLoadingView(){
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate p_showLoadingView];
}

void g_HideLoadingView(){
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate p_hideLoadingView];
}
