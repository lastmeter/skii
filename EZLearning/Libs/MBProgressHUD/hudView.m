//
//  hudView.m
//  EagleEye
//
//  Created by zwf on 15/5/24.
//  Copyright (c) 2015年 zwf. All rights reserved.
//

#import "hudView.h"

@implementation hudView

+(void)hudView:(NSString *)lableString showView:(UIView *)view delegate:(id)selfdelegate
{
    
   MBProgressHUD *_HUD= [[MBProgressHUD alloc] initWithWindow:[UIApplication sharedApplication].keyWindow];
    _HUD.delegate=selfdelegate;
    _HUD.labelText = lableString;
    _HUD.mode = MBProgressHUDModeText;
    _HUD.margin =10.0;
    _HUD.opacity =1.0;
    _HUD.labelFont = [UIFont boldSystemFontOfSize:13.0];
    [view.window addSubview:_HUD];
    [_HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    _HUD.yOffset = MAIN_SCREEN_HEIGHT/2-40;
}

-(void)myTask
{
    sleep(2.0);
}

@end
