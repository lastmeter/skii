//
//  hudView.h
//  EagleEye
//
//  Created by zwf on 15/5/24.
//  Copyright (c) 2015年 zwf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface hudView : UIViewController<MBProgressHUDDelegate>


+(void)hudView:(NSString *)lableString showView:(UIView *)view delegate:(id)selfdelegate;

@end
