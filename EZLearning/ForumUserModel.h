//
//  FormuUserModel.h
//  EZLearning
//
//  Created by zwf on 15/5/23.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForumUserModel : NSObject
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *forumId;
@property(nonatomic, copy) NSString *messagecount;
@end
