//
//  AppDelegate.h
//  EZLearning
//
//  Created by RenHongwei on 15/4/21.
//  Copyright (c) 2015年 YXB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EZLNavigationController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL isNetworkConnect;
@property (nonatomic, assign) BOOL isWifi;
@property (strong, nonatomic) EZLNavigationController *navigationController;
@property (nonatomic, strong) NSDate* startDate;
@property (nonatomic, strong) NSDate* endDate;


-(void)pushLearningViewControllerWithTitle:(NSString *)courseTitle Url:(NSString *)urlString;
+ (AppDelegate *)getAppDelegate;


extern void g_ShowLoadingView();
extern void g_HideLoadingView();

@end

